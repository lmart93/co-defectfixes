<%--
* -----------------------------------------------------------------------
* Copyright 2011, jcpenney Co., Inc.  All Rights Reserved.
*
* Reproduction or use of this file without explicit
* written consent is prohibited.
* Created by: cgeddam1
*
* Created on: June 296, 2016
* ----------------------------------------------------------------
--%>
<%-- ========================================= Description
/**
 * This page is used to display
 *  1.This Jsp contains all the read only pages
 * Includes Header and Footer JSPFs.
 *
 * @author cgeddam1
 * @version 0.1 26-June-2016
 **/
--%>
<%@ taglib uri="dsp" prefix="dsp" %>
<%@ taglib uri="dspel" prefix="dspel" %>
<%@ taglib uri="c" prefix="c"  %>
<%@ taglib uri="fmt" prefix="fmt" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<dsp:importbean bean="/com/jcpenney/core/ApplicationConfiguration"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/com/jcpenney/core/order/droplet/ValidShippingAddressDroplet"/>
<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
<dsp:getvalueof var="enableLargeAppShipZipValidation" bean="/com/jcpenney/core/ApplicationConfiguration.enableLargeAppShipZipValidation" />
<dsp:getvalueof var="contextroot" vartype="java.lang.String" bean="/OriginatingRequest.contextPath"/>
<dsp:getvalueof var="isTransient" bean="Profile.transient"/>
<dsp:getvalueof var="pageAction" param="action"/>
<c:set var="continueBtn" value="CONTINUE TO PAYMENT"/>
<c:set var="storePageId" value="Ship to Store" />
<c:set var="addressPageId" value="Ship to Adress" />
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<dsp:droplet name="Switch">
	<dsp:param name="value" bean="Profile.securityStatus"/>
	<dsp:oparam name="default">
	<script type="text/javascript">
		customerId='<dsp:valueof bean="ShoppingCart.current.vid"/>';
		isUserLoggedIn = false;
		profileVid = customerId;
	</script>
	</dsp:oparam>
	<dsp:oparam name="5">
		<script type="text/javascript">
		customerId='<dsp:valueof bean="Profile.userVid"/>';
		isUserLoggedIn = true;
		profileVid = customerId;
	</script>
	</dsp:oparam>
	<dsp:oparam name="2">
		<script type="text/javascript">
		customerId='<dsp:valueof bean="Profile.userVid"/>';
		isUserLoggedIn = false;
		profileVid = customerId;
		</script>
	</dsp:oparam>
</dsp:droplet>

<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/checkoutsimplified/commonValidations.js?v=<dsp:valueof bean="ApplicationConfiguration.releaseVersion"/>"></script>
<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/checkoutsimplified/formValidator.js?v=<dsp:valueof bean="ApplicationConfiguration.releaseVersion"/>"></script>
<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/checkoutsimplified/ShippingController.js?v=<dsp:valueof bean="ApplicationConfiguration.releaseVersion"/>"></script>
<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/bopuscheckoutsimplified.js?v=<dsp:valueof bean="ApplicationConfiguration.releaseVersion"/>"></script>
<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/checkoutsimplified/giftWrapSimplify.js?v=<dsp:valueof bean="/com/jcpenney/core/ApplicationConfiguration.releaseVersion"/>"></script>

</head>
<dsp:droplet name="/com/jcpenney/largeapp/droplet/LargeAppOrderCheckDroplet">
	<dsp:param name="order" bean="ShoppingCart.current"/>
	<dsp:oparam name="output">
		<dsp:getvalueof var="includeLargeAppItem" param="includeLargeAppItem" />
		<dsp:getvalueof var="largeAppZipCode" param="largeAppZipCode" />
		<dsp:getvalueof var="largeAppCommerceId" param="largeAppCommerceId" />
	</dsp:oparam>
</dsp:droplet>
<dsp:form id="removeItemsFromOrder" class="hide" method="post" action="/dotcom/jsp/cart/dummyLoadOrder.jsp">
<fieldset>
	<dsp:input id="removablecommerceItem" type="hidden" value="" bean="CartModifierFormHandler.removalCommerceIdString"/>
	<dsp:input type="hidden" value="" bean="CartModifierFormHandler.removeItemFromOrder"/>
</fieldset>
</dsp:form>

<dsp:page xml="true">
	<dsp:droplet name="ForEach">
	   	<dsp:param name="array" bean="ShoppingCart.current.shippingGroups" />
		<dsp:oparam name="output">
			<dsp:droplet name="Switch">
				<dsp:param name="value" param="element.shippingGroupClassType" />
				<dsp:oparam name="storeShippingGroup">
					<dsp:getvalueof var="stsShippingMethod" param="element.shippingMethod" scope="request"/>
					<dsp:getvalueof var="storeId"	param="element.storeId" scope="request"/>
					<dsp:getvalueof var="fName" param="element.firstName" scope="request" />
					<dsp:getvalueof var="lName" param="element.lastName" scope="request" />
					<dsp:getvalueof var="phNum" param="element.contactPhoneNumber" scope="request" />
				</dsp:oparam>
				<dsp:oparam name="hardgoodShippingGroup">
					<dspel:getvalueof var="streetAddress1" param="element.shippingAddress.address1" scope="request" />
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:getvalueof var="sthSelected" value=""/>
	<dsp:getvalueof var="stsSelected" value=""/>
	<c:choose>
		<c:when test="${not empty stsShippingMethod && stsShippingMethod eq 'WILL_CALL'}">
			<dsp:getvalueof var="stsSelected" value="checked"/>
		</c:when>
		<c:otherwise>
			<dsp:getvalueof var="sthSelected" value="checked"/>
		</c:otherwise>
	</c:choose>
	<dsp:getvalueof var="orderAvailShippingMethod" bean="ShoppingCart.current.availableShippingMethod" />
	<input type="hidden" id="stsSelected" name="stsSelected" value="${stsSelected}"/>
	<div class="step shipping ng-scope ng-enter" >
	<div class="header ng-scope">
	  <h1>
	    <img src="/dotcom/images/deltruck.png" alt="shipping">
	    SHIPPING
	  </h1>
	</div>
	<div class="dynamic_error_msgs hide" id="savedShippingAddressErrorContainer">
	<span class="error-msg-header" id="errorMessageId">
		<img alt="attention" src="/dotcom/images/error.svg">
		<span>
		</span>
	</span>
	<div class="float_fix error_list">
		<div id="savedShippingAddressError"> </div>
	</div>
   </div>
	<dsp:droplet name="/com/jcpenney/largeapp/droplet/LargeAppOrderCheckDroplet">
		<dsp:param name="order" bean="ShoppingCart.current"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="includeLargeAppItem" param="includeLargeAppItem" />
			<dsp:getvalueof var="largeAppZipCode" param="largeAppZipCode" />
			<dsp:getvalueof var="largeAppCommerceId" param="largeAppCommerceId" />
		</dsp:oparam>
	</dsp:droplet>
	<!-- form -->
 	<c:choose>
		<c:when test="${enableLargeAppShipZipValidation && includeLargeAppItem != null && includeLargeAppItem == 'true'}">
			<c:if test="${enableLargeAppShipZipValidation && includeLargeAppItem != null && includeLargeAppItem == 'true' }">
				<div class="large-app-checkout-info-message mrglft20px mrgtp20px" id="largeAppZipInfoAddAddress">
				<img alt="attention" src="<c:out value="${contextroot}"/>/images/dynamic_info_icn.gif" class="flt_lft  mrgr10 mrgrtop" />
				<div class="float_fix">
					<span class="disp_blk">
					<div class="largeAppText" id="largeAppText">
						<c:choose>
							<c:when test="${isTransient}">
								Parts, services, and installation are specific to your delivery zip code. Please select or add an address in zip
								<dsp:valueof bean="ShoppingCart.current.largeApplianceZipCode"/>.
							</c:when>
							<c:otherwise>
								<dsp:droplet name="/atg/dynamo/droplet/IsEmpty">
									<dsp:param name="value" bean="Profile.secondaryAddresses"/>
									<dsp:oparam name="true">
										Parts, services, and installation are specific to your delivery zip code:
										<dsp:valueof bean="ShoppingCart.current.largeApplianceZipCode"/>. Your zip cannot be changed.
									</dsp:oparam>
									<dsp:oparam name="false">
										<dsp:droplet name="ValidShippingAddressDroplet">
											<dsp:param name="checkAddressFrom" value="profile"/>
											<dsp:oparam name="output">
												<dsp:getvalueof var="hasValidAddress" param="hasValidAddress"/>
												<c:choose>
													<c:when test="${hasValidAddress == true}">
														Parts, services, and installation are specific to your delivery zip code.
														Please select or add an address in zip <dsp:valueof bean="ShoppingCart.current.largeApplianceZipCode"/>.
													</c:when>
												</c:choose>
											</dsp:oparam>
										</dsp:droplet>
									</dsp:oparam>
								</dsp:droplet>
							</c:otherwise>
						</c:choose>
						</div>
					</span>
					</div>
				</div>
				<input type="radio" id="shippingLocation" value="shipToHome" type="hidden" name="shippingLocation" ng-checked="true"
					   class="ng-valid ng-dirty ng-valid-parse ng-touched" checked="${sthSelected}"
					   onclick="triggerCoremetricsPageViewTag('<c:out value="${addressPageId}" />', 'JCP|Shipping', '');"/>
			</c:if>
		</c:when>
		<c:otherwise>
			<h2>Where will you ship these items?</h2>
			<c:choose>
				<c:when test="${not empty sthSelected && sthSelected eq 'checked'}">
					<input type="radio" id="shippingLocation" value="shipToHome" name="shippingLocation" ng-checked="true"
							class="ng-valid ng-dirty ng-valid-parse ng-touched" checked="${sthSelected}"
							onclick="triggerCoremetricsPageViewTag('<c:out value="${addressPageId}" />', 'JCP|Shipping', '');"/>
				</c:when>
				<c:otherwise>
					<c:if test="${not empty orderAvailShippingMethod && orderAvailShippingMethod eq 'both'}" >
						<input type="radio" id="shippingLocation" value="shipToHome" name="shippingLocation" ng-checked="true"
							class="ng-valid ng-dirty ng-valid-parse ng-touched"
							onclick="triggerCoremetricsPageViewTag('<c:out value="${addressPageId}" />', 'JCP|Shipping', '');"/>
					</c:if>
				</c:otherwise>
			</c:choose>
			<span></span>
			<label for="shippingLocation" class="sthLbl">Ship to Home</label>
			<c:if test="${not empty orderAvailShippingMethod && orderAvailShippingMethod eq 'both'}" >
				<c:choose>
					<c:when test="${not empty stsSelected && stsSelected eq 'checked'}">
						<input type="radio" id="shippingLocation" value="shipToStore" name="shippingLocation"
							class="ng-valid ng-dirty ng-touched" checked="${stsSelected}"
							onclick="triggerCoremetricsPageViewTag('<c:out value="${storePageId}" />', 'JCP|Shipping', '');"/>
					</c:when>
					<c:otherwise>
						<input type="radio" id="shippingLocation" value="shipToStore" name="shippingLocation" class="ng-valid ng-dirty ng-touched"
							onclick="triggerCoremetricsPageViewTag('<c:out value="${storePageId}" />', 'JCP|Shipping', '');"/>
					</c:otherwise>
				</c:choose>
				<span></span>

				<label for="shippingLocation">Ship to Store</label>
				<%-- Need to confirm if this condition check is required <c:if test="${shipToCountry eq 'US'}" >	</c:if> --%>
        	</c:if>
       	</c:otherwise>
	</c:choose>

		<div class="form" autocomplete="off">
			<!-- STS -->
			<c:if test="${(includeLargeAppItem == null || includeLargeAppItem != 'true') && (not empty orderAvailShippingMethod && orderAvailShippingMethod eq 'both')}">
			   <div id="sts-div" class="sts ng-leave" ng-class="{'ng-leave':shippingLocation=='shipToHome','ng-enter':shippingLocation=='shipToStore'}">
			     <dsp:include page="/jsp/checkout/secure/checkoutsimplified/shipping/shiptostore/shipToStore.jsp">
			     </dsp:include>
			   </div>
		   </c:if>
			<div id="sth-div" class="sth ng-enter" ng-class="{'ng-leave':shippingLocation=='shipToStore','ng-enter':shippingLocation=='shipToHome'}">
			   	<dsp:include page="/jsp/checkout/secure/checkoutsimplified/shipping/shiptohome/shipToHome.jsp"/>
		    </div> <!-- /.form -->
		</div>
	    <%-- Gift Wrap section  --%>
	    <c:if test="${(includeLargeAppItem == null || includeLargeAppItem != 'true')}">
		    <dsp:include page="/jsp/checkout/secure/checkoutsimplified/giftwrap/displayGiftWrap.jsp">
				<dsp:param name="displayName" value="" />
		    </dsp:include>
		</c:if>
		<!-- footer -->
		<div class="step-footer">
		    <h4> <img src="/dotcom/images/secure-lock.svg" alt=""> SECURE CHECKOUT </h4>

		    <c:if test="${pageAction == 'edit' || not empty streetAddress1}">
				<c:set var="continueBtn" value="CONTINUE"/>
			</c:if>
		   	<button id="shipping_submit" class="primary ng-binding" onclick="applyShippingMethodOnSubmit();" disabled="disabled"  ><c:out value="${continueBtn}"/></button>
		   	<button id="sts_submit" class="primary ng-binding" onclick="applyWillCallShippingMethodOnSubmit();" disabled="disabled" style="display:none" >${continueBtn}</button>
		    <div class="clear"></div>
		</div>
	</div>

	<script type="text/javascript">
		var action = '${pageAction}';

		$(document).ready(function() {
			var selectVal = $('#selectShippingCountry').find(":selected").val();
			if (selectVal == 'United States') {
				usShipAddress();
				removeErrorContainer();
			} else if(selectVal == 'AP') {
				militaryShipAddress();
				removeErrorContainer();
			}
			submittedShippingAddress.streetAddress1 = $('#streetAddress1').val();
			submittedShippingAddress.streetAddress2 = $('#streetAddress2').val();
			submittedShippingAddress.city  = $('#cityName').val();
			submittedShippingAddress.zipCode =  $('#zipCode').val();
			submittedShippingAddress.state =  $('#selectState').find(":selected").val();
			var streetAddress1 = $('#streetAddress1').val();
			if (action != null && action == 'edit' || streetAddress1 != null && streetAddress1 !='') {
			     $("#shippingAddress_continue").show();
			     if(includeLargeAppItem == 'true') {
			    	 $("#shipping_submit").hide();
			     }
				 detachShippingFormEventHandlers();
			}
		});
	</script>
</dsp:page>
