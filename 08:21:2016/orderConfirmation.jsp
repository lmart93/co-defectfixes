<%--
* -----------------------------------------------------------------------
* Copyright 2011, jcpenney Co., Inc.  All Rights Reserved.
*
* Reproduction or use of this file without explicit
* written consent is prohibited.
* Created by: cgeddam1
*
* Created on: June 26, 2016
* ----------------------------------------------------------------
--%>
<%-- ========================================= Description
/**
 * This page is used to display
 *  1.This Jsp contains all the read only pages
 * Includes Header and Footer JSPFs.
 *
 * @author cgeddam1
 * @version 0.1 26-June-2016
 **/
--%>
<%@ taglib uri="dsp" prefix="dsp" %>
<%@ taglib uri="dspel" prefix="dspel" %>
<%@ taglib uri="c" prefix="c"  %>
<%@ taglib uri="fmt" prefix="fmt" %>
<%@ page pageEncoding="UTF-8" %>

<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );
%>
<dsp:page xml="true">
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsNull"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	<dsp:importbean bean="/com/jcpenney/core/droplet/CaseConverterDroplet"/>
	<dsp:importbean bean="/com/jcpenney/core/order/droplet/GiftCardDroplet"/>
	<dsp:importbean bean="/com/jcpenney/dp/core/droplet/CheckoutPageRendererDroplet"/>
	<dsp:importbean bean="/com/jcpenney/giftregistry/droplet/DirectCheckShippingAddressDroplet"/>
	<dsp:importbean bean="/com/jcpenney/core/droplet/AntiSpamEmailAddressSuffixLookupDroplet"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/com/jcpenney/profile/bean/UserSessionBean" />
	<dsp:importbean bean="/com/jcpenney/core/ApplicationConfiguration" var="appConfig" />
	<dsp:importbean bean="/com/jcpenney/order/CheckoutApplicationConfiguration"/>
	<dsp:importbean bean="/com/jcpenney/profile/ProfileApplicationConfiguration"/>
	<dsp:getvalueof var="contextroot" vartype="java.lang.String" bean="/OriginatingRequest.contextPath" scope="request"/>
	<dsp:getvalueof var="confirmOrder" value="true" scope="request"/>
	<dsp:getvalueof var="externalOrderNumber" bean="ShoppingCart.last.externalOrderNumber"/>
	<dsp:getvalueof var="passwordEnhancement" bean="ApplicationConfiguration.enable2016S11DPXVI10305PasswordEnhancement"/>
	<dsp:getvalueof var="serverName" vartype="java.lang.String" bean="/OriginatingRequest.serverName"/>
	<dsp:getvalueof var="securePort" bean="/com/jcpenney/core/pipeline/ProtocolChangeServlet.securePort"/>
	<dsp:importbean bean="/com/jcpenney/core/droplet/NetworkInfoDroplet"/>
	
	<?xml version="1.0" encoding="UTF-8"?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" 	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
			<title>JCPenney | Order Confirmation Page</title>
			<link rel="stylesheet" type="text/css" href="<c:out value="${contextroot}"/>/css/webkit-checkoutsimplified.css?v=<dsp:valueof bean="ApplicationConfiguration.releaseVersion"/>"/>
			<link rel="stylesheet" type="text/css" href="<c:out value="${contextroot}"/>/css/webkit-ext.css?v=<dsp:valueof bean="ApplicationConfiguration.releaseVersion"/>"/>
		
			<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/lib/eluminate.js?v=<dsp:valueof bean="/com/jcpenney/core/ApplicationConfiguration.releaseVersion"/>"></script>
			<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/lib/jquery.js?v=<dsp:valueof bean="ApplicationConfiguration.releaseVersion"/>"></script>
			<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/modal.js?v=<dsp:valueof bean="ApplicationConfiguration.releaseVersion"/>"></script>
			<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/lib/jquery.min.js?v=<dsp:valueof bean="ApplicationConfiguration.releaseVersion"/>"></script>
			<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/lib/jquery-ui-1.8.16.custom.min.js?v=<dsp:valueof bean="ApplicationConfiguration.releaseVersion"/>"></script>
			<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/lib/jquery.form.js?v=<dsp:valueof bean="ApplicationConfiguration.releaseVersion"/>"></script>
			<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/lib/jquery.colorbox-min.js?v=<dsp:valueof bean="ApplicationConfiguration.releaseVersion"/>"></script>
			<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/lib/validate.js?v=<dsp:valueof bean="ApplicationConfiguration.releaseVersion"/>"></script>
			<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/namespace.js?v=<dsp:valueof bean="ApplicationConfiguration.releaseVersion"/>"></script>
			<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/lib/tooltip.js?v=<dsp:valueof bean="ApplicationConfiguration.releaseVersion"/>"></script>
			<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/cityStateLookUp.js?v=<dsp:valueof bean="ApplicationConfiguration.releaseVersion"/>"></script>
			<script type="text/javascript" src="<c:out value='${contextroot}'/>/js/commonCoreMetrics.js?v=<dsp:valueof bean="ApplicationConfiguration.releaseVersion"/>"></script>
			<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/optin.js?v=<dsp:valueof bean="/com/jcpenney/core/ApplicationConfiguration.releaseVersion"/>"></script>
			<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/common_validations.js?v=<dsp:valueof bean="/com/jcpenney/core/ApplicationConfiguration.releaseVersion"/>"></script>
			
			<%-- new js files for simplify checkout --%>
			<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/simplifyCheckout.js?v=<dsp:valueof bean="ApplicationConfiguration.releaseVersion"/>"></script>
			<script type="text/javascript">
				var coremetricsTagEnabled = false;
				coremetricsTagEnabled= <dsp:valueof bean="ApplicationConfiguration.coremetricsTagEnabled"/>;
			</script>
			<script type="text/javascript">
			    //<![CDATA[
			    $(document).ready(function(){
			    	
			    	/* check timeout for user - start */
			    	isProfileTransient = '<dsp:valueof bean="Profile.transient"/>';
			    	var timer=setInterval("showSessionExpired()",sessionOutTime);
			    	$('body').bind('click mousemove keypress', function reset_interval(){
			    	    clearInterval(timer);
			    	    timer=setInterval("showSessionExpired()",sessionOutTime);
			    	});
			    	$(window).scroll(function(){
			    	    clearInterval(timer);
			    	    timer=setInterval("showSessionExpired()",sessionOutTime);
			    	});
			    	/* check timeout for guest - end */
			    	
			        $("a[rel='sign_up_modal']").colorbox({open:"true",
			            scrolling:false,
			            overlayClose:false,
			            escKey: false,
			            width: "600px",
			            /* height: "637px", */
			            close: function(){
			                var img = "<img id='cboxCloseImg' src='/dotcom/images/modal_close.gif' alt='close' title='close' />";
			                return $(img);
			            },
			            onComplete:function(){
			                $('form input[type!=hidden]:first').focus();
			                $('#cboxTitle').hide();
			            }
			        });
			    	
			        isHeaderClicked = false;
			
			        $("a").bind("click", function(event){
			            isHeaderClicked = true;
			        });
			    	
			        isBackButtonDisabled = '<c:out value="${sessionScope.backButtonDisabled}"/>';
			
			        if(isBackButtonDisabled == "true"){
			            showOrderSubmittedPage();
			        }
			
			        if (isProfileTransient == 'true') {
			            paymentCMConversionEmailOptinOnOrderSubmit();
			        }
			    });
			    
			    function setOrderFormat(){
			        $('#jcpOrderId').html(setorderNumberFormat($.trim($('#jcpOrderId').html())));
			    }
			    
			    function showOrderSubmittedPage(){
			        if(!isHeaderClicked){
			           serverName="<dspel:valueof value="${serverName}" />";
			           securePort="<dspel:valueof value="${securePort}" />";
			           /* This redirects to empty my bag page once the order is submitted. Browser back handle. */
			           window.location = 'https://' + serverName + ':' + securePort + "/dotcom/jsp/checkout/secure/orderAlreadySubmitted.jsp";
			        }
			    }
			    //]]>
		    </script>
		    <%--Checkout Marketing Tags Section Starts--%>
			<dsp:droplet name="/atg/dynamo/droplet/Switch">
		        <dsp:param name="value" bean="/com/jcpenney/marketingtags/MarketingTagsConfiguration.tagMangementDataLayerEnabled"/>
		        <dsp:oparam name="true">
		              <%@ include file="/jsp/global/commonMarketingTags.jsp"%>
		        </dsp:oparam>
			</dsp:droplet>
			
			<dsp:droplet name="/atg/dynamo/droplet/Switch">
		          <dsp:param name="value" bean="/com/jcpenney/externalintegration/ExternalIntegrationConfiguration.ensightenTagEnabled"/>
		          <dsp:oparam name="true">
		            <script type="text/javascript" src="<dsp:valueof bean="/com/jcpenney/marketingtags/MarketingTagsConfiguration.tagManagementBootstrapURL"/>" id="tagManagmentSource"></script>
		       	  </dsp:oparam>
			</dsp:droplet>
			<%--Checkout Marketing Tags Section Ends--%>
		</head>
		<body id="checkoutPage">
			
			<dsp:droplet name="CheckoutPageRendererDroplet">
		        <dsp:param name="order" bean="ShoppingCart.last"/>
		        <dsp:param name="pageName" value="checkoutActionJSON" />
		        <dsp:oparam name="output">
		        <dsp:getvalueof var="panel" param="panel" />
		        <dsp:getvalueof var="availableBlocks" param="pageRenderInfoBean.availableBlocks" vartype="java.util.List"/>
		        <dsp:getvalueof var="pageListToBeRender" param="pageRenderInfoBean.pageListToBeRender" vartype="java.util.List"/>
		        </dsp:oparam>
		    </dsp:droplet>
			
			<%-- Checkout Sign-In Header Section Starts--%>
		    <header>
		    <dsp:include page="/jsp/global/checkoutHeader.jsp">
		    	<dsp:param name="fromConfirmationPage" value ="true"/>
		    </dsp:include>
		    </header>
		    <%--Checkout Sign-In Header Section Ends--%>
			
			<div class="container">
			
				<%--Confirmation Progress bar display section starts--%>
				<div id="checkoutProgressBarDiv">
					<dspel:include page="/jsp/checkout/secure/checkoutsimplified/orderconfirm/confirmProgressDisplay.jsp">
						<dspel:param name="panel" value="${panel}" />
					</dspel:include>
				</div>
				<%--Confirmation Progress bar display section Ends--%>
				
				<dsp:getvalueof var="jcpOrderNumber" bean="ShoppingCart.last.jcpOrderNumber"/>
		<%-- Adobe Omniture Tagging --%>
		
		<span data-anid="bundleFlag" style="display:none"><dsp:valueof bean="ShoppingCart.last.orderContainsBundleItems"/></span>
		<span data-anid="expressCheckout" style="display:none"><dsp:valueof bean="Profile.checkoutInitiated" /></span>
		<span data-anid="shoppingBagNumber" style="display:none"><dsp:valueof bean="ShoppingCart.last.id"/></span>
		
		<%-- Start - To assign the paymentGroup's billingAddress --%>
		<dsp:setvalue param="paymentTypeArrayVar" value=""/>
				<dsp:droplet name="ForEach">
					<dsp:param bean="ShoppingCart.last.paymentGroups" name="array" />
					<dsp:oparam name="output">
						<dsp:getvalueof param="element.paymentMethod" var="paymentMethod" vartype="java.lang.String"/>
						<c:set var="paymentMethod" value='${paymentMethod}'/>
			<c:choose>
				<c:when test="${empty paymentTypeArrayVar}">
					<c:set var="paymentTypeArrayVar" value='${paymentMethod }'/>
				</c:when>
				<c:otherwise>
					<c:set var="paymentTypeArrayVar" value='${paymentTypeArrayVar} | ${paymentMethod }'/>
				</c:otherwise>
			</c:choose>
						<c:if test="${(paymentMethod eq 'billMeLater')}">
							<dsp:getvalueof var="bAddress" param="element.billingAddress"/>
							<c:set var="paymentMethodType" value="billMeLater"/>
						</c:if>
						<c:if test="${(paymentMethod eq 'creditCard') and (paymentMethodType ne 'billMeLater')}">
							<dsp:getvalueof var="bAddress" param="element.billingAddress"/>
							<c:set var="paymentMethodType" value="creditCard"/>
						</c:if>
						<c:if test="${(paymentMethod eq 'paypal')}">
							<c:set var="paymentMethodType" value="payPalType"/>
							<%-- <dsp:getvalueof var="bAddress" param="element.addressInfo"/> --%>
							<%-- No need to show billingAddres for paypal --%>
						</c:if>
						<c:if test="${(paymentMethodType ne 'creditCard') and (paymentMethodType ne 'billMeLater') and (paymentMethodType ne 'payPalType') and (paymentMethod eq 'giftCard')}">
							<c:set var="paymentMethodType" value="giftCard"/>
							<dsp:droplet name="/atg/dynamo/droplet/IsEmpty">
								<dsp:param name="value" bean="ShoppingCart.last.addressInfo"/>
								<dsp:oparam name="false">
									<dsp:getvalueof var="bAddress" bean="ShoppingCart.last.addressInfo"/>
								</dsp:oparam>
							</dsp:droplet>
						</c:if>
					</dsp:oparam>
				</dsp:droplet>
         <span data-anid="paymentType" style="display:none">${paymentTypeArrayVar}</span>
				<dspel:setvalue param="billingAddress" value="${bAddress}"/>
				<%-- End - To assign the paymentGroup's billingAddress --%>
		         
				<dsp:droplet name="Switch">
					<dsp:param name="value" bean="Profile.securityStatus"/>
					<dsp:oparam name="5">
						<dsp:getvalueof var="userFirstName" bean="Profile.firstName"/>
					</dsp:oparam>
					<dsp:oparam name="default">
						<dsp:droplet name="GiftCardDroplet">
							<dsp:param name="order" bean="ShoppingCart.last"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="orderTotalAmount" param="orderBalance"/>
								<dsp:getvalueof var="creditCardInOrder" param="creditCardInOrder"/>
								<c:if test="${(creditCardInOrder eq 'success') && (paymentMethodType ne 'payPalType') && (orderTotalAmount != 0 || !priceAccurate) }" >
									<dsp:getvalueof var="userFirstName" param="billingAddress.firstName"/>
								</c:if>
								<c:if test="${orderTotalAmount == 0 && priceAccurate}" >
									<dsp:getvalueof var="userFirstName" bean="ShoppingCart.last.addressInfo.firstName"/>
								</c:if>
							</dsp:oparam>
						</dsp:droplet>
		          	</dsp:oparam>
				</dsp:droplet>
				<c:choose>
					<c:when test="${paymentMethodType eq 'payPalType'}">
						<span id="checkoutPath" style="display:none">PayPal</span>
					</c:when>
					<c:otherwise>
						<dspel:getvalueof var="isTransient" bean="Profile.securityStatus"/>
						<c:choose>
							<c:when test="${ isTransient != '5' }">
								<span id="checkoutPath" style="display:none">Guest</span>
							</c:when>
							<c:otherwise>
								<span id="checkoutPath" style="display:none">Registered</span>
							</c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose>		
				<dsp:droplet name="CaseConverterDroplet">
					<dspel:param name="inputString" value="${userFirstName}"/>
					<dspel:param name="case" value="titleCase"/>
					<dsp:oparam name="output">
						<dsp:getvalueof var="firstNameTitleCase" param="element"/>
					</dsp:oparam>
				</dsp:droplet>
			
				<div class="main-steps-holder">
					<%--Checkout Panels display section starts--%>
					<div id="checkoutPanelDisplayDiv left-panel" class="orderConfirm">
						<div class="review checkout_revamp_orderreview">   
							<div class="confirmation header confirm">
								<div class="orderconfirm">
                            <h3 class="orderconfirmheader">ORDER CONFIRMATION #<span data-anid="transactionID">${jcpOrderNumber}</span></h3>
									<c:if test="${externalOrderNumber ne null}">
										<div class="personName">
											Borderfree order number: <dsp:valueof bean="ShoppingCart.last.externalOrderNumber" />
										</div>
									</c:if>
									<div class="personName">
										Thank you <dspel:valueof value="${firstNameTitleCase}">Guest</dspel:valueof> for your order!
									</div>
								</div>
		                        
								<div class="right">
									<a href="javascript:void(printMyBag);" onclick="javascript:window.print();">print page</a>
									<button class="primary" onclick="keepShopping()">
										<span class="continue_shopping">CONTINUE SHOPPING</span>
									</button>
								</div>
								<div class="clear"></div>
							</div>
		                    
							<%-- Rewards account error & link scenario from the account creation pop-up [Start] --%>
							<dsp:droplet name="Switch">
								<dsp:param name="value" param="rewardAccFailed"/>
								<dsp:oparam name="true">
									<dsp:getvalueof var="rewardAccErrorMessage" param="rewardAccErrorMessage"/>
									<div id="confirm-attention-message">
										<p>
											<dsp:valueof param="rewardAccErrorMessage"/>
											<c:if test="${rewardAccErrorMessage == 'You already have a JCPenney Rewards account using this email.'}">
												<a href="<c:out value="${contextroot}"/>/jsp/profile/secure/linkRewardAccount.jsp?type=link&entry=orderConfirmation"/>Link your accounts.</a>
											</c:if>
										</p>
									</div>
								</dsp:oparam>
							</dsp:droplet>
							
							<dsp:droplet name="IsEmpty">
								<dsp:param name="value" bean="ShoppingCart.last.rewardCardNumber"/>
								<dsp:oparam name="true">
									<dspel:getvalueof var="hasRewardCard" value="false" />
								</dsp:oparam>
								<dsp:oparam name="false">
									<dspel:getvalueof var="hasRewardCard" value="true" />
								</dsp:oparam>
							</dsp:droplet>
							<dsp:getvalueof var="rewardsEnabled" bean="ApplicationConfiguration.rewardsEnabled"/>
							<c:if test="${hasRewardCard eq true and rewardsEnabled eq true and param.registered == 'true'}">
								<div id="confirm-attention-message">
									<p id="jcp_rw_coupon_error_msg">Would you like to connect your JCPenney Rewards profile with your jcp.com account?</p>
									<p id="jcp_rw_coupon_error_msg"><a href="<c:out value="${contextroot}"/>/jsp/profile/secure/linkRewardAccount.jsp?type=link&entry=orderConfirmation"/>Yes, I'd love to easily redeem my rewards</a></p>
								</div>
							</c:if>
							<%-- Rewards account error & link scenario from the account creation pop-up [End] --%>
						    
							<dsp:include page="orderreview/fragment/orderContactDetailsSection.jsp">
								<dsp:param name="order" bean="ShoppingCart.last"/>
							</dsp:include>
						    
							<%-- Order Promo Financing section for order confirmation page --%>
							<dsp:droplet name="/atg/dynamo/droplet/IsEmpty">
								<dsp:param name="value" bean="ShoppingCart.last.promoFinancingOffers"/>
								<dsp:oparam name="false">
									<div class="info-snippet financing">
										<strong>Selected Special Financing Offer</strong>
										<p><dsp:valueof bean="ShoppingCart.last.promoFinancingOffers[0].promoFinanceOfferDesc"/></p>
										<p><small><dsp:valueof bean="ShoppingCart.last.promoFinancingOffers[0].promoFinanceTermsConditions"/></small></p>
									</div>
								</dsp:oparam>
							</dsp:droplet>
						    
							<%-- Reward Number display for the profile --%>
							<dsp:droplet name="IsEmpty">
								<dsp:param  name="value" bean="ShoppingCart.last.rewardCardNumber"/>
								<dsp:oparam name="false">
									<div class="rewardDisplay">
										<div class="rewardCard"><strong>JCPenney Rewards Card #<dsp:valueof bean="ShoppingCart.last.rewardCardNumber" /></strong></div>
										<div class="rewardProfile"><dsp:valueof bean="ShoppingCart.last.rewardCardProfileName"/>,&nbsp; 
											you have successfully accrued JCPenney Reward points on this order.
										</div>                        
									</div>
								</dsp:oparam>           
							</dsp:droplet>
						    
							<dsp:include page="orderreview/fragment/orderShippingDetailsSection.jsp">
								<dsp:param name="order" bean="ShoppingCart.last"/>
								<dsp:param name="fromConfirmationPage" value ="true"/>
							</dsp:include>
						</div>
					</div>
					
					<div class="copyright-confirm">
						<p>The order total above reflects special offers that were in effect at the time the order was placed. All orders are the credit or review. 
							If you need futher assistance with your order or regarding JCPenney.com please call 1-800-322-1189.
						</p>
						<p>2015 J.C. Penney Corporation Inc. and/or JCP media 6501., Legacy Plano, Texas 75024,USA. All rights reserved.</p>
					</div>
			        
					<div class="step-footer-confirm">
						<h4><img src="${contextroot}/images/secure-lock.svg" alt="">SECURE CHECKOUT</h4>
						<button class="primary" onclick="keepShopping()">
							<span class="continue_shopping">CONTINUE SHOPPING</span>
						</button>
						<div class="clear"></div>
					</div>
					<%--Checkout Panels display section Ends--%>
				</div>
				
				<%--Checkout Pricing Summary display section starts--%>
				<div id="checkoutPricingSummaryDiv" class="right-rail">
					<div id="right-rail-holder" class="holder">
						<div class="holder flt_lft">
							<div class="bottom flt_lft" id="bottom">
								<dsp:include page="/jsp/checkout/secure/checkoutsimplified/checkoutPricingSummary.jsp">
									<dsp:param name="order" bean="ShoppingCart.last"/>
									<dsp:param name="fromConfirmationPage" value ="true"/>
								</dsp:include>	
							</div>
						</div>				
					</div>
				</div>
				<%--Checkout Pricing Summary display section Ends--%>
				
				<%-- Account Creation/Mobile Opt-in pop-up for Guest/Registered User [Start] --%>
				<dsp:getvalueof var="mobileOptinValue" value="Neutral"/>
				<dsp:getvalueof var="emailId" bean="ShoppingCart.last.email"/>
		
				<dsp:droplet name="Switch">
					<dsp:param bean="Profile.transient" name="value"/>
					<dsp:oparam name="true">
						<dsp:droplet name="/atg/dynamo/droplet/RQLQueryForEach">
							<dspel:param name="queryRQL" value="login EQUALS IGNORECASE \"${emailId}\""/>
							<dsp:param name="repository" value="/atg/userprofiling/ProfileAdapterRepository"/>
							<dsp:param name="itemDescriptor" value="user"/>
							<dsp:oparam name="empty">
								<div class="hide_display">
									<c:choose>
										<c:when test="${passwordEnhancement}">
											<a href="${contextroot}/jsp/checkout/secure/checkoutSignUpNew.jsp" rel="sign_up_modal" title="sign up modal"></a>
										</c:when>
										<c:otherwise>
											<a href="${contextroot}/jsp/checkout/secure/checkoutSignUp.jsp" rel="sign_up_modal" title="sign up modal"></a>                              
										</c:otherwise>
									</c:choose>
								</div>
								<c:set var="fullRegistrationFlag" value="true" scope="application"/>
							</dsp:oparam>
							<dsp:oparam name="output">
								<dsp:getvalueof var="emailOptIn" param="element.receiveEmail"/>
								<dsp:getvalueof var="mobileOptIn" param="element.mobileOptin"/>
								<c:choose>
									<c:when test="${mobileOptIn == mobileOptinValue}">
										<div class="hide_display">
											<a href="${contextroot}/jsp/checkout/secure/checkoutMobileOptin.jsp" rel="sign_up_modal" title="sign up modal" class="hide_display"></a>
										</div>
									</c:when>
								</c:choose>
								<c:set var="fullRegistrationFlag" value="false" scope="application"/>
							</dsp:oparam>
						</dsp:droplet>
					</dsp:oparam>
					<dsp:oparam name="false">
						<dsp:getvalueof var="mobileNumber" bean="Profile.mobilePhoneNumber"/>
						<dsp:getvalueof var="mobileOptin" bean="Profile.mobileOptin"/>
						<dsp:getvalueof var="receiveEmail" bean="Profile.receiveEmail"/>
						<c:choose>
							<c:when test="${fullRegistrationFlag ne 'true'}">
								<c:choose>
									<c:when test="${receiveEmail ne 'yes'}">
										<div class="hide_display">
											<a href="${contextroot}/jsp/checkout/secure/checkoutEmailOptin.jsp" rel="sign_up_modal" title="sign up modal" class="hide_display"></a>
										</div>
									</c:when>
								<c:otherwise>
									<c:if test="${mobileOptin == mobileOptinValue}" >
										<div class="hide_display">
											<a href="${contextroot}/jsp/checkout/secure/checkoutMobileOptin.jsp" rel="sign_up_modal" title="sign up modal" class="hide_display"></a>
										</div>
									</c:if>
								</c:otherwise>
				                </c:choose>
							</c:when>
							<c:otherwise>
								<c:set var="fullRegistrationFlag" value="false" scope="application"/>
							</c:otherwise>
						</c:choose>
					</dsp:oparam>
				</dsp:droplet>
				<%-- Account Creation/Mobile Opt-in pop-up for Guest/Registered User [End] --%>
				
				<c:set var="backButtonDisabled" value="false" scope="session"/>
			</div>
			<div class="clear"></div>
			
			<%-- Coremetrics section - Starts --%>
			<%@ include file="/jsp/checkout/secure/checkoutsimplified/orderConfirmationCoremetrics.jsp"%>
			<%-- Coremetrics section - Ends--%>
			
			<%--Checkout footer Section Starts--%>
			<footer>
				<%@ include file="/jsp/global/checkoutFooterSimplified.jsp"%>	
			</footer>
			<%--Checkout footer Section Ends--%>
			<%--Checkout Marketing Tags Section Starts--%>
			<dsp:include page="/jsp/browse/marketing/orderConfirmationMarketingTags.jsp">
                <dspel:param name="order" bean="ShoppingCart.current"/>
                <dspel:param name="profile" bean="Profile"/>
            </dsp:include>
            <%--Checkout Marketing Tags Section Ends--%>
			</div>
			
		</body>
	</html>
	<dsp:droplet name="NetworkInfoDroplet">
		<dsp:oparam name="output">
			<div id="userNetworkInfo" style="display:none"> 
				***************************************************				
				HOST NAME :-> <dsp:valueof param="HOST_NAME" />
				SERVER NAME :-> <dsp:valueof param="SERVER_NAME" />
				TIME :-> <dsp:valueof param="TIME" />          
				***************************************************
			</div>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>