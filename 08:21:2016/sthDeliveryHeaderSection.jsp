<%--
* -----------------------------------------------------------------------
* Copyright 2011, jcpenney Co., Inc.  All Rights Reserved.
*
* Reproduction or use of this file without explicit
* written consent is prohibited.
* Created by: Sushant
*
* Created on: July 14, 2016
* ----------------------------------------------------------------
--%>
<%-- ========================================= Description
/**
 * This page is used to display Order Contact details for OrderReview & Order Confirmation funtionality.
 *
 * @author Sushant
 * @version 0.1 14-July-2016
 **/
--%>
<%@ taglib uri="dsp" prefix="dsp" %>
<%@ taglib uri="dspel" prefix="dspel" %>
<%@ taglib uri="c" prefix="c"  %>
<%@ taglib uri="fmt" prefix="fmt" %>
<%@ page pageEncoding="UTF-8" %>
<dsp:page xml="true">
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/atg/dynamo/droplet/RQLQueryForEach"/>
<dsp:importbean bean="/com/jcpenney/core/order/droplet/PricingDisplayDroplet"/>
<dsp:importbean bean="/com/jcpenney/core/droplet/PhoneNumberDisplayFormatDroplet"/>
<dsp:importbean bean="/com/jcpenney/core/order/droplet/LargeAppItemCheckDroplet"/>
<dsp:importbean bean="/com/jcpenney/core/util/largeapp/LargeAppApplicationConfiguration"/>  
<dsp:importbean bean="/com/jcpenney/core/util/JSPInclude" />
<dsp:getvalueof var="priceAccurate" param="order.priceAccurate"/>
<dsp:getvalueof var="retailMode" bean="JSPInclude.retailMode" />
<dsp:getvalueof var="enableAltPhoneAndDeliveryInstruction" bean="LargeAppApplicationConfiguration.enableAltPhoneAndDeliveryInstruction" />

<c:set var="isLargeAppItemInBag" value="false" scope="request" />
<dsp:droplet name="LargeAppItemCheckDroplet">
<dsp:param name="order" param="order"/>
<dsp:oparam name="output">      
    <dsp:droplet name="Switch">                                         
    <dsp:param name="value" param="largeAppliance"/> 
    <dsp:oparam name="true">
        <c:set var="isLargeAppItemInBag" value="true" scope="request" />
    </dsp:oparam>
    </dsp:droplet>
</dsp:oparam>
</dsp:droplet>

<div class="top">
    <div class="icon">
        <img src="${contextroot}/images/truck.svg" alt="">
    </div>
    <div class="details">
        <h5><span data-anid="productDeliveryMethod">SHIP TO HOME</span> ITEMS</h5>
        <div class="snippet addr">
            <p><span><dsp:valueof param="shipGroup.shippingAddress.firstName"/></span> <span><dsp:valueof param="shipGroup.shippingAddress.lastName"/></span></p>
            <dsp:droplet name="Switch">
			<dsp:param name="value" param="element.shippingAddress.registrantAddress"/>
			<dsp:oparam name="true">
				<p>
					[address hidden for security]
				</p>
				<span id="grRegistrantAddress" style="display:none">true</span>				
			</dsp:oparam>
			<dsp:oparam name="default">
			   <dsp:getvalueof var="companyName" param="shipGroup.shippingAddress.companyName"/>
            <c:if test="${companyName != null && companyName != ''}">
                <p><dsp:valueof param="shipGroup.shippingAddress.companyName" /></p>
           </c:if>
           
            <dsp:getvalueof id="shipAddress1" param="shipGroup.shippingAddress.address1" />
            <dsp:getvalueof id="shipAddress2" param="shipGroup.shippingAddress.address2" />
            <dspel:getvalueof var="countryName" param="shipGroup.shippingAddress.country"/>
            <p>
	            <c:if test="${shipAddress1 != null}">
	            ${shipAddress1},&nbsp;
	            </c:if>
	            
	            <c:if test="${shipAddress2 != null}">
	                ${shipAddress2}&nbsp;
	            </c:if>
	            
	            <c:if test="${countryName != 'US' || countryName != 'AP'}">
                    <dsp:valueof param="shipGroup.shippingAddress.address3" />
                </c:if> 
	        </p>
	        
            <p><span data-anid="city"><dsp:valueof param="shipGroup.shippingAddress.city"/></span>,&nbsp;
	             <c:choose>
	                 <c:when test="${countryName == 'US' || countryName == 'AP' }">                                          
	                     <dsp:getvalueof var="stateCode" param="shipGroup.shippingAddress.state"/>
	                     <c:choose>
	                         <c:when test="${stateCode == 'AA'}">
	                             <span data-anid="state">Armed forces Americas</span>
	                         </c:when>
	                         <c:when test="${stateCode == 'AE'}">
	                             <span data-anid="state">Armed forces Europe</span>
	                         </c:when>
	                         <c:when test="${stateCode == 'AP'}">
	                             <span data-anid="state">Armed forces Pacific</span>
	                         </c:when>
	                         <c:otherwise>
	                         <span data-anid="state">${stateCode}</span>
	                             <dsp:droplet name="RQLQueryForEach">
	                                 <dspel:param name="queryRQL" value="stateAbbrev=\"${stateCode}\" "/>
	                                 <dsp:param name="repository"
	                                 value="/com/jcpenney/core/applicationConfig/repository/ApplicationConfigurationRepository"/>
	                                 <dsp:param name="itemDescriptor" value="countryStateList"/>
	                                     <dsp:oparam name="output">
	                                         <dsp:valueof param="element.stateName"/>
	                                     </dsp:oparam>
	                             </dsp:droplet>
	                         </c:otherwise>
	                     </c:choose>                         
	                 </c:when>
	                 <c:otherwise>   
	                     <dsp:getvalueof var="internationalState" param="shipGroup.shippingAddress.provinceRegion"/>
	                     <c:choose>
	                         <c:when test="${not empty internationalState}">
	                             <span data-anid="state"><dsp:valueof param="shipGroup.shippingAddress.provinceRegion"/></span>
	                         </c:when>
	                         <c:otherwise>
	                             <span data-anid="state"><dsp:valueof param="shipGroup.shippingAddress.state"/></span>
	                         </c:otherwise>                                              
	                     </c:choose>                                                                                                                                                                                                                     
	                 </c:otherwise>
	             </c:choose>&nbsp;<span data-anid="zip"><dsp:valueof param="shipGroup.shippingAddress.postalCode" /></span>
	        </p>
	        <c:if test="${(countryName != 'US') && (countryName != 'AP')}">
                <p><span data-anid="country"><dsp:valueof param="shipGroup.shippingAddress.countryName"/></span></p>
            </c:if>
            <p>
              <c:choose>
                  <c:when test="${countryName == 'US' || countryName == 'AP' }">  
                      <dsp:droplet name="PhoneNumberDisplayFormatDroplet">
                          <dsp:param name="phoneNumber" param="shipGroup.shippingAddress.phoneNumber"/>
                          <dsp:param name="phoneNumberType" value="domestic"/>
                          <dsp:oparam name="output">
                              <dsp:valueof param="formattedPhoneNumber"/>
                          </dsp:oparam>
                      </dsp:droplet>
                  </c:when>
                  <c:otherwise>
                      <dsp:valueof param="shipGroup.shippingAddress.phoneNumber" />
                  </c:otherwise>
              </c:choose></p>
			</dsp:oparam>
			</dsp:droplet>
            
              
              <c:if test="${confirmOrder == null}">
                <a onclick="loadPanelOnReview('shipto');" href="#">edit</a>
              </c:if>
        </div>
        
        <div class="snippet shipping-method">
            <strong>Shipping Method</strong>
            <p>
            <fmt:bundle basename="com.jcpenney.dp.core.order.OrderResources">                               
                <dsp:getvalueof var="shippingMethod" param="shipGroup.shippingMethod"/>
                <c:if test="${shippingMethod eq 'USPS_INTERNATIONAL_AIR' || shippingMethod eq 'UPS_INTERNATIONAL_AIR'}">
                   <fmt:message key='${shippingMethod}'/>
                </c:if>  
            
                <c:if test="${shippingMethod eq 'STANDARD_DDP' || shippingMethod eq 'EXPRESS_DDP'}">
                   <fmt:message key='${shippingMethod}'/>:<dsp:valueof  param="shipGroup.shippingCarrierName"/>
                </c:if>
            </fmt:bundle>
            
            <span data-anid="shippingMethod"><dsp:valueof param="shipGroup.shippingInterval"/></span>&nbsp;
            <dsp:droplet name="Switch">
                <dsp:param name="value" bean="Profile.checkoutInitiated"/>
                <dsp:oparam name="true">
                    <dsp:getvalueof var="orderShippingMethodCharge" param="shipGroup.priceInfo.amount"/>
                </dsp:oparam>
                <dsp:oparam name="default">
                    <dsp:getvalueof var="orderShippingMethodCharge" param="shipGroup.shippingMethodCharge"/>
                </dsp:oparam>
            </dsp:droplet>
            
            <dsp:droplet name="PricingDisplayDroplet">
                <dsp:param name="order" param="order"/>
                <dspel:param name="orderShippingMethodCharge"  value="${orderShippingMethodCharge}"/>
                <dsp:param name="fromOrderShippingFlag" value="true"/>
                <dsp:oparam name="result">  
                    <dsp:getvalueof var="finalOrderShippingCharge" param="finalOrderShippingCharge" vartype="java.lang.Double"/>
                </dsp:oparam>
            </dsp:droplet>
            
	          <c:choose>
	              <c:when test="${countryName == 'US' || countryName == 'AP' }">  
	                  <dsp:getvalueof var="shippingMethodCharge" param="shipGroup.shippingMethodCharge"/>                               
	                  <c:choose>
	                      <c:when test="${finalOrderShippingCharge > 0}">
	                          <dspel:valueof value="${finalOrderShippingCharge }" converter="currency"></dspel:valueof>
	                      </c:when>
	                      <c:otherwise>
	                      <c:choose>
	                          <c:when test="${isLargeAppItemInBag && !retailMode}">
	                              - FREE
	                          </c:when>
	                          <c:when test="${shippingMethod eq 'TRUCK' and isLargeAppItemInBag != 'true'}">
	                              - Shipping Included
	                          </c:when>
	                          <c:otherwise>
	                              - FREE
	                          </c:otherwise>
	                      </c:choose>
	                      </c:otherwise>
	                  </c:choose>             
	              </c:when>
	              <c:otherwise>
	                  - <dsp:valueof param="order.shipToCurrency" />
	                  <dsp:valueof param="order.priceInfo.targetCurrencyShippingPrice" number="0.00" />
	              </c:otherwise>
	          </c:choose>
           </p>
        </div>
        
        <c:if test="${enableAltPhoneAndDeliveryInstruction && isLargeAppItemInBag != null && isLargeAppItemInBag == 'true'}">
            <div class="snippet alt">
	            <strong>Alternate Phone</strong>
	             <dsp:droplet name="IsEmpty">
	             <dsp:param name="value" param="shipGroup.alternatePhoneNumber"/>
	             <dsp:oparam name="false">
		             <dsp:droplet name="PhoneNumberDisplayFormatDroplet">
		                 <dsp:param name="phoneNumber" param="shipGroup.alternatePhoneNumber" />
		                 <dsp:param name="phoneNumberType" value="domestic"/>
		                 <dsp:oparam name="output">
		                     <p><dsp:valueof param="formattedPhoneNumber"/></p>
		                 </dsp:oparam>
		             </dsp:droplet>
	             </dsp:oparam>
	             <dsp:oparam name="true">
	                 <p>Not Provided</p>
	             </dsp:oparam>
             </dsp:droplet>
	        </div>
        
            <div class="snippet instructions">
	            <strong>Special Delivery Instructions</strong>
	            <dsp:droplet name="IsEmpty">
                   <dsp:param name="value" param="shipGroup.deliveryInstruction"/>
                   <dsp:oparam name="false">       
	                   <p><dsp:valueof param="shipGroup.deliveryInstruction" valueishtml="true" /></p>
	                   
	                   <%-- <c:if test="${confirmOrder == null}">
	                       <a onclick="loadPanelOnReview('selectDate');" class="editLink" href="#">edit</a>
	                   </c:if>--%>
	               </dsp:oparam>
	               <dsp:oparam name="true">
	                    <p>Not Provided</p>
	               </dsp:oparam>
              </dsp:droplet>
	        </div>
        </c:if>
             
       <div class="clear"></div>
    </div>
    <div class="clear"></div>
</div>
</dsp:page>
