<%--
* -----------------------------------------------------------------------
* Copyright 2011, jcpenney Co., Inc.  All Rights Reserved.
*
* Reproduction or use of this file without explicit
* written consent is prohibited.
* Created by: Sushant
*
* Created on: July 14, 2016
* ----------------------------------------------------------------
--%>
<%-- ========================================= Description
/**
 * This page is used to display Order Contact details for OrderReview & Order Confirmation funtionality.
 *
 * @author Sushant
 * @version 0.1 14-July-2016
 **/
--%>
<%@ taglib uri="dsp" prefix="dsp" %>
<%@ taglib uri="dspel" prefix="dspel" %>
<%@ taglib uri="c" prefix="c"  %>
<%@ taglib uri="fmt" prefix="fmt" %>
<%@ page pageEncoding="UTF-8" %>
<dsp:page xml="true">
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/com/jcpenney/core/order/droplet/CommerceItemsByShippingGroupDroplet"/>

<%-- Check been made whether the orderConfirmation param is present or not, based on this few things 
     will change between Order Review & Order Confirmation Page --%>
<dsp:droplet name="IsEmpty">
    <dsp:param name="value" param="fromConfirmationPage" />
    <dsp:oparam name="true">
        <dsp:getvalueof var="orderConfirmationFinal" value="false" scope="request"/>
    </dsp:oparam>
    <dsp:oparam name="false">
        <dsp:getvalueof var="orderConfirmationFinal" value="true" scope="request"/>
    </dsp:oparam>
</dsp:droplet>
<dsp:getvalueof var="fromConfirmationPage" param="fromConfirmationPage"/>
 <dsp:droplet name="CommerceItemsByShippingGroupDroplet">
 <dsp:param name="order" param="order"/>
     <dsp:oparam name="output">
         <dsp:getvalueof var="shippingItemMap" param="shippingItemMap"/>
     </dsp:oparam>
 </dsp:droplet>
 <dsp:droplet name="ForEach">
		<dsp:param name="array" param="order.commerceItems"/>
		<dsp:setvalue param="commerceItem" paramvalue="element"/>
		<dsp:oparam name="outputStart">
			<dspel:getvalueof var="allItemsCount" param="count" />
			<span data-anid="ocp_arraySize" style="display:none"><c:out value="${allItemsCount}"/></span>
		</dsp:oparam>
	</dsp:droplet>
 
 <c:forEach items="${shippingItemMap}" var="entry">
     <c:if test="${(entry.key eq 'bopusItems')&& (not empty entry.value )}">
         <c:set var="bopusCommItems" value="${entry.value}"/>
         <dspel:setvalue param="bopusCommItems" value='${bopusCommItems}'/>
     </c:if>
     <c:if test="${(entry.key eq 'shipToStoreItems') && (not empty entry.value )}">
         <c:set var="shipToStoreCommItems" value="${entry.value}"/>
         <dspel:setvalue param="shipToStoreCommItems" value='${shipToStoreCommItems}'/>
     </c:if>
     
     <c:if test="${(entry.key eq 'shipToHomeItems') && (not empty entry.value )}">
         <c:set var="shipToHomeCommItems" value="${entry.value}"/>
         <dspel:setvalue param="shipToHomeCommItems" value='${shipToHomeCommItems}'/>
     </c:if>
     <c:if test="${(entry.key eq 'shipToItems') && (not empty entry.value )}">
         <c:set var="shipToCommItems" value="${entry.value}"/>
         <dspel:setvalue param="shipToCommItems" value='${shipToCommItems}'/>
     </c:if>
 </c:forEach>
 
 <dsp:droplet name="ForEach">
   <dsp:param name="array" param="order.shippingGroups" />
   <dsp:oparam name="output">
       <dsp:droplet name="Switch">
           <dsp:param name="value" param="element.shippingGroupClassType" />
           
           <%-- BOPUS Delivery Group --%>                              
           <dsp:oparam name="bopusShippingGroup">
				<div class="items-group sameday">
				    <dsp:include page="shipment/storeDeliveryHeaderSection.jsp">
				        <dsp:param name="shipGroup" param="element" />
				        <dsp:param name="bopus" value="true" />
				    </dsp:include>
				    
				    <dsp:include page="shipment/bopusDeliveryItemDetailsSection.jsp">
				        <dsp:param name="order" param="order"/>
				        <dsp:param name="commItems" param="bopusCommItems"/>
				    </dsp:include>
				</div>
				<span id="orderContainsBopis" style="display:none">true</span>
           </dsp:oparam>
        </dsp:droplet>
   </dsp:oparam>
</dsp:droplet>

 <dsp:droplet name="ForEach">
   <dsp:param name="array" param="order.shippingGroups" />
   <dsp:oparam name="output">
       <dsp:droplet name="Switch">
           <dsp:param name="value" param="element.shippingGroupClassType" />        
           
           <%-- SHIP TO STORE Delivery Group --%>
           <dsp:oparam name="storeShippingGroup">
           <span id="orderContainsShipToStore" style="display:none">true</span>
                <div class="items-group sameday">
				    <dsp:include page="shipment/storeDeliveryHeaderSection.jsp">
                        <dsp:param name="shipGroup" param="element" />
                        <dsp:param name="bopus" value="false" />
                    </dsp:include>
				    
				    <dsp:include page="shipment/nonBopusDeliveryItemDetailsSection.jsp">
				        <dsp:param name="commItems" param="shipToStoreCommItems"/>
				        <dsp:param name="order" param="order"/>
					    <c:if test="${fromConfirmationPage != null && fromConfirmationPage ==  true}">
					      	<dsp:param name="fromConfirmationPage" value ="true"/>
					    </c:if>
				    </dsp:include>
				</div>
           </dsp:oparam>
           
           <%-- SHIP TO HOME Delivery Group --%>
           <dsp:oparam name="hardgoodShippingGroup">
           	<span id="orderContainsShipToHome" style="display:none">true</span>
                <div class="items-group sameday">
				    <dsp:include page="shipment/sthDeliveryHeaderSection.jsp">
				        <dsp:param name="order" param="order"/>
				        <dsp:param name="shipGroup" param="element" />
				    </dsp:include>
				    
				    <dsp:include page="shipment/nonBopusDeliveryItemDetailsSection.jsp">
				        <dsp:param name="commItems" param="shipToHomeCommItems"/>
				        <dsp:param name="order" param="order"/>
					   <c:if test="${fromConfirmationPage != null && fromConfirmationPage ==  true}">
					      <dsp:param name="fromConfirmationPage" value ="true"/>
					   </c:if>
				    </dsp:include>
				</div>
           </dsp:oparam>
       </dsp:droplet>
   </dsp:oparam>
</dsp:droplet>
</dsp:page>
