<%@ taglib prefix="c" uri="c"%><%@ taglib prefix="fmt" uri="fmt"%><%@ taglib
	uri="dsp" prefix="dsp"%><%@ taglib uri="dspel" prefix="dspel"%>
<dsp:page>
<c:set var="isLargeAppItemInBag" value="false" scope="request" />
<dsp:importbean bean="/com/jcpenney/core/util/largeapp/LargeAppApplicationConfiguration"/>
<dsp:droplet name="/com/jcpenney/core/order/droplet/LargeAppItemCheckDroplet">
<dsp:param name="order" bean="ShoppingCart.current"/>
<dsp:oparam name="output">
    <dsp:droplet name="/atg/dynamo/droplet/Switch">
    <dsp:param name="value" param="largeAppliance"/>
    <dsp:oparam name="true">
        <c:set var="isLargeAppItemInBag" value="true" scope="request" />
        <dsp:droplet name='/com/jcpenney/core/integration/largeappliance/droplet/LargeAppDeliveryDateDroplet'>
		<dsp:param name="order" bean="ShoppingCart.current" />
		<dsp:param name="zipCode" param="zipCode"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="availableDates" param="deliveryDateArray"/>
			<dsp:getvalueof var="minDate" param="deliveryDateMinDate"/>
			<dsp:getvalueof var="maxDate" param="deliveryDateMaxDate"/>
			<dsp:getvalueof var="isManualDate" param="isManualDeliveryDate"/>
		</dsp:oparam>
	</dsp:droplet>
    </dsp:oparam>
    </dsp:droplet>
</dsp:oparam>
</dsp:droplet>

<style type="text/css">
	.ui-state-default, .ui-widget-content .ui-state-default {
     border: none;!important;
     background: none;!important;
    font-weight: 400;!important;
    color: #555;!important;
    outline: 0;!important;
}

#datepicker_heading1 {
    font: bold 18px/18px Helvetica, Arial, Sans-serif;
}

.ui-widget-header {
    background: none;!important;
    border: none;!important;
}
#selectedDate {
    font: 700 16px/0 Helvetica,Arial,Sans-serif;!important;
}

.ui-datepicker .ui-datepicker-title {
    text-align: left;!important;
    /*margin-left: 0;!important;*/
}




</style>
<c:if test="${isLargeAppItemInBag == true}">
       		<dsp:droplet name="/atg/dynamo/droplet/ForEach">
       			<dsp:param name="array" bean="ShoppingCart.current.shippingGroups" />
				<dsp:oparam name="output">
					<dsp:droplet name="Switch">
						<dsp:param name="value" param="element.shippingGroupClassType" />
						<dsp:oparam name="hardgoodShippingGroup">
						<span id="varselectedDeliveryDate" class="hide_display"><dsp:valueof param="element.deliveryDate"/></span>
						<dsp:getvalueof var="selectedDelDate" param="element.deliveryDate"/>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>

							<div id="datepicker_wrapper" class="padt15 mrgbtm10px">
								<span class="hide_display" id="isManualDate">${isManualDate}</span>
								<div id="dateReselect">
								<c:if test="${isManualDate eq 'true' && (selectedDelDate == null || selectedDelDate == '')}">

								<div data-anid="click_errorDescription" class="flt_lft_largeApp_cal_info_container" id="manualReselectError">
								<div class="hide_display" id="dateReselection">
									<div class="flt_lft mrgr15">
										<img title="attention" alt="attention" class="mrgltop10px"
											src="<c:out value='${contextroot}'/>/images/info.svg" />
									</div>
									<div data-anid="click_errorDescription" id="deliverydateInfo">
										<fmt:message key="keySelectedDeliveryDateErrorServiceDown"
											bundle="${checkoutResources}" >
											</fmt:message>
									</div>
									</div>

								<div id="manualDate">
									<div class="flt_lft mrgr15">
										<img title="attention" alt="attention" class="mrgltop10px"
											src="<c:out value='${contextroot}'/>/images/info.svg" />
									</div>
									<div data-anid="click_errorDescription" id="deliverydateInfo">
										<fmt:message key="keySelectDeliveryDateFailError"
											bundle="${checkoutResources}" />
									</div>
								</div>
								</div>
							</c:if>

							<div class="flt_lft_largeApp_cal_info_container hide_display" id="reselectInfo">
							<div class="flt_lft mrgr15">
										<img title="attention" alt="attention" class="mrgltop10px"
											src="<c:out value='${contextroot}'/>/images/info.svg" />
									</div>
							<div data-anid="click_errorDescription" id="deliverydateInfoMsg">
										<fmt:message key="keySelectedDeliveryDateError"
											bundle="${checkoutResources}" />
									</div>
									</div>
									</div>

									<p id="datepicker_heading2">Select Delivery Date</p>
									<p id="datepicker_heading1" class="hide_display">Selected Delivery Date: </p>

									<p class="selectedDateStr hide_display" id="selectedDate"></p>
									<span id="lostDeliveryDate" class="hide_display"></span>


			<input type="hidden" id="selectedDeliveryDate" name="selectedDeliveryDate"/>
									<div id="delivery_blk">
									<div id="datepicker" class="deliveryDatePicker"></div>
									<div id="delivery_txt">
									<p>Deliveries</p>
								   <c:choose>
				                        <c:when test="${isLargeAppItemInBag == true}">
				                        <ul> <li>Changing the delivery zip code, or adding other appliances to this order may require a change in delivery date.</li>
				                        <li><span class="dateDisplayLimit"><dsp:valueof bean="LargeAppApplicationConfiguration.deliveryDateMessage" valueishtml="true"></dsp:valueof></span></li>
										<li>Delivery will occur between 7 am - 7 pm local time, Tuesday - Saturday, with the specific day varying by market.</li>
										<li>A delivery agent will call the number you provided to confirm your 4-hour delivery window.</li>
										<li>The number of delivery dates per week may vary by market.</li>
				                        </c:when>
				                        <c:otherwise>
				                        <li>delivery agent will call 24 hours before delivery to schedule the time</li>
										<li><span class="dateDisplayLimit">delivery dates are limited to the 30 days displayed on the left</span></li>
										<li>will be scheduled via phone the day before at number provided</li>
										<li>delivery windows generally a 4 hour time period</li>
										<li>will be scheduled 7am-7pm local time</li>
										</ul>
										</p></p>
										<p>Number of delivery days per week
										may vary in some zip codes
										due to insufficient volume</p>
				                        </c:otherwise>
				                    </c:choose>
									</div>
									</div>


								</div>
								<dsp:getvalueof var="enableAltPhoneAndDeliveryInstruction" bean="LargeAppApplicationConfiguration.enableAltPhoneAndDeliveryInstruction" />
								<c:if test="${enableAltPhoneAndDeliveryInstruction}">
								<dsp:droplet name="/atg/dynamo/droplet/ForEach">
					       			<dsp:param name="array" bean="ShoppingCart.current.shippingGroups" />
									<dsp:oparam name="output">
										<dsp:droplet name="Switch">
											<dsp:param name="value" param="element.shippingGroupClassType" />
											<dsp:oparam name="hardgoodShippingGroup">
								<div id="delIns">
								<div class="mrgtp10px">
								<div id="invalidPhone" class="hide_display">
										<img title="attention" alt="attention"
											src="<c:out value='${contextroot}'/>/images/error_icon.svg" /> <span data-anid="click_errorDescription">Enter a valid phone number.</span>
									</div>
								<span class="bold">alternate phone number</span> (optional)
								</div>
								<p>
								<dsp:input autocomplete="off" type="text" iclass="altphone" id="largeAppAltContact" name="largeAppAltContact" paramvalue="element.alternatePhoneNumber" maxlength="13" bean="CreateHardgoodShippingGroupFormHandler.largeApplianceAltPhone">
											<dspel:tagAttribute name="placeholder" value="Ex: 555-555-5555" />
								</dsp:input>
								</p>

								<div class="mrgtp10px">
								<span class="bold">delivery instructions</span> (optional)<span class="flt_rgt">max 120 characters (avoid "@" or "&")</span>
								</div>


								<c:set var="placeHolder" value="Ex: I live in a gated community and the key code is 5555; my subdivision is new and not on Google maps; knock on the back door; 3rd floor delivery." />

								<dsp:getvalueof var="specialCharRegex" bean="/com/jcpenney/core/util/largeapp/LargeAppApplicationConfiguration.specialCharRegex" />
								<dspel:textarea autocomplete="off" iclass="deliveryins mrgtp10px" valueishtml="true" maxlength="120" id="largeApplianceDeliveryIns" name="largeApplianceDeliveryIns" value="${placeHolder}" onkeypress='if(event.keyCode == 13){event.preventDefault();} return ( this.value.length < 120);' onkeyup="preventSpecialChars('${specialCharRegex}');charCounter();"  onblur="preventSpecialChars('${specialCharRegex}');"
								placeholder="${placeHolder}" bean="CreateHardgoodShippingGroupFormHandler.largeApplianceDeliveryIns" ><dsp:valueof param="element.deliveryInstruction"/></dspel:textarea>
								<div class="flt_rgt" id="textarea_remain"></div>
								</div>
											</dsp:oparam>
											</dsp:droplet>
										</dsp:oparam>
									</dsp:droplet>
								</c:if>


<script type="text/javascript">
var availableDates ="<c:out value='${availableDates}'/>";
var avaialableDateArray = getAvailableDateValues(availableDates);

var dateArr = [], sortedDateArr = [], dateCount = avaialableDateArray.length;
for(var i = 0; i < dateCount; i++) {
	dateArr[i] = (new Date(avaialableDateArray[i]));
}
sortedDateArr = dateArr.sort(function(a,b) {
    return (a - b);
});

function available(date) {
   	return checkAvailable(date, avaialableDateArray, availableDates);
}

 $(function() {
   $( "#datepicker" ).datepicker({
     //numberOfMonths:[2,1],
     //minDate: "<c:out value='${minDate}'/>",
     minDate: addDays(sortedDateArr[0], 1),
     maxDate: addDays(sortedDateArr[dateCount - 1], 1),
     hideIfNoPrevNext: false,
     prevText: "",
     nextText: "",
     dayNamesMin:[ "S", "M", "T", "W", "Th", "F", "S" ],
     beforeShowDay: available,
     /*onChangeMonthYear: function(year, month, obj) {
    	 if(month == lastAvlDtArrayMon) {
    		 $("#delivery_blk .ui-datepicker-next").hide();
    	 }
     },*/
     onSelect: function(date, obj, e) {
    	 var date1 = $(this).datepicker('getDate');
    	 handleDateSelection(date1, avaialableDateArray, availableDates);
     }
   });

    var selectedDt = $("#varselectedDeliveryDate").text();
    var arr = "";
    var dt = "";
    if(selectedDt != "" && selectedDt != undefined) {
        arr = selectedDt.split("/");
        dt = arr[2] + "-" + arr[0] + "-" + arr[1];
    }

    setTimeout(function() {
    	if($("#varselectedDeliveryDate").text().toString().length <= 0 || availableDates.indexOf(dt) < 0) {
    		$("#delivery_blk td.ui-datepicker-current-day").removeClass("ui-datepicker-current-day");
    	}
    }, 500);
 });

 $(document).ready(function() {
		showDateUsingDatePicker(avaialableDateArray, availableDates);
		if ($('#deliverydateInfo') && $('#deliverydateInfo').html() != '') {
			handleCoreMetricsLargeAppTag('Delivery Date Service Call Failed', 'Project Silver Choose Delivery Date');
		}
		if ($('#streetAddress1') && $('#streetAddress1').val() != '' && ($('#varselectedDeliveryDate').html() == null || $('#varselectedDeliveryDate').html() == '')) {
			handleCoreMetricsLargeAppTag('Date Reselect Required', 'Project Silver Choose Delivery Date');
		}

	});
</script>


</c:if>
</dsp:page>
