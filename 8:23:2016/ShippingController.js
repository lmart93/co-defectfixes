$(document).ready(function() {
	$("#shipToHomeContinueForm ul.sm-btns li").live("click", function(e) {
		$("#shipToHomeContinueForm ul.sm-btns li").removeClass("active");
		$(this).addClass("active");
		setHdnShippingMethodValues($(this));
	});
	
	if($("#holidayShippingOptionEn")[0]){
		getCheckBoxValue();
	} else {
	$("input#holidayOptIn").val(false);
	}
	createEvent ('modify');
	// Listen for the event.
	if (document.getElementById("zipCode") != null && document.getElementById("zipCode") != undefined) {
		document.getElementById("zipCode").addEventListener('modify', validateShipToHomeForm, false);
	}
	
	$("#giftProduct").live("click", function (e) {
		// $(this).parent().toggleClass("selected");
	});
	
	// switch shipping options - on load
	var shipLocValue	=	$('input[name=shippingLocation]:checked').val() ;
	if(shipLocValue!=null && shipLocValue == 'shipToStore') {
		selectShipToStore();
		setTimeout(function(){switchToStoreShipping();}, 1000);
	} else {
		selectShipToHome();
	}
	
	// switch shipping options - on click
  $("input[name=shippingLocation]").click(function () {
    if (this.value == "shipToStore") {
    	selectShipToStore();
    	setTimeout(function(){switchToStoreShipping();}, 1000);
    } else {
    	selectShipToHome();
    	shippingGroupChanged();
    }
  });
  
});

function getAvailableDateValues(availableDates){
	var avaialableDateArray = availableDates.split(",");
	return avaialableDateArray;
}
function getFormatedDate(date) {
	   var dmy;
	   if (date) {
		   var monthNum = date.getMonth() + 1;
			if (monthNum < 10) {
				monthNum = '0' + monthNum;
			}
			var dayNum = date.getDate();
			if (dayNum < 10) {
				dayNum = '0' + dayNum;
			}
	       	dmy = date.getFullYear() + "-" + monthNum + "-" + dayNum;
	   }
	   
	   return dmy;
}


var weekday=new Array(7);
weekday[0]="Sun";
weekday[1]="Mon";
weekday[2]="Tue";
weekday[3]="Wed";
weekday[4]="Thu";
weekday[5]="Fri";
weekday[6]="Sat";
var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];


function handleDateSelection(date, avaialableDateArray, availableDates) {
	   if (availableDates != null) {
			var dmy = getFormatedDate(date);
			
			if ($.inArray(dmy, avaialableDateArray) != -1) {
				var dateString = $.datepicker.formatDate('mm/dd/yy', date);
		      	var dayOfWeek = weekday[date.getDay()];
				var selectedDate = dayOfWeek+", "+months[date.getMonth()]+" "+ dateRank(date.getDate());
		 	 	$( "#selectedDate" ).html(selectedDate);
		 	 	$('#largeAppSelDelDate').html(selectedDate);
		 	 	document.getElementById("selectedDeliveryDate1").value = dateString;	 
		 	 	$( "#datepicker_heading2" ).hide();
		 	 	$("#datepicker_heading1").removeClass('hide_display');
		 	 	$("#selectedDate").removeClass('hide_display');
			}
	   }
}
function dateRank(day) {
    var suffix = "";
    switch(day.toString()) {
        case '1': case '21': case '31': suffix = 'st'; break;
        case '2': case '22': suffix = 'nd'; break;
        case '3': case '23': suffix = 'rd'; break;
        default: suffix = 'th';
    }

    return day + suffix ;
}
function addDays(theDate, days) {
    return new Date(theDate.getTime() + days*24*60*60*1000);
}

function daysBetween(one, another) {
	var d1 = new Date(one);
	var d2 = new Date(another);
	return Math.round(Math.abs(d1 - d2) / 8.64e7);
} 

function showDateUsingDatePicker(avaialableDateArray,availableDates){
	var datePickerSetted = false;
	var selectedDt = $("#varselectedDeliveryDate").text();
    var arr = "";
    var dt = "";
    if(selectedDt != "" && selectedDt != undefined) {
        arr = selectedDt.split("/");
        dt = arr[2] + "-" + arr[0] + "-" + arr[1];
    }
    if(availableDates.indexOf(dt) < 0) {
    	$("#varselectedDeliveryDate").text("");
    }
	var varselectedDeliveryDate= $("#varselectedDeliveryDate").text();
	
	   if(varselectedDeliveryDate) {
	   	document.getElementById("selectedDeliveryDate1").value=varselectedDeliveryDate;
	   	var date = new Date(varselectedDeliveryDate);
	   	var fd = getFormatedDate(date);
	    if (fd && $.inArray(fd, avaialableDateArray) != -1) {
		   	var dayOfWeek = weekday[date.getUTCDay()];   	
		    var dateFormat = $.datepicker.formatDate('mm/dd/yy', new Date(date));
		   	var selectedDate = dayOfWeek+", "+months[date.getMonth()]+" "+date.getUTCDate()+", "+date.getFullYear();
		   	var day = date.getDate();
		   	var monthIndex = date.getMonth();
		   	var year = date.getFullYear();
		    $("#datepicker").datepicker( "setDate", dateFormat);
		   	$("#datepicker_heading2").hide();
		   	$("#datepicker_heading1").removeClass('hide_display');
	 	 	$("#selectedDate").removeClass('hide_display');
		   	
		    $("#selectedDate").html(selectedDate);
		    datePickerSetted = true;
	    }
	   } 
	    
	  
   var currentDate = $("#datepicker").datepicker( "getDate");
   if (currentDate) {
   		handleDateSelection(currentDate, avaialableDateArray, availableDates);
   }
}

function checkAvailable(date, avaialableDateArray, availableDates){
	 if (availableDates != null) {
			var dmy = getFormatedDate(date);
		    if ($.inArray(dmy, avaialableDateArray) == -1) {
		    	return [false, "", "Unavailable"];
		    } else {
		        return [true, ""];
		    }
		  }
}
//date picker js code End//
function getValidShippingMethods() {
    if (profileValue == "true") {
        if (document.getElementById('phoneNumber').value != "") {
            document.getElementById('hdnPhoneNumber').value = document.getElementById('phoneNumber').value;
        }else{
        	document.getElementById('hdnPhoneNumber').value ='';
        }
        document.getElementById('hdnCountryName').value = document.getElementById('selectShippingCountry').value;
        if (document.getElementById('selectShippingCountry').value == 'United States') {
        	// need to disable the AP country related fields
        	$('#militarySpecific :input').attr('disabled','disabled');        	
            if (document.getElementById('streetAddress1').value != "") {
                document.getElementById('hdnStreetAddress1').value = document.getElementById('streetAddress1').value;
            }else{
            	 document.getElementById('hdnStreetAddress1').value='';
            }
            document.getElementById('hdnStreetAddress2').value = document.getElementById('streetAddress2').value;
            if (document.getElementById('cityName').value != "") {
                document.getElementById('hdnCityName').value = document.getElementById('cityName').value;
            }
            if (document.getElementById('zipCode').value != "") {
                document.getElementById('hdnZipCode').value = document.getElementById('zipCode').value;
            }
        } else if (document.getElementById('selectShippingCountry').value == 'AP') {
        	// need to disable the US country related fields
        	$('#usSpecific:input').attr('disabled','disabled');
            if (document.getElementById('militarystreetAddress1').value != "") {
                document.getElementById('hdnStreetAddress1').value = document.getElementById('militarystreetAddress1').value;
            }
            document.getElementById('hdnStreetAddress2').value = document.getElementById('militarystreetAddress2').value;
            if (document.getElementById('militaryzipCode').value != "") {
                document.getElementById('hdnZipCode').value = document.getElementById('militaryzipCode').value;
            }
            document.getElementById('hdnCityName').value = ''
        }
    }
   
    var options = {
        success: populateShippingMethods,
        error: populateShippingMethodsError,
        timeout: ajaxTimeOut
    };
    
	$('#addShippingForm').ajaxSubmit(options);
	trackPageLoadTime('add-Shipping-Submitted');

}

function populateShippingMethodsError(responseText, statusText, xhr, $form)
{
    if(statusText=='timeout'){
		$('#shippingAddressError').html("<ul>There is some technical issue.</ul>");
        shippingFrag = true;
		$('#shipToHomeErrorContainer').show();
		
	}else{
		redirectOnError();
    }
}

function populateShippingMethods(responseText, statusText, xhr, $form)  {
	var isDeliveryDateErrorPresent = false;
	if (responseText.errorExist == "false") {
		if (includeLargeAppItem == 'true') {
			$("#largeapp").removeClass("hide_display");
		}
		startTimeStamp = (new Date()).getTime();
		var currentTimeStamp = (new Date()).getTime();
		var timeDiff = (currentTimeStamp - startTimeStamp)/1000;
		//console.log("timeDiff="+timeDiff );
		shippingTimeDiffLimit = '-1';
		if(timeDiff > shippingTimeDiffLimit  && !shippingMethodsFetched) {
			$("#emptyShippingMethod").html("Retrieving shipping methods");
			$(".subhead .loading-spinner").css("display", "inline-block");
			startTimeStamp = null;
		}
		if(responseText.shipToHomeAddress != undefined && null != responseText.shipToHomeAddress) {
			submittedShippingAddress.streetAddress1=responseText.shipToHomeAddress.streetAddress1;
			submittedShippingAddress.streetAddress2=responseText.shipToHomeAddress.streetAddress2;
			submittedShippingAddress.city=responseText.shipToHomeAddress.city;
			submittedShippingAddress.state=responseText.shipToHomeAddress.state;
			submittedShippingAddress.zipCode=responseText.shipToHomeAddress.zipCode;
			
		}
		
	}	
    var temp = new Array();
    var commerceItems = new Array();
	var removablecommerceItem = new Array();
	var removablecommerceItemValue = '';
	var itemsInEligible = false;
	var j=0;
	
	$("#streetAddress1").removeClass("input_error_txt");
	//added to handle the session expirty redirection.
	if (responseText.redirectURL) {
		window.location.href = responseText.redirectURL + '?sessionExpired=true';
		return;
	}
	
	
	if(responseText.errorArray != undefined && null != responseText.errorArray && null != responseText.errorArray.addShippingErrors)
    {
		var errorHtml = '';
		var map = responseText.errorArray.addShippingErrors;
		var orderHasOOSItem = false;
		var errorsShownInline = false;
		$('#firstNameError').empty();
		$("#lastNameError").empty();
		$('label[for="phoneNumber"]').prev('.error').remove();
		$('label[for="militarystreetAddress1"]').prev('.error').remove();
		$('label[for="streetAddress1"]').prev('.error').remove();
				
		
		for(var i in map) {
			key = map[i].propertyName;
			value = map[i].errorMessage;
			if (value.indexOf("&amp;gt") > -1) {
				var errorMsg = $('<textarea />').html(value).text();
				var errorMsgText = $('<textarea />').html(errorMsg).text();
				value = errorMsgText;
			}			
			value = removeEncodedSpecialChar(value);
			if(isCDIStdErrKey(key)){
                var standardCount = $('#addressStandardCount').val();
			standardCount++;
			$('#addressStandardCount').val(standardCount);
			}
			// DPXVI-5174  Coremetrics tagging for Shipping panel Error - Starts
			if(coremetricsTagEnabled) { 
				createCoremetricsElementTag(key+'.invalid','Error_Checkout_Shipping');
			}
			// DPXVI-5174  Coremetrics tagging for Shipping panel Error - Ends
			if(key == "firstName"){
				$('#firstName').addClass("error");
				$("#firstNameError").css("display","block");
				$("#firstNameError").html( "<div class='error'>"+value+"</div>" );
				
			}

			if(key == "lastName"){
				
				$('#lastName').addClass("error");
				$("#lastNameError").css("display","block");
				$("#lastNameError").html( "<div class='error'>"+value+"</div>" );
				
			}

            if(key == "phoneNumber"){
            
                $('#phoneNumber').addClass("error");
                $("<div class='error'>"+value+"</div>").insertBefore( 'label[for="phoneNumber"]' );	
				errorsShownInline = true;
				
			}
           
			if(key == "postalCode"){
	                $("#postalCode").addClass("error");
	                $("#militaryZipCode").addClass("error");
	                $("#zipCode").addClass("error");
	                $("#cityName").addClass("error");
	                $("#selectState").addClass("error");
	                $("#zipCityError").html( "<div class='error'>"+value+"</div>" );
	                $("#milZipCityError").html( "<div class='error'>"+value+"</div>" );
	                if($('#zipCityError').length > 0)
					{
	                	$('#shippingPageForGuest_autoPopulateError').empty();
	                	$('#shippingPageForGuest_autoPopulateError').removeClass('error');
					}
	                
				errorsShownInline = true;
			}else{
				$('#zipCityError').removeClass('error');
				$('#cityName').removeClass('error');
				$('#selectState').removeClass('error');
				
			}
			
			if(value.indexOf("matching city for that ZIP code") != -1){
				$("#cityName").addClass("error");
	           	errorsShownInline = true;
				
			}
			//Added for defect #5211 Start.
			if(key == "address1"){
				
                $("#streetAddress1").addClass("error");
                $("#militarystreetAddress1").addClass("error");
                $("<div class='error'>"+value+"</div>").insertBefore( 'label[for="militarystreetAddress1"]' );	
                $("<div class='error'>"+value+"</div>").insertBefore( 'label[for="streetAddress1"]' );	
                errorsShownInline = true;
                
			}

			if(key == "address2"){
                $("#streetAddress2").addClass("error");
                $("#militarystreetAddress2").addClass("error");
				
				
			}
					
			if(key == "city"){
                $("#cityName").addClass("error");
               }
			
			if(key == "keyOCSPostalCodeSuspended"){
                $("#cityName").addClass("error");
                $("#streetAddress1").addClass("error");
                $("#militarystreetAddress1").addClass("error");
                $("#militaryZipCode").addClass("error");
                $("#zipCode").addClass("error");
                $("#streetAddress2").addClass("error");
                $("#militarystreetAddress2").addClass("error");
				$("#selectState").addClass("error");
				
			}		
			
			if(key=="keyItemsMovedToShipTo" || key == "keyOrderHasAllOutOfStockItems" 
				|| key == "keyOrderHasOutOfStockItem") {
				openCookieDisableMessage('/dotcom/jsp/checkout/secure/checkoutsimplified/itemsOutOfStockMessage.jsp?oosMsgKey='+key, key);
				if (responseText.checkoutStepsHtml != null) {
					var Title = $('<textarea />').html(responseText.checkoutStepsHtml).text();
					$('#checkoutProgressBarDiv').html(Title);
					$('#pickup').hide();
					$('#shipto').show();
					$('#shipto').addClass("ng-enter");
				}
				if (responseText.shippingHtml != null) {
					$('#sameDayPickUpPanel').hide();
					var Title = $('<textarea />').html(responseText.shippingHtml).text();
					$('#shippingPanel').html(Title);
				}
			}
			
			// Code to check the items eligible to be shipped
			// Splitting to get the commerce items
			temp = value.split('$');

            for(i=0;i<temp.length;i++){
				if(temp[i].substring(0,2)=="ci") {
				removablecommerceItem[j] = temp[i];
				removablecommerceItemValue='$'+removablecommerceItem[j]+'$';
				j++;
				value=value.replace(removablecommerceItemValue,"");
				itemsInEligible = true;
			}
            }

			value = value.replace("?","&trade;");//Fix for trademark
			value = value.replace("\?","&trade;");//Fix for trademark

			var isPOBoxErrorsPresent = false;
            var isAllItemsInEligible = false;
			if(key == "allRestricedItems"){
				isAllItemsInEligible = true;
				$("#errorMessageId").addClass("padt10");
				var errorArray = value.replace(/&amp;gt;/g,'>').replace(/&amp;lt;/g,'<').split("<ul></ul>");
				document.getElementById('POBoxError').innerHTML = errorArray[0];
				errorHtml+="<ul><li>"+errorArray[1]+"</li></ul>";
				
            }
			if(key == "keyOCSPOBoxError"){
				isPOBoxErrorsPresent = true;
				document.getElementById('POBoxError').innerHTML=value;
				$("#streetAddress1").addClass("input_error_txt");
			} else if(key == "keyOCSPostalCodeSuspended") {
				isPOBoxErrorsPresent = true;
				var shippingSuspMessage = "Shipping to this area is unavailable due to a natural disaster. Try a different address, or ";
				shippingSuspMessage+="<a href='/dotcom/jsp/cart/viewShoppingBag.jsp?shippingSuspended=true'>";
				shippingSuspMessage+="return to your bag </a> and save items for later.";
				document.getElementById('POBoxError').innerHTML=shippingSuspMessage;
				$("#errorMessageId").hide();
			} else if(key == "keySelectedDeliveryDateError") {
				isDeliveryDateErrorPresent = true;
				$("#shipToHomeErrorContainer").hide();
				errorHtml+=value;
				
				if(document.getElementById('truckableItemServerErrors') != undefined && document.getElementById('truckableItemServerErrors') != null) {
					document.getElementById('selectedDateUnavailable').innerHTML=errorHtml;
				}
				$('#truckableItemServerErrorContainer').show();	
				$("#largeapp").removeClass("hide_display");
				$("#shippingAddress_continue").hide();
				$("#shipping_submit").show();	
				$("#shipping_submit").removeAttr("disabled");
			} else {
				if(!isAllItemsInEligible) {
					
					if (!errorHtml.indexOf("removeItems()")){
						errorHtml+="<li>"+value+"</li>";
					} else {
						errorHtml+="<li>"+value+"</li>";
					}
				}
            }
			if (isPOBoxErrorsPresent || isAllItemsInEligible) {
				$('#POBoxError').show();
			} else {
				$('#POBoxError').hide();
			}
        }


		do{
			errorHtml = errorHtml.replace("<li></li>","");
		}while(errorHtml.indexOf("<li></li>") > 0);

        if (errorHtml.indexOf("removeItems()") == -1) {
			$('#shippingAddressError').html("<ul>"+errorHtml+"</ul>");
		} else {
			if(errorHtml.startsWith("<li>")){
				errorHtml = errorHtml.replace("<li>","");
				errorHtml = errorHtml.substring(0,(errorHtml.length-5));
			}
			$('#shippingAddressError').html("<ul>"+errorHtml+"</ul>");
			$("#errorMessageId").hide();
			$("#errorMessageBlkAddr").show();
		}

        if(itemsInEligible) {
            document.getElementById('removablecommerceItem').value=removablecommerceItem;
			$("#errorMessageId").hide();
		} else {
			if(errorHtml != ''){
				$("#errorMessageId").show();												
			}
		}
		shippingFrag = true;
		if (!orderHasOOSItem && !errorsShownInline && !isDeliveryDateErrorPresent) {
			$('#shipToHomeErrorContainer').show();
			if(itemsInEligible) {
				$('#errorMessageId').hide();
				$('errorMessageBlkAddr').show();
			}
		}
		if($('#shipToHomeErrorContainer').is(":visible")) {
			$("#shippingAddress_continue").show();
			detachShippingFormEventHandlers();
			if ($('#showShippingMethod ul').length > 0) {
				displayEmptyShippingMethod();
			}		
		}
		
	} else {
		setTimeout(function(){loadShippingMethodsForShipToHome(responseText);}, 500);
			
	}
	if(includeLargeAppItem == 'true') {
		$('#showShippingMethod').hide();
	}
	
}
function loadShippingMethodsForShipToHome(responseText) {
	removeShippingErrorContainer();
	$("#shipping_submit").show();	
	$("#shipping_submit").removeAttr("disabled");
	$("#shippingAddress_continue").hide();
	detachShippingFormEventHandlers();
	attachShippingFormEventHandlers();
	
	if (responseText.shippingMethodHtml != null) {
		$('#showShippingMethod').html(formatHTML(responseText.shippingMethodHtml));			
			$('#emptyShippingMethod').hide();
			$(".subhead .loading-spinner").hide();
			$('#showShippingMethod').hide().fadeIn();

			shippingMethodsFetched = true;
			showHideGiftWrap();
			if(includeLargeAppItem == 'true') {
				$('#showShippingMethod').hide();
				if(document.getElementById('selectedDateUnavailable') != null && document.getElementById('selectedDateUnavailable').innerHTML != undefined && document.getElementById('selectedDateUnavailable').innerHTML != "") {
					$('#truckableItemServerErrorContainer').show();
				}
			}
	}
	if (responseText.pricingSummaryHtml != null) {			
		$('#checkoutPricingSummaryDiv .holder').html(formatHTML(responseText.pricingSummaryHtml));
		$('#checkoutPricingSummaryDiv .holder #bottom').addClass("sticky sticky-pricing");
		$('a.helpIcon').tooltip({
    		overrideClass: "helpIcontip"
    	});
	}
	if (responseText.checkoutStepsHtml != null) {
	    var checkoutStepsHtml = $('<textarea />').html(responseText.checkoutStepsHtml).text();
	    $('#checkoutProgressBarDiv').html(checkoutStepsHtml);
	}
	if (responseText.shippingGroupsHtml != null) {
		$('#saved-shipping-groups').html(formatHTML(responseText.shippingGroupsHtml));
	}
    $('#addressStandardCount').val('');
    
    coreMetricsPart("Shipping", "JCP|Checkout");
}
function removeErrorContainer() {
	$('.dynamic_error_msgs').hide();
	$('.dynamic_error_msgs ul li').empty();
	$('#willCallAddressForm input, #willCallAddressForm label ').removeClass('error');
	$('div.error').remove();
	
}

function setHdnShippingMethodValues(self) {  

    $('#hdnShippingMethodName').val(self.attr("data-shippingMethodName"));
    $('#hdnNumberOfDays').val(self.attr("data-numberOfDays"));
    $('#hdnShippingCharge').val(self.attr("data-shippingCharge"));
    if($('#hdnShippingCarrierName').length > 0 && self.attr("data-shippingDescription") != undefined)
    	$('#hdnShippingCarrierName').val(self.attr("data-shippingDescription"));
		
}


function preventSpecialChars(specialCharRegex){
	var input = $('#largeApplianceDeliveryIns1').val();
	var regex = new RegExp(specialCharRegex, 'gi');
	var isSplChar = regex.test(input);
	if(isSplChar)
	{
		var no_spl_char = input.replace(regex, '');
		$('#largeApplianceDeliveryIns1').val(no_spl_char);
	}
}

function charCounter() {
	var text_max = 120;
    $('#textarea_remain').html(text_max + ' characters left');
        var text_length = $('#largeApplianceDeliveryIns1').val().replace(/\r(?!\n)|\n(?!\r)/g, "\r\n").length;
        var text_remaining = text_max - text_length;
        $('#textarea_remain').html(text_remaining + ' characters left');
}
		


function applyShippingMethodOnSubmit() {
	var shippingGroup = $('#shippingLocation').val();
	if (shippingGroup == 'shipToHome') {
		setHdnShippingMethodValues($("#shipToHomeContinueForm ul.sm-btns li.active"));
		$('#selectedDeliveryDate').val($('#selectedDeliveryDate1').val());
		$('#largeAppAltContact').val($('#altPhoneNumber').val());
		$('#largeApplianceDeliveryIns').val($('#largeApplianceDeliveryIns1').val());
		setShippingMethod();
	} 
}


function setShippingMethod(){
		var options = {
	        success: setShippingMethodAndPopulatePayment,
			error: setShippingMethodError,
			timeout: ajaxTimeOut
		};
		
	    $('#shipToHomeContinueForm').ajaxSubmit(options);
	    trackPageLoadTime('Apply-Shipping-Requested');	 
	
}

function setShippingMethodError(responseText, statusText, xhr, $form)
{
    if(statusText=='timeout'){
        $('#shippingAddressError').html("<ul><li>There is some technical issue.</li></ul>");
		$('#shipToHomeErrorContainer').show();
	}else{

		redirectOnError();
    }
}

function setShippingMethodAndPopulatePayment(responseText, statusText, xhr, $form)  {
    trackPageLoadTime('Apply-Shipping-Response-Served');

	//added to handle the session expire redirection.
	if (responseText.redirectURL) {
		window.location.href = responseText.redirectURL + '?sessionExpired=true';
		return;
	}
	if(responseText.errorArray != undefined && null != responseText.errorArray && null != responseText.errorArray.addShippingErrors)	
    {
		$('#shipToHomeErrorContainer').show();
		var map = responseText.errorArray.addShippingErrors;
		var errorHtml = '';
		
		for(var i in map) {
			key = map[i].propertyName;
			value = map[i].errorMessage;
			if(key == "keyOCSEmptyDeliveryDate"){
				
				errorHtml+=value;
				if(document.getElementById('truckableItemServerErrors') != undefined && document.getElementById('truckableItemServerErrors') != null) {
					document.getElementById('truckableItemServerErrors').innerHTML=errorHtml;
				}
				if ($('#deliverydateInfo') && $('#deliverydateInfo').html() != '' && $('#deliverydateInfo').html() != null
						&& document.getElementById('isManualDate').innerHTML == 'true') {
					showErrorMessage("manualReselectError");
				} else if(document.getElementById('isManualDate').innerHTML != 'true' && $('#reselectInfo').is(":visible")){
					showErrorMessage("reselectInfo");
				} else {
				$('#truckableItemServerErrorContainer').show();	
				}
			}else if(key == "phoneNumber"){
				$('#invalidPhone').show();
				$('#largeAppAltContact').css("border", "1px solid #cc0000");
				$('#invalidPhone').css("color", "#cc0000");
			}
			else{
				var valueText = $('<textarea />').html(value).text();
				$('#savedShippingAddressError').html(valueText);
				$('#savedShippingAddressErrorContainer').show();
				
				value = "<li>"+value+"</li>";
				errorHtml+=value;
				if (null != document.getElementById('ServerSideErrorMessage0')) {
				document.getElementById('ServerSideErrorMessage0').innerHTML = errorHtml;
				}
				$("#errorMessageId").show();
				$('#shippingMethodsId').hide();
				$('#shippingSummary').hide();					
			}
		};
				
    } else {
    	loadPaymentPanelForShipToHome(responseText);  
    }
}

function loadPaymentPanelForShipToHome(responseText){
	var completedPanels = responseText.completedPanels;
	//updateProgressBar(responseText.currentPanel, completedPanels);
	if (responseText.paymentHtml != null) {
		$('#paymentPanel').html(formatHTML(responseText.paymentHtml));
		setTimeout(function(){
			initiateCardCorousel();
			$('#shippingPanel').hide();
			$('#paymentPanel').show();
			scrollToTop();
		}, 500);		
		//$('#emptyShippingMethod').css("display","none");
		coreMetricsPart("Payment", "JCP|Checkout");
		handleDeliveryDateReselectCoreMetrics();
	} else if (responseText.reviewHtml != null) {
		$('#paymentPanel').hide();
		$('#shippingPanel').hide();
		$('#orderReview').html(formatHTML(responseText.reviewHtml)).show();
		scrollToTop();
	}
	if (responseText.pricingSummaryHtml != null) {
		$('#shippingPanel').hide();
		$('#checkoutPricingSummaryDiv .holder').html(formatHTML(responseText.pricingSummaryHtml));
		$('a.helpIcon').tooltip({
    		overrideClass: "helpIcontip"
    	});
	}
	if (responseText.checkoutStepsHtml != null) {
	    var checkoutStepsHtml = $('<textarea />').html(responseText.checkoutStepsHtml).text();
	    $('#checkoutProgressBarDiv').html(checkoutStepsHtml);
	}
}

function isEmptyObj (obj) {
	var keyArr = Object.keys(obj), counter = 0;
	for(var i = 0; i < keyArr.length; i++) {
		if(includeLargeAppItem == 'true') {
			if(keyArr[i] == "zipCode") continue;
		}
		if(obj[keyArr[i]].toString().length > 0) { counter++;}
		if(counter > 0) return false;
	}
	return (counter <= 0);
}
function validateShipToHomeForm () {
	if (document.getElementById('selectShippingCountry').value == 'United States') {
		if(!isEmptyObj(submittedShippingAddress)) {
       if( $('#streetAddress1').val() != submittedShippingAddress.streetAddress1 ||
		   $('#streetAddress2').val() != submittedShippingAddress.streetAddress2 ||
		   $('#cityName').val() != submittedShippingAddress.city  ||
		   $('#zipCode').val() != submittedShippingAddress.zipCode ||
		   $('#selectState').find(":selected").val() != submittedShippingAddress.state) {
    	   if ($("form.jcp-form").valid()) {
    		   getValidShippingMethods();
    	   }
    	   else {
    		   var errorDescription = "";
    		   if($('#firstName').val() == "") {
    			   errorDescription = errorDescription +"Enter a first name"+"|";
    		   }
    		   
    		   if($('#lastName').val() == "") {
    			   errorDescription = errorDescription +"Enter a last name"+"|";
    		   }
    		   
    		   if($('#zipCode').val() == "") {
    			   errorDescription = errorDescription +"Enter a valid ZIP code"+"|";
    		   }
    		   
    		   if($('#cityName').val() == "") {
    			   errorDescription = errorDescription +"Enter a valid city"+"|";
    		   }
    		   
    		   if($('#selectState').val() == "") {
    			   errorDescription = errorDescription +"select a state"+"|";
    		   }
    		   
    		   if($('#streetAddress1').val() == "") {
    			   errorDescription = errorDescription +"Enter a valid street address"+"|";
    		   }
    		   
    		   if($('#emailId').val() == "") {
    			   errorDescription = errorDescription +"Enter an email"+"|";
    		   }
    		   
    		   if($('#phoneNumber').val() == "") {
    			   errorDescription = errorDescription +"Enter a valid phone number"+"|";
    		   }
    		   
    		   if($("#pickupfirstName").val() == "") {
    			   errorDescription = errorDescription +"Enter the contact information of the pickup person"+"|";
    		   }
    		   
    		   if($("#pickuplastName").val() == "") {
    			   errorDescription = errorDescription +"Enter the contact information of the pickup person"+"|";
    		   }
    		   
    		   if(($("#altPhoneNumber").val() == "" || $('#pickupphone').val() == "")) {
    			   errorDescription = errorDescription +"Enter the contact information of the pickup person"+"|";
    		   }
    		   
    		   if($('#billingPhoneNumber').val() == "") {
    			   errorDescription = errorDescription +"Enter a valid billing phone number|";
    		   }
    		   
    		   if(typeof pageDataTracker != "undefined") {
	 			  if(typeof errorDescription != "undefined") {
	 				  errorDescription = errorDescription.substring(0,errorDescription.length-1);
	 				 var omniString = "ShippingAddressValidation"+","+errorDescription+","+"ShippingAddressValidation"+","+","+","+",";
	 	 			   pageDataTracker.trackAnalyticsEvent ("formError", omniString);
	 			  }
	 			   
	 		   }
    		   
    	   }
    	   
       }
    }
} 
}

function onSelectChange(shippingGroupSelected) {
	submittedShippingAddress = {};
	$("#addShippingForm input[type='text']").removeClass("valid-input").removeClass("invalid-input").val("");
	$('#militaryselectState,#militaryType,#selectState').val('');
	removeShippingErrorContainer();
	removeSavedAddressErrorContainer();
	if(shippingGroupSelected != 'true') {
		displayEmptyShippingMethod();	
		$("#shippingAddress_continue").hide();
	}
	detachShippingFormEventHandlers();
	attachShippingFormEventHandlers();
	//var selectVal=$('#selectShippingCountry').find(":selected").val();
	var selectVal=$('#selectShippingCountry').val();
	if(undefined != $("#phoneNumberLength").val() && $("#phoneNumberLength").val()!= "") {
		var phoneNumberLength=$("#phoneNumberLength").val();
	}
	if(undefined != $('#internationalPhoneNumberLength').val() && $('#internationalPhoneNumberLength').val()!= "") {
		var internationalPhoneNumberLength=document.getElementById("internationalPhoneNumberLength").value;
	}
	switch(selectVal){
	case 'United States':
		usShipAddress();
		$('#phoneNumber').attr('maxlength',phoneNumberLength);		
		break;
	case 'AP':
		militaryShipAddress();
		$('#phoneNumber').attr('maxlength',internationalPhoneNumberLength);
		break;
	default:
		usShipAddress();
		$('#phoneNumber').attr('maxlength',phoneNumberLength);		
		break;
	}
	
}

function onEditAddressSelectChange() {
	submittedShippingAddress = {};
	$("#editShippingAddressForm input[type='text']").removeClass("valid-input").removeClass("invalid-input").val("");
	$('#editMilitaryselectState,#editMilitaryType,#editSelectState').val('');
	removeSavedAddressErrorContainer();
	removeShippingErrorContainer();
	displayEmptyShippingMethod();
	$("#shippingAddress_continue").hide();
	detachShippingFormEventHandlers();
	attachShippingFormEventHandlers();
	//var selectVal=$('#selectShippingCountry').find(":selected").val();
	var selectVal=$('#editSelectShippingCountry').val();
	if(undefined != $("#phoneNumberLength").val() && $("#phoneNumberLength").val()!= "") {
		var phoneNumberLength=$("#phoneNumberLength").val();
	}
	if(undefined != $('#internationalPhoneNumberLength').val() && $('#internationalPhoneNumberLength').val()!= "") {
		var internationalPhoneNumberLength=document.getElementById("internationalPhoneNumberLength").value;
	}
	switch(selectVal){
	case 'United States':
		usShipAddress();
		$('#editPhoneNumber').attr('maxlength',phoneNumberLength);		
		break;
	case 'AP':
		militaryShipAddress();
		$('#editPhoneNumber').attr('maxlength',internationalPhoneNumberLength);
		break;
	case 'APO/FPO/DPO':
		militaryShipAddress();
		$('#editPhoneNumber').attr('maxlength',internationalPhoneNumberLength);
		break;
	default:
		usShipAddress();
	   $('#editPhoneNumber').attr('maxlength',phoneNumberLength);		
		break;
	}
	
}

function removeShippingErrorContainer() {
	$('.dynamic_error_msgs').hide();
	$('.dynamic_error_msgs ul li').empty();
	$('#addShippingForm input, #addShippingForm label, #addShippingForm select').removeClass('error');
	$('#addAddressShippingForm input, #addAddressShippingForm label,#addAddressShippingForm select').removeClass('error');
	$('#editShippingAddressForm input, #editShippingAddressForm label, #editShippingAddressForm select').removeClass('error');
	$('div.error').remove();
	
}

function usShipAddress(){
	$('#usSpecific').show();
	$('#addUsSpecific').show();
	$('#editUsSpecific').show();
    $('#usSpecific :input').removeAttr('disabled');
    $('#addUsSpecific:input').removeAttr('disabled','disabled');
	$('#editUsSpecific:input').removeAttr('disabled','disabled');
	$('#militarySpecific').hide();
	$('#addMilitarySpecific').hide();
	$('#editMilitarySpecific').hide();
	$('#militarySpecific :input').attr('disabled','disabled');	
	$('#addMilitarySpecific :input').attr('disabled','disabled');	
	$('#editMilitarySpecific :input').attr('disabled','disabled');
	$('#phoneNumber').attr('placeholder','(555) 555-5555');

}

function militaryShipAddress(){
	$('#usSpecific').hide();
	$('#addUsSpecific').hide();
	$('#editUsSpecific').hide();
	$('#usSpecific:input').attr('disabled','disabled');
	$('#addUsSpecific:input').attr('disabled','disabled');
	$('#editUsSpecific:input').attr('disabled','disabled');
	$('#phoneNumber').attr('placeholder','');
	$('#militarySpecific').show();
	$('#addMilitarySpecific').show();
	$('#editMilitarySpecific').show();
    $('#militarySpecific :input').removeAttr('disabled');	
    $('#addMilitarySpecific :input').removeAttr('disabled');	
    $('#editMilitarySpecific :input').removeAttr('disabled');	
}

function submitShipToHomeAddressFormOnContinue() {
	if($("form.jcp-form").valid()) {
		$("#emptyShippingMethod").html("Retrieving shipping methods...");
		$(".subhead .loading-spinner").css("display", "inline-block");
		getValidShippingMethods();		
	}else {
		var errorDescription = "";
		   if($('#firstName').val() == "") {
			   errorDescription = errorDescription +"Enter a first name"+"|";
		   }
		   
		   if($('#lastName').val() == "") {
			   errorDescription = errorDescription +"Enter a last name"+"|";
		   }
		   
		   if($('#zipCode').val() == "") {
			   errorDescription = errorDescription +"Enter a valid ZIP code"+"|";
		   }
		   
		   if($('#cityName').val() == "") {
			   errorDescription = errorDescription +"Enter a valid city"+"|";
		   }
		   
		   if($('#selectState').val() == "") {
			   errorDescription = errorDescription +"select a state"+"|";
		   }
		   
		   if($('#streetAddress1').val() == "") {
			   errorDescription = errorDescription +"Enter a valid street address"+"|";
		   }
		   
		   if($('#emailId').val() == "") {
			   errorDescription = errorDescription +"Enter an email"+"|";
		   }
		   
		   if($('#phoneNumber').val() == "") {
			   errorDescription = errorDescription +"Enter a valid phone number"+"|";
		   }
		   
		   if($("#pickupfirstName").val() == "") {
			   errorDescription = errorDescription +"Enter the contact information of the pickup person"+"|";
		   }
		   
		   if($("#pickuplastName").val() == "") {
			   errorDescription = errorDescription +"Enter the contact information of the pickup person"+"|";
		   }
		   
		   if(($("#altPhoneNumber").val() == "" || $('#pickupphone').val() == "")) {
			   errorDescription = errorDescription +"Enter the contact information of the pickup person"+"|";
		   }
		   
		   if($('#billingPhoneNumber').val() == "") {
			   errorDescription = errorDescription +"Enter a valid billing phone number|";
		   }
		   
		   if(typeof pageDataTracker != "undefined") {
 			  if(typeof errorDescription != "undefined") {
 				  errorDescription = errorDescription.substring(0,errorDescription.length-1);
 				 var omniString = "ShippingAddressValidation"+","+errorDescription+","+"ShippingAddressValidation"+","+","+","+",";
 	 			   pageDataTracker.trackAnalyticsEvent ("formError", omniString);
 			  }
 			   
 		   }
	}
}

function displayEmptyShippingMethod() {
	$('#showShippingMethod').css("display","none");
	$('#emptyShippingMethod').show();
	$("#shipping_submit").attr('disabled','disabled');	
}
function detachShippingFormEventHandlers() {
	$("#streetAddress1, #streetAddress2, #zipCode, #cityName, #phoneNumber").unbind("blur");
	$("#selectState").unbind("change");
	document.getElementById("zipCode").removeEventListener('modify', validateShipToHomeForm, false);
}

function attachShippingFormEventHandlers () {
	$("#streetAddress1, #streetAddress2, #zipCode, #cityName").blur(function (e) {
		//console.log(e);
		setTimeout(function () {
			if($(e.target).attr("id") == "streetAddress1" && clicky != null &&
					(clicky.hasClass("pac-container") || clicky.parents("pac-container").length > 0)) {
				return false;
			} else {
				validateShipToHomeForm();
			}
		});
	});
	$("#selectState").change(function (e) {
		validateShipToHomeForm();
	});
	$("#phoneNumber").blur(function (e) {
		validatePhoneNumber(e);
		if ($("form.jcp-form").valid()){
			$("#emptyShippingMethod").html("Retrieving shipping methods...");
			$(".subhead .loading-spinner").css("display", "inline-block");
			getValidShippingMethods();
		} else {
			var errorDescription = "";
 		   if($('#firstName').val() == "") {
 			   errorDescription = errorDescription +"Enter a first name"+"|";
 		   }
 		   
 		   if($('#lastName').val() == "") {
 			   errorDescription = errorDescription +"Enter a last name"+"|";
 		   }
 		   
 		   if($('#zipCode').val() == "") {
 			   errorDescription = errorDescription +"Enter a valid ZIP code"+"|";
 		   }
 		   
 		   if($('#cityName').val() == "") {
 			   errorDescription = errorDescription +"Enter a valid city"+"|";
 		   }
 		   
 		   if($('#selectState').val() == "") {
 			   errorDescription = errorDescription +"select a state"+"|";
 		   }
 		   
 		   if($('#streetAddress1').val() == "") {
 			   errorDescription = errorDescription +"Enter a valid street address"+"|";
 		   }
 		   
 		   if($('#emailId').val() == "") {
 			   errorDescription = errorDescription +"Enter an email"+"|";
 		   }
 		   
 		   if($('#phoneNumber').val() == "") {
 			   errorDescription = errorDescription +"Enter a valid phone number"+"|";
 		   }
 		   
 		   if($("#pickupfirstName").val() == "") {
 			   errorDescription = errorDescription +"Enter the contact information of the pickup person"+"|";
 		   }
 		   
 		   if($("#pickuplastName").val() == "") {
 			   errorDescription = errorDescription +"Enter the contact information of the pickup person"+"|";
 		   }
 		   
 		   if(($("#altPhoneNumber").val() == "" || $('#pickupphone').val() == "")) {
 			   errorDescription = errorDescription +"Enter the contact information of the pickup person"+"|";
 		   }
 		   
 		   if($('#billingPhoneNumber').val() == "") {
 			   errorDescription = errorDescription +"Enter a valid billing phone number"+"|";
 		   }
 		   
 		   if(typeof pageDataTracker != "undefined") {
 			  if(typeof errorDescription != "undefined") {
 				  errorDescription = errorDescription.substring(0,errorDescription.length-1);
 				 var omniString = "ShippingAddressValidation"+","+errorDescription+","+"ShippingAddressValidation"+","+","+","+",";
 	 			   pageDataTracker.trackAnalyticsEvent ("formError", omniString);
 			  }
 			   
 		   }
		}
	});
	createEvent ('modify');
	// Listen for the event.
	document.getElementById("zipCode").addEventListener('modify', validateShipToHomeForm, false);
}


/*
Method to be called when user removed items ineligible to be shipped.
*/
function removeItems()
{
	var options = {
       success: removeItemsFromOrder,
		error: removeItemsFromOrder,
		timeout: ajaxTimeOut
	};
	$('#removeItemsFromOrder').ajaxSubmit(options);
	
	trackPageLoadTime('Remove_Ineligible_To_Ship_Items');

}

function removeItemsFromOrder(responseText, statusText, xhr, $form)  {
   //var addressId = $("#shippingGroupAddress").val();
	//setCookie("addressId",addressId,1);
	//var regButAddFlag = getRegistryAddressFlag();
	var regButAddFlag;
	window.location.href="/dotcom/jsp/checkout/secure/checkout.jsp?panel=Shipping&method=Home&regButAddFlag="+regButAddFlag;
}

function getRegistryAddressFlag() {
	var regButAddFlag = false;
	var registryDiv  = document.getElementById('giftRegistryAddShippingFrag');
	if (registryDiv != undefined && registryDiv != null) {
		var registryDivDisplay = document.getElementById('giftRegistryAddShippingFrag').style.display;
		if (registryDivDisplay == 'block') {
			regButAddFlag = true;
		}
	}
	return regButAddFlag;
}

function changeShippingAddress()
{
	if(shipToCountryId == null || shipToCountryId == 'US'){
		$('#shipToHomeErrorContainer').hide();
		$('#savedShippingAddressErrorContainer').hide();
	} else {
        var changeShippingId = $('#changeShippingAddress');
		openContextChooser("/dotcom/jsp/checkout/contextChooser.jsp?pageName=/dotcom/jsp/checkout/secure/checkout.jsp");
	}

}

function openContextChooser(url){
	$.fn.colorbox({
        href:url,
		scrolling: false,
		overlayClose: false,
		escKey: false,
        close: function() {
            var img = "<img id='cboxCloseImg' src='/dotcom/images/modal_close.gif' alt='close' title='close' />";
				return $(img);
		}});
}
// Defect 25913 - Start
// Method moved to simplifycheckout.js since it is used from Shipping and Order review pages.
//function generateModalURL(){
//	document.getElementById('truckItemModalLink').href="/dotcom/jsp/browse/truckAndWhiteGloveDelivery/truckDeliveryModalForWebId.jsp?pageId="+document.getElementById('pageId').value;
//	$("#truckItemModalLink").trigger('click');
//}
// Defect 25913 - End
function selectShipToStore() {
	$("input[name='shippingLocation']").removeClass('ng-valid-parse');
	$(this).addClass('ng-valid-parse');

     $("#sth-div").removeClass('sth ng-enter');
     $("#sth-div").addClass('sth ng-leave');
     $("#sth-div").attr("ng-checked", "false");
    
     $("#sts-div").removeClass('sts ng-leave');
     $("#sts-div").addClass('sts ng-enter');
     $("#sts-div").attr("ng-checked", "true");

     $("#sth-div").hide();
     $("#sts-div").show();
     $("#sts_submit").show();
     $("#shipping_submit").hide();
     setGiftWrapCaption('Select a store');
}

function selectShipToHome() {
	$("input[name='shippingLocation']").removeClass('ng-valid-parse');
 	$(this).addClass('ng-valid-parse');

    $("#sth-div").removeClass('sth ng-leave');
    $("#sth-div").addClass('sth ng-enter');

    $("#sts-div").removeClass('sts ng-enter');
    $("#sts-div").addClass('sts ng-leave');
    $("#sts-div").hide();	
    $("#sth-div").show();	
    $("#shipping_submit").show();
    $("#sts_submit").hide();
    setGiftWrapCaption('Enter address');
}

function setGiftWrapCaption(caption) {
	if(!$("#selectGiftWrap").is(":visible")) {
		$("#giftWrapCaption").text(caption);
		/*
		var shippingGroup = $('input[name=shippingLocation]:checked').val() ;
		if(shippingGroup == 'shipToHome') {
			$("#giftWrapCaption").text('Enter address');
		} else {
			$("#giftWrapCaption").text('Select a store');
		}
		*/
	}
}

function shippingGroupChanged(flag) {
	removeSavedAddressErrorContainer();
	var selectedAddress = $('.saved-address-blk-active').first().prev().html();	
	var selectedAddressId = getCookie("addressId");
	var grId = $('#giftRegistryId').val();
	if (selectedAddress == null && selectedAddressId != null && selectedAddressId != "") {
		selectedAddress = selectedAddressId;
		eraseCookie("addressId");
	}
	if (grId != '' && grId != "undefined" && grId != null && (selectedAddress == '' || selectedAddress ==null)) {
		selectedAddress = grId;
	}
	
	if (typeof selectedAddress != "undefined" && selectedAddress != ""
			&& selectedAddress != null) {
		$("#shippingGroupAddress").val(selectedAddress);
		$('.savedAddressBlk').removeClass('saved-address-blk-active');
		$('#saved-address-blk-' + selectedAddress).addClass('saved-address-blk-active');
		
	}
	
	var selectedShippingAddress = $("#shippingGroupAddress").val();
	setCookie("addressId", selectedShippingAddress, 1);
	
	$('#shippingFirstTimeSelectDate').val('true');
	
	var options = {
		success : handleShippingGroupChangeSuccess,
		error : shippingGroupChangedError,
		timeout : ajaxTimeOut
	};
	$('#checkout_shipping_hot').ajaxSubmit(options);
	
	trackPageLoadTime('Shipping-Group-Change-Requested');
	//showSpinner('container');
}

function handleShippingGroupChangeSuccess (responseText, statusText, xhr, $form) {
	if (responseText.errorExist == "true") {
		if(responseText.errorArray != undefined && null != responseText.errorArray && null != responseText.errorArray.shippingGroupChangeErrors)	
	    { 
			var map = responseText.errorArray.shippingGroupChangeErrors;
			var errorHtml = '';
		    var temp = new Array();
		    var commerceItems = new Array();
			var removablecommerceItem = new Array();
			var removablecommerceItemValue = '';
			var itemsInEligible = false;
			var j=0;
			for(var i in map) {
				key = map[i].propertyName;
				value = map[i].errorMessage;
				
				if (value.indexOf("&amp;gt") > -1) {
					var errorMsg = $('<textarea />').html(value).text();
					var errorMsgText = $('<textarea />').html(errorMsg).text();
					value = errorMsgText;
				}			
				value = removeEncodedSpecialChar(value);
				
				if (key == 'keyLargeAppZipCodeMismatchError') {
					$('#largeAppText').css({'color': 'rgb(204, 0, 0)'})
					showErrorMessage("largeAppZipInfoAddAddress");
				} else if(key=="keyItemsMovedToShipTo" || key == "keyOrderHasAllOutOfStockItems" 
					|| key == "keyOrderHasOutOfStockItem") {
					openCookieDisableMessage('/dotcom/jsp/checkout/secure/checkoutsimplified/itemsOutOfStockMessage.jsp?oosMsgKey='+key, key);
				} else if (value != null && value != '') {
					// Code to check the items eligible to be shipped
					// Splitting to get the commerce items
					temp = value.split('$');

		            for(i=0;i<temp.length;i++){
						if(temp[i].substring(0,2)=="ci") {
						removablecommerceItem[j] = temp[i];
						removablecommerceItemValue='$'+removablecommerceItem[j]+'$';
						j++;
						value=value.replace(removablecommerceItemValue,"");
						itemsInEligible = true;
						}
		            }
		            if(itemsInEligible) {
		            document.getElementById('removablecommerceItem').value=removablecommerceItem;
		            }
		            
					do{
						value = value.replace("<li></li>","");
					}while(value.indexOf("<li></li>") > 0);

			        if (value.indexOf("removeItems()") == -1) {
						$('#savedShippingAddressError').html("<ul>"+value+"</ul>");
						$('#savedShippingAddressErrorContainer').find('img#savedShippingAddressErrorImg').css('margin-bottom','0px');
						if ($form.selector =='#giftRegistry_shipping') {
							$("#errorMessageId").hide();
							$('#add-address-ship-other-country-extra').show();				
						}
					} else {
						if(value.startsWith("<li>")){
							value = value.replace("<li>","");
							value = value.substring(0,(value.length-5));
						}
						$('#savedShippingAddressError').html("<ul>"+value+"</ul>");
						$("#errorMessageId").hide();
						$("#errorMessageBlkAddr").show();
					}
			        $('#savedShippingAddressErrorContainer').show();
			        
				} else {
					 $('#savedShippingAddressError').html("There is some technical issue.");
					 $('#savedShippingAddressErrorContainer').show();
				}
			}
			
			scrollToTop();
	    }
	} else {		
		$("#shipping_submit").removeAttr("disabled");
		$("#shippingAddress_continue").hide();
		detachShippingFormEventHandlers();	
		shippingFrag = false;
		var grId = $('#giftRegistryId').val();
		var displayGRFrag = false;
		var selectedAddressId = getCookie("addressId");
		if (grId != '' && grId != "undefined" && grId != null) {
			displayGRFrag = true;
		}
		if (responseText.shipToHomeAddress != undefined && null != responseText.shipToHomeAddress && displayGRFrag) {
			/*
			 * Display the name from GR address. 
			 * if(typeof responseText.middleName != "undefined" && responseText.middleName!= "" 
				&& responseText.shipToHomeAddress.middleName != null &&  jQuery.trim(responseText.shipToHomeAddress.middleName) != "undefined" ) {
				document.getElementById('giftregAddressName').innerHTML = responseText.shipToHomeAddress.firstName + " " + responseText.shipToHomeAddress.middleName + " " + responseText.shipToHomeAddress.lastName;
			} else {
				document.getElementById('giftregAddressName').innerHTML = responseText.shipToHomeAddress.firstName + " " + responseText.shipToHomeAddress.lastName;
			}*/				
			if ($form.selector =='#giftRegistry_shipping') {
				$('#add-address-ship-other-country-extra').show();				
			}
			if (profileValue == 'false' && (selectedAddressId == grId)){
				$('#gift-registry-address-blk').addClass('savedAddressBlk');
				$('#gift-registry-address-blk').addClass('saved-address-blk-active');
			}
	      }	
		if (responseText.shippingMethodHtml != null) {
			var shippingMethodText = $('<textarea />').html(responseText.shippingMethodHtml).text();
			$('#showShippingMethod').html(shippingMethodText);			
			$('#emptyShippingMethod').hide();
			$('#showShippingMethod').hide().fadeIn();
			shippingMethodsFetched = true;
		}
		if (responseText.pricingSummaryHtml != null) {
			$('#checkoutPricingSummaryDiv .holder').html(formatHTML(responseText.pricingSummaryHtml));
			$('a.helpIcon').tooltip({
	    		overrideClass: "helpIcontip"
	    	});
		}
		if (responseText.checkoutStepsHtml != null) {
		    var checkoutStepsHtml = $('<textarea />').html(responseText.checkoutStepsHtml).text();
		    $('#checkoutProgressBarDiv').html(checkoutStepsHtml);
		}
		
		if (responseText.shippingGroupsHtml != null) {
			var shippingGroupsText = $('<textarea />').html(responseText.shippingGroupsHtml).text();
			$('#saved-shipping-groups').html(shippingGroupsText);
			handleLoadExtraShippingBlock();
		}
		
		if(includeLargeAppItem == 'true') {
			$('#showShippingMethod').hide();
			$('#largeAppText').css({'color': '#2060ca'})
			showInfoMessage("largeAppZipInfoAddAddress");
		}
	    $('#addressStandardCount').val('');
	    showHideGiftWrap();
	    
	    coreMetricsPart("Shipping", "JCP|Checkout");	
	}
	
	hideSpinner();
}

function showErrorMessage(divid) {
	var self = $('#'+divid),
		icon = self.find("img");
	self.css("border", "1px solid #cc0000");
	self.css("background", "#fbeaeb");

	icon.attr("src","/dotcom/images/error_icon.svg");
	self.css("color", "#cc0000");
}

function populateSavedAddress(responseText, statusText, xhr, $form)
{
	trackPageLoadTime('Shipping-Group-Change-Response-Received');
    try{
		if (responseText.redirectURL) {	
			window.location.href = responseText.redirectURL + '?sessionExpired=true';
			return;
		}
    }catch(e){
    	redirectOnError();
    }
    
    hideSpinner();
}

function shippingGroupChangedError(responseText, statusText, xhr, $form)
{
    if(statusText=='timeout'){
        $('#savedShippingAddressError').html("There is some technical issue.");
		$('#savedShippingAddressErrorContainer').show();
		hideSpinner();
	}else{
		//redirectOnError()
		var enableShippingAjaxError = false;
		if (document.getElementById('enable2016S8DPXVI5958ShippingOops') != undefined && document.getElementById('enable2016S8DPXVI5958ShippingOops') != null) {
			enableShippingAjaxError = document.getElementById('enable2016S8DPXVI5958ShippingOops').value;
		}
		if(enableShippingAjaxError == 'true') {
			window.location = '/dotcom/jsp/cart/viewShoppingBag.jsp?checkoutError=true';
		} else {
			redirectOnError();
		}

    }
}

function closeShippingAddress () {
	removeShippingErrorContainer();
	removeSavedAddressErrorContainer ();
	$('.savedAddressBlk').removeClass('saved-address-blk-selected');
	$('.savedAddressBlk').removeClass('saved-address-blk-active');
	$('.add-new-address-blk').removeClass('saved-address-blk-active');
	$('#edit-shipping-address-blk').hide();
	$('#add-shipping-address-blk').hide();
	
	var selectedSeqNum = $('#original-selected-addres-seq-num').html();
	$('#saved-address-blk-' + selectedSeqNum).addClass('saved-address-blk-active');
}

function addShippingAddress(event, form) {
	event.preventDefault();
	bindValidatorRules("#addAddressShippingForm");
	if (typeof form != "undefined"  && $(form).valid()){
		$(form).submit(function(event){event.preventDefault();});
		handleAddShippingAddressFormSubmit();
	}
}
function handleAddShippingAddressFormSubmit() {
	removeShippingErrorContainer();
	removeSavedAddressErrorContainer ();	
	
	if (document.getElementById('phoneNumber').value != "") {
        document.getElementById('hdnPhoneNumber').value = document.getElementById('phoneNumber').value;
    }else{
    	document.getElementById('hdnPhoneNumber').value ='';
    }
    document.getElementById('hdnCountryName').value = document.getElementById('selectShippingCountry').value;
    if (document.getElementById('selectShippingCountry').value == 'United States') {
    	// need to disable the AP country related fields
    	$('#addMilitarySpecific :input').attr('disabled','disabled');        	
        if (document.getElementById('streetAddress1').value != "") {
            document.getElementById('hdnStreetAddress1').value = document.getElementById('streetAddress1').value;
        }else{
        	 document.getElementById('hdnStreetAddress1').value='';
        }
        document.getElementById('hdnStreetAddress2').value = document.getElementById('streetAddress2').value;
        if (document.getElementById('cityName').value != "") {
            document.getElementById('hdnCityName').value = document.getElementById('cityName').value;
        }
        if (document.getElementById('zipCode').value != "") {
            document.getElementById('hdnZipCode').value = document.getElementById('zipCode').value;
        }
        var state = $('#selectState').val();
        document.getElementById('hdnState').value = state;
    } else if (document.getElementById('selectShippingCountry').value == 'AP') {
    	// need to disable the US country related fields
    	$('#addUsSpecific:input').attr('disabled','disabled');
        if (document.getElementById('militarystreetAddress1').value != "") {
            document.getElementById('hdnStreetAddress1').value = document.getElementById('militarystreetAddress1').value;
        }
        document.getElementById('hdnStreetAddress2').value = document.getElementById('militarystreetAddress2').value;
        if (document.getElementById('militaryzipCode').value != "") {
            document.getElementById('hdnZipCode').value = document.getElementById('militaryzipCode').value;
        }

        var state = $('#militaryselectState').val();
        document.getElementById('hdnState').value = $('#militaryselectState').val();
        document.getElementById('hdnCityName').value = ''
    } else {
    	//document.getElementById('hdnState').value = $('#selectState').find(":selected").val();
    	 var state = $('#selectState').val();
    	 document.getElementById('hdnState').value = state;
    }
    
   
    
    var options = {
    		success : pouplateSavedShippingAddresses,
    		error : pouplateSavedShippingAddressesError,
    		timeout : ajaxTimeOut
    	};
    	//$('#checkout_add_shipping_hot').ajaxSubmit(options);
    $('#addAddressShippingForm').ajaxSubmit(options);
    //showSpinner('container');
}

function pouplateSavedShippingAddresses (responseText, statusText, xhr, $form) {
	if (responseText.redirectURL) {
		window.location.href = responseText.redirectURL + '?sessionExpired=true';
		return;
	}
	
	if(responseText.errorArray != undefined && null != responseText.errorArray && null != responseText.errorArray.addShippingErrors)
    {
		handleShippingFormError(responseText);
		hideSpinner();
    } else {
		if (responseText.shippingGroupsHtml != null) {
			var shippingGroupsText = $('<textarea />').html(responseText.shippingGroupsHtml).text();
			$('#saved-shipping-groups').html(shippingGroupsText);
			$('#add-shipping-address-blk').hide();
			
			initLoadSavedShippingAddress();
			hideSpinner();
			
			setTimeout(function(){shippingGroupChanged();}, 500);			
		} else {
			hideSpinner();
		}
    }
	
}

function handleShippingFormError(responseText) {
	var temp = new Array();
    var commerceItems = new Array();
	var removablecommerceItem = new Array();
	var removablecommerceItemValue = '';
	var itemsInEligible = false;
	var j=0;
	
	$("#streetAddress1").removeClass("input_error_txt");
		
	var errorHtml = '';
	var map = responseText.errorArray.addShippingErrors;
	var orderHasOOSItem = false;
	var errorsShownInline = false;
	$('#firstNameError').empty();
	$("#lastNameError").empty();
	var isNonDeliveryDateErrorPresent = false;
	
	for(var i in map) {
		key = map[i].propertyName;
		value = map[i].errorMessage;
		var errorMsg = $('<textarea />').html(value).text();
		var errorMsgText = $('<textarea />').html(errorMsg).text();
		value = errorMsgText;
				
		if(isCDIStdErrKey(key)){
            var standardCount = $('#addressStandardCount').val();
            standardCount++;
            $('#addressStandardCount').val(standardCount);
		}
		if(coremetricsTagEnabled) { 
			createCoremetricsElementTag(key+'.invalid','Error_Checkout_Shipping');
		}
		
		if(key == "firstName"){
			$('#firstName').addClass("error");
			$("#firstNameError").css("display","block");
			$("<div class='error'>"+value+"</div>").insertBefore( 'label[for="firstName"]' );
			isNonDeliveryDateErrorPresent = true;
		}

		if(key == "lastName"){
			
			$('#lastName').addClass("error");
			$("#lastNameError").css("display","block");
			$("<div class='error'>"+value+"</div>").insertBefore( 'label[for="lastName"]' );
			isNonDeliveryDateErrorPresent = true;
		}

        if(key == "phoneNumber"){
        
            $('#phoneNumber').addClass("error");
            if ($('label[for="phoneNumber"]').prev().hasClass('error')) {
            	$('label[for="phoneNumber"]').prev().html(value);                	
            } else {
            	$("<div class='error'>" + value + "</div>").insertBefore( 'label[for="phoneNumber"]' );
            }
            if (!$('label[for="altPhoneNumber"]').prev().hasClass('error')) {
            	$("<div class='error'>&nbsp;</div>").insertBefore( 'label[for="altPhoneNumber"]' );
            	$('input[name="phoneNumber"]').css("display", "initial");
            }
			errorsShownInline = true;
			isNonDeliveryDateErrorPresent = true;
		}
       
		if(key == "postalCode"){
            $("#postalCode").addClass("error");
            $("#militaryZipCode").addClass("error");
            $("#zipCode").addClass("error");
            $("#editZipCode").addClass("error");
            $("#cityName").addClass("error");
            $("#editCityName").addClass("error");
            $("#selectState").addClass("error");
            $('#editSelectState').addClass("error");
            $("#zipCityError").html( "<div class='error'>"+value+"</div>" );
            $("#milZipCityError").html( "<div class='error'>"+value+"</div>" );
            if($('#zipCityError').length > 0)
			{
            	$('#shippingPageForGuest_autoPopulateError').empty();
            	$('#shippingPageForGuest_autoPopulateError').removeClass('error');
			}
            if ($('label[for="zipCode"]').prev().hasClass('error')) {
            	$('label[for="zipCode"]').prev().html(value);                	
            } else {
            	$("<div class='error'>" + value + "</div>").insertBefore( 'label[for="zipCode"]' );
            }
            if (!$('label[for="cityName"]').prev().hasClass('error')) {
            	$("<div class='error'>&nbsp;</div>").insertBefore( 'label[for="cityName"]' );
            }
            if (!$('label[for="selectState"]').prev().hasClass('error')) {
            	$("<div class='error'>&nbsp;</div>").insertBefore( 'label[for="selectState"]' );
            }
            if (!$('label[for="militarycityName"]').prev().hasClass('error')) {
            	$("<div class='error'>&nbsp;</div>").insertBefore( 'label[for="militarycityName"]' );
            }
            if (!$('label[for="militaryselectState"]').prev().hasClass('error')) {
            	$("<div class='error'>&nbsp;</div>").insertBefore( 'label[for="militaryselectState"]' );
            }
            isNonDeliveryDateErrorPresent = true;
            errorsShownInline = true;
		}
		
		if(value.indexOf("matching city for that ZIP code") != -1){
			$("#cityName").addClass("error");
           	errorsShownInline = true;
			isNonDeliveryDateErrorPresent = true;
		}
		if(key == "address1"){
			
            $("#streetAddress1").addClass("error");
            $("#militarystreetAddress1").addClass("error");
            $("<div class='error'>"+value+"</div>").insertBefore( 'label[for="militarystreetAddress1"]' );	
            $("<div class='error'>"+value+"</div>").insertBefore( 'label[for="streetAddress1"]' );	
            errorsShownInline = true;
            isNonDeliveryDateErrorPresent = true;
		}

		if(key == "address2"){
            $("#streetAddress2").addClass("error");
            $("#militarystreetAddress2").addClass("error");
			
			isNonDeliveryDateErrorPresent = true;
		}
		
		if(key == "city"){
            $("#cityName").addClass("error");
            $("#militarycityName").addClass("error");
            if ($('label[for="cityName"]').prev().hasClass('error')) {
            	$('label[for="cityName"]').prev().html(value);
            } else {
            	$("<div class='error'>"+value+"</div>").insertBefore( 'label[for="cityName"]' );
            }
            if ($('label[for="militarycityName"]').prev().hasClass('error')) {
            	$('label[for="militarycityName"]').prev().html(value);
            } else {
            	$("<div class='error'>"+value+"</div>").insertBefore( 'label[for="militarycityName"]' );
            }
            
            if (!$('label[for="zipCityError"]').prev().hasClass('error')) {
            	$("<div class='error'>&nbsp;</div>").insertBefore( 'label[for="zipCityError"]' );
            } 
            if (!$('label[for="selectState"]').prev().hasClass('error')) {
            	$("<div class='error'>&nbsp;</div>").insertBefore( 'label[for="selectState"]' );
            }
            if (!$('label[for="militaryselectState"]').prev().hasClass('error')) {
            	$("<div class='error'>&nbsp;</div>").insertBefore( 'label[for="militaryselectState"]' );
            }
            if (!$('label[for="zipCode"]').prev().hasClass('error')) {
            	$("<div class='error'>&nbsp;</div>").insertBefore( 'label[for="zipCode"]' );
            }
            isNonDeliveryDateErrorPresent = true;
		}
		if(key == "state"){
            $("#selectState").addClass("error");
            if ($('label[for="selectState"]').prev().hasClass('error')) {
            	$('label[for="selectState"]').prev().html(value);
            } else {
            	$("<div class='error'>"+value+"</div>").insertBefore( 'label[for="selectState"]' );
            }
            if ($('label[for="militaryselectState"]').prev().hasClass('error')) {
            	$('label[for="militaryselectState"]').prev().html(value);
            } else {
            	$("<div class='error'>"+value+"</div>").insertBefore( 'label[for="militaryselectState"]' );
            }
            
            if (!$('label[for="zipCityError"]').prev().hasClass('error')) {
            	$("<div class='error'>&nbsp;</div>").insertBefore( 'label[for="zipCityError"]' );
            } 
            if (!$('label[for="cityName"]').prev().hasClass('error')) {
            	$("<div class='error'>&nbsp;</div>").insertBefore( 'label[for="cityName"]' );
            }
            if (!$('label[for="militarycityName"]').prev().hasClass('error')) {
            	$("<div class='error'>&nbsp;</div>").insertBefore( 'label[for="militarycityName"]' );
            }
            if (!$('label[for="militaryType"]').prev().hasClass('error')) {
            	$("<div class='error'>&nbsp;</div>").insertBefore( 'label[for="militaryType"]' );
            }
            if (!$('label[for="zipCode"]').prev().hasClass('error')) {
            	$("<div class='error'>&nbsp;</div>").insertBefore( 'label[for="zipCode"]' );
            }
            isNonDeliveryDateErrorPresent = true;
		}
		if (key == 'militaryAddressType') {
			if ($('label[for="militaryType"]').prev().hasClass('error')) {
            	$('label[for="militaryType"]').prev().html(value);
            } else {
            	$("<div class='error'>"+value+"</div>").insertBefore( 'label[for="militaryType"]' );
            }
			 if (!$('label[for="zipCityError"]').prev().hasClass('error')) {
	            	$("<div class='error'>&nbsp;</div>").insertBefore( 'label[for="zipCityError"]' );
            } 
            if (!$('label[for="cityName"]').prev().hasClass('error')) {
            	$("<div class='error'>&nbsp;</div>").insertBefore( 'label[for="cityName"]' );
            }
            if (!$('label[for="militarycityName"]').prev().hasClass('error')) {
            	$("<div class='error'>&nbsp;</div>").insertBefore( 'label[for="militarycityName"]' );
            }
            if (!$('label[for="selectState"]').prev().hasClass('error')) {
            	$("<div class='error'>&nbsp;</div>").insertBefore( 'label[for="selectState"]' );
            }
            if (!$('label[for="militaryselectState"]').prev().hasClass('error')) {
            	$("<div class='error'>&nbsp;</div>").insertBefore( 'label[for="militaryselectState"]' );
            }
            if (!$('label[for="zipCode"]').prev().hasClass('error')) {
            	$("<div class='error'>&nbsp;</div>").insertBefore( 'label[for="zipCode"]' );
            }
		}
		
		if(key == "keyOCSPostalCodeSuspended"){
            $("#cityName").addClass("error");
            $("#streetAddress1").addClass("error");
            $("#militarystreetAddress1").addClass("error");
            $("#militaryZipCode").addClass("error");
            $("#zipCode").addClass("error");
            $("#streetAddress2").addClass("error");
            $("#militarystreetAddress2").addClass("error");
			$("#selectState").addClass("error");
			isNonDeliveryDateErrorPresent = true;
		}		
		
		if(key=="keyItemsMovedToShipTo" || key == "keyOrderHasAllOutOfStockItems" 
			|| key == "keyOrderHasOutOfStockItem") {
			openCookieDisableMessage('/dotcom/jsp/checkout/secure/checkoutsimplified/itemsOutOfStockMessage.jsp?oosMsgKey='+key, key);
			isNonDeliveryDateErrorPresent = true;
			if (responseText.checkoutStepsHtml != null) {
				var Title = $('<textarea />').html(responseText.checkoutStepsHtml).text();
				$('#checkoutProgressBarDiv').html(Title);
				$('#pickup').hide();
				$('#shipto').show();
				$('#shipto').addClass("ng-enter");
			}
			if (responseText.shippingHtml != null) {
				$('#sameDayPickUpPanel').hide();
				var Title = $('<textarea />').html(responseText.shippingHtml).text();
				$('#shippingPanel').html(Title);
			}
		}
		
		// Code to check the items eligible to be shipped
		// Splitting to get the commerce items
		temp = value.split('$');

        for(i=0;i<temp.length;i++){
			if(temp[i].substring(0,2)=="ci") {
			removablecommerceItem[j] = temp[i];
			removablecommerceItemValue='$'+removablecommerceItem[j]+'$';
			j++;
			value=value.replace(removablecommerceItemValue,"");
			itemsInEligible = true;
		}
        }

		value = value.replace("?","&trade;");//Fix for trademark
		value = value.replace("\?","&trade;");//Fix for trademark

		var isPOBoxErrorsPresent = false;
        var isAllItemsInEligible = false;
		if(key == "allRestricedItems"){
			isAllItemsInEligible = true;
			$("#errorMessageId").addClass("padt10");
			var errorArray = value.replace(/&amp;gt;/g,'>').replace(/&amp;lt;/g,'<').split("<ul></ul>");
			document.getElementById('POBoxError').innerHTML = errorArray[0];
			errorHtml+="<ul><li>"+errorArray[1]+"</li></ul>";
			isNonDeliveryDateErrorPresent = true;
        }
		if(key == "keyOCSPOBoxError"){
			isPOBoxErrorsPresent = true;
			document.getElementById('POBoxError').innerHTML=value;
			$("#streetAddress1").addClass("input_error_txt");
		} else if(key == "keyOCSPostalCodeSuspended") {
			isPOBoxErrorsPresent = true;
			var shippingSuspMessage = "Shipping to this area is unavailable due to a natural disaster. Try a different address, or ";
			shippingSuspMessage+="<a href='/dotcom/jsp/cart/viewShoppingBag.jsp?shippingSuspended=true'>";
			shippingSuspMessage+="return to your bag </a> and save items for later.";
			document.getElementById('POBoxError').innerHTML=shippingSuspMessage;
			$("#errorMessageId").hide();
		} else {
			if(!isAllItemsInEligible) {
				//document.getElementById('POBoxError').innerHTML="";
				if (!errorHtml.indexOf("removeItems()")){
					errorHtml+="<li>"+value+"</li>";
				} else {
					errorHtml+="<li>"+value+"</li>";
				}
			}
        }
		
    }


	do{
		errorHtml = errorHtml.replace("<li></li>","");
	}while(errorHtml.indexOf("<li></li>") > 0);

    if (errorHtml.indexOf("removeItems()") == -1) {
		$('#addShippingAddressError').html("<ul>"+errorHtml+"</ul>");
		$('#editShippingAddressError').html("<ul>"+errorHtml+"</ul>");
	} else {
		if(errorHtml.startsWith("<li>")){
			errorHtml = errorHtml.replace("<li>","");
			errorHtml = errorHtml.substring(0,(errorHtml.length-5));
		}
		$('#shippingAddressError').html("<ul>"+errorHtml+"</ul>");
		$('#editShippingAddressError').html("<ul>"+errorHtml+"</ul>");
		
		$('#addShippingAddressErrorContainer').show();
		$('#editShippingAddressErrorContainer').show();
	}

    if(itemsInEligible) {
        document.getElementById('removablecommerceItem').value=removablecommerceItem;
		$("#errorMessageId").hide();
	} else {
		if(errorHtml != ''){
			$("#errorMessageId").show();												
		}
	}
	shippingFrag = true;
	if (!orderHasOOSItem && !errorsShownInline) {
		$('#addShippingAddressErrorContainer').show();
		$('#editShippingAddressErrorContainer').show();
		
	}
}

function pouplateSavedShippingAddressesError (responseText, statusText, xhr, $form) {
	console.log('error when populate saved shipping address');
	redirectOnError();
}

function openEditShippingAddress(seqNum) {	
	var firstName = $('#cur_addr_first_name_' + seqNum).html();
	var lastName = $('#cur_addr_last_name_' + seqNum).html();
	var companyName = $('#cur_company_name_' + seqNum).html();
	var address1 = $('#cur_addr_address1_' + seqNum).html();
	var address2 = $('#cur_addr_address2_' + seqNum).html();
	var city = $('#cur_addr_city_' + seqNum).html();
	var state = $('#cur_addr_state_' + seqNum).html();
	var postCode = $('#cur_addr_postcode_' + seqNum).html();
	var companyName = $('#cur_addr_company_name_' + seqNum).html();
	var countryName = $('#cur_addr_country_name_' + seqNum).html();
	if (countryName == 'AP') {
		countryName = 'APO/FPO/DPO';
	}
	var phoneNumber = $('#cur_addr_phoneNumber_' + seqNum).html();
	if (phoneNumber){
		phoneNumber = formatPhoneNumber(phoneNumber);
	}
	var militaryType = $('#cur_addr_military_type_' + seqNum).html();
	
	$('#editSelectShippingCountry').val(countryName);
	onEditAddressSelectChange();
	
	if (countryName == 'AP' || countryName == 'APO/FPO/DPO') {
		$('#editMilitarystreetAddress1').val(address1);
		$('#editMilitarystreetAddress2').val(address2);
		$('#editMilitaryzipCode').val(postCode);
		$('#editMilitaryType').val(militaryType);
		$('#editMilitaryselectState').val(state);
	} else {
		$('#editStreetAddress1').val(address1);
		$('#editStreetAddress2').val(address2);
		$('#editZipCode').val(postCode);
		$('#editCityName').val(city);
		$('#editSelectState').val(state);
	}
	
	$('#editFirstName').val(firstName);
	$('#editLastName').val(lastName);
	$('#editCompanyName').val(companyName);
	$('#editPhoneNumber').val(phoneNumber)
	$('#seqNohdn').val(seqNum);
	
	
	$('#add-shipping-address-blk').hide();
	$('#edit-shipping-address-blk').show();
	
	return false;
}

function editProfileShippingAddress (form) {
	var countryName = $('#editSelectShippingCountry').val();
	var firstName = $('#editFirstName').val();
	var lastName = $('#editLastName').val();
	var companyName = $('#editCompanyName').val();
	var address1 = $('#editStreetAddress1').val();
	var address2 = $('#editStreetAddress2').val();
	var city = $('#editCityName').val();
	var state =$('#editSelectState').val();
	var postCode = $('#editZipCode').val();
	var phoneNumber = $('#editPhoneNumber').val();
	var companyName = $('#editCompanyName').val();
	var militaryAddress1= $('#editMilitarystreetAddress1').val();
	var militaryAddress2= $('#editMilitarystreetAddress2').val();
	var militaryZipCode = $('#editMilitaryzipCode').val();
	var militaryType= $('#editMilitaryType').val();
	var militaryselectState= $('#editMilitaryselectState').val();
	var editMilitaryType = $('#editMilitaryType').val();
	var altPhone = $('#altPhoneNumber').val();
	
	
	if (countryName == 'AP' || countryName == 'APO/FPO/DPO') {
		$('#hdnEditStreetAddress1').val(militaryAddress1);
		$('#hdnEditStreetAddress2').val(militaryAddress2);
		$('#hdnEditZipCode').val(militaryZipCode);
		$('#hdnEditSelectState').val(militaryselectState);
		$('#hdnEditMilitaryAddressType').val(editMilitaryType);
	} else {
		$('#hdnEditStreetAddress1').val(address1);
		$('#hdnEditStreetAddress2').val(address2);
		$('#hdnEditZipCode').val(postCode);
		$('#hdnEditSelectState').val(state);
	}
	
	$('#hdnEditFirstName').val(firstName);
	$('#hdnEditLastName').val(lastName);
	$('#hdnEditCityName').val(city);
	
	
	$('#hdnEditSelectShippingCountry').val(countryName);
	$('#hdnEditPhoneNumber').val(phoneNumber);
	$('#hdnEditCompanyName').val(companyName);
	$('#altPhoneNumber').val(altPhone);
	
	if ($(form).valid()){
		$(form).submit(function(event){event.preventDefault();});
		setTimeout(function(){handleEditShippingAddressFormSubmit();}, 1000);
	}
}

function handleEditShippingAddressFormSubmit() {
	removeShippingErrorContainer ();
	var options = {
			success : editShippingAddressSuccess,
	    	error : editShippingAddressError,
	    	timeout : ajaxTimeOut
	 };
	setTimeout(function(){$('#checkout_edit_shipping_hot').ajaxSubmit(options);}, 1000);
	 //showSpinner('container');
	
}

function editShippingAddressSuccess (responseText, statusText, xhr, $form) {
	if (responseText.redirectURL) {
		window.location.href = responseText.redirectURL + '?sessionExpired=true';
		return;
	}
	
	if(responseText.errorArray != undefined && null != responseText.errorArray && null != responseText.errorArray.addShippingErrors)
    {
		handleShippingFormError(responseText);
		hideSpinner();
    } else {
    	if (responseText.shippingGroupsHtml != null) {
    		var shippingGroupsText = $('<textarea />').html(responseText.shippingGroupsHtml).text();
    		$('#saved-shipping-groups').html(shippingGroupsText);   
    		initLoadSavedShippingAddress();		
    		
			setTimeout(function(){shippingGroupChanged();}, 500);	
    	} else {
    		hideSpinner();
    		$('#edit-shipping-address-blk').hide();
    	}
    }
	
}

function editShippingAddressError(responseText, statusText, xhr, $form) {
	console.log('error');
	redirectOnError();
}

function handleExpressCheckoutSelection() {
	var options = {
            success: makeAddrresDefaultAjaxSuccess,
    		error: errorMakeAddressDefault,
    };
    $('#makeDefaultAddr').ajaxSubmit(options);
    //showSpinner('container');
}

function makeAddrresDefaultAjaxSuccess(responseText, statusText, xhr, $form) {
	hideSpinner();
}

function errorMakeAddressDefault(responseText, statusText, xhr, $form) {
	hideSpinner();
}

function initLoadSavedShippingAddress() {
	//$("#shipping_submit").attr('disabled','disabled');	
	resetSavedAddressSelect();
	var selectAddressClass = $('.savedAddressBlk').hasClass('saved-address-blk-active');
	if (selectAddressClass == false) {
		$('.savedAddressBlk').first().addClass('saved-address-blk-active');
	}
	
	$('.saved-shipping-group-details').bind('click', function(event) {
		var curAddress = $(this).prev().html();	
		if($(event.target).hasClass('edit-shipping-address')) {
			setOriginalSelectedSeqNum();
			$('.savedAddressBlk').removeClass('saved-address-blk-active');
			$('.savedAddressBlk').removeClass('saved-address-blk-selected');
			$(this).addClass('saved-address-blk-selected');
			$(this).addClass('saved-address-blk-active');	
			
			openEditShippingAddress(curAddress);
		  	
		} else {
			$('.savedAddressBlk').removeClass('saved-address-blk-active');
			$('.savedAddressBlk').removeClass('saved-address-blk-selected');
			$(this).addClass('saved-address-blk-active');
			$('#edit-shipping-address-blk').hide();
			$('#add-shipping-address-blk').hide();
			shippingGroupChanged(true);
		}
	})
	
	$('.add-new-address-blk').bind('click', function(event) {
		setOriginalSelectedSeqNum();
		removeShippingErrorContainer();
		var selectedAddressId = $(this).attr('id');
		if (selectedAddressId == 'gift-registry-address-blk') {			
			$('#add-shipping-address-blk').hide();
			$('#edit-shipping-address-blk').hide();
			$('.savedAddressBlk').removeClass('saved-address-blk-active');
			$('.savedAddressBlk').removeClass('saved-address-blk-selected');
			$(this).addClass('saved-address-blk-active');
			shippingGroupChanged(true);
		} else {
			initAddShippingAddress();
			coreMetricsPart("Add Address","JCP|Checkout");
			onSelectChange('true');
			$('.savedAddressBlk').removeClass('saved-address-blk-active');
			$('.savedAddressBlk').removeClass('saved-address-blk-selected');
			$(this).addClass('saved-address-blk-selected')
			$(this).addClass('saved-address-blk-active');
			$('#edit-shipping-address-blk').hide();
            $('#add-shipping-address-blk').show();
		}	
		
		
	})
	
	$('.expresscheckout_text').bind('click', function(event) {
		$(this).toggleClass("tickclass");
		var curElemhasTickClass = $(this).hasClass('tickclass');
		$('.expresscheckout_text').removeClass('tickclass');
		if (curElemhasTickClass) {
			$(this).addClass('tickclass');
			var addressSeqNum = $(this).prev().html();
			$('#addressSeqNum').val(addressSeqNum);
			cmCreateConversionEventTag('Set default shipping address', '2', 'Express Checkout');
			handleExpressCheckoutSelection();
		}
	})
	
	handleLoadExtraShippingBlock();
}

function initAddShippingAddress() {
	$('#selectShippingCountry').val('US');
		
	$('#militarystreetAddress1').removeClass('valid-input').val('');
	$('#militarystreetAddress2').removeClass('valid-input').val('');
	$('#militaryzipCode').removeClass('valid-input').val('');
	$('#militaryType').removeClass('valid-input').val('');
	$('#militaryselectState').removeClass('valid-input').val('');
	$('#streetAddress1').removeClass('valid-input').val('');
	$('#streetAddress2').removeClass('valid-input').val('');
	$('#zipCode').removeClass('valid-input').val('');
	$('#cityName').removeClass('valid-input').val('');
	$('#selectState').removeClass('valid-input').val('');
	
	$('#firstName').removeClass('valid-input').val('');
	$('#lastName').removeClass('valid-input').val('');
	$('#companyName').removeClass('valid-input').val('');
	$('#phoneNumber').removeClass('valid-input').val('');	
	
	var largeAppZipCode = $('#largeAppZipCode').html();
	if (largeAppZipCode != null && largeAppZipCode != '') {
		$('#zipCode').val(largeAppZipCode);
		$('#militaryzipCode').val(largeAppZipCode);
		zipProc.validateZip(largeAppZipCode,'cityBlk #cityName','stateSelectBlk #selectState','#zipBlk #zipCode','addshippingAddressModal')
	}
}

function setOriginalSelectedSeqNum() {
	var selectedAddress = $('.saved-address-blk-active').first().prev().html();	
	var selectedAddressId = getCookie("addressId");
	if (selectedAddress == null && selectedAddressId != null && selectedAddressId != "") {
		selectedAddress = selectedAddressId;
	}
	
	$('#original-selected-addres-seq-num').html(selectedAddress);
}

function resetSavedAddressSelect() {
	//$('.savedAddressBlk').removeClass('saved-address-blk-active');
	$('.add-new-address-blk').removeClass('saved-address-blk-active');
	$('.savedAddressBlk').removeClass('saved-address-blk-selected');	
	$('#edit-shipping-address-blk').hide();
	$('#add-shipping-address-blk').hide();
}

function handleLoadExtraShippingBlock() {
	var savedAddressCount = $('#saved-address-count').html();
	if (typeof savedAddressCount != undefined) {
		if (parseInt(savedAddressCount) != 1) {
			$('#add-address-ship-other-country-extra').show();
		} else {
			$('#add-address-ship-other-country-extra').hide();
		}
		if (parseInt(savedAddressCount) < 1) {
			$('.add-new-address-blk').addClass('saved-address-blk-selected');
			$('#add-shipping-address-blk').show();
		}
	}
}

function removeSavedAddressErrorContainer () {
	$('#savedShippingAddressErrorContainer').hide();
}

function showInfoMessage(divid) {
	var self = $('#'+divid),
		icon = self.find("img");
	self.css("border", "1px solid #2060ca");
	self.css("background", "#fafafa");

	icon.attr("src","/dotcom/images/info.svg");
	self.css("color", "#4B4B4B");
}

function loadGiftRegistryShippingDetails() {
	$('#add-new-guest-address-blk').bind('click', function(event) {
		onSelectChange();
		$('.giftregAddressBlk').removeClass('saved-address-blk-selected');
		$('.giftregAddressBlk').removeClass('saved-address-blk-active');
		$('#add-new-guest-address-blk').addClass('saved-address-blk-selected');
		$('#add-new-guest-address-blk').addClass('saved-address-blk-active');		
		$('#add-guest-shipping-address-blk').show();
	})
	$('#gift-registry-address-blk').bind('click', function(event) {
		$('.giftregAddressBlk').removeClass('saved-address-blk-selected');
		$('.giftregAddressBlk').removeClass('saved-address-blk-active');
		$('#gift-registry-address-blk').addClass('saved-address-blk-active');		
		$('#add-guest-shipping-address-blk').hide();
	})
	var options = {
			success : handleShippingGroupChangeSuccess,
			error : shippingGroupChangedError
		};
		$('#giftRegistry_shipping').ajaxSubmit(options);
	
		
}

function handleDeliveryDateReselectCoreMetrics() {
	if(coremetricsTagEnabled) {
		var elementId = '';
		var elementCategory = 'Project Silver Choose Delivery Date Distance';
		var origDate = '';
		if ($('#varselectedDeliveryDate')) {
			origDate = $('#varselectedDeliveryDate').html();
		}
		var newDate = '';
		if ($('#selectedDeliveryDate')) {
			newDate = $('#selectedDeliveryDate').val();
		}
		if (origDate != '' && origDate != newDate) {
			elementId = 'Date Reselect Optional';
			cmCreateElementTag(elementId, elementCategory, '');
		}
		
		var currentDate = getFormatedDate(new Date());
		if (newDate != '') {
			var diff = daysBetween (currentDate, newDate);
			elementId = diff + ' days';
			cmCreateElementTag(elementId, elementCategory, '');
		}
	}
}
var getCheckBoxValue = function() {
	var checkBox = $("input#holidayShippingOptionEn");
	//binding event to change value on click
	checkBox.live("click", onCheckBoxClicked);
	//populate value on page load
	onCheckBoxClicked();
};

var onCheckBoxClicked = function(e) {
	if(e){
		var value = e.target.checked;
	} else {
		var value = $("input#holidayShippingOptionEn")[0].checked;
	}
	$("input#holidayOptIn").val(value);
};