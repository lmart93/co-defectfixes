<%--
* -----------------------------------------------------------------------
* Copyright 2011, jcpenney Co., Inc.  All Rights Reserved.
*
* Reproduction or use of this file without explicit
* written consent is prohibited.
* Created by: smalli
*
* Created on: July 5, 2016
* ----------------------------------------------------------------
--%>
<%-- ========================================= Description
/**
 * This page is used to display
 *  1.This Jsp contains all the read only pages
 * Includes Header and Footer JSPFs.
 *
 * @author smalli
 * @version 0.1 05-July-2016
 **/
--%>
<%@ taglib uri="dsp" prefix="dsp" %>
<%@ taglib uri="dspel" prefix="dspel" %>
<%@ taglib uri="c" prefix="c"  %>
<%@ taglib uri="fmt" prefix="fmt" %>
<%@ page contentType="text/json;charset=UTF-8" %>
<dsp:page xml="true">
<dsp:getvalueof var="contextroot" vartype="java.lang.String" bean="/OriginatingRequest.contextPath"/>
<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
<dsp:importbean bean="/com/jcpenney/core/order/droplet/GiftCardDroplet"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/com/jcpenney/dotcom/order/formhandler/GiftCardPaymentGroupFormHandler"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>

<script type="text/javascript" src="/dotcom/js/checkoutsimplified/giftcard.js"></script>

<div class="short-field gift-card-section">
		<div class="gift-card">
			<dsp:droplet name="GiftCardDroplet">
			<dsp:param name="order" bean="ShoppingCart.current"/>
			<dsp:oparam name="output">
			<dsp:getvalueof var="orderBalanceCheck" param="orderBalance" />
			<dsp:getvalueof var="maxGiftCardLimit" param="maxGiftCard" />	
			<dsp:getvalueof var="giftListSize" param="giftCardCountFinal" />	
			<c:choose>
			<c:when test="${giftListSize == 0}">
			<label for="gift-card-Checkbox" class="gift-card-Checkbox-label">
		        <input class="gift-card-Checkbox" id="gift-card-Checkbox" name="/com/jcpenney/roundup/RoundUpFormHandler.roundUpElection" value="" type="checkbox" onclick="toggleGiftCardForm(this);"/>
		        <span class="gift-card-cb"></span>
		        <span id="" class="Use-a-JCPenney-gift">Use a JCPenney gift card</span>
		    </label>
			<p class="gift-card-short-field-info">
				You may use up to 2 Gift Cards
			</p>
			
			<dsp:form name="giftCardFormAdd" id="giftCardFormAdd" action="/dotcom/jsp/checkout/secure/checkoutsimplified/payment/giftcard/giftCard.jsp" iclass="jcp_form hide" method="post">
				<div class="gift_card_error_msgs mrgb10" id="giftCardErrorContainer" style="display:none">
					<dsp:droplet name="IsEmpty">
					<dsp:param bean="GiftCardPaymentGroupFormHandler.formExceptions" name="value"/>							  
					<dsp:oparam name="false">
					 <dsp:include page="/jsp/checkout/secure/checkoutsimplified/payment/giftcard/giftcardError.jsp"/>
					</dsp:oparam>
					</dsp:droplet>
				</div>
				<div class="gift_card_error_msgs mrgb10" id="giftCardErrorMsgContainer" style="display:none">
					<span class="error-msg-header"><img alt="attention" src="/dotcom/images/error-fill-copy.png">Please correct the errors listed in red below:</span>
					<ul></ul>
				</div>
				<div class="input-holder">
					<p class="Enter-gift-card-numb">Enter gift card number 
						<a href="#giftCardNumberTip" class="gift-card-tooltip">
							<img alt="tooltip" src="<c:out value="${contextroot}" />/images/tooltip_quesmark.png" />
							<span class="gift-card-tooltiptext" id="giftCardNumberTip">If using a gift card, find your 19-digit number on the back of the card. Scratch off your 4-digit ID number, which is also found on the back of your card. If using an e-Gift card, find your 19-digit e-Gift card number and ID number at the top of your email, just under the amount.</span>
			 			</a>
					</p>
					<dsp:input id="giftCardNumber" autocomplete="off" bean="GiftCardPaymentGroupFormHandler.giftCards[0].cardNumber" name="giftCardNumber" type="text" class="gift-card-number" maxlength="19"  onclick="clearGiftCardFieldStyle(this, 'giftCardNumberLabel', 'gift-card-number', '','true')" onfocus="clearGiftCardFieldStyle(this, 'giftCardNumberLabel', 'gift-card-number', '','true')" placeholder="enter gift card number" value="" />
					
				</div>
				<div class="input-holder">
					<p class="Enter-gift-card-numb">4-digit ID</p>
					<input name="giftCardId" id="giftCardId" type="text" autocomplete="off" class="four-digit-id" maxlength="4" value="" oncopy="return false" oncut="return false" onclick="clearFieldStyle(this, 'giftCardNumberLabel', 'four-digit-id', '','true')" onfocus="clearFieldStyle(this, 'giftCardPinLabel', 'four-digit-id', '','true')" />
	     			<dsp:input id="giftCardIdDSP" bean="GiftCardPaymentGroupFormHandler.giftCards[0].cardPin" name="giftCardIdDSP" type="hidden" iclass="flt_lft four-digit-id" maxlength="4" />
	     			<input id="applyGiftCard" name="applyGiftCard" type="button" alt="Apply" value="Apply" onclick="validateGiftCardForm('giftCardFormAdd')"/>
					<dsp:input type="hidden" value="" bean="GiftCardPaymentGroupFormHandler.applyGiftCard"/>
				</div>
			</dsp:form>
			</c:when>
			<c:otherwise>
			<label for="gift-card-Checkbox" class="gift-card-Checkbox-label">
		        <input class="gift-card-Checkbox" id="gift-card-Checkbox" name="/com/jcpenney/roundup/RoundUpFormHandler.roundUpElection" value="" type="checkbox" disabled checked="true"/>
		        <span class="gift-card-cb"></span>
		        <span id="" class="Use-a-JCPenney-gift">Use a JCPenney gift card</span>
		        <img src="<c:out value="${contextroot}" />/images/gift-card.png" />
		    </label>
			<p class="gift-card-short-field-info">
				You may use up to 2 Gift Cards in combination with any payment method
			</p>
			<div class="gift_card_error_msgs mrgb10" id="giftCardErrorContainer" style="display:none">
				<dsp:droplet name="IsEmpty">
				<dsp:param bean="GiftCardPaymentGroupFormHandler.formExceptions" name="value"/>							  
				<dsp:oparam name="false">
				 <dsp:include page="/jsp/checkout/secure/checkoutsimplified/payment/giftcard/giftcardError.jsp"/>
				</dsp:oparam>
				</dsp:droplet>
			</div>
			<div class="gift_card_error_msgs mrgb10" id="giftCardErrorMsgContainer" style="display:none">
					<span class="error-msg-header"><img alt="attention" src="/dotcom/images/error-fill-copy.png">Please correct the errors listed in red below:</span>
					<ul></ul>
			</div>
			<div class="applied-cards-section">
				<div class="row">
					<div class="col-1">Applied Gift Card(s)</div>
					<div class="col-2">Amount charged</div>
					<div class="col-3">Card balance</div>
					<div class="col-4"></div>
				</div>
			
			<dsp:droplet name="ForEach">
			<dsp:param name="array" param="giftCardList" />
			<dsp:oparam name="output">
			<dsp:getvalueof var="giftCardAmount" param="element.amount" />
			<dsp:getvalueof var="giftCardBalance" param="element.balance" />
			<div class="row">
				<div class="col-1">gift card:<dsp:valueof param="element.maskedCardNumber"/></div>
				<div class="col-2">-<dspel:valueof value="${giftCardAmount}"  number="0.00" converter="currency"/>&nbsp;</div>
				<div class="col-3"><dspel:valueof value="${giftCardBalance}"  number="0.00" converter="currency"/></div>&nbsp;
				<div class="col-4">
				<dsp:getvalueof var="giftCardIdToRemove" param="element.id" />
				<dsp:form name="giftCardDeleteForm_${giftCardIdToRemove}" id="giftCardDeleteForm_${giftCardIdToRemove}" method="post" iclass="jcp_form" action="/dotcom/jsp/checkout/secure/checkoutsimplified/payment/giftcard/giftCard.jsp">														    
					<a href="javascript:void(removeGiftCardFromOrder);" id="removeGiftCard" name="removeGiftCard" title="Remove gift card from order" onclick="getRemoveGiftCardDetails('giftCardDeleteForm_${giftCardIdToRemove}');">remove</a>
					<dspel:input id="removeGiftCardId" type="hidden" bean="GiftCardPaymentGroupFormHandler.giftCardId" value="${giftCardIdToRemove}"/>
					<dsp:input type="hidden" value="" bean="GiftCardPaymentGroupFormHandler.removeGiftCard"/>					
				</dsp:form>
				</div>
			</div>
			</dsp:oparam>
			</dsp:droplet>
			</div>
			
			 <c:if test="${orderBalanceCheck ne 0 && giftListSize ne maxGiftCardLimit}">
			 <dsp:form name="giftCardFormAdd" id="giftCardFormAdd" action="/dotcom/jsp/checkout/secure/checkoutsimplified/payment/giftcard/giftCard.jsp" iclass="jcp_form" method="post">
			<div class="input-holder">
				<p class="Enter-gift-card-numb">Enter gift card number <a href="javascript:void(0);" class="gift-card-tooltip">
				<img alt="tooltip" src="<c:out value="${contextroot}" />/images/tooltip_quesmark.png" />
				<span class="gift-card-tooltiptext" id="giftCardNumberTip">If using a gift card, find your 19-digit number on the back of the card. Scratch off your 4-digit ID number, which is also found on the back of your card. If using an e-Gift card, find your 19-digit e-Gift card number and ID number at the top of your email, just under the amount.</span>
				</a></p>
				<dsp:input id="giftCardNumber" autocomplete="off" bean="GiftCardPaymentGroupFormHandler.giftCards[0].cardNumber" name="giftCardNumber" type="text" class="gift-card-number" maxlength="19"  onclick="clearGiftCardFieldStyle(this, 'giftCardNumberLabel', 'gift-card-number', '','true')" onfocus="clearGiftCardFieldStyle(this, 'giftCardNumberLabel', 'gift-card-number', '','true')" placeholder="enter gift card number" value="" />
			</div>
			<div class="input-holder">
				<p class="Enter-gift-card-numb">4-digit ID</p>
				<input name="giftCardId" id="giftCardId" type="text" autocomplete="off" class="four-digit-id" maxlength="4" value="" oncopy="return false" oncut="return false" onclick="clearFieldStyle(this, 'giftCardNumberLabel', 'four-digit-id', '','true')" onfocus="clearFieldStyle(this, 'giftCardPinLabel', 'four-digit-id', '','true')" />
     			<dsp:input id="giftCardIdDSP" bean="GiftCardPaymentGroupFormHandler.giftCards[0].cardPin" name="giftCardIdDSP" type="hidden" iclass="flt_lft four-digit-id" maxlength="4" />
     			<input id="applyGiftCard" name="applyGiftCard" type="button" alt="Apply" value="Apply" onclick="validateGiftCardForm('giftCardFormAdd')"/>
				<dsp:input type="hidden" value="" bean="GiftCardPaymentGroupFormHandler.applyGiftCard"/>
			</div>
			</dsp:form>
			 </c:if>
			<div class="error-msg-container">
				<div class="image"><img src="<c:out value="${contextroot}" />/images/tick-mark.png" /></div>
				<c:if test="${orderBalanceCheck ne 0}">
				<div class="message">Your remaining order balance due is <dspel:valueof value="${orderBalanceCheck}"  number="0.00" converter="currency"/>. Choose a payment method you would like to use for the balance.</div>
				</c:if>
				<c:if test="${orderBalanceCheck eq 0}">
				<div class="message">Your remaining order balance due is $00.00. </div>
				</c:if>
				<div class="clear"></div>
			</div>
			</c:otherwise>
			</c:choose>
			</dsp:oparam>
		</dsp:droplet>
			
			<div class="clear"></div>
		</div>
	</div>
		
</dsp:page>