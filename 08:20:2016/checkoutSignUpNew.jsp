<%--
* -----------------------------------------------------------------------
* Copyright 2010, jcpenney Co., Inc.  All Rights Reserved.
*
* Reproduction or use of this file without explicit
* written consent is prohibited.

* ----------------------------------------------------------------
--%>
<%-- ========================================= Description
/**
* This page is used to display the fields in Login password update form
* 1.Error Message to the user if any failure
*
* @author Twinkle Sharma
* @version 0.1 15-July-2016
**/
--%><?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<%@ taglib uri="dsp" prefix="dsp" %>
<%@ taglib uri="dspel" prefix="dspel" %>
<%@ taglib uri="c" prefix="c"  %>
<%@ taglib uri="fmt" prefix="fmt" %>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<dsp:page xml="true">
<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
<dsp:importbean bean="/atg/userprofiling/ProfileErrorMessageForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
<dsp:importbean bean="com/jcpenney/optin/droplet/OptinUnsubscribeOffersDroplet"/>
<dsp:importbean bean="/com/jcpenney/core/ApplicationConfiguration" />
<dsp:importbean bean="/com/jcpenney/optin/formhandler/OptinFormHandler"/>
<dsp:importbean bean="/com/jcpenney/core/droplet/AntiSpamEmailAddressSuffixLookupDroplet"/>
<dsp:droplet name="AntiSpamEmailAddressSuffixLookupDroplet">
	<dsp:param name="emailAddress" bean="ShoppingCart.last.email"/>
	<dsp:param name="currentOrder" bean="ShoppingCart.last" />
	<dsp:oparam name="output">
		<dsp:getvalueof var="antiSpamEmailAddressSuffixes" param="antiSpamEmailAddressSuffixList"/>
		<dsp:getvalueof var="isAntiSpamCustomer" param="isAntiSpamCustomer"/>
		<dsp:getvalueof var="isAntiSpamBillingAddress" param="isAntiSpamBillingAddress" />
	</dsp:oparam>
</dsp:droplet>
<input id="cmOptinClientId" type="hidden" value="<dspel:valueof bean="/com/jcpenney/coremetrics/droplet/CoremetricsTagDroplet.clientId"/>"/>
<input id="cmOptinRenTagUrl" type="hidden" value="<dspel:valueof bean="/com/jcpenney/coremetrics/droplet/CoremetricsTagDroplet.url"/>"/>
<input id="cmOptinRenTagDomain" type="hidden" value="<dspel:valueof bean="/com/jcpenney/coremetrics/droplet/CoremetricsTagDroplet.domain"/>"/>              

	<fmt:setBundle basename="com/jcpenney/dp/core/profile/ProfileResources" var="profileBundle" scope="application"/>
	<fmt:setBundle basename="com/jcpenney/dp/core/loyalty/manager/LoyaltyResources" var="loyaltyBundle" scope="application"/>

	<dsp:droplet name="/com/jcpenney/core/droplet/FetchActiveMessagesDroplet">
	<dsp:param name="page" value="cam"/>
	<dsp:param name="keys" value="keyProfilePasswordFormatMessage_New1,keyProfilePasswordFormatMessage_New2,keyProfilePasswordFormatMessage_New3,keyProfilePasswordFormatMessage_New4,keyProfilePasswordFormatMessage_New5,keyProfilePasswordFormatMessage_New6,keyProfilePasswordFormatMessage_New7,keyProfilePasswordFormatMessage1,keyProfilePasswordFormatMessage2,keyProfilePasswordFormatMessage3,keyProfilePasswordFormatMessage5,keyProfilePasswordFormatMessage5,keyProfilePasswordMissing,keyProfileConfirmPasswordMissing"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="activeMessagesJson" param="activeMessagesJson"/>
		</dsp:oparam>
	</dsp:droplet>
	<script type="text/javascript">
		function GetActiveMessages() {
			var src = ${activeMessagesJson};
			for (i in src) {
				var key = src[i].key;
				var message = src[i].message;
				if(document.getElementById(key)){
					document.getElementById(key).innerHTML = message;							
				}
			}
		}
	</script>
	
<dsp:getvalueof var="contextroot" vartype="java.lang.String" bean="/OriginatingRequest.contextPath"/>
<dsp:getvalueof var="isExpressCheckoutEnabled" bean="/com/jcpenney/core/ApplicationConfiguration.enableExpressCheckout"/>
<head>

<script type="text/javascript">

$('.closeModal').click(function(){
	//alert('close');
	$('#colorbox').remove();
	$('#cboxOverlay').remove();	
});


function submitEmailPreference(checkbox){

    var isCheckedEmailOptin =$(checkbox).attr("checked");
    if(isCheckedEmailOptin == true) {
        $("#signUpEmailValue").val(true);
        $("input[id=signUpEmail]").attr('checked',true);
        $("#signupforEmailHiddenId").val('yes')
        $("#profileReceiveEmail").val('yes')        
    }else if(isCheckedEmailOptin == false){
        $("#signUpEmailValue").val(false);
    	$("input[id=signUpEmail]").attr('checked',false);
        $("#signupforEmailHiddenId").val('no')
        $("#profileReceiveEmail").val('no')    	
    }

}
</script>
</head>
<body>

	<dsp:droplet name="ForEach">
  		<dsp:param bean="ShoppingCart.last.paymentGroups" name="array"/>
      		<dsp:oparam name="output">
        		<dsp:getvalueof var="bAddress" param="element.billingAddress"/>
      	</dsp:oparam>
	</dsp:droplet>
<dspel:setvalue param="billingAddress" value="${bAddress}" />
<input id="firstName" type="hidden" value="<dspel:valueof param="billingAddress.firstName"/>"/>
<input id="lastName" type="hidden" value="<dspel:valueof param="billingAddress.lastName"/>"/>


	<div id='processingImageReg' style='display:none'><img alt="loader" src='<c:out value="${contextroot}"/>/images/loadingImage.gif' /></div>
	
	<dsp:form iclass="jcp_form" method="post" id="registerAjax" action="/dotcom/jsp/checkout/secure/orderConfirmation.jsp">
	   <dspel:input type="hidden" id="jcp_rw_enroll_flag" bean="ApplicationConfiguration.rewardsEnabled"/>
		<div class="jcp-rw-order-confirm-wrap">
	        <div class="jcp-rw-thankyou-modal-head bold">
		        CREATE ACCOUNT
	        </div>
	        <div class="jcp-rw-order-confirm-thankyou">
	        	<h1>Thank you, your order has been placed.</h1>
  				<h5>Save your information for next time, create a JCPenney account!</h5>
	        	
	        	<div class="jcp-rw-enroll-points-wrap">
	        		<div class="floatL jcp-rw-points-lhs">
	        			<ul class="jcp-rw-enroll-points jcp-rw-points-lhs">
	          			<li>
		          			<img src="/dotcom/images/express-checkout.svg" alt="" />
		          			Checkout faster with Express Checkout
	          			</li>
	          			<li>
	          				<img src="/dotcom/images/truck.svg" alt="" />
	          				Easy access to order history
	          			</li>
	          			<li>
	          				<img src="/dotcom/images/coupon-rewards.svg" alt="" />
        					Earn &amp; redeem JCPenney Rewards
	          				</li>
	          			</ul>
	        		</div>
	        		
	        		<div class="floatL jcp-rw-points-rhs">
	        			<ul class="jcp-rw-enroll-points">
	         			<li>
					        <img src="/dotcom/images/mobile.svg" alt="" />
					        Access your account from any device
					      </li>
					      <li>
					        <img src="/dotcom/images/coupon-fill.svg" alt="" />
					        Get coupons and sale notifications
					      </li>
	         			</ul>
	        		</div>
	         		<div class="clear"></div>
	        	</div>
	        	<div id="acceptTermsConditionsErrors" style="display:none" class="float_fix dynamic_error_msgs mrgb10">
					<div class="flt_lft mrgr15 exclamation_icn"><img alt="attention" src="<c:out value='${contextroot}'/>/images/error.svg" /><span>attention</span></div>
					<ul class="tncError">
						 <li class="nodisc">Please correct the errors listed in red below:</li>	 
						 <li><fmt:message key="loyaltyAcceptTandC" bundle="${ loyaltyBundle }"/></li>
					</ul>	   
				</div>
				
					
	        	<div id="errorContainer" class="dynamic_error_msgs mrgb10 hide_display">                                                  
					<div class="flt_lft mrgr15 exclamation_icn"><img alt="attention" src="<c:out value='${contextroot}'/>/images/error.svg" /><span>attention</span></div>
					  <div id="passwordInvalid" style="display:none" class="float_fix">
							
						  <span class="disp_blk">Please correct the errors listed in red below:</span>
					  
		                  <div>
			                  <fmt:message key="keyProfilePasswordFormatMessage_New1" bundle="${ profileBundle }"/><br/>
			                  <fmt:message key="keyProfilePasswordFormatMessage_New2" bundle="${ profileBundle }"/><br/>
			                  <fmt:message key="keyProfilePasswordFormatMessage_New3" bundle="${ profileBundle }"/><br/>
			                  <fmt:message key="keyProfilePasswordFormatMessage_New4" bundle="${ profileBundle }"/><br/>
			                  <fmt:message key="keyProfilePasswordFormatMessage_New5" bundle="${ profileBundle }"/><br/>
			                  <fmt:message key="keyProfilePasswordFormatMessage_New6" bundle="${ profileBundle }"/><br/>
			                  <fmt:message key="keyProfilePasswordFormatMessage_New7" bundle="${ profileBundle }"/><br/>
			                  <fmt:message key="keyProfilePasswordFormatMessage_New8" bundle="${ profileBundle }"/><br/>
                		</div>
						
					</div>
					<div id="errorPlaceHolder" style="display:none"></div>		
					  <div id="passwordBlank" class="float_fix" style="display:none"> 
						   <ul>
							   <li class="nodisc">Please correct the errors listed in red below:</li>	                                                            
							   <li id="passwordRequiredReg" style="display:none"><div id="keyProfilePasswordMissing"></div></li>
							   <li id="confPasswordRequiredReg" style="display:none"><div id="keyProfileConfirmPasswordMissing"></div></li>
						  </ul>                                                       
					  </div>
				</div>
	        	<p class="jcp-rw-order-item-label helvetica-13 bold">email address</p>
	        	<div>
	        		<p class="jcp-rw-dk-Grey helvetica-14"><dsp:valueof bean="ShoppingCart.last.email" ></dsp:valueof></p>
	        		<input id="emailAddress" type="hidden" value="<dsp:valueof bean="ShoppingCart.last.email" />"/>
	        		<p class="jcp-rw-light-Grey italic helvetica-12">we'll send confirmation to this email address</p>
	        	</div>
	        	
				<c:set var="cssInputPassword" value="input_txt signup_password disp_blk"/>
					<c:set var="cssLabelPassword" value=""/>
					<dsp:droplet name="IsEmpty">
						<dsp:param bean="ProfileFormHandler.propertyExceptions.password.errorCode" name="value"/>							  
						<dsp:oparam name="false">
							<c:set var="cssInputPassword" value="input_txt signup_password disp_blk input_error_txt" />
							<c:set var="cssLabelPassword" value="error" />		
						 </dsp:oparam>															
				</dsp:droplet>
				
				
				<div class="thankyou-pwd-container">
	        	<div class="password_container left_half">
	        	<div>
	        	<label for="password" id="mypasswdLabelReg" class="<c:out value='${cssLabelPassword}'/>"><p class="jcp-rw-order-item-label helvetica-13 bold">password</p></a></label>
				<span style="float:right;">
				<img src="/cam/images/icon-show-password.png" id="hideShowPasswd" onclick="changeImage()" alt="show">
				</span>
				
				<dspel:input id="password" name="password" type="password" iclass="${cssInputPassword}" value="" bean="ProfileFormHandler.value.password" onclick="clearFieldStyle(this, 'mypasswdLabelReg', 'input_txt signup_password disp_blk', '','true')" onfocus="clearFieldStyle(this, 'mypasswdLabelReg', 'input_txt signup_password disp_blk', '', 'true')" />
				</div>
				<span class="remove" style="visibility:hidden;">remove: <span class="errorMessage"></span></span>
	        	<div class="jcp-rw-light-Grey italic helvetica-12">&nbsp;</div>
				

				<c:set var="cssInputConfirmPassword" value="input_txt signup_password disp_blk" />
					<c:set var="cssLabelConfirmPassword" value=""/>
					<dsp:droplet name="IsEmpty">
						<dsp:param bean="ProfileFormHandler.propertyExceptions.confirmPassword.errorCode" name="value"/>							  
						<dsp:oparam name="false">
							<c:set var="cssInputConfirmPassword" value="input_txt signup_password disp_blk input_error_txt" />
							<c:set var="cssLabelConfirmPassword" value="error" />		
						 </dsp:oparam>															
				</dsp:droplet>
	        	
				<label for="confpasswd" id="myconfpasswdLabel" class="<c:out value='${cssLabelConfirmPassword}'/>"><p class="jcp-rw-order-item-label helvetica-13 bold">confirm password</p></label>
	        	<div><dspel:input id="confpasswd" name="confpasswd" type="password" iclass="${cssInputConfirmPassword}" bean="ProfileFormHandler.editValue.confirmPassword" onclick="clearFieldStyle(this, 'myconfpasswdLabel', 'input_txt signup_password disp_blk', '','true')" onfocus="clearFieldStyle(this, 'myconfpasswdLabel', 'input_txt signup_password disp_blk', '', 'true')" /></div>
	        	<span class="exclamation" style="visibility:hidden;"><img src="/dotcom/images/icon-fielderror@3x.png" style="position:absolute;left:230px;top:33px;width: 20px;height: 20px;border-radius: 1px;"/></span>
	        		
	        		<div class="input-wrapper">
				      <input type="checkbox" id="test" name="test" value="true">
				      <label for="test">
				        save as your <strong>Express Checkout Payment</strong>
				      </label>
				      <div class="clear"></div>
				    </div>
				    <div class="input-wrapper">
				      <input type="checkbox" id="test2" name="test2" value="true">
				      <label for="test2">
				        Enroll in JCPenney Rewards to turn points into future savings.
				        <p>I have read and agree to the terms and conditions of the JCPenney Rewards program and the JCPenney privacy policy. I am a legal resident of the United States (including PR and U.S. Territories) and am 18 years of age or older. I understand that, by enrolling in JCPenney Rewards, I agree to receive promotional email offers and updates at the email address I provided.</p>
				      </label>
				      <div class="clear"></div>
				    </div>
				    	        	
	        	</div>
	        	
	        	<div class="right_half">
		        <div class="info_box">
				
				<div class="info_body" >
					<strong class="pwd_strength">password strength <span></span></strong>
					<div class="hr_line"><div class="pwdstrength"></div></div>
					<div class="validation_rules">
					<label>Must include...</label>
						<ul class="minReq">
							<li id="length"><span class="validation_length"><img src="/dotcom/images/icon-fieldcheck@3x.png" style="width: 10px;height: 10px;border-radius: 1px;"/></span>use 8-16 characters</li>
							<li id="upper"><span class="validation_uppercase"><img src="/dotcom/images/icon-fieldcheck@3x.png" style="width: 10px;height: 10px;border-radius: 1px;"/></span>use at least one upper case letter (e.g. ABC)</li>
							<li id="lower"><span class="validation_lowercase"><img src="/dotcom/images/icon-fieldcheck@3x.png" style="width: 10px;height: 10px;border-radius: 1px;"/></span>use at least one lower case letter (e.g. abc)</li>
							<li id="number"><span class="validation_numeric"><img src="/dotcom/images/icon-fieldcheck@3x.png" style="width: 10px;height: 10px;border-radius: 1px;"/></span>use a number (e.g. 1234)</li>
						</ul>
						</br>
		    			<label>Should not include...</label>
						<ul class="notInclude">
							<li id="email"><span class="validation_nameemail"><img src="/dotcom/images/icon-cross-active@3x.png" style="width: 10px;height: 10px;border-radius: 1px;"/></span>your name or email</li>
							<li id="space"><span class="validation_spaces"><img src="/dotcom/images/icon-cross-active@3x.png" style="width: 10px;height: 10px;border-radius: 1px;"/></span>spaces</li>
							<li id="consecutive"><span class="validation_consecutive"><img src="/dotcom/images/icon-cross-active@3x.png" style="width: 10px;height: 10px;border-radius: 1px;"/></span>consecutive characters</li>
							<li id="jcp"><span class="validation_jcp"><img src="/dotcom/images/icon-cross-active@3x.png" style="width: 10px;height: 10px;border-radius: 1px;"/></span>jcp or jcpenney</li>
						</ul>
					</div>
				</div>
			</div>
	        </div>
	        <div class="clear"></div>
	        </div>
	        	
	        	<div class="jcp-rw-enroll-signup-area">
				<dsp:getvalueof var="shipToCountry" bean="Profile.shipToCountry" />
				  <c:if test="${checkBoxSaveCCDisplay != 'false' and shipToCountry eq 'US'}">
					<dsp:droplet name="IsEmpty">
						<dsp:param name="value" bean="ShoppingCart.last.paymentGroups"/>
						<dsp:oparam name="false">
						 <dsp:getvalueof var="paymentMethod" vartype="java.lang.String" bean="ShoppingCart.last.paymentGroups[0].paymentMethod"/>
						 <c:if test="${ paymentMethod == 'creditCard' }">
							<div class="jcp-rw-save-cc-area">
								<div class="floatL jcp-rw-enroll-check-wrap"><dspel:input id="saveCreditCardInfo" type="checkbox" bean="ProfileFormHandler.editValue.makeCreditCardDefault" name="saveCreditCard" checked="false" /></div>
								<label for="saveCreditCardInfo" class="nobold float_fix"><div class="floatL jcp-rw-enroll-text-wrap jcp-rw-save-credit-text">save credit card information to my account</div><span class="disp_blk smallf11"><div class="jcp-rw-light-Grey italic helvetica-12">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(all other information is automatically saved)</div></span></label>
							</div>
						 </c:if>
						</dsp:oparam>
					</dsp:droplet>
				  </c:if>	         	
	         	
	         	<dsp:droplet name="Switch">
							<dsp:param name="value" bean="ApplicationConfiguration.enableExpressCheckout"/>
							<dsp:oparam name="true">
			                     <fieldset>
			                        <div class="floatL checkoutSignUpCheckbox">
			                        	<input type="checkbox" id="saveDefaultShippingAndPaymentInfo" name="useShippingAndPaymentForExpressCheckout" value="true" />
			                        </div>
			                        <div class="floatL expressCheckoutLabelSection">
										<label class="expressCheckout" for="saveDefaultShippingAndPaymentInfo">set as your default credit card for <span class="bold">Express Checkout</span>
											<a id="a1_up_express" href="#guestCheckoutTipMessage" class="helpIconBig toolTipExpressCheckoutHelpIcon"><img src="<c:out value="${contextroot}"/>/images/expressCheckoutTooltip.png" alt="?"/></a>
											<div id="guestCheckoutTipMessage" class="infoToolTip hidden">
												<p>Express Checkout stores your address info to make checkout faster and easier</p>
											</div>
										</label>
									</div>
			                         <div class="clear"></div>
			                    </fieldset>
			                </dsp:oparam>
						</dsp:droplet>
						
<%-- Enrolll Rewards account  --%>
			<dsp:droplet name="IsEmpty">
                    <dsp:param name="value" bean="ShoppingCart.last.rewardCardNumber"/>
                    <dsp:oparam name="true">
                        <dspel:getvalueof var="hasRewardCard" value="false" />
                    </dsp:oparam>
                    <dsp:oparam name="false">
                        <dspel:getvalueof var="hasRewardCard" value="true" />
                    </dsp:oparam>
               </dsp:droplet>
               <dsp:getvalueof var="rewardsEnabled" bean="ApplicationConfiguration.rewardsEnabled"/>
               <c:if test="${hasRewardCard eq false and rewardsEnabled eq true and not isAntiSpamBillingAddress and not isAntiSpamCustomer}">
               		<div class="jcp-rw-enroll-jcp">
						<div class="floatL jcp-rw-enroll-check-wrap">
							<dspel:input type="checkbox" id="jcp_rw_enroll_me" bean="ProfileFormHandler.editValue.rewardsEnrolled" checked="false" />
						</div>
						<div class="floatL jcp-rw-enroll-text-wrap">
							<span class="bold jcp-rw-red-Text">Enroll in JCPenney Rewards</span> to turn points into future savings.
						</div>
						<div class="clear"></div>
						
						<div class="jcp-rw-terms-agreement hide" id="jcp_rw_enroll_terms" style="display: block;">
							<dspel:input type="hidden" id="jcp_rw_terms_conditions" bean="ProfileFormHandler.editValue.termsConditionsAccepted" name="jcp_rw_terms_conditions" checked="true" />
							<div class="floatL jcp-rw-enroll-text-wrap jcp-rw-enroll-terms-agree">
								<%@ include file="/jsp/profile/secure/rewardsCheckBox.jspf"%>  
							</div>
							<div class="clear"></div>
						</div>
					</div>
               </c:if>
				
				<dsp:droplet name="Switch">
                    <dsp:param bean="Profile.receiveEmail" name="value" />
                    <dsp:oparam name="no">
						<div class="jcp-rw-signup-affiliates">
							<div class="floatL jcp-rw-enroll-check-wrap"><dspel:input id="signUpEmail" type="checkbox" bean="ProfileFormHandler.editValue.receiveEmail" onclick="submitEmailPreference(this);"/></div>
							<div class="floatL jcp-rw-sigmup-agree jcp-rw-enroll-text-wrap">
									Yes, sign me up to receive email offers, sales, coupons and more from JCPenney, its affiliates and licensees.
							</div>
							<div class="clear"></div>
						</div>
                    </dsp:oparam>
                </dsp:droplet>
	         	<dspel:input type="hidden" bean="ProfileFormHandler.value.receiveEmail" name="a" id="signupforEmailHiddenId" />
	         	<dspel:input type="hidden" bean="Profile.receiveEmail" name="b" id="profileReceiveEmail" />		         	
	         	<div class="jcp-rw-create-account-btn-area">
	         		<div class="floatR">
					<dsp:input type="hidden" value="fullRegistration" bean="ProfileFormHandler.userAction" />
					 
						<dspel:input type="hidden" bean="ProfileFormHandler.createSuccessURL" value="${contextroot}/jsp/checkout/secure/ajax/signinJson.jsp" />
						<dspel:input type="hidden" bean="ProfileFormHandler.createErrorURL" value="${contextroot}/jsp/checkout/secure/ajax/signinJson.jsp" />
										
						<dsp:input id="hdnSubmit" type="hidden" value="Submit"  bean="ProfileFormHandler.fullRegistration" />
						<input class="red-Button jcp-rw-create-btn" type="button" id="create_btn" value="create account" onclick="return validateCompleteForm();" />
					</div>
	         		<div class="floatR jcp-rw-skip-to-text"></span></span><a title="no thanks, skip to order confirmation" href="javascript:void(noThanks_SkipToOrderConfirmation)" class="flt_lft" onclick="closeOptin();">no thanks, skip to order confirmation</a> </div>
	         		<div class="clear"></div>
	         	</div>
	        	</div>
	        </div>
	       </div>
	       
		   <input type="text" onfocus="$('#password').focus();" class="modal_focus_input" />
	</dsp:form>

<script type="text/javascript">
// jcp.checkout.init();
 jcp.form.init();
 </script>
<script type="text/javascript">

function initExpressCheckoutShippingCheckboxes(){

	$("#saveCreditCardInfo").click(function(){
		if(!$("#saveCreditCardInfo").is(":checked")){
			$("#saveDefaultShippingAndPaymentInfo").attr("checked", false);
			$($("#saveDefaultShippingAndPaymentInfo").parent().children()[0]).removeClass("ui-checkbox-on").addClass("ui-checkbox-off");
			$($("#saveDefaultShippingAndPaymentInfo").parent().children()[0]).find(".ui-icon").removeClass("ui-icon-checkbox-on").removeClass("ui-icon-checkbox-off").addClass("ui-icon-checkbox-off");
		} 
	});

	$("#saveDefaultShippingAndPaymentInfo").click(function(){
		//If Express checkout check - Save address to profile also need to be checked
		if($("#saveDefaultShippingAndPaymentInfo").is(":checked")) {
			$("#saveCreditCardInfo").attr("checked", true);
			$($("#saveCreditCardInfo").parent().children()[0]).removeClass("ui-checkbox-off").addClass("ui-checkbox-on");
			$($("#saveCreditCardInfo").parent().children()[0]).find(".ui-icon").removeClass("ui-icon-checkbox-on").removeClass("ui-icon-checkbox-off").addClass("ui-icon-checkbox-on");
		} 
	});

}		
$(document).ready(function() {
	$('#jcp_rw_enroll_terms').hide();
	$('#jcp_rw_enroll_me').click(function() {
		//canada spam compilance
        CACompliance('<dsp:valueof bean="ShoppingCart.last.email" />','<c:out value="${isAntiSpamBillingAddress}"/>','<c:out value="${antiSpamEmailAddressSuffixes}"/>');
		if($('#jcp_rw_enroll_me').is(':checked')) {
		$('#rewardsTandC').attr("disabled", true);
			$('#jcp_rw_enroll_terms').show();
			rewardsSignUpCoreMetrics("guest");
			/* document.getElementById("rewardsTandC").checked = true; */
			resizeModalHeight();
			
		} else {
			$('#jcp_rw_enroll_terms').hide();
			resizeModalHeight();
		}
	});
	initExpressCheckoutShippingCheckboxes();
});

$('#form_wrapper1').ready(function(){
	jcp.checkout.toolTipForCheckOut();	
	$("#a1_up_express").tooltip();
})

</script>
<script type="text/javascript">
	$(function(){
			GetActiveMessages();
	});
/*---------------------------------------Password Enhancement----------------------------------------------------	*/
	document.getElementById("password").addEventListener("keyup", passwordDeltaErrors);
	document.getElementById("password").addEventListener("focus", passwordDeltaErrors);
	$("#password").focus(function (e) {
		$(".info_box").show();
	});
	$("#password").focusout(function (e) {
		$(".info_box").hide();
	});
	
	var newImg = "icon-show-password.png";
	function changeImage() {
		var tag = document.getElementById('password');
		var tag2 = document.getElementById('confpasswd');
	  if ( newImg == "icon-show-password.png" ) {
		document.images["hideShowPasswd"].src = "/cam/images/icon-hide-password.png";
	    tag.setAttribute('type', 'text');
	    tag2.setAttribute('type', 'text');
	    document.images["hideShowPasswd"].alt = "hide";
	    newImg  = "icon-hide-password.png";
	  }
	  else {
	    document.images["hideShowPasswd"].src = "/cam/images/icon-show-password.png";
	    tag.setAttribute('type', 'password');
	    tag2.setAttribute('type', 'password');
	    document.images["hideShowPasswd"].alt = "show";
	    newImg  = "icon-show-password.png";
	  }
	}
	
	function passwordDeltaErrors(){
		var val=document.getElementById("password").value;
		var length = charLength(val);
			var lower = lowerCase(val);
			var upper = upperCase(val);
			var number=number(val);
			var spaces = noSpaces(val);
			var special = false;
			/* if password has space then don't make string
			   "Must contain a number or special character." green. */
			if(!spaces){
				special = specialChar(val);	
			}
			var pass_strength=passStrength(val);
			var nameEmail = nameEmail(val);	
			var consecutive = consecutiveChars(val);
			var jcp = jcpString(val);
			var res=message(nameEmail,consecutive,spaces,jcp);
		
			function charLength(val){
				if(val.length < 8) {
					$(".minReq .validation_length").css("visibility", "hidden");
					$("#length").css("color", "");
					return false;
				}else if(val.length==8){
					$(".minReq .validation_length").css("visibility", "visible");
					$("#length").css("color", "#008a44");
					return true;
				}else if(val.length > 8 && val.length <=12) {
					$(".minReq .validation_length").css("visibility", "visible");
					$("#length").css("color", "#008a44");
					return true;
				}else if(val.length >=13 && val.length <=16) {
					$(".minReq .validation_length").css("visibility", "visible");
					$("#length").css("color", "#008a44");
					return true;
				}  
				else {
					$(".minReq .validation_length").css("visibility", "hidden");
					$("#length").css("color", "");
					return false;
				}
			   }
	
			function lowerCase(val) {
	            var regex = /^(?=.*[a-z]).+$/;
	            if (regex.test(val)) {
					$(".minReq .validation_lowercase").css("visibility", "visible");
					$("#lower").css("color", "#008a44");
	                return true;
	            }else{
					$(".minReq .validation_lowercase").css("visibility", "hidden");
					$("#lower").css("color", "");
	                return false;
	            }
	        }
	
		function upperCase(val) {
	            var regex = /^(?=.*[A-Z]).+$/;
	            if (regex.test(val)) {
					$(".minReq .validation_uppercase").css("visibility", "visible");
					$("#upper").css("color", "#008a44");
	                return true;
	            }else{
					$(".minReq .validation_uppercase").css("visibility", "hidden");
					$("#upper").css("color", "");
	                return false;
	            }
	        }
	
			function number(val) {
	            var regex = /\d/;
	            if (regex.test(val)) {
					$(".minReq .validation_numeric").css("visibility", "visible");
					$("#number").css("color", "#008a44");
	                return true;
	            }else{
					$(".minReq .validation_numeric").css("visibility", "hidden");
					$("#number").css("color", "");
	                return false;
	            }
	        }
	
		function nameEmail(val) {
				var fname=document.getElementById("firstName").value;
				var lname=document.getElementById("lastName").value;
				var emailid=document.getElementById("emailAddress").value;
				var fnameEq =false;
				var lnameEq =false;
				var emailEq =false;
				
				if(!fname.trim()){
					// is empty or whitespace
				}else{
                    fnameEq = (val.toUpperCase().indexOf(fname.toUpperCase()) != -1);
				}
				if(!lname.trim()){
					// is empty or whitespace
				}else{
                    lnameEq = (val.toUpperCase().indexOf(lname.toUpperCase()) != -1);
				}
				if(!emailid.trim()){
					// is empty or whitespace
				}else{
                    emailEq = (val.toUpperCase().indexOf(emailid.toUpperCase()) != -1);
				}
							
	        	if(fnameEq || lnameEq || emailEq){
					$(".notInclude .validation_nameemail").css("visibility", "visible");
					$("#email").css("color", "#cc0000");
	        		return true;
	        	}else{
					$(".notInclude .validation_nameemail").css("visibility", "hidden");
					$("#email").css("color", "");
	        		return false;
	        	} 
	        }
			
			function specialChar(val) {
	            var regex = /^(?=.*[0-9_\W]).+$/;
	            if (regex.test(val)) { 
	            	return true;
	            }else{ 
	            	return false;
	            }
	        }
	
			function noSpaces(val) {
	        	var patt=/\s/g; 
	        	if((patt.test(val))){
					$(".notInclude .validation_spaces").css("visibility", "visible");
					$("#space").css("color", "#cc0000");
	        		return true;
	        	}else{
					$(".notInclude .validation_spaces").css("visibility", "hidden");
					$("#space").css("color", "");
	        		return false;
	        		}
	        	}
	        
	        function consecutiveChars(val) {
	        	var patt=/(.)\1\1/; 
	        	if(patt.test(val)){
					$(".notInclude .validation_consecutive").css("visibility", "visible");
					$("#consecutive").css("color", "#cc0000");
	        		return true;
	        	}else{
					$(".notInclude .validation_consecutive").css("visibility", "hidden");
					$("#consecutive").css("color", "");
	        		return false;
	        	}
	        }
	        
	        function jcpString(val) {	        	
	        	if(val.match(/jcp/i)){
					$(".notInclude .validation_jcp").css("visibility", "visible");
					$("#jcp").css("color", "#cc0000");
	        		return true;
	        	}else if(val.match(/jcpenney/i)){
					$(".notInclude .validation_jcp").css("visibility", "visible");
					$("#jcp").css("color", "#cc0000");
	        		return true;
	        	}else{
					$(".notInclude .validation_jcp").css("visibility", "hidden");
					$("#jcp").css("color", "");
	        		return false;
	        	}
	        }	
    
	        if (nameEmail ||spaces || consecutive || jcp) {
				   document.querySelector("#password").style.border="1px solid #cc0000";
					$(".exclamation").css("visibility", "visible");
	        }else{
				   document.querySelector("#password").style.border="";
					$(".exclamation").css("visibility", "hidden");
	        }
	        
		   if (length && lower && upper && number && !nameEmail && !spaces && !consecutive && !jcp) {
	        	$(".pwd_strength span").css("visibility", "visible");
	        	$(".hr_line .pwdstrength").css("visibility", "visible");				
			}else{
				$(".pwd_strength span").css("visibility", "hidden");
	        	$(".hr_line .pwdstrength").css("visibility", "hidden");	
			}
		   function message(nameEmail,consecutive,spaces,jcp){
				 if (nameEmail || spaces || consecutive || jcp) {
						$(".remove").css("visibility", "visible");
			        }else{
						$(".remove").css("visibility", "hidden");
			        }
				 var errors=[];
				 if(nameEmail)
					{ errors.push("your name or email");
					}
				if(consecutive)
					{ errors.push("consecutive characters");
					}
				if(spaces)
					{ errors.push("spaces");
					}
				if(jcp)
					{ errors.push("jcp or jcpenney");
					}
					$(".errorMessage").text(errors.join(", "));
			}
		
				function passStrength(val) {
					var score =0;
					if(val.length < 8) {
						score+=0;
					}else if(val.length==8){
						score+=1;
					}else if(val.length > 8 && val.length <=12) {
						score+=5;
					}else if(val.length >=13 && val.length <=16) {
						score+=10;
					}  
					else {
						score+=10;
					}
					
					if(val=="")
					{
					$(".validation_uppercase").css("visibility", "hidden");
					$(".validation_lowercase").css("visibility", "hidden");
					$(".validation_numeric").css("visibility", "hidden");
					}
					else{
						for (var i = 0; i < val.length; i++) {	
							var value = val.charAt(i); 
							if(/[a-z]/.test(value)) {
								score+=1;
							}
							else if(/[A-Z]/.test(value)) {
								score+=5;
							}
							else if(/\d/.test(value)) {
								score+=5;
							}
							 else if(/[@#$*_!]/.test(value)){
								score+=5;
							}else if(/\s/.test(value)) {
								score+=0;
							}
						}
					}
		
					var strength_text = $(".pwd_strength span");
					var strength_bar = $(".hr_line .pwdstrength");
					
					if (score <= 0) {
						strength_bar.css({"background-color": "#666666", "width": "0%"});
						strength_text.css("color", "#666666").text("");
					} else if (score >0 && score <=27) {
						strength_bar.css({"background-color": "#ade143", "width": "33%"});
						strength_text.css("color", "#666666").text("good");
					} 
					 else if (score >27 && score <=56) {
						strength_bar.css({"background-color": "#23a31d", "width": "66%"});
						strength_text.css("color", "#666666").text("strong");
					} else if (score >56) {
						strength_bar.css({"background-color": "#0d763f", "width": "100%"});
						strength_text.css("color", "#666666").text("very strong");
						strength_text.show();
						return true;
					}
				
				strength_text.show();
				return false;
			}
	
		
	}
</script>
 </body>
 </html>
</dsp:page>