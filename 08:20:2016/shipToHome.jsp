<%--
* -----------------------------------------------------------------------
* Copyright 2011, jcpenney Co., Inc.  All Rights Reserved.
*
* Reproduction or use of this file without explicit
* written consent is prohibited.
* Created by: cgeddam1
*
* Created on: June 296, 2016
* ----------------------------------------------------------------
--%>
<%-- ========================================= Description
/**
 * This page is used to display
 *  1.This Jsp contains all the read only pages
 * Includes Header and Footer JSPFs.
 *
 * @author cgeddam1
 * @version 0.1 26-June-2016
 **/
--%>
<%@ taglib uri="dsp" prefix="dsp" %>
<%@ taglib uri="dspel" prefix="dspel" %>
<%@ taglib uri="c" prefix="c"  %>
<%@ taglib uri="fmt" prefix="fmt" %>
<%@ page pageEncoding="UTF-8" %>

<dsp:page xml="true">
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/commerce/order/purchase/CreateHardgoodShippingGroupFormHandler"/>
<dsp:importbean bean="/atg/dynamo/droplet/RQLQueryForEach"/>
<dsp:importbean bean="/com/jcpenney/ocs/OCSApplicationConfiguration"/>
<dsp:importbean bean="/com/jcpenney/core/ApplicationConfiguration"/>
<dsp:getvalueof var="giftWrapEnabled" bean="ApplicationConfiguration.giftWrapEnabled"/>
<dsp:getvalueof var="profileTransientValue" bean="Profile.transient" />
<dsp:getvalueof var="displayShippingMethodsMsgInSeconds" bean="ApplicationConfiguration.displayShippingMethodsMsgInSeconds" />
<dsp:getvalueof var="contextroot" vartype="java.lang.String" bean="/OriginatingRequest.contextPath"/>
<dsp:importbean bean="/com/jcpenney/giftregistry/droplet/GiftRegistryFetcherDroplet"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
<dsp:importbean bean="/com/jcpenney/giftregistry/droplet/DirectCheckShippingAddressDroplet"/>
<dsp:importbean bean="/com/jcpenney/dp/core/droplet/GetBillingAddressFromShippingGroup"/>
<fmt:setBundle basename="com/jcpenney/dp/core/order/CheckoutResources" var="checkoutResources" scope="application"/>


<dsp:droplet name="/atg/dynamo/droplet/ForEach">
<dsp:param name="array" bean="ShoppingCart.current.shippingGroups" />
<dsp:oparam name="output">
<dsp:droplet name="Switch">
	<dsp:param name="value" param="element.shippingGroupClassType"/>
	<dsp:oparam name="hardgoodShippingGroup">
	<dsp:getvalueof var="countryCode" param="element.shippingAddress.country"/>
			<c:if test="${countryCode != null }">
				<c:set var="countryCodeUS" value="${countryCode}"/>
			</c:if>
	<dsp:getvalueof var="stateCode" param="element.shippingAddress.state"/>
	 <dsp:getvalueof var="isHardgoodShippingGroup" value="true" />
	</dsp:oparam>
</dsp:droplet>
</dsp:oparam>
</dsp:droplet>
<c:if test="${isHardgoodShippingGroup eq true}">
 <dsp:droplet name="GetBillingAddressFromShippingGroup">
		<dsp:param name="order" bean="ShoppingCart.current" />
		<dsp:oparam name="output">
		  <dspel:getvalueof var="firstName" param="firstName" />
		  <dspel:getvalueof var="lastName" param="lastName" />
		  <dspel:getvalueof var="isRegistrantAddr" param="isRegistrantAddr" />
			
		</dsp:oparam>
 </dsp:droplet>
 </c:if>
<dsp:droplet name="/atg/dynamo/droplet/IsEmpty">
		<dsp:param name="value" param="currentOrder" />
		<dsp:oparam name="true">
			<dsp:getvalueof var="order" value="current" />
		</dsp:oparam>
		<dsp:oparam name="false">
			<dsp:getvalueof var="order" value="last" />
		</dsp:oparam>
	</dsp:droplet>
	<%-- Configurable messages for billing address page --%>    
<dsp:include page="/jsp/global/configurableMessage.jsp">
		<dsp:param name="page" value="cam"/>
		<dsp:param name="keys" value="keyProfileZipCodeInvalid"/>
</dsp:include>

<dsp:droplet name="/com/jcpenney/largeapp/droplet/LargeAppOrderCheckDroplet">
	<dsp:param name="order" bean="ShoppingCart.current"/>
	<dsp:oparam name="output">
		<dsp:getvalueof var="includeLargeAppItem" param="includeLargeAppItem" />
		<dsp:getvalueof var="largeAppZipCode" param="largeAppZipCode" />
		<dsp:getvalueof var="largeAppCommerceId" param="largeAppCommerceId" />
	</dsp:oparam>
</dsp:droplet>
<dsp:getvalueof var="enableLargeAppShipZipValidation" bean="/com/jcpenney/core/ApplicationConfiguration.enableLargeAppShipZipValidation" />
<c:if test="${enableLargeAppShipZipValidation && includeLargeAppItem != null && includeLargeAppItem == 'true' }">
<div id="largeAppZipCode" class="hide_display"><dsp:valueof bean="ShoppingCart.current.largeApplianceZipCode"/></div>
</c:if>
<span id="includeLargeAppItem" style="display:none"><c:out value="${includeLargeAppItem}"/></span>
<dsp:droplet name="GiftRegistryFetcherDroplet">
 <dsp:param name="order" bean="ShoppingCart.current"/>
 <dsp:param name="pageflow" value="checkoutshipping"/>
	<dsp:oparam name="output">
		<dsp:getvalueof var="giftRegistryCount" param="giftRegistryCount"/>
	</dsp:oparam>
</dsp:droplet>
						
<c:if test="${not profileTransientValue}">	
	<dsp:include page="savedShippingAddress.jsp">
	<dsp:param name="giftRegistryCount" value="${giftRegistryCount}"/>
	</dsp:include>
	
	<c:if test="${includeLargeAppItem != null && includeLargeAppItem == 'true'}">
		<dspel:form id="deliveryForm"  method="post" iclass="jcp-form"  action="/dotcom/jsp/checkout/secure/checkoutsimplified/checkout.jsp">
		<div class="largeapp" id="largeapp">
		<%@ include file="/jsp/checkout/secure/checkoutsimplified/shipping/largeAppDeliveryDateSelectCalendarDisplay.jsp" %> 
		</div>
		</dspel:form>
	</c:if>
</c:if>

<%--start new shipping address form for anonymous user --%>
<c:if test="${profileTransientValue}">	
   <c:choose>
	<c:when test="${giftRegistryCount == 1}">	      
      <dsp:include page="/jsp/checkout/secure/checkoutsimplified/shipping/giftRegistrySavedShippingAddress.jsp">
	    <dsp:param name="giftRegistryCount" value="${giftRegistryCount}"/>
	  </dsp:include>
	</c:when>
	<c:otherwise>
	   <dsp:include page="/jsp/checkout/secure/checkoutsimplified/shipping/shiptohome/addGuestShippingAddress.jsp"/>
	</c:otherwise>
   </c:choose>
</c:if>
	<c:choose>
	<c:when test="${includeLargeAppItem != null && includeLargeAppItem == 'true'}">
	<div id ="showShippingMethod" class="inner-container ng-hide">       
      </div>
	</c:when>
	<c:otherwise>
	
      <div class="inner-container">
        <div class="subhead">
          <h2>
          <img src="/dotcom/images/track-order-icon.svg" alt="shipping method">
          Shipping method
          </h2>
          <div class="loading-spinner small"></div>
          <p id="emptyShippingMethod">Enter your address to view shipping methods.</p>
        </div>
      </div>
	
      <div id ="showShippingMethod" class="inner-container ng-hide">       
      </div>

      <div class="clear"></div>
      <br>
   
    </c:otherwise>
	</c:choose>
<script type="text/javascript">
$(document).ready(function(){
	profileValue = "<dspel:valueof  value='${profileTransientValue}'/>";
	var isRegistrantAddr = '${isRegistrantAddr}';
	var hiddenPhoneNumber = $('#hdnPhoneNumber').val();
	var streetAddress1 = $('#streetAddress1').val();
	if(hiddenPhoneNumber != ""){
		$('#phoneNumber').val(hiddenPhoneNumber);
	}
    if ($('#largeAppZipCode') != undefined && $('#largeAppZipCode').html() != null) {
		zipProc.validateZip($('#largeAppZipCode').html(),'usSpecific #cityBlk #cityName','usSpecific #stateSelectBlk #selectState','usSpecific #zipBlk #zipCode','largeAppAddAddress');
		zipProc.validateZip($('#largeAppZipCode').html(),'addUsSpecific #cityBlk #cityName','addUsSpecific #stateSelectBlk #selectState','addUsSpecific #zipBlk #zipCode','addShippingAddress');
		setTimeout(function(){$('#firstName').focus()}, 1000);
	}
	var grId = $('#giftRegistryId').val();
	if (grId != '' && grId != "undefined" && grId != null ) {
	if (isRegistrantAddr == 'false' && streetAddress1 != '' && streetAddress1 != "undefined" && streetAddress1 != null) {
	   $('.giftregAddressBlk').removeClass('saved-address-blk-selected');
		$('.giftregAddressBlk').removeClass('saved-address-blk-active');
		$('#add-new-guest-address-blk').addClass('saved-address-blk-selected');
		$('#add-new-guest-address-blk').addClass('saved-address-blk-active');		
		$('#add-guest-shipping-address-blk').show();
		$('#add-address-ship-other-country-extra').show();
		$('#gift-registry-address-blk').bind('click', function(event) {
		$('.giftregAddressBlk').removeClass('saved-address-blk-selected');
		$('.giftregAddressBlk').removeClass('saved-address-blk-active');
		$('#gift-registry-address-blk').addClass('saved-address-blk-active');		
		$('#add-guest-shipping-address-blk').hide();
		$("#shippingGroupAddress").val(grId);
		 var options = {
			success : handleShippingGroupChangeSuccess,
			error : shippingGroupChangedError
		};
		$('#giftRegistry_shipping').ajaxSubmit(options);
	})
	$('#add-new-guest-address-blk').bind('click', function(event) {
		onSelectChange();
		$('.giftregAddressBlk').removeClass('saved-address-blk-selected');
		$('.giftregAddressBlk').removeClass('saved-address-blk-active');
		$('#add-new-guest-address-blk').addClass('saved-address-blk-selected');
		$('#add-new-guest-address-blk').addClass('saved-address-blk-active');		
		$('#add-guest-shipping-address-blk').show();
	})
	} else {
	    $("#shippingGroupAddress").val(grId);
		 loadGiftRegistryShippingDetails();
	}
		
    }
});
var shippingTimeDiffLimit = '<c:out value="${displayShippingMethodsMsgInSeconds}"/>';
</script>
</dsp:page>
