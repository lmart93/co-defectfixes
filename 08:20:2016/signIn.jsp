<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="dsp" prefix="dsp" %>   
<%@ taglib uri="c" prefix="c"  %>   
<%@ taglib uri="dspel" prefix="dspel" %>     
<%@ taglib uri="fmt" prefix="fmt" %>
<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache, no-store, must-revalidate" );
   response.setDateHeader( "Expires", 0 ); 
%>
<dsp:page xml="true"> 
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/com/jcpenney/profile/bean/UserSessionBean"/>
<dsp:importbean bean="/com/jcpenney/core/ApplicationConfiguration"/>
<dsp:importbean bean="/com/jcpenney/coremetrics/droplet/CoremetricsInputParameterMapDroplet"/>
<dsp:importbean bean="/com/jcpenney/profile/ProfileApplicationConfiguration"/>
<dsp:importbean bean="/com/jcpenney/core/order/formhandler/GuestCheckoutFormHandler"/>
<dsp:getvalueof var="camContextPath" bean="/com/jcpenney/core/util/JSPInclude.camContextPath"/>
<dsp:getvalueof var="enableConfigurableMessage" bean="ApplicationConfiguration.enableConfigurableMessage"/>
<dsp:droplet name="/atg/dynamo/droplet/IsEmpty">
    <dsp:param param="serverName" name="value"/>                              
    <dsp:oparam name="false">
        <dsp:getvalueof var="serverName" vartype="java.lang.String" param="serverName"/>
    </dsp:oparam>                                           
    <dsp:oparam name="true">
        <dsp:getvalueof var="serverName" vartype="java.lang.String" bean="/OriginatingRequest.serverName"/>
    </dsp:oparam>
</dsp:droplet>
<dsp:getvalueof var="securePort" bean="/com/jcpenney/core/pipeline/ProtocolChangeServlet.securePort"/>
<dsp:getvalueof var="insecurePort" bean="/com/jcpenney/core/pipeline/ProtocolChangeServlet.InsecurePort"/>
<dsp:getvalueof var="protocolType" bean="/com/jcpenney/core/pipeline/ProtocolChangeServlet.protocolType"/>
<input type="hidden" id="camContextPathId" value="<c:out value="${camContextPath}"/>" />
	<dsp:droplet name="/com/jcpenney/pci/droplet/DetectPCIViolation">
		<dsp:param name="stringParameters" value="sessionExpired,fromLoginCoreMetric"/>
		<dsp:oparam name="success">
		</dsp:oparam>
	</dsp:droplet>
<dsp:getvalueof var="sessionExpired" param="sessionExpired"/>
<c:if test="${ sessionExpired == null }">
<dsp:getvalueof var="sessionExpired" value="false"/>
</c:if>

<dsp:setvalue bean="ProfileFormHandler.extractDefaultValuesFromProfile" value="false"/>
<dsp:getvalueof var="contextroot" vartype="java.lang.String" bean="/OriginatingRequest.contextPath"/>
<dsp:getvalueof var="submissionContainerPageURL" bean="UserSessionBean.submissionContainerPageURL"/>
<dsp:getvalueof var="queryString" bean="/OriginatingRequest.queryString"/>
<dsp:getvalueof var="captchaEnabled" vartype="java.lang.Boolean" bean="ProfileApplicationConfiguration.captchaEnabled"/>
<dsp:getvalueof var="invalidEmailAttempts" vartype="java.lang.Integer" bean="UserSessionBean.invalidEmailAttempts"/>
<dsp:getvalueof bean='/com/jcpenney/profile/bean/UserSessionBean.latestVisitedURL' var="loginSuccessURL"/>
<dsp:getvalueof var="contextroot" vartype="string" bean="/OriginatingRequest.contextPath"/>
<dsp:getvalueof var="camContextPath" bean="/com/jcpenney/core/util/JSPInclude.camContextPath"/>
<dsp:getvalueof var="enable2016S9DPXVI12799UpgradeReCaptcha" bean="ApplicationConfiguration.enable2016S9DPXVI12799UpgradeReCaptcha"/>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>JCPenney | Login</title>
<c:if test="${ profileBundle == null }">
<fmt:setBundle basename="com/jcpenney/dp/core/profile/ProfileResources" var="profileBundle" scope="application"/>
</c:if>
<%-- DPXVI-1453 - New jspf file for sign in page removing the unnecessary includes --%>
<%@ include file="/jsp/checkout/secure/signin/signInJsCss.jspf"%>	
<dsp:droplet name="Switch">
	<dsp:param name="value" bean="ApplicationConfiguration.gruntEnable" />
	<dsp:oparam name="true">
		<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/bundleJS/dotcom_signIn_global_jsBundle.js?v=<dsp:valueof bean="ApplicationConfiguration.releaseVersion"/>"></script>
	</dsp:oparam>
	<dsp:oparam name="false">
		<%-- DPXVI-1453 - JS files moved to the bottom of the page to improve loading time --%>
   	</dsp:oparam>
</dsp:droplet>

	<dsp:droplet name="/atg/dynamo/droplet/RQLQueryForEach">
		<dsp:param name="queryRQL" value="messageCode=\"keySuppliedPasswordInvalid\" AND messagePageName=\"cam\" " />
		<dsp:param name="repository" value="/com/jcpenney/core/repository/JcpMessageRepository" />
		<dsp:param name="itemDescriptor" value="jcpconfigurablemessage" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="keySuppliedPasswordInvalid" param="element.messageDescription"/>
		</dsp:oparam>
	</dsp:droplet>

	<dsp:droplet name="/com/jcpenney/core/droplet/FetchActiveMessagesDroplet">
	<dsp:param name="page" value="cam"/>
	<dsp:param name="keys" value="keyProfileEmailMissing,keyProfileEmailInvalid,keyProfilePasswordMissing,keySuppliedPasswordInvalid,keyProfileAccountToBeLockedLogin,keyProfileAccountLocked"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="activeMessagesJson" param="activeMessagesJson"/>
		</dsp:oparam>
	</dsp:droplet>
	<script type="text/javascript">
		function GetActiveMessages() {
			var src = ${activeMessagesJson};
			for (i in src) {
				var key = src[i].key;
				var message = src[i].message;
				if(document.getElementById(key)){
					document.getElementById(key).innerHTML = message;							
				}
			}
		}
	</script>
<script type="text/javascript">
	//redirect to shopping back if browser back button is clicked
	if(${sessionScope.enterChkoutFlow})	{
		window.location = '/dotcom/jsp/cart/viewShoppingBag.jsp';
	}
</script>
<script >
$(document).ready(function() {
	signInCaptchaCount = <dsp:valueof bean="ProfileApplicationConfiguration.captchaEnableCount"/>;
	enable2016S9DPXVI12799UpgradeReCaptcha = '<c:out value="${enable2016S9DPXVI12799UpgradeReCaptcha}" />';
	});
</script>
</head>
<body id="bodyWrapper" onload="clearField();">
<c:set var="onSignInPage" value="true" scope="request"/>
<%--<%@ include file="/jsp/global/signInHeader.jspf"%>--%>
<div id="coSignIn" class="container_main checkout_flow">
	<div class="div_shadow">
    <div class="container_signIn">
        <%@ include file="/jsp/global/checkoutSignInHeader.jspf"%>
		<%@ include file="/jsp/global/topBanner.jspf"%>
        <div class="content mrgt10 padb50 mrgb10 checkoutSignInPage seperatePage" id="checkoutSignInPage">
        <div class="checkout_page_container simplify-co">
            <div class="flt_wdt common_header padt0">
            <h2 class="flt_lft login_title"><img alt="SIGN IN" src='<c:out value="${contextroot}"/>/images/spacer.gif' /><span>SIGN IN</span></h2>
            <h2 class="flt_rgt secure_checkout">
            		<img alt="Secure checkout" src='<c:out value="${contextroot}"/>/images/secure-lock.svg' />
            		<span>SECURE CHECKOUT</span>
            </h2>
            </div>
            <div class="sign_in_container fltclr padt10">
                <div class="flt_wdt mrgt5">
                
                    <div class="flt_lft return_cust_blk">
                    	<h2 class="returning_cutomer_title">Returning customers</h2>                        
                            <dsp:form name="login" id="loginAjax" formid="loginAjax" iclass="jcp_form" action="signIn.jsp" method="post">
                            	<fieldset>
                                
                                <div id="errorsLogin" class="dynamic_error_msgs mrgr20 hide_display" >
								<div class="flt_lft mrgr15 exclamation_icn"><img src="<c:out value="${contextroot}"/>/images/spacer.gif" alt="attention" /><span>attention</span></div>
                                    <div class="float_fix"> <span class="disp_blk">Please correct the errors listed in red below:</span>
                                    <ul>
                                        <li id="emailRequiredLogin" style="display:none"><div id="keyProfileEmailMissing"></div></li>
											<li id="emailInvalid" style="display:none"><div id="keyProfileEmailInvalid"></div></li>
											<li id="passwordRequiredLogin" style="display:none"><div id="keyProfilePasswordMissing"></div></li>
										    <c:choose>
												<c:when test="${enableConfigurableMessage || enableConfigurableMessage == 'true' }">
													<li id="suppliedPasswordInvalid" style="display:none">${keySuppliedPasswordInvalid}</li>
												</c:when>
												<c:otherwise>
													<li id="suppliedPasswordInvalid" style="display:none"><fmt:message key="keySuppliedPasswordInvalid" bundle="${ profileBundle }"/></li>
												</c:otherwise>
											</c:choose>
                                    </ul>
									</div>
                                </div>
								<c:set var="pswdErrorMsg" value="The supplied password was invalid."></c:set>
								<c:set var="pswdAttemptErrorMsg" value="You have only one more attempt to login before the account gets locked."></c:set>
								<c:set var="pswdLockErrorMsg" value="For your security, we have locked your account due to 5 unsuccessful sign in attempts. An email has been sent to the following email address with a temporary password which you can use to unlock your account, and then reset your password."></c:set>
								<dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
								<dsp:param bean="ProfileFormHandler.formExceptions" name="exceptions"/>
								<dsp:oparam name="output">
									<dsp:getvalueof var="message" vartype="java.lang.String" param="message"/>
									<c:choose>
										<c:when test="${message == pswdErrorMsg}">
											<c:set var="hasPasswordError" value="true"/>
										</c:when>
										<c:when test="${message == pswdAttemptErrorMsg}">
											<c:set var="hasPasswordAttempts" value="true"/>
										</c:when>
										<c:when test="${message == pswdLockErrorMsg}">
											<c:set var="hasAccountLocked" value="true"/>
										</c:when>
										<c:otherwise>
										</c:otherwise>
									</c:choose>
								</dsp:oparam>
								</dsp:droplet>
								<dsp:droplet name="Switch">
									<dsp:param bean="ProfileFormHandler.formError" name="value"/>
									<dsp:oparam name="true">
									<div id="serverErrors" class="dynamic_error_msgs mrgr20">
									<div class="flt_lft mrgr15 exclamation_icn"><img src="<c:out value="${contextroot}"/>/images/spacer.gif" alt="attention" /><span>attention</span></div>
										<div class="float_fix"> <span class="disp_blk">Please correct the errors listed in red below:</span>
											<c:choose>
												<c:when test="${hasPasswordError == 'true' && hasPasswordAttempts == 'true'}">											
													<ul>
														<li><div id="keyProfileAccountToBeLockedLogin"></div></li>
														<li><div id="keySuppliedPasswordInvalid"></div></li>
													</ul>
												</c:when>
												<c:when test="${ hasPasswordError == 'true' && hasAccountLocked == 'true'}">
													<ul>
														<li><div id="keyProfileAccountLocked"></div></li>
													</ul>
												</c:when>
												<c:when test="${ hasPasswordError == 'true'}">
													<ul>
														<li><div id="keySuppliedPasswordInvalid"></div></li>
													</ul>
												</c:when>
												<c:when test="${ hasAccountLocked == 'true'}">
													<ul>
														<li><div id="keyProfileAccountLocked"></div></li>
													</ul>
												</c:when>
												<c:otherwise>
													<ul>
														<li><div id="keySuppliedPasswordInvalid"></div></li>
													</ul>
												</c:otherwise>
											</c:choose>
										</div>
									</div>
									</dsp:oparam>
								</dsp:droplet>
                                <div class="padt30">
                                    <c:set var="cssInputEmail" value="input_txt input_long_txt"/>
									<c:set var="cssLabelEmail" value="noblock"/>
									<dsp:droplet name="IsEmpty">
										<dsp:param bean="ProfileFormHandler.propertyExceptions.login.errorCode" name="value"/>							  
										<dsp:oparam name="false">
											<c:set var="cssInputEmail" value="input_txt input_long_txt input_error_txt" />
											<c:set var="cssLabelEmail" value="noblock error" />		
										</dsp:oparam>															
									</dsp:droplet>				
									<label id="emailidLabelLogin">Email</label>
									<dspel:input type="text" iclass="${cssInputEmail}" onkeydown="return hotkey(event,this.form.id);" id="emailidLogin" maxlength="70" onclick="clearFieldStyle(this, 'emailidLabelLogin', 'input_txt input_long_txt', '','false')" onfocus="clearFieldStyle(this, 'emailidLabelLogin', 'input_txt input_long_txt', '','false')" bean="ProfileFormHandler.value.login"/>
								</div>
                                
                                <div class="padt10">
                                <c:set var="cssInputPwd" value="input_txt input_long_txt "/>
								<c:set var="cssLabelPwd" value="noblock"/>
								<dsp:droplet name="IsEmpty">
										<dsp:param bean="ProfileFormHandler.propertyExceptions.password.errorCode" name="value"/>							  
										<dsp:oparam name="false">
											<c:set var="cssInputPwd" value="input_txt input_long_txt input_error_txt" />
											<c:set var="cssLabelPwd" value="noblock error" />		
										</dsp:oparam>															
									</dsp:droplet>	
									<label id="mypasswdLabelLogin">Password</label>
									<dspel:input type="password" iclass="${cssInputPwd}" onkeydown="return hotkey(event,this.form.id);" id="mypasswdLogin" value="" onclick="clearFieldStyle(this, 'mypasswdLabelLogin', 'input_txt input_long_txt', '','true')" onfocus="clearFieldStyle(this, 'mypasswdLabelLogin', 'input_txt input_long_txt', '','true')" bean="ProfileFormHandler.value.password"/>
								</div>	
                                <div class="remember-me-container">
							        <dspel:input type="checkbox" bean="ProfileFormHandler.rememberMe" id="rememberMe" iclass="flt_lft mrgr5 mrgt3"/>
							        <dspel:input type="hidden" bean="ProfileFormHandler.rememberMe" id="rememberMeHiddenId"/>
							        <span>remember me</span>
						        </div>
                                <p class="forgot_pwd"><a href="javascript:void(forgotPassword);" onclick="fnInitiateModal('<c:out value="${protocolType}"/>://<c:out value="${serverName}"/>:<c:out value="${securePort}"/><c:out value="${camContextPath}"/>/jsp/profile/secure/signinModal.jsp?displayModal=forgotPassword&amp;ref=closeModal', window.location.href);" title="forgot password?">forgot password</a></p>
								<div id="captchaSection" style="display: none;">
                                    <c:choose>
									<c:when test="${enable2016S9DPXVI12799UpgradeReCaptcha || enable2016S9DPXVI12799UpgradeReCaptcha == 'true'}">
										<%@ include file="/jsp/profile/secure/reCaptcha.jspf" %>
                                    </c:when>
									<c:otherwise>
										<%@ include file="/jsp/profile/secure/captcha.jspf" %>
									</c:otherwise>
								</c:choose>
                                </div>
                                <div class="mrgt20">
										<div class="sign_in_btn_returning_container_checkout btn_input_holder"> 
											<span>
			                                	<dspel:input type="hidden" bean="ProfileFormHandler.loginSuccessURL" value="${contextroot}/jsp/profile/secure/ajax/signinJson.jsp" />
												<dspel:input type="hidden" bean="ProfileFormHandler.loginErrorURL" value="${contextroot}/jsp/profile/secure/ajax/signinJson.jsp" />
			                                	<dsp:input type="hidden" bean="ProfileFormHandler.userLoginFromCheckout" value="1" />
												<dsp:input id="hdnSubmit" type="hidden" value="login" bean="ProfileFormHandler.login" />
			                                    <input type="button" class="red-Button" title="Sign In" value="sign in" onclick="return validateMultipleSubmit(this.form.id);"/>
											</span> 
										</div>
								</div>
			                                <input type="hidden" name="coming_page" id="coming_page" value="landing page" />
								</fieldset>
                            </dsp:form>                        
                    </div>
			               
			               	<ul class="flt_lft login_benefits">
								<li><span class="login-icon"><img border="0" src='<c:out value="${contextroot}"/>/images/icon-express-checkout.svg' /></span><span>Checkout faster with Express Checkout</span></li>
								<li><span class="login-icon"><img border="0" src='<c:out value="${contextroot}"/>/images/list.svg' /></span><span>Easy access to order history</span></li>
								<li><span class="login-icon"><img border="0" src='<c:out value="${contextroot}"/>/images/icon-mobile.svg' /></span><span>Access your account from any device</span></li>
								<li><span class="login-icon"><img border="0" src='<c:out value="${contextroot}"/>/images/icon-coupon-rewards.svg' /></span><span>Earn & redeem JCPenney Rewards</span></li>
								<li><span class="login-icon"><img border="0" src='<c:out value="${contextroot}"/>/images/coupon-fill.svg' /></span><span>Get coupons and sale notifications</span></li>
							</ul>
			               <img class="flt_lft checkout_separator" border="0" src='<c:out value="${contextroot}"/>/images/divider.png' height="300px">
			                <div class="flt_lft">
			                	<div class="new_cust_blk">
				                      <h2 class="new_cutomer_title">New + guest customers</h2>
				                      <div id="guestCheckoutErrors" class="dynamic_error_msgs mrgr20 hidden"></div>
										<div class="guest_customer_intro_text">
											You can still create a JCPenney.com account and set up Express Checkout.
										</div>
									<dspel:form id="checkoutGuestLogin" iclass="jcp_form" action="${contextroot}/jsp/checkout/secure/signin/signIn.jsp" method="post">
	                                 	<input type="button" class="blue-Button btn_continue_as_guest" value="continue as guest" alt="continue as guest" title="continue as guest" onclick="cmCreateConversionEventTag('Guest Checkout','2','Checkout','','');return validateMultipleSubmit(this.form.id);" />
	                                     <dsp:input id="guestSubmit" type="hidden" bean="GuestCheckoutFormHandler.guestUserCheckout" value="" />
										<input type="text" class="modal_focus_input" onfocus="$('#emailidLogin').focus();" />
	                                </dspel:form>
	                            <%-- <div class="co_createaccount">
		                            <span>Would you like to </span>
									<c:if test="${sessionExpired eq 'true' }">
										<c:choose>
											<c:when test="${!empty submissionContainerPageURL && ((submissionContainerPageURL eq rewardRedirectLink) or (submissionContainerPageURL eq rewardAccountLink)) }">
												<a  href="<c:out value="${protocolType}"/>://<c:out value="${serverName}"/>:<c:out value="${securePort}"/><c:out value="${camContextPath}"/>/jsp/profile/secure/rewardsSignUp.jsp?entry=enroll" id="newUser_Signup">Create an account</a>
											</c:when>
											<c:when test="${!empty fromEvent}">
												<a title="create jcp.com account" href="javascript:void(signUp);" onclick="fnInitiateModal('<c:out value="${protocolType}"/>://<c:out value="${serverName}"/>:<c:out value="${securePort}"/><c:out value="${camContextPath}"/>/jsp/profile/secure/signinModal.jsp?fromBagReward=<c:out value="${fromBagReward}"/>&amp;displayModal=register&amp;userAction=loginPage&amp;fromEvent=<c:out value="${fromEvent}"/>', window.location.href);cmCreateManualPageviewTag('Registration', 'JCP|Accountmanagement',window.location.href,document.referrer);" id="newUser_Signup">Create an account</a>
											</c:when>
											<c:otherwise>
												<a title="create jcp.com account" href="javascript:void(signUp);" onclick="fnInitiateModal('<c:out value="${protocolType}"/>://<c:out value="${serverName}"/>:<c:out value="${securePort}"/><c:out value="${camContextPath}"/>/jsp/profile/secure/signinModal.jsp?fromBagReward=<c:out value="${fromBagReward}"/>&amp;displayModal=register&amp;userAction=loginPage', window.location.href);cmCreateManualPageviewTag('Registration', 'JCP|Accountmanagement',window.location.href,document.referrer);" id="newUser_Signup">Create an account</a>
											</c:otherwise>
										</c:choose>
									  </c:if>
									<c:if test="${sessionExpired eq 'false' }">
										<c:choose>
											<c:when test="${!empty submissionContainerPageURL && ((submissionContainerPageURL eq rewardRedirectLink) or (submissionContainerPageURL eq rewardAccountLink)) }">
												<a  href="<c:out value="${protocolType}"/>://<c:out value="${serverName}"/>:<c:out value="${securePort}"/><c:out value="${camContextPath}"/>/jsp/profile/secure/rewardsSignUp.jsp?entry=enroll" id="newUser_Signup">Create an account</a>
											</c:when>
											<c:when test="${!empty fromEvent}">
												<a  href="javascript:void(signUp);" onclick="fnInitiateModal('<c:out value="${protocolType}"/>://<c:out value="${serverName}"/>:<c:out value="${securePort}"/><c:out value="${camContextPath}"/>/jsp/profile/secure/signinModal.jsp?fromBagReward=<c:out value="${fromBagReward}"/>&amp;displayModal=register&amp;userAction=loginPage&amp;fromEvent=<c:out value="${fromEvent}"/>', window.location.href);cmCreateManualPageviewTag('Registration', 'JCP|Community',window.location.href,document.referrer);" title="create jcp.com account" id="newUser_Signup">Create an account</a>
											</c:when>
											<c:otherwise>
												<a  href="javascript:void(signUp);" onclick="fnInitiateModal('<c:out value="${protocolType}"/>://<c:out value="${serverName}"/>:<c:out value="${securePort}"/><c:out value="${camContextPath}"/>/jsp/profile/secure/signinModal.jsp?fromBagReward=<c:out value="${fromBagReward}"/>&amp;displayModal=register&amp;userAction=loginPage', window.location.href);cmCreateManualPageviewTag('Registration', 'JCP|Community',window.location.href,document.referrer);" title="create jcp.com account" id="newUser_Signup">Create an account</a>
											</c:otherwise>
										</c:choose>
									</c:if>
								</div> --%>                
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <%-- end of content --%>
        <%@ include file="/jsp/global/checkoutFooter.jsp"%>
    </div>
    </div>
</div>
<script type="text/javascript">
//<![CDATA[
	serverName="<dspel:valueof value="${serverName}" />";	
	insecurePort="<dspel:valueof value="${insecurePort}" />";
	parentUrl= 'http://' + serverName + ':' + insecurePort + "/dotcom/jsp/cart/viewShoppingBag.jsp";		
	//]]>	
</script>
<%-- BEGIN COREMETRICS--%>
		<dsp:droplet name="/com/jcpenney/core/droplet/StringUtilsDroplet">
			<dsp:param name="inputString" param="cm_mmc"/>
			<dsp:param name="inputFindTerm" value="-_-"/>
			<dsp:param name="inputReplaceTerm" value="|"/>
				<dsp:oparam name="output">
					<dsp:getvalueof var="jcp_mmc" param="element"/>
				</dsp:oparam>
		</dsp:droplet>
		<dsp:droplet name="Switch">
			<dsp:param name="value" bean="ApplicationConfiguration.coremetricsTagEnabled"/>
			<dsp:oparam name="true">
				<dsp:droplet name="CoremetricsInputParameterMapDroplet">
						<dsp:param name="tagType" value="pageview"/>
						<dsp:param name="pageID" value="Login"/>
						
						<c:if test="${sessionExpired eq 'true' }">
							<dsp:param name="categoryID" value="Accountmanagement"/>
						</c:if>
						<c:if test="${sessionExpired eq 'false' }">
							<dsp:param name="categoryID" value="Community"/>
						</c:if>
						<dspel:param name="cmJCP_MMC" value="${jcp_mmc}"/>
						<dsp:param name="cmJCP_C" param="cmJCP_C"/>
						<dsp:param name="cmJCP_T" param="cmJCP_T"/>
							<dsp:oparam name="output">
								<dsp:getvalueof param="parameterMap" var="parameterMap"/>
							</dsp:oparam>
					</dsp:droplet>
				<dsp:include page="/jsp/global/analyticsTag.jsp">
							<dspel:param name="tagParameter" value="${parameterMap}"/>
				</dsp:include>
				</dsp:oparam>	
		</dsp:droplet>
<%--END COREMETRICS --%>

<%-- DPXVI-1453 - JS files moved to the bottom of the page to improve loading time --%>
<dsp:droplet name="Switch">
	<dsp:param name="value" bean="ApplicationConfiguration.gruntEnable" />
	<dsp:oparam name="false">
		<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/signinModal.js?v=<dsp:valueof bean="/com/jcpenney/core/ApplicationConfiguration.releaseVersion"/>"></script>
		<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/common_validations.js?v=<dsp:valueof bean="/com/jcpenney/core/ApplicationConfiguration.releaseVersion"/>"></script>
		<script type="text/javascript" src="<c:out value="${dotcomContextPath}"/>/js/lib/validate.js?v=<dsp:valueof bean="/com/jcpenney/core/ApplicationConfiguration.releaseVersion"/>"></script>
		<script type="text/javascript" src="<c:out value="${dotcomContextPath}"/>/js/lib/tooltip.js?v=<dsp:valueof bean="/com/jcpenney/core/ApplicationConfiguration.releaseVersion"/>"></script>
		<%--<script type="text/javascript" src="<c:out value="${dotcomContextPath}"/>/js/search.js?v=<dsp:valueof bean="/com/jcpenney/core/ApplicationConfiguration.releaseVersion"/>"></script>--%>
		<script type="text/javascript" src="<c:out value="${dotcomContextPath}"/>/js/lib/jquery-jtemplates.js?v=<dsp:valueof bean="/com/jcpenney/core/ApplicationConfiguration.releaseVersion"/>"></script>
		<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/checkout.js?v=<dsp:valueof bean="/com/jcpenney/core/ApplicationConfiguration.releaseVersion"/>"></script>
		<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/captcha.js?v=<dsp:valueof bean="/com/jcpenney/core/ApplicationConfiguration.releaseVersion"/>"></script>
		<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/signIn.js?v=<dsp:valueof bean="/com/jcpenney/core/ApplicationConfiguration.releaseVersion"/>"></script>
		<script type="text/javascript" src="<c:out value="${dotcomContextPath}"/>/js/lib/jquery.form.js?v=<dsp:valueof bean="/com/jcpenney/core/ApplicationConfiguration.releaseVersion"/>"></script>  
		<script type="text/javascript" src="<c:out value='${dotcomContextPath}'/>/js/global/debugKeyHandler.js?v=<dsp:valueof bean="/com/jcpenney/core/ApplicationConfiguration.releaseVersion"/>"></script>
				<%--DP-4427 -Adding Common Core metrics JS --%>
		<script type="text/javascript" src="<c:out value='${dotcomContextPath}'/>/js/commonCoreMetrics.js?v=<dsp:valueof bean="/com/jcpenney/core/ApplicationConfiguration.releaseVersion"/>"></script>
			
	</dsp:oparam>
</dsp:droplet>

</body>
	<script type="text/javascript">
		$(function(){
				GetActiveMessages();
		});
	</script>
</html>
</dsp:page>