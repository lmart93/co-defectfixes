module.exports = function(grunt) {
  grunt.initConfig({
    sass: {
      dist: {
        files: {
          'ux-updates.css': 'scss/ux-updates.scss'
        }
      }
    },
    connect: {
      server: {
        options: {
          protocol: 'http',
          port: 8000,
          keepalive: false,
          base: '.'
        }
      }
    },
    watch: {
      source: {
        files: ['scss/*.scss','*.html','*.js'],
        tasks: ['sass'],
        options: {
          livereload: true
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-sass');
  grunt.registerTask('default', ['sass', 'connect', 'watch' ]);
};
