<%-- ======================================== Taglibs --%>
<%@ taglib uri="dsp" prefix="dsp" %>
<%@ taglib uri="dspel" prefix="dspel" %>
<%@ taglib uri="c" prefix="c"  %>
<%@ taglib uri="dspel" prefix="dspel"  %>
<%@ taglib uri="fmt" prefix="fmt" %>
<%-- ======================================== Imports --%>
<dsp:droplet name="/atg/dynamo/droplet/Switch">
	<dsp:param name="value" bean="/com/jcpenney/core/ApplicationConfiguration.enableOOSItemsRemoval"/>
	<dsp:oparam name="true">
		<style type="text/css">
			#cboxClose {
		    	background: url(/dotcom/images/modalskava_close.png) 0 0 no-repeat;
    			background-size: 25px;
    		}
			#cboxClose img{
				opacity: 0;
				height: 30px;
    			width: auto;
    		}
		</style>
	</dsp:oparam>
</dsp:droplet>
<dsp:page>
<fmt:setBundle basename="/com/jcpenney/dp/core/order/CheckoutResources" var="checkoutResources" scope="application"/>
<dsp:importbean bean="/com/jcpenney/core/util/JSPInclude" />
<dsp:importbean bean="/com/jcpenney/core/order/formhandler/RemoveOOSItemsFormHandler" />
<dsp:importbean bean="/com/jcpenney/core/ApplicationConfiguration" />
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
<dsp:importbean bean="/com/jcpenney/catalog/custom/ProductPresentationItemLookupDroplet" />
<dsp:importbean bean="/com/jcpenney/catalog/bean/CatalogConfiguration"/>
<dsp:importbean bean="/com/jcpenney/core/order/droplet/CommerceItemImageLookupDroplet" />
<dsp:importbean bean="/com/jcpenney/core/order/droplet/ItemNumberDisplayDroplet" />
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="com/jcpenney/core/order/droplet/OOSAndMovedToItemsDroplet"/>
<dspel:getvalueof var="oosMsgKey" param="oosMsgKey"/>
<dsp:getvalueof var="dotcomContextPath" bean="JSPInclude.dotcomContextPath" />
<dsp:getvalueof var="contextPath" bean="JSPInclude.contextPath" />
<dsp:getvalueof var="enableOOSItemsRemoval" bean="ApplicationConfiguration.enableOOSItemsRemoval" />
<dsp:getvalueof var="enableRevisedOOSModal" bean="ApplicationConfiguration.enable2016S8DPXVI3348RevisedOOSModal" />
<dsp:getvalueof var="camContextPath" bean="/com/jcpenney/core/util/JSPInclude.camContextPath"/>
<input type="hidden" id="camContextPathId" value="<c:out value="${camContextPath}"/>" />
<dsp:droplet name="/com/jcpenney/core/droplet/FetchActiveMessagesDroplet">
    	<dsp:param name="page" value="checkout"/>
    	<dsp:param name="keys" value="keyOrderHasOOSItemRevised"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="activeMessagesJson_OOS" param="activeMessagesJson"/>
		</dsp:oparam>
	</dsp:droplet>

		<div class="module_overlay external_site modal_form">
		<div class="sign_header_padd" style="margin-top: 5px;">
				<h4 class="oos_modal_header" style="font-family: Helvetica; font-weight: 400;">ITEM(S) OUT-OF-STOCK</h4>
		</div>
		<dsp:getvalueof var="oosItemsTempIndex" value="0"/>
		<dsp:getvalueof var="itemSMovedToShipToIndex" value="0"/>
		<dsp:droplet name="OOSAndMovedToItemsDroplet">
		<dsp:param name="order" bean="ShoppingCart.current"/>
		<dsp:oparam name="output">

					<dsp:droplet name="IsEmpty">
					<dsp:param name="value" param="movedToItems"/>
					<dsp:oparam name="false">
					<div class="modal_content flt_clr padb10 padt10">
					<div class="external_checkout_content mrgb10">

						<h4 style="border-left-width: 10px; margin-left: 20px; padding-top: 10px; font-family: Helvetica; font-size: 18px;font-weight: 700;">
				       		<img src="/dotcom/images/same-day.svg" alt="SAME DAY PICKUP">
				        	SAME DAY PICKUP ITEMS
				    	</h4>
				    	<div class="sbredesign-msg sbredesign-info"
				    	     style="padding-left: 48px;border-left-width: 1px;margin-left: 20px;margin-right: 20px;
				    		 font-family: Helvetica; font-size: 16px; font-weight: 400;">
				    		Some of your items unavailable for Same Day Pickup but still can be shipped.
				    		Continue to shipping to view your shipping options.
				    	</div>

					<div id="outOfStock_scroll" style="margin-left: 20px;margin-right: 5px;width: 625px;margin-top: 25px;">
					<ul class="oosItemsList" style="margin-left: 0px;">
					<dsp:droplet name="ForEach">
						<dsp:param name="array" param="movedToItems"/>
						<dsp:param name="sortProperties" value="-lastModifiedDate.time,-id"/>
						<dsp:setvalue param="commerceItem" paramvalue="element"/>
						<dsp:oparam name="outputStart">
							<dsp:getvalueof var="itemsMovedToShipToIdx" value="0"/>
						</dsp:oparam>
						<dsp:oparam name="output">
						<dsp:getvalueof var="size" param="size"/>
						<dsp:getvalueof var="index" param="count"/>
						<dsp:getvalueof var="lotType" param="commerceItem.auxiliaryData.productRef.lotFeed.lotType" />
						<dsp:getvalueof var="itemMovedToShipTo" param="commerceItem.itemMovedToShipTo"/>
							<dsp:getvalueof var="itemType" param="commerceItem.itemType" />
							<dsp:getvalueof var="syndicateItem" param="commerceItem.itemSource"/>
							<c:choose>
							<c:when test="${size == index}">
									<c:set var="oosItemClass" value="oos_item_no_border"/>
								</c:when>
								<c:otherwise>
									<c:set var="oosItemClass" value="oos_item"/>
								</c:otherwise>
								</c:choose>
								<%@ include file="/jsp/checkout/secure/checkoutsimplified/oosItemDetails.jsp" %>

						</dsp:oparam>
						</dsp:droplet>
					</ul>
				</div>
				</div>
				</dsp:oparam>
				</dsp:droplet>

				<dsp:droplet name="IsEmpty">
				<dsp:param name="value" param="oosItems"/>
				<dsp:oparam name="false">
				<div class="external_checkout_content mrgb10">

						<h4 style="border-left-width: 10px; margin-left: 20px; padding-top: 10px; font-family: Helvetica; font-size: 18px;font-weight: 700;" >
				       		<img src="/dotcom/images/deltruck.png" alt="SHIP TO HOME">
				        	SHIP TO HOME ITEMS
				    	</h4>
				    	<div class="sbredesign-msg sbredesign-info"
				    		 style="padding-left: 48px;border-left-width: 1px;margin-left: 20px;margin-right: 20px;font-family: Helvetica;
				    		 font-size: 16px;font-weight: 400;"">
				    		Some of your items are out-of-stock and not available for purchase.
				    		These items will be removed from your order, along with any special pricing.
				    	</div>

					<div id="outOfStock_scroll" style="margin-left: 20px;margin-right: 5px;width: 625px;margin-top: 25px;">
					<ul class="oosItemsList">
					<dsp:droplet name="ForEach">
						<dsp:param name="array" param="oosItems"/>
						<dsp:param name="sortProperties" value="-lastModifiedDate.time,-id"/>
						<dsp:setvalue param="commerceItem" paramvalue="element"/>
						<dsp:oparam name="output">
						<dsp:getvalueof var="size" param="size"/>
						<dsp:getvalueof var="index" param="count"/>
						<dsp:getvalueof var="lotType" param="commerceItem.auxiliaryData.productRef.lotFeed.lotType" />
						<dsp:getvalueof var="stateAsString" param="commerceItem.stateAsString"/>
							<dsp:getvalueof var="itemType" param="commerceItem.itemType" />
							<dsp:getvalueof var="syndicateItem" param="commerceItem.itemSource"/>
							<c:choose>
							<c:when test="${size == index}">
									<c:set var="oosItemClass" value="oos_item_no_border"/>
								</c:when>
								<c:otherwise>
									<c:set var="oosItemClass" value="oos_item"/>
								</c:otherwise>
								</c:choose>
								<%@ include file="/jsp/checkout/secure/checkoutsimplified/oosItemDetails.jsp" %>

						</dsp:oparam>
						</dsp:droplet>
					</ul>
				</div>
				</div>
				</dsp:oparam>
				</dsp:droplet>
				<c:if test="${oosMsgKey == 'keyOrderHasAllOutOfStockItems'}">
				</c:if>
					<div class="oos_modal_button_div" style="margin-left:10px; margin-bottom:10px; height:40px;">
						<div class="oos_modal_button_div_revamp" style="margin-top: 5px; height:30px; ">
							<a class="oos_modal_button" href="<c:out value="${contextPath}"/>/jsp/cart/viewShoppingBag.jsp">CONTINUE TO BAG</a>
						</div>
						<div class="oos_modal_button_div_revamp right" style="margin-right: 30px;  height:30px;">
							<dsp:form  formid="outOfStockContinueForm" id="outOfStockContinueForm" method="post" iclass="jcp_form" action="/dotcom/jsp/checkout/secure/checkout.jsp">
								<div id="outOfStockContinueButtonDiv">
									<input class="oos_modal_button red" type="button" alt="CONTINUE TO CHECKOUT"  title="CONTINUE TO CHECKOUT" value="CONTINUE TO CHECKOUT" onclick="validateOOS();" id="oosItemsContinueButton"/>
										<dsp:input type="hidden" bean="RemoveOOSItemsFormHandler.removeOOSItemsSuccessURL" value="/jsp/checkout/secure/checkout.jsp"/>
										<dsp:input type="hidden" bean="RemoveOOSItemsFormHandler.removeOOSItemsErrorURL" value="/jsp/checkout/secure/checkout.jsp"/>
										<dsp:input type="hidden" value="continue"  title="continue" alt="continue" bean="RemoveOOSItemsFormHandler.removeOOSItems" />
								</div>
							</dsp:form>
						</div>

					</div>

					</dsp:oparam>
				</dsp:droplet>
			</div>
		</div>
	</div>

<script>
	pageDataTracker.trackAnalyticsEvent("openModal", "item out of stock - modal");
</script>

<script>
	function closeMessageModal(){
		$.colorbox.close();
	}
</script>

<script>
function getActiveMessages_OOS() {
	var src = ${activeMessagesJson_OOS};
	var key = src[0].key;
	$('.' + key).each(function(i, obj) {
		var message = src[0].message;
		$(this).html(message);
	});
}
</script>
<script type="text/javascript">
	$(function(){
			$('#cboxCloseImg').click(function (e) {
				validateOOS();
			});
			getActiveMessages_OOS();
	});
</script>
</dsp:page>
