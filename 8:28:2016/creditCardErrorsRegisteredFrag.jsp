<%@ taglib uri="dsp" prefix="dsp" %>
<%@ taglib uri="dspel" prefix="dspel" %>
<%@ taglib uri="c" prefix="c"  %>
<%@ taglib uri="fmt" prefix="fmt" %>
<%@ page pageEncoding="UTF-8" %>
<dsp:page xml="true">

<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
<dsp:importbean bean="/com/jcpenney/core/droplet/MonthYearDroplet"/>
<dsp:importbean bean="/com/jcpenney/profile/bean/UserSessionBean" />
<dsp:importbean bean="/atg/commerce/order/purchase/PaymentGroupFormHandler"/>

<form class="payment_form">
<div id="paymentCreditCard" class="row">

<div class="flt_lft cc_number">
	
						<dsp:droplet name="ForEach">
						<dsp:param bean="ShoppingCart.current.paymentGroups" name="array" />
						<dsp:oparam name="output">
							<dsp:getvalueof param="element.paymentMethod" var="paymentMethod" vartype="java.lang.String"/>
							<c:if test="${(paymentMethod eq 'creditCard')}">
								<dsp:getvalueof var="pgMethod" param="element.paymentMethod"/>
								<dsp:getvalueof var="paymentGroup" param="element"/>
							</c:if>
						</dsp:oparam>
						</dsp:droplet>
						<div class="card-type-selected hide">${paymentGroup.creditCardType}</div>

									
										<c:if test="${(pgMethod eq 'creditCard')}">
										<dsp:droplet
											name="/com/jcpenney/core/order/droplet/CreditCardExpiryValidationDroplet">
											<dsp:param name="creditCard"
												value='${paymentGroup}' />
											<dsp:oparam name="true">
												<c:set var="isCCExpired" value="true" />
											</dsp:oparam>
										</dsp:droplet>
										<dsp:getvalueof var="errorsInCard"
												value='${paymentGroup.creditCardAuthorizationRequired}' />
										<dsp:getvalueof var="isCVVRequired"
												value='${paymentGroup.cvvAuthorizationRequired}' />
												
										<c:if test="${(errorsInCard eq true || isCVVRequired eq true || isCCExpired eq true) }">
										<div class="cc_err_msgs dynamic_error_msgs hide" id="itrmCreditCardErrorContainer">
										<!-- 8/28 LM - updated error icon -->
										<span class="error-msg-header"><img alt="attention" src="/dotcom/images/error_icon.svg">Please correct the errors listed in red below:</span>
										<ul class="error-msgs" id="savedCardErrorMessage">
										
										<c:if test="${isCCExpired ne true}">
											<li class="cardErrorMsg"><span data-anid="click_errorDescription" id="keyCCAuthorisationNeeded"><fmt:message key="keyCCAuthorisationNeeded" bundle="${ checkoutResources }" /></span></li>
										</c:if>
										<c:if test="${isCCExpired eq true}">
											<li class="cardErrorMsg"><span data-anid="click_errorDescription" id="CREDIT_CARD_EXPIRED"><fmt:message key="CREDIT_CARD_EXPIRED" bundle="${ checkoutResources }" /></span></li>
										</c:if>

										</ul>
										</div>
										</c:if>

										<c:if test="${errorsInCard eq true || isCCExpired eq true}">
										
										<div class="sbredesign-msg sbredesign-info" id="authMsg">
											<span data-anid="click_errorDescription" id="keyCCAuthorisationNeeded"><fmt:message key="keyCCAuthorisationNeeded" bundle="${ checkoutResources }" /></span>
										</div>
										
										<div class="cc-input">
										<!-- 8/27 LM fixed MasterCard -->
										<label id="creditcardnumberLabel" for="creditcardnumber">JCPenney Card, Visa, MasterCard, Discover, American Express</label>
										<input class="input_txt masterCreditCardNumber" type="text" value="" maxlength="16"
											placeholder="re-enter credit card number"
											id="reenteredCCNumber" name="reenteredCCNumber" />
										<input class="hide" type="text" value="${paymentGroup.creditCardType}" id="cardType" name="cardType" />
										</div>
										</c:if>
										</div>
								<div style="clear:both;">
										<div class="cc_details">

										<c:if test="${(errorsInCard eq true || isCVVRequired eq true || isCCExpired eq true) }">
											<c:set var="cvvRentryFieldsRendered" value="true" />
											<c:if test="${isCCExpired ne true}">
												<div class="flt_lft cc_exp_date ${paymentGroup.creditCardType eq 'JCP' ? 'hide' : ''}" id="expDate">
												<label id="expirationMonthLabel" for="expirationMonth">Expiration Date</label>
												
												<div class="row exp-details">
												<div class="flt_lft exp-month">
												<select name="reEnteredExpirationMonth"
												id="reEnteredExpirationMonth" class="flt_lft expireDate"
												onfocus="clearFieldStyle(this, 'expirationMonthLabel', 'flt_lft expireDate', '','false')">
												<option value="month">month</option>
												<dsp:droplet name="MonthYearDroplet">
													<dsp:param name="yearType" value="cardExpiryType" />
													<dsp:oparam name="months">
														<dsp:getvalueof var="monthWithTwoDigits"
															param="monthWithTwoDigits" />
														<option value="<c:out value='${monthWithTwoDigits }' />">
															<dsp:getvalueof id="monthNumber" param="monthNumber"
																idtype="java.lang.String">
																<%=monthNumber%>
															</dsp:getvalueof>-
															<dsp:getvalueof id="monthName" param="month"
																idtype="java.lang.String">
																<%=monthName%>
															</dsp:getvalueof>
														</option>
													</dsp:oparam>
												</dsp:droplet>
											</select>
											</div>
											<div class="flt_lft exp-year">
											<select name="reEnteredExpirationYear"
												id="reEnteredExpirationYear" class="flt_lft expireYear">
												<option value="year">year</option>
												<dsp:droplet name="MonthYearDroplet">
													<dsp:param name="yearType" value="cardExpiryType" />
													<dsp:oparam name="years">
														<dsp:getvalueof var="yearValue" param="yearValue" />
														<option value="<c:out value='${yearValue }' />">
															<dsp:getvalueof id="yearValue" param="yearValue"
																idtype="java.lang.String">
																<%=yearValue%>
															</dsp:getvalueof>
														</option>
													</dsp:oparam>
												</dsp:droplet>
											</select>
											</div>
											</div>
												
											</div>
											</div>

									<div class="flt_lft cc_cvv" id="cvvId">
									<label for="cardverificationnumber" id="cardVerificationNumberLabel">Card verification number</label> 
									
										<input name="reEnteredCCVerificationNumber"
											id="reEnteredCCVerificationNumber" type="text"
											class="input_pwd cardVerification" maxlength="4" value=""
											oncopy="return false" oncut="return false"
											onfocus="clearFieldStyle(this, 'cardVerificationNumberLabel', 'input_pwd cardVerification', '','false')"/>
											<%@ include file="/jsp/checkout/secure/checkoutsimplified/payment/creditcard/creditCardCVNImages.jspf"%>
									</div>
									<div class="clear"></div>
								</div>
							<input id="isCreditCardExpired" name="isCreditCardExpired"
								type="hidden" value="<c:out value='${isCCExpired}'/>" />
											
										</c:if>
										</c:if>
											
										</c:if>
									</div>
							</form>
							
							</dsp:page>