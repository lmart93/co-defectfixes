var editPaymentpanel = false;
var enableGiftCardError = false;
var savedAddressWithoutCardFlag = false;
var selectedInternational = false;
var selectedUS = true;
var reloadShippingMethod = false;
var cardValidationNeeded = false;
// Variables for core-metrics
var promoOfferUserAction = "";
var isPromoOfferSelected = false;
var isCreditCardExpired = false;
var enable2016S10CCDPXVI13877CoreMetCallCredit = true;
// Variables for ITRM switch scenarios. 
var isAuthRequired = false;
var isCVVRequired = false;

$(document).ready(function(){
	// 8/28 LM - set focus on CC input
	$("#masterCreditCardNumber").focus();
	
	// Tab functionality for credit card and paypal - Guest Flow
	$('.guest .cards ul li').live('click', function(){
		// Make the tab panel head active
		$('.cards ul li').removeClass('active active-new');
		if ($(this).hasClass('paypal')) {
			$(this).addClass('active');
			$('.card-contents').hide();
			$('.paypal-footer').show();
		} else {
			$('.paypal-footer').hide();
			$(this).addClass('active active-new');
			$('.card-contents').show();
			$('.credit-card-content').show();
			$('#paymentContiue').removeAttr('disabled').show();
		}
		
		/*// Display the selected panel content
		var cardContentIndex = $(this).index() + 1;
		$('.card-contents div.card-content').hide();
		$('.card-contents div.card-content:nth-child('+ cardContentIndex +')').show();*/
	});
	
	// Registered : Show the add credit card form for registered users
	$('.registered .cards ul li').live('click', function(){
		// Check if user has clicked on add new credit card
		if ($(this).hasClass('new-cc')){
			$('.credit-card-content').show();
			$('li.card').removeClass('active active-new');
			$(this).addClass('active active-new');
			
			// Hide other forms
			$('.card-contents').show();
			$('.savedCardPaymentFormContainer').hide();
			$('#itrmCreditCardRegistered').hide();
			$('#paymentContiueSubmit').attr('disabled',true);
			
		} else {
			$('.credit-card-content').hide();
			$('li.card').removeClass('active active-new');
			$(this).addClass('active');
			
			// Enable express checkout checkbox for selected card
			$('li.card .card-payment-express input').attr('disabled', 'disabled');
			$(this).find('.card-payment-express input').removeAttr('disabled');
			
		}
	});
	
	// Registered : Hide the add new credit card form when cancel button is clicked
	$('.add-cc-cancel').live('click', function(){
		// Remove active class from add new credit card 
		$('.credit-card-content').hide();
		$('li.card').removeClass('active active-new');
		
		// Make the previously selected card as active 
		$('li.card.selected').addClass('active');
		$('#paymentContiueSubmit').removeAttr('disabled');
	});
	
	// If the user doesn't have any saved cards, show the add credit card form
	if ($('.registered .new-cc').hasClass('active')){
		$('.registered .new-cc').click();
	} 
	
	// Load the credit card ranges for card type selection
	cardRangeObject.loadCreditCardRanges();
	
	// Changes to billing address that needs to be done after payment panel is loaded
	loadBillingAddress(true);
	
	$(".editCCCardCancel").live("click", function(e) {
		$(".editCreditCardInfo").hide();
		$('#updateCreditCardRegistered').hide();
		$(".saved_cards_carousel_element_ul ul_card > li").removeClass("active-new");
	});
	
	// Bind validations to billing address fields
	$(".input_name").live("keyup blur", validateInputName);
	$(".input_phone").live("keyup blur", validatePhoneNumber);
	$(".input_email").live("keyup blur", validateEmailId);
	$(".input_address").live("keyup blur", validateStreet);
	$(".input_zip").live("keyup blur", validateZipCode);
	$(".input_city").live("keyup blur", validateCity);
	$(".textAlertNum").live("keyup blur", validatePhoneNumber);	
	$(".input_cvn").live("keyup blur", validateCVN);		
	if($("#text-alert").is(':checked')){
	     // show SMS phone number field always if opt in checkbox is checked
		// 8/28 LM - added animation and set focus
		$("#smsPhone").slideDown();
		$("#textAlertNum").focus();
	} else {
		$("#smsPhone").slideUp();
	}
	$(".maskedCCNumber").each(function(){
		cardno=$(this).text().trim();
		var cardno_hidden=cardno.substr(cardno.length - 8);	
		$(this).text(cardno_hidden);
	});
	$('.termsAndConditionsModal').colorbox({
		scrolling: true,
		overlayClose: false,						
		escKey: false,
		width:'685px',
		height:'716px',
		opacity:'1',
		title:false,
		close: function(){
			 $('#cboxClose').css({'top':'25px','background-image':'none','margin-right': '26px'});
			var modalClose = "<span id='modalClose'> X </span>";		
			return $(modalClose);
		},
		onComplete: function () {
            $('#cboxLoadedContent .closeModal').bind("click", function () {
                $('#cboxClose').trigger("click");
            });
            $('#cboxTitle').hide();
            $('#smsTermsModalClose').hide();
        }
	});
});


/** 
 * Function to display billing address form based on the edit checkbox 
 * 
 */
function showBillingAddressForm(checkbox){
	if($(checkbox).is(':checked')){
		// Show the shipping address summary and hide the billing form
		
		// 8/28 LM - added slideDown animation
		$('.billing-summary').slideDown();
		$('.billing-info').hide();
		$('.edit-add-checkbox').addClass('selected');
		
		// Copy shipping address to billing form
		$('input#billingFirstName').val($('#sbafirstname').text().trim());
		$('input#billingLastName').val($('#sbalastname').text().trim());
		$('input#billingStreetAddress1').val($('#sbastreetaddr1').text().trim());
		$('input#billingStreetAddress2').val($('#sbastreetaddr2').text().trim());
		$('input#billingCity').val($('.billing-summary #sbacity').text().trim());
		$('input#billingZipCode').val($('#sbazip').text().trim());
		$('#billingState option[value='+ $('#sbastate').text().trim() +']').attr('selected', true);
		$('#selectBillingCountry option[value='+ $('#sbacountry').text().trim() +']').attr('selected', true);
		
	} else {
		// Show the billing form and hide the summary
		// 8/28 LM - added slideDown animation and set focus
		$('.billing-summary').hide();
		$('.billing-info').slideDown();
		$('#billingFirstName').focus();
		$('.edit-add-checkbox').removeClass('selected');
	}
}

/**
 * Function to populate values to hidden fields in payment form before it is submitted
 * 
 */
function paymentSubmitClick(paymentFormId) {
	// Validating the payment form before submitting
	if (!$("#"+paymentFormId).valid()) {
		var errorDescription = "";
		if($("#masterCreditCardNumber").val() == ""){
			errorDescription = errorDescription +"Enter a valid credit card number"+"|";
			$("#masterCreditCardNumber").focus();
		}
		if($("#cardVerificationNumber").val() == "") {
		   errorDescription = errorDescription +"Enter a valid CVV number|";
		   $("#cardVerificationNumber").focus();
	    }		
		if( typeof pageDataTracker != "undefined"  && typeof pageDataTracker.trackAnalyticsEvent == "function") {
			if(typeof errorDescription != "undefined") {
				errorDescription = errorDescription.substring(0,errorDescription.length-1);
				var omniString = "Error_checkout_payment,"+errorDescription+","+"Error_checkout_payment,"+", , ,";
				   pageDataTracker.trackAnalyticsEvent ("formError", omniString);
			}
		}
		
		return false;
	}

	trackPageLoadTime('Payment_Panel_Continue_Requested');
	
	// Clear the styling of fields with error
	$('.input_zip').removeClass("error");
	$('#billingPhoneNumber').removeClass("error");
	$('#billingStreetAddress1').removeClass("error");
	$('#billingStreetAddress2').removeClass("error");
	$('#billingAddresscity1').removeClass("error");
	$('#billingAddressProvince1').removeClass("error");

	var paymentForm = document.forms[paymentFormId];	
	var copyFieldsMap = new Map();
	
	// Copy the user entered values to the hidden input fields
	if (savedAddressWithoutCardFlag) {
		copyFieldsMap.set('masterCreditCardNumber', 'hdnCreditCardNumber');
		if (paymentForm.elements['ccType'].value != "JCP") {
			copyFieldsMap.set('cardVerificationNumber', 'cardVerificationNumber1');
		}
	} else {
		if (paymentForm.elements['ccType'].value == "Visa") {
			copyFieldsMap.set('cardVerificationNumber', 'cardVerificationNumber1');
		} else if (paymentForm.elements['ccType'].value != "JCP") {
			copyFieldsMap.set('cardVerificationNumber', 'cardVerificationNumber1');
		}
		copyFieldsMap.set('masterCreditCardNumber', 'hdnCreditCardNumber');
		
		if (paymentForm.elements['billingPhoneNumber'].value != "") {
			copyFieldsMap.set('billingPhoneNumber', 'hdnPhoneNumber');
		}

		if (paymentForm.elements['selectBillingCountry'].value == 'United States') {
			if (paymentForm.elements['billingStreetAddress1'].value != "") {
				copyFieldsMap.set('billingStreetAddress1', 'hdnBillingStreetAddress1');
			}
			copyFieldsMap.set('billingStreetAddress2', 'hdnBillingStreetAddress2');
			if (paymentForm.elements['billingCity'].value != "") {
				copyFieldsMap.set('billingCity', 'hdnBillingCityName');
			}
			if (paymentForm.elements['billingZipCode'].value != "") {
				copyFieldsMap.set('billingZipCode', 'hdnBillingZipCode');
			}
		} else if (paymentForm.elements['selectBillingCountry'].value == 'AP') {
			if (paymentForm.elements['militaryStreetAddress1'].value != "") {
				copyFieldsMap.set('militaryStreetAddress1', 'hdnBillingStreetAddress1');
			}
			if (paymentForm.elements['militaryStreetAddress2'].value != "") {
				copyFieldsMap.set('militaryStreetAddress2', 'hdnBillingStreetAddress2');
			}
			if (paymentForm.elements['militaryZipCode'].value != "") {
				copyFieldsMap.set('militaryZipCode', 'hdnBillingZipCode');
			}
		} else {
			if (paymentForm.elements['internationalStreetAddress'].value != "") {
				copyFieldsMap.set('internationalStreetAddress', 'hdnBillingStreetAddress1');
			}
			if (paymentForm.elements['internationalStreetAddress2'].value != "") {
				copyFieldsMap.set('internationalStreetAddress2', 'hdnBillingStreetAddress2');
			}
			if (paymentForm.elements['internationalStreetAddress3'].value != "") {
				copyFieldsMap.set('internationalStreetAddress3', 'hdnBillingStreetAddress3');
			}
			if (paymentForm.elements['billingPhoneNumberSecondary'].value != "") {
				copyFieldsMap.set('billingPhoneNumberSecondary', 'hdnSecondaryPhoneNumber');
			} else if (paymentForm.elements['billingPhoneNumberSecondary'].value == ""
					&& paymentForm.elements['hdnSecondaryPhoneNumber'].value != "") {
				paymentForm.elements['hdnSecondaryPhoneNumber'].value = "";
			}

			if (paymentForm.elements['internationalCity'].value != "") {
				copyFieldsMap.set('internationalCity', 'hdnBillingCityName');
			}
			if (paymentForm.elements['internationalPostalCode'].value != "") {
				copyFieldsMap.set('internationalPostalCode', 'hdnBillingZipCode');
			}
		}
		copyFieldsMap.set('selectBillingCountry', 'hdnCountry');
	}
	copyFieldsMap.set('ccType', 'hdnCreditCardType');
	if (paymentForm.elements['billingEmailId'] != undefined) {
		copyFieldsMap.set('billingEmailId', 'hdnEmailAddress');
	}
	if (paymentFormId == "UpdateCreditCardFormHandler") {
		paymentForm.elements['editOverlay'].value = "AddressValidationRequired";
	} else {
		//paymentForm.elements['editOverlay'].value = "";
	}
	//DPXVI-20388 - start
	if (paymentForm.elements['textAlertChk'] != undefined) {
		copyFieldsMap.set('textAlertChk', 'hdnTextAlertChk');
	}
	
	if (paymentForm.elements['textAlertNum'] != undefined) {
		copyFieldsMap.set('textAlertNum', 'hdnTextAlertNum');
	}
	//DPXVI-20388 - end
	// Copy the values to hidden form fields
	copyFormFields(paymentForm, paymentForm, copyFieldsMap);

	paymentFormIdVar = paymentFormId;
	var options = {
		success : paymentFormSuccess,
		error : paymentFormError,
		timeout : ajaxTimeOut
	};
	$("#" + paymentFormId).ajaxSubmit(options);
	showPageSpinner('checkoutPage');
}

/** 
 * Function to copy form fields across forms. In the map passed - 
 * key - will have field names to which value needs to be set
 * value - will have field names from which value has to be taken
 * 
 */
function copyFormFields(fromForm, toForm, fieldMap){
	fieldMap.forEach(function(value, key) {
		toForm.elements[value].value = fromForm.elements[key].value;
	});
}

/**
 * Function which will be called on successful submission of payment form
 * 
 */
function paymentFormSuccess(responseText, statusText, xhr, $form){
	
	var completedPanels = responseText.completedPanels;
	//updateProgressBar(responseText.currentPanel, completedPanels);

	if (responseText.redirectURL) {
		window.location.href = responseText.redirectURL + '?sessionExpired=true';
		return;
	}
	
	/* DPXVI-10183 Session is not getting terminated after 5th unsuccessful attempt - Start*/
    if(typeof responseText.logoutOnAccountLocked != "undefined"){
    	var map = responseText.logoutOnAccountLocked;
    	var cookies = document.cookie.split(';');
    	for(var i=0; i<cookies.length; i++){
    		while (cookies[i].charAt(0) === ' ') {
	            cookies[i] = cookies[i].substring(1);
	        }
    		eraseCookie(cookies[i].split("=")[0]);
    	}
    	for (var key in map) {
    		if(key == 'accountLocked'){
    			window.location.href = '/dotcom/jsp/cart/viewShoppingBag.jsp?accountLocked=true';
    		} else if (key == 'guestAccountLocked'){
    			window.location.href = '/dotcom/jsp/cart/viewShoppingBag.jsp?guestAccountLocked=true';
    		}
    	}
    	return;
    }
	
	if(null != responseText.paymentErrors){
		// Call function to show the error messages
		populatePaymentErrorMsgs(responseText.paymentErrors);
		hidePageSpinner('checkoutPage');
	} else if (responseText.isPromoFinacingOffersAvailable != undefined
			&& responseText.isPromoFinacingOffersAvailable == 'true') {
		// Show the promo financing offers modal
		openModal("/dotcom/jsp/checkout/secure/checkoutsimplified/payment/promoFinancingOffersModal.jsp", null, null, "promoFinancingDeclineFormSubmit");
		hidePageSpinner('checkoutPage');
		if( typeof pageDataTracker != "undefined"  && typeof pageDataTracker.trackAnalyticsEvent == "function") {
			pageDataTracker.trackAnalyticsEvent ("financeOfferView");
		}
	} else {
		// Continue to review panel
		if (responseText.reviewHtml != null) {
			$('#paymentPanel').hide();
			$('#orderReview').html(formatHTML(responseText.reviewHtml)).show(hidePageSpinner('checkoutPage'));
			scrollToTop();
		}
		// Refresh the pricing summary panel
		if (responseText.pricingSummaryHtml != null) {
			$('#paymentPanel').hide();
			$('#checkoutPricingSummaryDiv .holder').html(formatHTML(responseText.pricingSummaryHtml));
			$('a.helpIcon').tooltip({
	    		overrideClass: "helpIcontip"
	    	});
		}
		if (responseText.checkoutStepsHtml != null) {
		    var checkoutStepsHtml = $('<textarea />').html(responseText.checkoutStepsHtml).text();
		    $('#checkoutProgressBarDiv').html(checkoutStepsHtml);
		}
		
		coreMetricsPart("Order Summary", "JCP|Checkout");
	}
	$('.helpIcon').tooltip({"overrideClass" : "helpIcontip"});
	hideSpinner('body');
}

/**
 * Function which will be called on failure of payment form submission
 * 
 */
function paymentFormError(responseText, statusText, xhr, $form){
	if (statusText == 'timeout') {
		var errorHTML = "<li>There is an internal error. please try after some time.</li>";
		$('.cc_err_msgs ul').append(errorHTML);
	} else {
		redirectOnError();
	}
	$('.helpIcon').tooltip({"overrideClass" : "helpIcontip"});
	hidePageSpinner('checkoutPage');
}

/**
 * Function to populate the error messages received from form submission in the payment panel
 * For each error message, an li tag is created and is appended to the existing ul in error containers
 * 
 */
function populatePaymentErrorMsgs(errorMsgsMap){
	var errorMsgDiv = '';
	var errorHtml = '';
	
	// Clear existing error messages
	$('.cc_err_msgs ul').html('');
	$('.orderContact_err_msgs ul').html('');
	$('.bill_add_err_msgs ul').html('');
	
	// TODO: Need to replace with key names instead of values
	var creditCardInvalidErrorServer = "The credit card number contains invalid characters.";
	var creditCardInvalidError = "Enter a valid credit card number.";
	
	$.each(errorMsgsMap, function(key, value){
        if (key == 'login') {
            $('#billingEmailId').addClass("error");
            if (coremetricsTagEnabled) {
            	createCoremetricsElementTag('emailID.invalid','Error_Checkout_Payment');
            }
        }
		if(key == "lastName"){
			$('#billingLastName').addClass("error");
		}
        if(key == "firstName"){
            $('#billingFirstName').addClass("error");
		}
        if(key == "phoneNumber"){
            $('.billingPhoneNumber').addClass("error");
		}
		if(key == "postalCode"){
            $(".input_zip").addClass("error");
		}
		if(key == "address1"){
            $('.billingAddress1').addClass("error");
		}

		/*if(key == "keyOCSOrderTotalExceeded"){
			$('#shopbag_summary').load('/dotcom/jsp/checkout/secure/checkoutShoppingBagSummary.jsp #shopbag_summary');
			$('#order_pricing_sum').load('/dotcom/jsp/checkout/secure/checkoutPricingSummary.jsp');
			addScrollBarToCheckoutShoppingBagSummary();
		}*/
		
		if(key == "address2"){
            $('.billingAddress2').addClass("error");
		}

		if(key == "city"){
			$('.billingAddresscity1').addClass("error");
		}

		if(key == "provinceRegionTerritory"){
			$('.billingAddressProvince1').addClass("error");
		}

		if(key == "creditCardNumber"){
			$('.masterCreditCardNumber').addClass("error");
			$('.visaCreditCardNumber').addClass("error");
		}
		
		//var customErrorStringForOmniture = errorCategory+errorDescription+errorId+errorSku+formField+formName;
		// Need to check and remove if not needed
		if(isCDIStdErrKey(key) || key=="expirationMonth" || key=="expirationYear"){
			addressStandarizedFlag = true;
			
			var customErrorStringForOmniture = "Error_Checkout_Payment" + "," + value + "," + key + "," + "," + ","+ ",";
			if( typeof pageDataTracker != "undefined"  && typeof pageDataTracker.trackAnalyticsEvent == "function") {
				pageDataTracker.trackAnalyticsEvent ("formError",customErrorStringForOmniture);
			}
		}

		if(value == "The credit card number is not valid."){
			value = creditCardInvalidError;
			if (key!='hdnCreditCardNumber' && coremetricsTagEnabled){
				createCoremetricsElementTag('creditCard.invalid','Error_Checkout_Payment');
			}
			var customErrorStringForOmniture = "Error_Checkout_Payment" + "," + value + "," + "creditCard.invalid" + "," + "," + ","+ ",";
			if( typeof pageDataTracker != "undefined"  && typeof pageDataTracker.trackAnalyticsEvent == "function") {
				pageDataTracker.trackAnalyticsEvent ("formError",customErrorStringForOmniture);
			}
		}

		if(value == creditCardInvalidErrorServer)	{
			value = creditCardInvalidError;
			if (key != 'hdnCreditCardNumber' && coremetricsTagEnabled) {
				createCoremetricsElementTag('creditCard.invalid','Error_Checkout_Payment');
			}
			var customErrorStringForOmniture = "Error_Checkout_Payment" + "," + value + "," + "creditCard.invalid" + "," + "," + ","+ ",";
			if( typeof pageDataTracker != "undefined"  && typeof pageDataTracker.trackAnalyticsEvent == "function") {
				pageDataTracker.trackAnalyticsEvent ("formError",customErrorStringForOmniture);
			}
		}
		
		if (key != 'hdnCreditCardNumber') {
			if(key == 'keyOCSCallCreditCardAuthFailed'){
				// Value for this is populated in checkoutJSFrag
				errorHtml+="<li>"+callCreditMsgForCheckout+"</li>";
			} else {
				errorHtml+="<li>"+value+"</li>";
			}
			if (coremetricsTagEnabled) {
				createCoremetricsElementTag(key + '.invalid','Error_Checkout_Payment');
			}
			var customErrorStringForOmniture = "Error_Checkout_Payment" + "," + value + "," + key  + "," + "," + ","+ ",";
			if( typeof pageDataTracker != "undefined"  && typeof pageDataTracker.trackAnalyticsEvent == "function") {
				pageDataTracker.trackAnalyticsEvent ("formError",customErrorStringForOmniture);
			}
		}

		if (key == "keyOCSCreditCardAuthFailed" || key== "keyOCSCallCreditCardAuthFailed") {
			if(coremetricsTagEnabled && enable2016S10CCDPXVI13877CoreMetCallCredit && key== "keyOCSCallCreditCardAuthFailed") {
				createCoremetricsElementTag('Checkout Call Credit Response','Call Credit Response');
			}
		}
		if(key=='keyOCSAccountLock') {
			var cookies = document.cookie.split(";");
			for (var i = 0; i < cookies.length; i++) {
			  eraseCookie(cookies[i].split("=")[0]);
			}
			if (document.getElementById('checkoutlogoutForm')) {
				document.getElementById('checkoutlogoutForm').submit();
			}
		}
		
		// Check if the current message is related to credit card or billing
		if (isCreditCardErrKey(key)) {
			errorMsgDiv = '.cc_err_msgs';
		} else if (isOrderContactInformationErrKey(key)) {
			errorMsgDiv = '.orderContact_err_msgs';
		} else {
			errorMsgDiv = '.bill_add_err_msgs';
		}
		
	});
	
	// Add error messages to the container
	$(errorMsgDiv + ' ul').append(errorHtml);
	
	// Scroll to the error message container and show it
	$(errorMsgDiv).show();
	$(errorMsgDiv + ' ul').show();
	$('html, body').animate({scrollTop: $(errorMsgDiv).offset().top}, 'slow');
	
}

/**
 * Function to check if given error message key belongs to credit card value
 * 
 */
function isCreditCardErrKey(key){	
	if (key == 'cardVerificationNumber' || key == 'keyOCSCreditCardAuthFailed'
			|| key == 'keyOCSCallCreditCardAuthFailed'
			|| key == 'creditCardNumber' || key == 'hdnCreditCardNumber'
			|| key == 'creditCardType' || key == 'expirationMonth'
			|| key == 'login') {
		return true;
	} else {
		return false;
	}
}

/**
 * Function to check if given error message key belongs to Order Contact Information
 * 
 */
function isOrderContactInformationErrKey(key){	
	if (key == 'phoneNumber' || key == 'smsMobileNumber') {
		return true;
	} else {
		return false;
	}
}

/**
 * Coremetrics for promo financing modal
 * 
 */
function cmPromoFinanceVarible(){
	var promoFinanceOffer="promoFinanceOffer";
	var promoFinanceOfferCount ="Promo_Finance_Offer_" + promoOfferCount;
	var cmAttribute23= '-_-'+'-_-'+'-_-'+'-_-'+'-_-'+'-_-'+'-_-'+'-_-'+'-_-'+'-_-'+'-_-'+'-_-'+'-_-'+'-_-'+'-_-'+'-_-'+'-_-'+'-_-'+'-_-'+'-_-'+'-_-'+'-_-'+promoFinanceOffer;
	cmCreateElementTag(promoOfferUserAction, promoFinanceOfferCount, cmAttribute23)
}


function onSelectboxChange(selectInput){
    $("#milZipCityBillingError").empty();

	var selected = (null != selectInput) ? $(selectInput).find('option:selected') : $("#selectBillingCountry option:selected");
	
	var selectVal = selected.val();
	if(selectVal != undefined) {
	if (selectVal == "Canada") {
		$('#ProvinceDiv').hide();
		$('#ProvinceDiv :input').attr('disabled','disabled');
		$('#ProvinceDivCanada').show();
		$('#ProvinceDivCanada :input').removeAttr('disabled');
	}
	else{
		$('#ProvinceDivCanada').hide();
		$('#ProvinceDivCanada :input').attr('disabled','disabled');
		$('#ProvinceDiv').show();
		$('#ProvinceDiv :input').removeAttr('disabled');
	}
		
	if(performCheckoutErrors == ""){
		$('.dynamic_error_msgs').hide();
		$('.dynamic_error_msgs ul li').remove();
		$('div[id^="paymentInfo"]').each(function() {
			$("div.error").each(function() {
				$(this).remove();
			});
			$(".error").each(function() {
				$(this).removeClass("error");					
			});
		});
	}

	$('.checkout_content_blk :input, .checkout_content_blk label').removeClass('error');

	if($("#phoneNumberLength").val() != "") {
		var phoneNumberLength = $("#phoneNumberLength").val();
	}
	if($("#internationalPhoneNumberLength").val() != "") {
		var internationalPhoneNumberLength = $("#internationalPhoneNumberLength").val();
	}
	switch (selectVal) {
		case 'United States':
			selectedInternational = false;
			selectedMilitary = false;
			usBillingAddress();
			$("#billingPhoneNumber").attr("maxlength", phoneNumberLength);
			selectedUS = true;
			break;
		case 'AP':
		case 'APO/FPO/DPO':
			selectedUS = false;
			selectedInternational = false;
			militaryBillingAddress();
			$("#billingPhoneNumber").attr("maxlength", internationalPhoneNumberLength);
			selectedMilitary = true;
			break;
		default:
			selectedUS = false;
			selectedMilitary = false;
			internationalBillingAddress(selectInput);
			$("#billingPhoneNumber").attr("maxlength", internationalPhoneNumberLength);
			selectedInternational = true;
			break;
	}
	window.spamBillShip(selectVal);
}
}

function usBillingAddress(){
	if (!selectedUS) {
		$('#usBillingSpecific .company_name').removeClass("opened");
	}
	
	// For create payment flow
	$('#usBillingSpecific').show();
	$('#usBillingSpecific :input').removeAttr('disabled');
	$('.company_name').show();
	$('.company_name :input').removeAttr('disabled');
	$('#militaryBillingSpecific').hide();
	$('#militaryBillingSpecific :input').attr('disabled','disabled');
	$('#internationalBillingSpecific').hide();
	$('#internationalBillingSpecific :input').attr('disabled','disabled');
	// Hide secondary phone number
	$('.contact_detail').hide();
	
	// For update payment flow
	$('#usBillingSpecificForEdit').show();
	$('#usBillingSpecificForEdit :input').removeAttr('disabled');
	$('#militaryBillingSpecificForEdit').hide();
	$('#militaryBillingSpecificForEdit :input').attr('disabled','disabled');
	$('#internationalBillingSpecificForEdit').hide();
	$('#internationalBillingSpecificForEdit :input').attr('disabled','disabled');
	
}

function militaryBillingAddress(){

	// For create payment flow
	$('.company_name').show();
	$('.company_name :input').removeAttr('disabled');
	$('#usBillingSpecific').hide();
	$('#usBillingSpecific :input').attr('disabled','disabled');
	$('#militaryBillingSpecific').show();
	$('#militaryBillingSpecific :input').removeAttr('disabled');
	$('#internationalBillingSpecific').hide();
	$('#internationalBillingSpecific :input').attr('disabled','disabled');
	$('#streetAddress3').hide();
	$('.contact_detail').hide();
	$("#militaryBillingSpecific .ship_state").show();

	// For update payment flow
	$('#usBillingSpecificForEdit').hide();
	$('#usBillingSpecificForEdit :input').attr('disabled','disabled');
	$('#militaryBillingSpecificForEdit').show();
	$('#militaryBillingSpecificForEdit :input').removeAttr('disabled');
	$('#internationalBillingSpecificForEdit').hide();
	$('#internationalBillingSpecificForEdit :input').attr('disabled','disabled');
	
}

function internationalBillingAddress(selectInput){
	if (null == selectInput){
		selectInput = $('#selectBillingCountry');
	}
	var selectedCountry = $(selectInput).find("option:selected").val();
	
	// For create payment flow
	$('#usBillingSpecific').hide();
	$('#usBillingSpecific :input').attr('disabled','disabled');
	$('#militaryBillingSpecific').hide();
	$('#militaryBillingSpecific :input').attr('disabled','disabled');
	$('#internationalBillingSpecific').show();
	$('#internationalBillingSpecific :input').removeAttr('disabled');
	$('#internationalAlternatePhoneNumber :input').removeAttr('disabled');
	$('.contact_detail').show();
	
	// If Canada is selected, show the province selectbox instead of textbox
	if (selectedCountry == "Canada") {
		$('.intl_province').hide();
		$('.intl_province :input').attr('disabled','disabled');
		$('.can_province').show();
		$('.can_province :input').removeAttr('disabled');
	} else {
		$('.can_province').hide();
		$('.can_province :input').attr('disabled','disabled');
		$('.intl_province').show();
		$('.intl_province :input').removeAttr('disabled');
	}
	
	// For update payment flow
	$('#usBillingSpecificForEdit').hide();
	$('#usBillingSpecificForEdit :input').attr('disabled','disabled');
	$('#militaryBillingSpecificForEdit').hide();
	$('#militaryBillingSpecificForEdit :input').attr('disabled','disabled');
	$('#internationalBillingSpecificForEdit').show();
	$('#internationalBillingSpecificForEdit :input').removeAttr('disabled');
	$('.company_name').hide();
	$('.company_name :input').attr('disabled','disabled');
}

function checkCreditCardEnteredInfo(){
	if ($('.card-type-selected').length){
		var cardType = $('.card-type-selected').text().trim();
		var formId = $("form.payment_form").attr('id');
		// show card image
		selectCreditCardType(cardType, formId);
		// show cvn image
		showCVVImage(formId, cardType);
	}
}

function displayErrorOnInvalidCreditCard(formSelector) {

    trackPageLoadTime('end user entered invalid credit card');
	var errorHtml = "<li id=\"invalidCardType\">Oops. We did not recognize the \"type\" of card you entered. Make sure you entered your \"card number\" correctly. </li>";
	performCheckoutErrors = errorHtml;
	var errormessage = "Oops. We did not recognize the type of card you entered. Make sure you entered your card number correctly.";
	
	
	/** DPXVI-6233 Coremetrics tagging for Payment- Client side Validation Error Starts*/
	if(coremetricsTagEnabled) {
		createCoremetricsElementTag('invalidCardType','Error_Checkout_Payment');
	}
	/** DPXVI-6233 Coremetrics tagging for Payment- Client side Validation Error Ends*/
	
	$('.cc_exp_date').hide();
	$('.cc_cvv').hide();
	$('#' + formSelector + ' #ccType').val("");
	$('#' + formSelector + ' #creditCardIconList').children().hide();
	
	if (formSelector != undefined && formSelector == "updateCreditCardRegisteredForm") {
	$('.update_cc_err_msgs ul').html(errorHtml);
	$('.update_cc_err_msgs').show();
	}
	else {
	$(".cc_err_msgs ul").html(errorHtml);
	$(".cc_err_msgs").show();
	}
	
}


function clearCreditCardErrorDetails(data, formSelector){
	var isExpFieldsErrorShown = ($("#expDate").css("display") == "block") ? true : false;
	var isCVVErrorShown = ($("#cvvId").css("display") == "block") ? true : false;
	if(!isExpFieldsErrorShown && !isCVVErrorShown){
		$("#ccExpireError").empty();
	}
}


function fnChangeCreditCardValue(uid, id) {
	$("#addAddressErrorContainer").html("");
	$(".addAddressError").hide();
	//$('#add_new_cc_savedadd :input,#add_new_cc_savedadd label ').not(':disabled').removeClass('error');
	$('.expireDate').val("");
	$('.expireYear').val("");
	$(".CVVInfoDiv").hide();
	$(".paymentErrorContainer").hide();

	$('div[id^="paymentInfo"]').each(function() {
		$(".error").each(function() {
			$(this).removeClass("error");
		});
	});

	showMaskedCreditCardNumberFlag = false;
	if (id == 'Visa') {
		if (typeof document.getElementById('ccType') != "undefined"
				&& document.getElementById('ccType') != null) {
			document.getElementById('ccType').value = "Visa";
		}

		// Added for Add Credit Card modal window
		if (typeof document.getElementById('creditCardNumber') != "undefined"
				&& document.getElementById('creditCardNumber') != null) {
			$('.masterCreditCardNumber').attr("maxlength", 16);
		} else if (typeof document.getElementById('creditcardnumber') != "undefined"
				&& document.getElementById('creditcardnumber') != null) {
			$('.masterCreditCardNumber').attr("maxlength", 16);
		}
		// 8/28 LM - set focus to CVV when displayed
		$('.cc_exp_date').show();
		$('.cc_cvv').show(function(){
			$('#cardVerificationNumber').focus();
		});
		
		$('.cardVerification').attr("maxlength", 3);
		$('.expireDate').attr("disabled", false);
		$('.expireYear').attr("disabled", false);

		if (typeof document.getElementById('cvvId') != "undefined"
				&& document.getElementById('cvvId') != null) {
			document.getElementById('cvvId').style.display = 'block';
		}
		if (typeof document.getElementById('expDate') != "undefined"
				&& document.getElementById('expDate') != null) {
			document.getElementById('expDate').style.display = 'block';
		}

		$('.cardVerification').attr("disabled", false);
		$('.cardVerification').val("");
		$('.expireDate').attr("disabled", false);
		$('.expireYear').attr("disabled", false);
	} else if (id == 'JCP') {
		if (typeof document.getElementById('masterCreditCardNumber') != "undefined"
				&& document.getElementById('masterCreditCardNumber') != null) {
			$('.masterCreditCardNumber').attr("disabled", false);
			$('.masterCreditCardNumber').show();
			$('.lbMasterCreditCardNumber').show();

		}
		if (typeof document.getElementById('visaCreditCardNumber') != "undefined"
				&& document.getElementById('masterCreditCardNumber') != null) {
			$('.visaCreditCardNumber').hide();
			$('.lbVisaCreditCardNumber').hide();
			$('.visaCreditCardNumber').attr("disabled", true);
		}
		if (captureCVNForJCPCards) {
			if (document.getElementById('hdnCreditCardNumber') != null) {
				var cardNbr = document.getElementById('hdnCreditCardNumber').value;
			} else {
				var cardNbr = document.getElementById('hdnCreditCardNumber')
				if (cardNbr == null
						&& document.getElementById('creditCardNumber') != null) {
					cardNbr = document.getElementById('creditCardNumber').value;
				}
			}
			$('.cc_exp_date').hide();
			
			// 8/28 LM - added animations & set focus to CVV when displayed
			$('.cc_cvv').slideDown(function(){
				$('#cardVerificationNumber').focus();
			});
			$('.cardVerification').attr("maxlength", 3);
			
			if (editPaymentpanel) {
				$('#paymentInfoUpdateDiv #cvvId').show();
			}
			$('.expireDate').attr("disabled", true);
			$('.expireYear').attr("disabled", true);

			if (typeof document.getElementById('expDate') != "undefined"
					&& document.getElementById('expDate') != null) {
				document.getElementById('expDate').style.display = 'none';
			}
			$('.cardVerification').attr("disabled", false);
			$('.cardVerification').val("");
			$('.expireDate').attr("disabled", true);
			$('.expireYear').attr("disabled", true);
		} else {
			$('.cc_exp_date').hide();
			$('.cc_cvv').hide();
			$('.expireDate').attr("disabled", true);
			$('.expireYear').attr("disabled", true);

			if (typeof document.getElementById('cvvId') != "undefined"
					&& document.getElementById('cvvId') != null) {
				document.getElementById('cvvId').style.display = 'none';
			}
			if (typeof document.getElementById('expDate') != "undefined"
					&& document.getElementById('expDate') != null) {
				document.getElementById('expDate').style.display = 'none';
			}

			$('.cardVerification').attr("disabled", true);
			$('.expireDate').attr("disabled", true);
			$('.expireYear').attr("disabled", true);
		}
	} else {

		if (typeof document.getElementById('visaCreditCardNumber') != "undefined"
				&& document.getElementById('masterCreditCardNumber') != null) {
			$('.visaCreditCardNumber').hide();
			$('.lbVisaCreditCardNumber').hide();
			$('.visaCreditCardNumber').attr("disabled", true);
		}
		if (shipToCountryId != null && shipToCountryId == 'US') {
			$('.cc_cvv').show();
			$('input[id="cardVerificationNumber"]').val('');
		}
		if (id == 'Amex') {
			$('.masterCreditCardNumber').attr("maxlength", 16);
			if (shipToCountryId != null && shipToCountryId == 'US') {
				$('.cardVerification').attr("maxlength", 4);
			}
		} else {
			$('.masterCreditCardNumber').attr("maxlength", 16);
			if (shipToCountryId != null && shipToCountryId == 'US') {
				$('.cardVerification').attr("maxlength", 3);
			}

		}
		$('.cc_exp_date').show();
		// 8/28 LM set focus to exp month
		$('#expirationMonth').focus()

		$('.expireDate').attr("disabled", false);
		$('.expireYear').attr("disabled", false);
		if (typeof document.getElementById('cvvId') != "undefined"
				&& document.getElementById('cvvId') != null) {
			document.getElementById('cvvId').style.display = 'block';
		}

		if (typeof document.getElementById('expDate') != "undefined"
				&& document.getElementById('expDate') != null) {
			document.getElementById('expDate').style.display = 'block';
		}
		if (shipToCountryId != null && shipToCountryId == 'US') {
			$('.cardVerification').attr("disabled", false);
		}
		$('.expireDate').attr("disabled", false);
		$('.expireYear').attr("disabled", false);
	}

}

function loadBillingAddress(isFromEdit){
	
	// Disable autocomplete for billing address fields
	$('#billingFirstName').attr('autocomplete','off');
	$('#billingLastName').attr('autocomplete','off');
	$('#companyName').attr('autocomplete','off');
	$('#internationalProvince').attr('autocomplete','off');
	
	// Bind the function that needs to be called on select country selectbox
	$('#selectBillingCountry').change(onSelectboxChange);
	
	// Check if the function is called from update flow
	if (isFromEdit) {
		// Triggering a change event to hide the unwanted fields for selected country
		$('#selectBillingCountry').change();
	} else {
		// Show US shipping by default and hide military and international fields
		$('#coldBillingAddress #usBillingSpecific').show();
		$('#coldBillingAddress #usBillingSpecific :input').removeAttr('disabled');
		$('#coldBillingAddress #militaryBillingSpecific').hide();
		$('#coldBillingAddress #militaryBillingSpecific :input').attr('disabled','disabled');
		$('#coldBillingAddress #internationalBillingSpecific').hide();
		$('#coldBillingAddress #internationalBillingSpecific :input').attr('disabled','disabled');
		$('.billing-info #phoneNumberSecondary').hide();
	}
	
	// Format the phone number shown
	if ($('.guest').length){
		$('.billingPhoneNumber').val(formatPhoneNumber($('.billingPhoneNumber').val()));
	}
	
	//checkCreditCardEnteredInfo();
	
}

function spamBillShip(pmtDropdownVal){
	/*if (pmtDropdownVal != undefined && pmtDropdownVal != null) {
		var testCountries = window.antiSpamCountries.replace(/-/g, ',').split(',');
		var checkboxStatus = $.inArray(pmtDropdownVal.toUpperCase(), testCountries) !== -1 || $.inArray(window.jcpDLjcp.common.shipToCountry, testCountries) !== -1 ? false : true;
		$('.signupCheck').attr('checked', checkboxStatus);
		submitEmailPreference2(checkboxStatus);
	}*/
}

function savedCardSubmitClick() {
	showLoadingSpinner("itrmCreditCardRegistered");
	if (document.getElementById("paymentSavedContiue") != null) {
		document.getElementById("paymentSavedContiue").disabled=true;
	}
	var options = {
			success: populateSavedCardInformation,
			error: errorSavedCardInformation,
			timeout: ajaxTimeOut
		};
	if (document.getElementById('pgKey') != null) {
		document.getElementById('paymentGroupKey').value = document.getElementById('pgKey').value;
	}
	$('#savedCardPaymentForm').ajaxSubmit(options);
	//showSpinner('checkoutPage');
	trackPageLoadTime('Saved-Card-Payment-Requested');
}
function showLoadingSpinner(divId) {
	if($("#"+divId) != undefined) {
		$("#"+divId).html('<div class="loading-spinner small" style="display: inline-block;"></div>').addClass("overlay-bg");
	}
}
function hideLoadingSpinner(divId) {
	if($("#"+divId) != undefined) {
		$("#"+divId).removeClass("overlay-bg");
	}
}
function showItrmContainer() {
	if (isAuthRequired || isCVVRequired) {
		$('#itrmCreditCardRegistered').load('/dotcom/jsp/checkout/secure/checkoutsimplified/payment/creditcard/creditCardErrorsRegisteredFrag.jsp', function(){
		});
		$("#itrmCreditCardRegistered").show();
		$("li.card.active").addClass("active-new");
		$("#authMsg").show();
		$("#cardErrorMsg").show();
	}
	$('#paymentContiueSubmit').show().removeAttr('disabled');
	$('#paymentContiue').hide().attr('disabled',true);
}

function populateSavedCardInformation(responseText, statusText, xhr, $form) {
	hideLoadingSpinner("itrmCreditCardRegistered");
	trackPageLoadTime('Saved-Card-Payment-Response-Received');

	var cardAuthFailure = false;
	//added to handle the session expirty redirection.
	if (responseText.redirectURL) {
		window.location.href = responseText.redirectURL + '?sessionExpired=true';
		return;
	}
	/* DPXVI-10183 Session is not getting terminated after 5th unsuccessful attempt - Start*/
    if(typeof responseText.logoutOnAccountLocked != "undefined"){
    	var map = responseText.logoutOnAccountLocked;
    	var cookies = document.cookie.split(';');
    	for(var i=0; i<cookies.length; i++){
    		
    		while (cookies[i].charAt(0) === ' ') {
    	            cookies[i] = cookies[i].substring(1);
    	        }
    		eraseCookie(cookies[i].split("=")[0]);
    	}
    	for (var key in map) {
    		if(key == 'accountLocked'){
    			window.location.href = '/dotcom/jsp/cart/viewShoppingBag.jsp?accountLocked=true';
    		} else if (key == 'guestAccountLocked'){
    			window.location.href = '/dotcom/jsp/cart/viewShoppingBag.jsp?guestAccountLocked=true';
    		}
    	}
    	return;
    }
    /* DPXVI-10183 Session is not getting terminated after 5th unsuccessful attempt - End*/
    if(responseText.paymentErrors != undefined && null != responseText.paymentErrors) {
    	var map = responseText.paymentErrors;
    	var errorHtml = '';
    	var infoHtml = '';
		var disableContinueButton = true;
		var errorCount = 0;
		var paymentRestrictedError = false;
		var noCVVAuthRequired=true;
		isExpiredCard = false;
		$(".cardErrorMsg").hide();
        for (var key in map) {
        	/* DPXVI-5173 Coremetrics tagging for Payment Error Starts*/
        	if(coremetricsTagEnabled) {
				createCoremetricsElementTag(key,'Error_Checkout_Payment');
			}
        	/* DPXVI-5173 Coremetrics tagging for Payment Error Ends*/
        	if (key == "keyCCAuthorisationNeeded") {
        		isAuthRequired = true;
        		$('#itrmCreditCardRegistered').load('/dotcom/jsp/checkout/secure/checkoutsimplified/payment/creditcard/creditCardErrorsRegisteredFrag.jsp', function(){
        		});
        		$("#itrmCreditCardRegistered").show();
        		$("li.card.active").addClass("active-new");
        		$("#itrmCreditCardErrorContainer").show();
        		$("#authMsg").show();
        		$("#authMsg").html(map[key]);
        		$("#cardErrorMsg").show();
        		$('#paymentContiueSubmit').show().removeAttr('disabled');
        		$('#paymentContiue').hide().attr('disabled',true);
        	}
        	if (key =="keyCvvAuthRequired" || key == "keyCvvAuthRequiredForJCPCard"){
        		isCVVRequired = true;
        		$('#itrmCreditCardRegistered').load('/dotcom/jsp/checkout/secure/checkoutsimplified/payment/creditcard/creditCardErrorsRegisteredFrag.jsp', function(){
        			$("#itrmCreditCardErrorContainer").show();
            		$("#savedCardErrorMessage").html(map[key]);
        		});
        		$("#itrmCreditCardRegistered").show();
        		$("li.card.active").addClass("active-new");
        		$("#cardErrorMsg").show();
        		$('#paymentContiueSubmit').show().removeAttr('disabled');
        		$('#paymentContiue').hide().attr('disabled',true);
        	}
			if (key == "keyOCSPaymentRestrictedCardInvalid" || key == "keyOCSCouponNotQualified"){
				paymentRestrictedError = true;
				reloadShippingMethod = true;
			}
			if (key == "keyOCSCreditCardAuthFailed") {
				cardAuthFailure=true;
				errorHtml+="<li>"+map[key]+"</li>";
				noCVVAuthRequired=false;
				document.getElementById("hdnBillingAddressEnabledForBlankPage").value = true;
			} else if(key== "keyOCSCallCreditCardAuthFailed"){
				cardAuthFailure=true;
				errorHtml+="<li>"+callCreditMsgForCheckout+"</li>";
				noCVVAuthRequired=false;
				document.getElementById("hdnBillingAddressEnabledForBlankPage").value = true;
				if(coremetricsTagEnabled && enable2016S10CCDPXVI13877CoreMetCallCredit) {
					createCoremetricsElementTag('Checkout Call Credit Response','Call Credit Response');
					}
			} else if (key == "CREDIT_CARD_EXPIRED") {
				cardValidationNeeded=true;
				isExpiredCard=true;
				errorHtml+="<li>"+map[key]+"</li>";
				disableContinueButton=false;
			} else {
				errorCount++;
				if (key != "keyCCAuthorisationNeeded"){
					errorHtml += "<li>"+map[key]+"</li>";
				} else {
					//for key = keyccauthorisationneeded
					cardValidationNeeded=true;
					infoHtml = map[key];
					disableContinueButton=false;
				}

			}
			if (key =="keyCvvAuthRequired" || key == "keyCvvAuthRequiredForJCPCard"){
				noCVVAuthRequired=false;
			}

		}
		if (paymentRestrictedError && errorCount==1){
			disableContinueButton=false;
		}
		if(editPaymentpanel){
			if (cardAuthFailure) {
				/*$("#paymentInfoUpdateDiv").find('#invalidSavedCCError').html(errorHtml);
	            $("#paymentInfoUpdateDiv").find('#invalidSavedCCErrorContainer').show();*/
			} else {
            /*$("#paymentInfoUpdateDiv").find('#savedCardAddressError').html("<ul>"+errorHtml+"</ul>");
            $("#paymentInfoUpdateDiv").find('#savedCardAddressErrorContainer').show();*/
			}
			if (disableContinueButton && noCVVAuthRequired) {
				$("#paymentInfoUpdateDiv").find("#paymentSavedContiue").attr("disabled","disabled");
			}
			
			if($(document.getElementById('paymentInfoUpdateDiv')).find('#invalidSavedCCErrorContainer').css("display") == 'block') {
				$('html, body').animate({ scrollTop: $("#paymentInfoUpdateDiv").find('#invalidSavedCCErrorContainer').offset().top }, 'slow');
			}
			else if($(document.getElementById('paymentInfoUpdateDiv')).find('#savedCardAddressErrorContainer').css("display") == 'block') {
				$('html, body').animate({ scrollTop: $("#paymentInfoUpdateDiv").find('#savedCardAddressErrorContainer').offset().top }, 'slow');
			}
		} else {
			if (cardAuthFailure) {
				/*document.getElementById('invalidSavedCCError').innerHTML=errorHtml;
	            $('#invalidSavedCCErrorContainer').show();*/
			} else if (!isExpiredCard) {
			/*if(undefined !=document.getElementById('savedCardAddressError')) {
             document.getElementById('savedCardAddressError').innerHTML="<ul>"+errorHtml+"</ul>";
			}
            $('#savedCardAddressErrorContainer').show();*/
			}
			if (disableContinueButton && noCVVAuthRequired) {
				if (document.getElementById("paymentSavedContiue") != null) {
					document.getElementById("paymentSavedContiue").disabled=true;
				}
			}
			
			if($('#invalidSavedCCErrorContainer').css("display") == 'block') {
				$('html, body').animate({ scrollTop: $("#invalidSavedCCErrorContainer").offset().top }, 'slow');
			}
			
			else if($('#savedCardAddressErrorContainer').css("display") == 'block') {
				$('html, body').animate({ scrollTop: $("#savedCardAddressErrorContainer").offset().top }, 'slow');
				
			}
		}
        
		if(cardValidationNeeded){
			var cardIndex = document.getElementById('pgKey').value;
			if (document.getElementById('cardType'+cardIndex) != undefined || document.getElementById('cardType'+cardIndex) != null) {
			var cardType = document.getElementById('cardType'+cardIndex).innerHTML;
			}
			if (isExpiredCard) {
				loadEditCCForm(event, cardIndex, cardType, true);
				$("#expirationMonth").val('MM');
				$("#masterCreditCardNumber").val('');
				$("#updateCCard").hide();
				$("#cancelCCUpdate").hide();
			}
    		if (errorHtml != '') {
        		$("#updateCreditCardErrorContainer").show();
        		$("#updateCreditCardErrorContainer").html(errorHtml);    			
    		} 
    		if (infoHtml != ''){
    			$("#updateCreditCardInfoContainer .info-msgs").html(infoHtml);
    			$("#updateCreditCardInfoContainer").show();
    		}
		}
		 hideSpinner();
    } else {
    	// Continue to review panel
		/*if (responseText.paymentHtml != null) {
			$('#shippingPanel').hide();
			$('#paymentPanel').html(formatHTML(responseText.paymentHtml));
			scrollToTop();
		}*/
		// Refresh the pricing summary panel
		if (responseText.pricingSummaryHtml != null) {
			$('#shippingPanel').hide();
			$('#checkoutPricingSummaryDiv .holder').html(formatHTML(responseText.pricingSummaryHtml));
			$('a.helpIcon').tooltip({
	    		overrideClass: "helpIcontip"
	    	});
		}
		if (responseText.checkoutStepsHtml != null) {
		    var checkoutStepsHtml = $('<textarea />').html(responseText.checkoutStepsHtml).text();
		    $('#checkoutProgressBarDiv').html(checkoutStepsHtml);
		}
		$("#itrmCreditCardRegistered").hide();
		$('#paymentContiueSubmit').show().removeAttr('disabled');
		$('#paymentContiue').hide().attr('disabled',true);
    }
    hideSpinner();
}

function errorSavedCardInformation(responseText, statusText, xhr, $form){
    if(statusText=='timeout'){
        document.getElementById('savedCardAddressError').innerHTML="<ul>There is some technical issue.</ul>";
		$('#savedCardAddressErrorContainer').show();
	}else{
		redirectOnError();
    }
}

function onClickCarouselNavigator(){
	$('.carousel_element_ul').each(function() {
	   var elCarousel = new ProductCarousel();
	   elCarousel.init({
	       carouselListElem : $(this),
	       carouselLeftNav : $(this).prev().prev(),
	       carouselRightNav : $(this).prev()
	   });
	});
}

function addNewCreditCardSubmit(){
	
	// Validating the payment form before submitting
	if (!$("form.add-new-cc").valid()) return false;
	
	//showSpinner('checkoutPage');
	var options = {
		success : addNewCreditCardSuccess,
		error : addNewCreditCardError,
		timeout : ajaxTimeOut
	};
	$("form.add-new-cc").ajaxSubmit(options);
}

function addNewCreditCardSuccess(responseText, statusText, xhr, $form){
	hideSpinner();
	trackPageLoadTime('Add_New_Credit_Card_Response_Served');

	if (responseText.redirectURL) {
		window.location.href = responseText.redirectURL + '?sessionExpired=true';
		return;
	}
	
	if(null != responseText.paymentErrors){
		// Call function to show the error messages
		populateAddNewCreditCardErrors(responseText.paymentErrors);
	} else {
		// Continue to review panel
		if (responseText.paymentHtml != null) {
			$('#paymentPanel').html(formatHTML(responseText.paymentHtml)).show();
			scrollToTop();
		}
		// Refresh the pricing summary panel
		if (responseText.pricingSummaryHtml != null) {
			$('#checkoutPricingSummaryDiv .holder').html(formatHTML(responseText.pricingSummaryHtml));
			$('a.helpIcon').tooltip({
	    		overrideClass: "helpIcontip"
	    	});
		}
		if (responseText.checkoutStepsHtml != null) {
		    var checkoutStepsHtml = $('<textarea />').html(responseText.checkoutStepsHtml).text();
		    $('#checkoutProgressBarDiv').html(checkoutStepsHtml);
		}
		$('#paymentContiueSubmit').removeAttr('disabled');
		initiateCardCorousel();
		coreMetricsPart("Order Summary", "JCP|Checkout");
	}
}

function addNewCreditCardError(responseText, statusText, xhr, $form){
	if (statusText == 'timeout') {
		var errorHTML = "<li>There is an internal error. please try after some time.</li>";
		$('.cc_err_msgs ul').append(errorHTML);
	} else {
		redirectOnError();
	}
}


function populateAddNewCreditCardErrors(errorMsgsMap){
	// Clear existing error messages
	$('.cc_err_msgs ul').html('');
	$('form.add-new-cc .error-msg').remove();
	var errorHtml = "";
	var errorDescription;
	var errorId;
	$.each(errorMsgsMap, function(key, value){
		// Field level messages
		if (key == 'firstName') {
			showFieldErrorMsg('#billingFirstName', value);
		} else if (key == 'lastName') {
			showFieldErrorMsg('#billingLastName', value);
		} else if (key == 'address1') {
			showFieldErrorMsg('#billingStreetAddress1', value);
		} else if (key == 'postalCode') {
			showFieldErrorMsg('#billingZipCode', value);
		} else if (key == 'city') {
			showFieldErrorMsg('#billingCity', value);
		} else if (key == 'phoneNumber') {
			showFieldErrorMsg('#billingPhoneNumber', value);
		} else {
			// Form error message
			errorHtml += "<li>" + value + "</li>";
		}
		errorDescription += value+"|";
		errorId += key+"|";
		// Coremetrics changes - Start.
        // coremetricsTagEnabled is initialized in the jsp (checkout.jsp)
        if(coremetricsTagEnabled) { 
        	createCoremetricsElementTag(key+'.invalid','Error_Checkout_Payment');
        }
        // Coremetrics changes - End.
	});
	if( typeof errorDescription != "undefined"){
		errorDescription = errorDescription.substring(0,errorDescription.length-1)+",";
	}
	if( typeof errorId != "undefined"){
		errorId = errorId.substring(0,errorId.length-1)+",";
	}
	var customErrorStringForOmniture = "Error_checkout_payment,"+errorDescription+errorId+","+","+",";
	//trackAnalyticsEvent ("formError","errorCategory, errorDescription, errorID, errorSKU, formField, formName");//DPXVI-25180 reward errors
	if( typeof pageDataTracker != "undefined"  && typeof pageDataTracker.trackAnalyticsEvent == "function") {
		pageDataTracker.trackAnalyticsEvent ("formError",customErrorStringForOmniture);
	}
	if (errorHtml.length) {
		// Show the error messages
		$('.cc_err_msgs ul').append(errorHtml).show();
		$('.cc_err_msgs').show();
		
		// Scroll to the error messages div
		$('html, body').animate({scrollTop: $('.cc_err_msgs').offset().top}, 'slow');
	}
	
}

function showFieldErrorMsg(selector, msg){
	var errorMsgHtml = '<div class="error error-msg">' + msg + '</div>'
	$(selector).parent().append(errorMsgHtml);
	$(selector).addClass('invalid-input error');
}

function smsOptIn(){
	if($("#text-alert").not(':checked')){
	     // clear SMS phone number field if opt in checkbox is unchecked
		document.getElementById('textAlertNum').value = "";
	} 
	// 8/28 LM - focus on SMS input field
	$("#smsPhone").slideToggle(function(){
		$("#textAlertNum").focus();
	});
}

function smsTCs(){
    if(document.getElementById("moreText").style.display == 'none') {
        document.getElementById("moreText").style.display = 'block';
        document.getElementById("seeLess").innerHTML = 'less details';
    } else {
        document.getElementById("moreText").style.display = 'none';
        document.getElementById("seeLess").innerHTML = 'more details';        
    }
}

var editCreditCard = {
	sequence : ""
};

function isSameCreditCard(self) {
	var seqId = self.hasClass("card") ? self.attr("data-seqid") : self.parents("li.card").attr("data-seqid"),
		flag = (editCreditCard.sequence == seqId) ? true : false;
	editCreditCard.sequence = seqId;
	return flag;
}

function editBillingAddress(event, seqNum) {
	event.stopPropagation();
	$('#addressRepositoryId').val($('#billingAddressId'+seqNum).text());
	var self = $(event.target);
	$(".saved_cards_carousel_element_ul > li").removeClass("active-new").removeClass("active");
	editCreditCard.sequence = self.parents("li.card").addClass("active active-new").attr("data-seqid");
	
	$(".editCreditCardInfo").show();
	$('#updateCreditCardRegistered').hide();
	$(".card-contents").hide();
	$('#itrmCreditCardRegistered').hide();
	// Data populating logic
	
	var countryName = ($('#countryName'+seqNum).text()).trim();
	
	$('#selectCCCountry').val(countryName);
	
	$('#ccFirstName').val($.trim($('#firstName'+seqNum).text()));
	$('#ccLastName').val($.trim($('#lastName'+seqNum).text()));
	$('#ccCompanyName').val($('#company'+seqNum).text().trim());
	var phoneNumber = $('#phoneNumber'+seqNum).text();
	if (phoneNumber){
		phoneNumber = formatPhoneNumber(phoneNumber);
	}
	// US Address
	if(countryName == 'United States'){
		$('#ccStreetAddress1').val($.trim($('#address1'+seqNum).text()));
		$('#ccStreetAddress2').val($('#address2'+seqNum).text().trim());
		$('#ccZipCode').val($('#postalCode'+seqNum).text());
		$('#ccCity').val($.trim($('#city'+seqNum).text()));
		$('#ccState').val($.trim($('#state'+seqNum).attr("data-statecode")));		
		$('#ccPhoneNumber').val($.trim(phoneNumber));
	}else if(countryName == 'APO/FPO/DPO'){
		$('#militaryStreetAddress1').val($.trim($('#address1'+seqNum).text()));
		$('#militaryStreetAddress2').val($('#address2'+seqNum).text());
		$('#militaryZipCode').val($('#postalCode'+seqNum).text());
		$('#militaryType').val($.trim($('#city'+seqNum).text()));
		$('#militaryState').val($.trim($('#state'+seqNum).attr("data-statecode")));
		$('#militaryPhoneNumber').val($.trim($('#phoneNumber'+seqNum).text()));
	}else{
		$('#internationalStreetAddress').val($.trim($('#address1'+seqNum).text()));
		$('#internationalStreetAddress2').val($('#address2'+seqNum).text());
		$('#internationalPostalCode').val($('#postalCode'+seqNum).text());
		$('#internationalCity').val($.trim($('#city'+seqNum).text()));
		$('#internationalPhoneNumber').val($.trim($('#phoneNumber'+seqNum).text()));
	}
	$('#selectCCCountry').change();
	
}

function setSelectedCardSeqNumber(elem, selectedCardSeqNumber) {
	var bool = !isSameCreditCard($(elem));
	if(bool) {
		if($(".editCreditCardInfo").is(":visible")) {
			$(".editCreditCardInfo").hide();
		}
		if($("#updateCreditCardRegistered").is(":visible")) {
			$("#updateCreditCardRegistered").hide();
		}
	}
	document.getElementById('pgKey').value = editCreditCard.sequence = selectedCardSeqNumber;
}
	
/*function setSelectedCardSeqNumber(elem, selectedCardSeqNumber) {
	if(!isSameCreditCard($(elem)) && $(".editCreditCardInfo").is(":visible")) {
		$(".editCreditCardInfo").slideToggle(800, function() {
			editCreditCard.editFormVisible = $(".editCreditCardInfo").is(":visible");
		});
	}
	document.getElementById('pgKey').value = editCreditCard.sequence = selectedCardSeqNumber;
}*/
	
function validateBillingAddressForm(billingAddressFormId){
	if(!$("#BillingAddressForm").valid()) return false;
	var billingForm = document.forms[billingAddressFormId];	
	var copyFieldsMap = new Map();
	
	if (billingForm.elements['billingPhoneNumber'].value != "") {
		copyFieldsMap.set('billingPhoneNumber', 'hdnPrimaryPhoneNumber');
	}
	if (billingForm.elements['militaryPhoneNumber'].value != "") {
		copyFieldsMap.set('militaryPhoneNumber', 'hdnPrimaryPhoneNumber');
	}
	if (billingForm.elements['internationalPhoneNumber'].value != "") {
		copyFieldsMap.set('internationalPhoneNumber', 'hdnPrimaryPhoneNumber');
	}

	if (billingForm.elements['selectBillingCountry'].value == 'United States') {
		if (billingForm.elements['streetAddress1'].value != "") {
			copyFieldsMap.set('streetAddress1', 'hdnBillingStreetAddress1');
		}
		copyFieldsMap.set('streetAddress2', 'hdnBillingStreetAddress2');
		if (billingForm.elements['cityName'].value != "") {
			copyFieldsMap.set('cityName', 'hdnBillingCityName');
		}
		if (billingForm.elements['zipCode'].value != "") {
			copyFieldsMap.set('zipCode', 'hdnBillingZipCode');
		}
	} else if (billingForm.elements['selectBillingCountry'].value == 'AP'  || billingForm.elements['selectBillingCountry'].value == 'APO/FPO/DPO') {
		if (billingForm.elements['militaryStreetAddress1'].value != "") {
			copyFieldsMap.set('militaryStreetAddress1', 'hdnBillingStreetAddress1');
		}
		if (billingForm.elements['militaryStreetAddress2'].value != "") {
			copyFieldsMap.set('militaryStreetAddress2', 'hdnBillingStreetAddress2');
		}
		if (billingForm.elements['militaryZipCode'].value != "") {
			copyFieldsMap.set('militaryZipCode', 'hdnBillingZipCode');
		}
	} else {
		if (billingForm.elements['internationalStreetAddress'].value != "") {
			copyFieldsMap.set('internationalStreetAddress', 'hdnBillingStreetAddress1');
		}
		if (billingForm.elements['internationalStreetAddress2'].value != "") {
			copyFieldsMap.set('internationalStreetAddress2', 'hdnBillingStreetAddress2');
		}
		if (billingForm.elements['internationalStreetAddress3'].value != "") {
			copyFieldsMap.set('internationalStreetAddress3', 'hdnBillingStreetAddress3');
		}
		if (billingForm.elements['billingPhoneNumberSecondary'].value != "") {
			copyFieldsMap.set('billingPhoneNumberSecondary', 'hdnSecondaryPhoneNumber');
		} else if (billingForm.elements['billingPhoneNumberSecondary'].value == ""
				&& billingForm.elements['hdnSecondaryPhoneNumber'].value != "") {
			billingForm.elements['hdnSecondaryPhoneNumber'].value = "";
		}

		if (billingForm.elements['internationalCity'].value != "") {
			copyFieldsMap.set('internationalCity', 'hdnBillingCityName');
		}
		if (billingForm.elements['internationalPostalCode'].value != "") {
			copyFieldsMap.set('internationalPostalCode', 'hdnBillingZipCode');
		}
	}
	
	copyFieldsMap.set('selectBillingCountry', 'hdnCountry');
	if (billingForm.elements['emailId'] != undefined) {
		copyFieldsMap.set('emailId', 'hdnEmailAddress');
	}
	
	// Copy the values to hidden form fields
	copyFormFields(billingForm, billingForm, copyFieldsMap);
	paymentFormIdVar = billingAddressFormId;
	var options = {
		success :saveBillingAddress,
		error :saveBillingAddressFailure,
    };
	$('#'+billingAddressFormId).ajaxSubmit(options);
	
	// If error anchor to message
	if(options.error.name == "saveBillingAddressFailure"){
		$('html, body').animate({
			scrollTop: $(".editCreditCardInfo").offset().top
		}, 1000);
	}
}
	
function saveBillingAddress(responseText, statusText, xhr, $form) {
	if (responseText.paymentHtml != null) {
        var paymentHtml = $('<textarea />').html(responseText.paymentHtml).text();
        $('#paymentPanel').html(paymentHtml);
        $('#paymentContiueSubmit').show().removeAttr('disabled');
		$('#paymentContiue').hide().attr('disabled',true);
		initiateCardCorousel();
  }
	 if (responseText.billingAddressErrorHtml != null) {
	      var billingAddressErrorHtml = $('<textarea />').html(responseText.billingAddressErrorHtml).text();
	      $('#billingAddRegErrorContainer').html(billingAddressErrorHtml);
	      $('#billingAddRegErrorContainer').show();
	      /*$('#giftCardNumber').addClass("error");
	      $('#giftCardId').addClass("error");  */
	}

  hideSpinner('body');

}

function saveBillingAddressFailure(responseText, statusText, xhr, $form){

  hideSpinner('body');
  redirectOnError();
}

function getTrimmedCCNumber(){
	if(editPaymentpanel) {
		selectedCCNumberTemp = $('#paymentInfoUpdateDiv').find('#savedCreditCard').val();
	} else {
		selectedCCNumberTemp = $('#paymentInfoCreateDiv').find('#savedCreditCard').val();
	}
	var selectedCreditCardHdn = $('#'+selectedCCNumberTemp+'selectedCreditCardHdn').val();
	var selectedCreditCardnoHdn = $('#'+selectedCCNumberTemp+'selectedCreditCardNumHdn').val();
	var ccLength = selectedCreditCardnoHdn.length;
	var ccNumTrimmed = selectedCreditCardnoHdn.substring(ccLength-4,ccLength);
	if(editPaymentpanel) {
		if (selectedCreditCardHdn=='Amex'){
			$("#paymentInfoUpdateDiv").find('#checkCreditCardValidityDiv #reenteredCCNumber').attr("maxlength",15);
		}else if (selectedCreditCardHdn=='JCP'){
			$("#paymentInfoUpdateDiv").find('#checkCreditCardValidityDiv #reenteredCCNumber').attr("maxlength",11);
		}else {
			$("#paymentInfoUpdateDiv").find('#checkCreditCardValidityDiv #reenteredCCNumber').attr("maxlength",16);
		}
	}else {
		if (selectedCreditCardHdn=='Amex'){
			$("#paymentInfoCreateDiv").find('#checkCreditCardValidityDiv #reenteredCCNumber').attr("maxlength",15);
		}else if (selectedCreditCardHdn=='JCP'){
			$("#paymentInfoCreateDiv").find('#checkCreditCardValidityDiv #reenteredCCNumber').attr("maxlength",11);
		}else {
			$("#paymentInfoCreateDiv").find('#checkCreditCardValidityDiv #reenteredCCNumber').attr("maxlength",16);
		}
	}
	return ccNumTrimmed;
}

function loadEditCCForm(event, seqNum, cardType, isfromITRM) {
	$('#paymentContiueSubmit').attr('disabled',true);
	$("#updateCCard").show();
	$("#cancelCCUpdate").show();
	$('#masterCreditCardNumber').removeClass('invalid-input error');
	$('#cardVerificationNumber').removeClass('invalid-input error');
	event.stopPropagation();
	var self = $(event.target);
	if (isfromITRM) {
		$(".saved_cards_carousel_element_ul li.card-" + seqNum).addClass("active active-new");
	} else {
		$(".saved_cards_carousel_element_ul > li").removeClass("active-new active");
		editCreditCard.sequence = self.parents("li.card").addClass("active active-new").attr("data-seqid");
	}
	
	$(".editCreditCardInfo").hide();
	$(".card-contents").hide();
	$('.update_cc_err_msgs').hide();
	$('.update_cc_info_msgs').hide();
	$('#itrmCreditCardRegistered').hide();
	$('#seqNum').val(seqNum);
	editedCreditCardType = cardType;
	selectCreditCardType(cardType, "updateCreditCardRegisteredForm");
	$('#cardVerificationNumber').val('');
	$('#cardVerificationNumber').removeClass('valid-input');
	$('#updateCreditCardRegistered').show();
	$('#masterCreditCardNumber').val($('#ccNumber'+seqNum).text());
	$('#cardVerificationNumber').attr("maxlength",3);
	if (cardType == 'JCP') {
		$('#expDate').hide();
	} else {
		$('#expDate').show();
		// 8/28 LM - set focus to exp month
		$('#expirationMonth').focus()
		$('#expirationMonth').val($('#expMonth'+seqNum).text());
		$('#expirationYear').val($('#expYear'+seqNum).text());
		if (cardType == 'Amex') {
			$('#cardVerificationNumber').attr("maxlength",4);
		}
	}
	$('#billingAddrId').val(($('#billingAddressId'+seqNum).text()).trim());
}

function submitEditCCFrom() {
	
	var clientErrors = false;
	if ($('.update_cc_err_msgs ul').html() == null) {
		var html = '<span class="error-msg-header"><img alt="attention" src="/dotcom/images/error-fill-copy.png">Please correct the errors listed in red below:</span><ul></ul>';
		$('.update_cc_err_msgs').html(html);
	}
	$('.update_cc_err_msgs ul').html("");
	if($("#masterCreditCardNumber").val().trim() == ""){
		$('.update_cc_err_msgs ul').append("<li>Enter a valid credit card number.</li>");
		$('.update_cc_err_msgs').show();
		$('#masterCreditCardNumber').addClass('invalid-input error');
		clientErrors = true;
		return false;
	}
	if ($('#cardVerificationNumber').val() == undefined || $('#cardVerificationNumber').val().trim() == '') {
		$('.update_cc_err_msgs ul').append("<li>Enter a valid card verification number.</li>");
		$('.update_cc_err_msgs').show();
		$('#cardVerificationNumber').addClass('invalid-input error');
		clientErrors = true;
	}
	if (editedCreditCardType != 'JCP') {
	if ($('#expirationMonth').val() == undefined || $('#expirationMonth').val().trim() == 'MM'
		|| $('#expirationMonth').val().trim() == '') {
		$('.update_cc_err_msgs ul').append("<li>Enter a valid card expiration month.</li>");
		$('.update_cc_err_msgs').show();
		$('#expirationMonth').addClass('invalid-input error');
		clientErrors = true;
	}
	
	if ($('#expirationYear').val() == undefined || $('#expirationYear').val().trim() == 'YYYY'
		|| $('#expirationYear').val().trim() == '') {
		$('.update_cc_err_msgs ul').append("<li>Enter a valid card expiration year.</li>");
		$('.update_cc_err_msgs').show();
		$('#expirationYear').addClass('invalid-input error');
		clientErrors = true;
	}
	}
	
	if (clientErrors) {
		return false;
	}
	
	$('#hdnCreditCardNumber').val($('#masterCreditCardNumber').val().trim());
	$('#editedCCType').val(editedCreditCardType);
	$('#cardVerificationNumber1').val($('#cardVerificationNumber').val().trim());
	
	//showSpinner('checkoutPage');
	var options = {
			success : updateCreditCardSuccess,
			error : updateCreditCardError,
			timeout : ajaxTimeOut
		};
	$("#updateCreditCardRegisteredForm").ajaxSubmit(options);
}

function updateCreditCardSuccess(responseText, statusText, xhr, $form) {
	hideSpinner();
	//added to handle the session expirty redirection.
	if (responseText.redirectURL) {
		window.location.href = responseText.redirectURL + '?sessionExpired=true';
		return;
	}
	
	trackPageLoadTime('Update_Credit_Card_Response_Served');

	if (null != responseText.updateCCErrors && undefined != responseText.updateCCErrors) {
	var errorMsgDiv = '';
	var errorHtml = '';

	$('.update_cc_err_msgs ul').html('');
	var errorHtml = "";
	var errorDescription="";
	var errorId;
	$.each(responseText.updateCCErrors, function(key, value){
		errorHtml += "<li>" + value + "</li>";
		errorDescription += value+"|";
		errorId += key+"|";
	});
	if( typeof errorDescription != "undefined"){
		errorDescription = errorDescription.substring(0,errorDescription.length-1)+",";
	}
	if( typeof errorId != "undefined"){
		errorId = errorId.substring(0,errorId.length-1)+",";
	}
	var customErrorStringForOmniture = "Error_checkout_payment,"+errorDescription+errorId+","+","+",";
	//trackAnalyticsEvent ("formError","errorCategory, errorDescription, errorID, errorSKU, formField, formName");//DPXVI-25180 reward errors
	if( typeof pageDataTracker != "undefined"  && typeof pageDataTracker.trackAnalyticsEvent == "function") {
		pageDataTracker.trackAnalyticsEvent ("formError",customErrorStringForOmniture);
	}
	$('.update_cc_err_msgs ul').append(errorHtml);
	$('.update_cc_err_msgs').show();

	$('html, body').animate({scrollTop: $('.update_cc_err_msgs').offset().top}, 'slow');
	
	} else if (responseText.paymentHtml != null) {
		/*$('.payment-methods').load('/dotcom/jsp/checkout/secure/checkoutsimplified/payment/payment.jsp .cards');*/
		//Commenting below codes, as some browser doesn't load the content properly due to having '<textarea />'
		//var paymentHtml = $('<textarea />').html(responseText.paymentHtml).text();
		//$('#paymentPanel').html(paymentHtml);
		$('#paymentPanel').html(formatHTML(responseText.paymentHtml));
		$('#paymentContiueSubmit').show().removeAttr('disabled');
		$('#paymentContiue').hide().attr('disabled',true);
	}
	else if (responseText.reviewHtml != null) {
		$('#paymentPanel').html(formatHTML(responseText.reviewHtml));
	}
}

function updateCreditCardError() {
	
}

function closeEditCCForm() {

}

function savedCardContinueSubmitClick(){

/*	if(editPaymentpanel){
    	if($('#paymentInfoUpdateDiv').find('#savedCardAddressErrorContainerExpired') != null){
    		 $('#paymentInfoUpdateDiv').find("#savedCardAddressErrorContainerExpired").hide();
 	    }
	}else{
		if($('#paymentInfoCreateDiv').find('#savedCardAddressErrorContainerExpired') != null){
    		 $('#paymentInfoCreateDiv').find("#savedCardAddressErrorContainerExpired").hide();
 	    }
	}
	if(isExpiredCard) {
		$('#paymentInfoCreateDiv').find("#errorMessage").hide();
	}*/
	$('#reEnteredCCVerificationNumber').removeClass('invalid-input error');
	$('#reenteredCCNumber').removeClass('invalid-input error');
	if(validateCCReenteredForm()) {
		 var cvvElem = document.getElementById('reEnteredCCVerificationNumber');
		 var cvv= $("#itrmCreditCardRegistered #reEnteredCCVerificationNumber").val();
		 
		 if ((cvvElem != null)  && (cvv == undefined || cvv == '')) {
     		var errorHtml = 'Enter a valid card verification number.';
     	
	     	$("#itrmCreditCardRegistered #itrmCreditCardErrorContainer").find("ul li").html(errorHtml);
			$("#itrmCreditCardRegistered #itrmCreditCardErrorContainer").show();
			$('#reEnteredCCVerificationNumber').addClass('invalid-input error');
			$("#authMsg").hide();
			return false;
		}
	
			/*if(isCreditCardExpired){
			if(editPaymentpanel){
	           $('#paymentInfoUpdateDiv').find("#expiredCardError").html(expiredCreditCardError);
            //Fix for PBI000000032944 - Change Credit card Expiry Error message
			$('#paymentInfoUpdateDiv').find("#savedCardAddressErrorContainer").show();
			$('#paymentInfoUpdateDiv').find("#savedCardAddressError").hide();

			}else{
			$('#paymentInfoCreateDiv').find("#expiredCardError").html(expiredCreditCardError);

			//Fix for PBI000000032944 - Change Credit card Expiry Error message
			$('#paymentInfoCreateDiv').find("#savedCardAddressErrorContainer").show();
			$('#paymentInfoCreateDiv').find("#savedCardAddressError").hide();

			}
			if($(document.getElementById('paymentInfoUpdateDiv')).find('#savedCardAddressErrorContainer').css("display") == 'block') {
				$('html, body').animate({ scrollTop: $("#paymentInfoUpdateDiv").find('#savedCardAddressErrorContainer').offset().top }, 'slow');
			}
			else if($(document.getElementById('paymentInfoCreateDiv')).find('#savedCardAddressErrorContainer').css("display") == 'block') {
				$('html, body').animate({ scrollTop: $("#paymentInfoCreateDiv").find('#savedCardAddressErrorContainer').offset().top }, 'slow');
			}
			else if($(document.getElementById('paymentInfoUpdateDiv')).find('#savedCardAddressError').css("display") == 'block') {
				$('html, body').animate({ scrollTop: $("#paymentInfoUpdateDiv").find('#savedCardAddressError').offset().top }, 'slow');
			}
			else if($(document.getElementById('paymentInfoCreateDiv')).find('#savedCardAddressError').css("display") == 'block') {
				$('html, body').animate({ scrollTop: $("#paymentInfoCreateDiv").find('#savedCardAddressError').offset().top }, 'slow');
			}
			return ;
		}*/
		var giftCardFlag = false;
	  
		var options = {
			success: populateSavedCardContinueInformation,
			error: errorSavedCardContinueInformation,
			timeout: ajaxTimeOut
		};

/*		if(editPaymentpanel) {
			$("input[id*=hdnReenteredCCNumber]").val($('#paymentInfoUpdateDiv').find("#reenteredCCNumber").val());
			$("input[id*=hdnReEnteredExpirationMonth]").val($('#paymentInfoUpdateDiv').find("#reEnteredExpirationMonth").val());
			$("input[id*=hdnReEnteredExpirationYear]").val($('#paymentInfoUpdateDiv').find("#reEnteredExpirationYear").val());
			$("input[id*=hdnReEnteredCCVerificationNumber]").val($('#paymentInfoUpdateDiv').find("#reEnteredCCVerificationNumber").val());
			$("input[id*=paymentGroupId]").val(pgKey);
			$("input[id*=hdnUpdateExpiredCreditCard]").val($('#paymentInfoUpdateDiv').find("#isCreditCardExpired").val());
			$('#paymentInfoUpdateDiv').find("#savedCardPaymentContinueForm").ajaxSubmit(options);
		} else {*/
		
			if ($('#itrmCreditCardRegistered').find("#reenteredCCNumber").val() != undefined) {
			$("input[id*=hdnReenteredCCNumber]").val($('#itrmCreditCardRegistered').find("#reenteredCCNumber").val());
			}
			if ($('#itrmCreditCardRegistered').find("#reEnteredExpirationMonth").val() != undefined) {
			$("input[id*=hdnReEnteredExpirationMonth]").val($('#itrmCreditCardRegistered').find("#reEnteredExpirationMonth").val());
			}
			if ($('#itrmCreditCardRegistered').find("#reEnteredExpirationYear").val() != undefined) {
			$("input[id*=hdnReEnteredExpirationYear]").val($('#itrmCreditCardRegistered').find("#reEnteredExpirationYear").val());
			}
			if ($('#itrmCreditCardRegistered').find("#reEnteredCCVerificationNumber").val() != undefined) {
			$("input[id*=hdnReEnteredCCVerificationNumber]").val($('#itrmCreditCardRegistered').find("#reEnteredCCVerificationNumber").val());
			}
			if (document.getElementById("textAlertNum") != null && document.getElementById("textAlertNum") != undefined) {
				if (document.getElementById("textAlertNum").value != null && document.getElementById("textAlertNum").value != undefined) {
					document.getElementById("textalertnum").value = document.getElementById("textAlertNum").value
				}
			}
			if($('#text-alert').value != undefined){
				if($("#text-alert").is(':checked')){
			    	$('#savedCardPaymentContinueForm').find("#smsalertStatus").val('true');
			    }else{
			    	$('#savedCardPaymentContinueForm').find("#smsalertStatus").val('false');
			    }
			}
			$("input[id*=paymentGroupId]").val(document.getElementById('pgKey').value);
			$("input[id*=hdnUpdateExpiredCreditCard]").val($('#itrmCreditCardRegistered').find("#isCreditCardExpired").val());
			$("#savedCardPaymentContinueForm").ajaxSubmit(options);
/*		}*/
		
/*		setTimeout(function(){$('#checkout_submit_order_btn *:input[type!=hidden]:first').focus()}, 1000);
		if(editPaymentpanel){
			showSpinner('bodyWrapper');
		}else{
			showSpinner('bodyWrapper');
		}*/
	}
	/*else {
		if($(document.getElementById('paymentInfoUpdateDiv')).find('#savedCardAddressErrorContainerExpired').css("display") == 'block') {
			$('html, body').animate({ scrollTop: $("#paymentInfoUpdateDiv").find('#savedCardAddressErrorContainerExpired').offset().top }, 'slow');
		}
		else if($(document.getElementById('paymentInfoCreateDiv')).find('#savedCardAddressErrorContainerExpired').css("display") == 'block') {
			$('html, body').animate({ scrollTop: $("#paymentInfoCreateDiv").find('#savedCardAddressErrorContainerExpired').offset().top }, 'slow');
		}
		else if($(document.getElementById('paymentInfoUpdateDiv')).find('#savedCardAddressErrorContainer').css("display") == 'block') {
			$('html, body').animate({ scrollTop: $("#paymentInfoUpdateDiv").find('#savedCardAddressErrorContainer').offset().top }, 'slow');
		}
		else if($(document.getElementById('paymentInfoCreateDiv')).find('#savedCardAddressErrorContainer').css("display") == 'block') {
			$('html, body').animate({ scrollTop: $("#paymentInfoCreateDiv").find('#savedCardAddressErrorContainer').offset().top }, 'slow');
		}
		else if($(document.getElementById('paymentInfoUpdateDiv')).find('#savedCardAddressError').css("display") == 'block') {
			$('html, body').animate({ scrollTop: $("#paymentInfoUpdateDiv").find('#savedCardAddressError').offset().top }, 'slow');
		}
		else if($(document.getElementById('paymentInfoCreateDiv')).find('#savedCardAddressError').css("display") == 'block') {
			$('html, body').animate({ scrollTop: $("#paymentInfoCreateDiv").find('#savedCardAddressError').offset().top }, 'slow');
		}
	}
*/}

function enterKeyOnCVV(elem, e){
	var code = e.which; 
    if(code == 13){
    	savedCardContinueSubmitClick();
    	
    	e.stopPropagation();
    	e.preventDefault();
    	e.stopImmediatePropagation();
    	
    	return false;
    }
}

function checkNullForCCValue(ccValIsNull) {

/*		if(editPaymentpanel) {
			var enteredCCNumber = $("#itrmCreditCardRegistered").find("#reenteredCCNumber").val();
			if(enteredCCNumber == ""){
				ccValIsNull = true;
			}
		} else {*/
		var enteredCCNumber = $("#itrmCreditCardRegistered").find("#reenteredCCNumber").val();
		if (enteredCCNumber != undefined) {
			if(enteredCCNumber == ""){
				ccValIsNull = false;
			} else {
				ccValIsNull = true;
			}
		} else {
			ccValIsNull = true;
		}
		return ccValIsNull;
}

function validateCCReenteredForm() {
	var ccValIsNull = false;
	ccValIsNull = checkNullForCCValue(ccValIsNull);
	if (!ccValIsNull){
	var ccRegex =  /^[0-9]+$/;
/*	if(editPaymentpanel) {
		var enteredCCNumber = $("#paymentInfoUpdateDiv").find("#checkCreditCardValidityDiv #reenteredCCNumber").val();
		var enteredMonth = $("#paymentInfoUpdateDiv").find("#expDate #reEnteredExpirationMonth").val();
		var enteredYear = $("#paymentInfoUpdateDiv").find("#expDate #reEnteredExpirationYear").val();
		var cvv= $("#paymentInfoUpdateDiv #reEnteredCCVerificationNumber").val();
		        if (ccValIsNull || (enteredCCNumber != undefined && !enteredCCNumber.match(ccRegex))) {
		        	 DPXVI-6233 Coremetrics tagging for Payment- Client side Validation Error Starts
		        	if(coremetricsTagEnabled) {
						createCoremetricsElementTag('reenteredCCNumber.invalid','Error_Checkout_Payment');
					}
		        	 DPXVI-6233 Coremetrics tagging for Payment- Client side Validation Error Ends
		        	$("#paymentInfoUpdateDiv").find('#checkCreditCardValidityDiv #errorMessage div span').html('Enter a valid credit card number');
		        	$("#paymentInfoUpdateDiv").find('#checkCreditCardValidityDiv #infoMessage').hide();
		        	$("#paymentInfoUpdateDiv").find('#checkCreditCardValidityDiv #errorMessage').show();
		        	$("#paymentInfoUpdateDiv").find("#checkCreditCardValidityDiv #reenteredCCNumber").val("");
		        	$("#paymentInfoCreateDiv").find("#checkCreditCardValidityDiv #reenteredCCNumber").attr("placeholder","re-enter credit card number")
		        	ccValIsNull=false;
		        	return false;
			}
		        if((enteredMonth!= undefined && enteredMonth =='month')|| (enteredYear!=undefined && enteredYear=='year')) {
		        	 DPXVI-6233 Coremetrics tagging for Payment- Client side Validation Error Starts
		        	if(coremetricsTagEnabled) {
						createCoremetricsElementTag('reenteredExpDateYear.invalid','Error_Checkout_Payment');
					}
		        	 DPXVI-6233 Coremetrics tagging for Payment- Client side Validation Error Ends
		        	$("#savedCardAddressErrorContainer #savedCardAddressError").find("ul li").html('Enter a valid Expiration date');
		        	return false;
		        }
		        if (cvv !=undefined && cvv=='') {
		        	 DPXVI-6233 Coremetrics tagging for Payment- Client side Validation Error Starts
		        	if(coremetricsTagEnabled) {
						createCoremetricsElementTag('reEnteredCCVerificationNumber.invalid','Error_Checkout_Payment');
					}
		        	 DPXVI-6233 Coremetrics tagging for Payment- Client side Validation Error Ends
		        	$("#savedCardAddressErrorContainer #savedCardAddressError").find("ul li").html('Enter a valid CVV number');
		        	return false;
		        }
		}else{*/
			var enteredCCNumber = $("#itrmCreditCardRegistered").find("#reenteredCCNumber").val();
			var enteredMonth = $("#itrmCreditCardRegistered").find("#expDate #reEnteredExpirationMonth").val();
			var enteredYear = $("#itrmCreditCardRegistered").find("#expDate #reEnteredExpirationYear").val();
			var cvv= $("#itrmCreditCardRegistered #reEnteredCCVerificationNumber").val();
			var cardType = $("#itrmCreditCardRegistered #cardType").val();
	        if (ccValIsNull || (enteredCCNumber != undefined && !enteredCCNumber.match(ccRegex))) {
	        	/* DPXVI-6233 Coremetrics tagging for Payment- Client side Validation Error Starts*/
	        	if(coremetricsTagEnabled) {
					createCoremetricsElementTag('reenteredCCNumber.invalid','Error_Checkout_Payment');
				}
	        	/* DPXVI-6233 Coremetrics tagging for Payment- Client side Validation Error Ends*/
	        	if (enteredCCNumber == undefined || enteredCCNumber == '') {
	        	var errorHtml = 'Enter a valid credit card number.';
	        	}
	        	$('#reenteredCCNumber').addClass('invalid-input error');
	        	$("#itrmCreditCardRegistered #itrmCreditCardErrorContainer").find("ul li").html(errorHtml);
    			$("#itrmCreditCardRegistered #itrmCreditCardErrorContainer").show();
    			$("#authMsg").hide();
	        	ccValIsNull=false;
	        	return false;
	        }

		        
		        /*if(isExpiredCard){
		        	console.log("Here", $("#savedCardAddressErrorContainerExpired #expiredCardError"));
		        	if((enteredMonth!= undefined && enteredMonth =='month')|| (enteredYear!=undefined && enteredYear=='year')) {
		        		 DPXVI-6233 Coremetrics tagging for Payment- Client side Validation Error Starts
		        		if(coremetricsTagEnabled) {
							createCoremetricsElementTag('reenteredExpDateYear.invalid','Error_Checkout_Payment');
						}
		        		 DPXVI-6233 Coremetrics tagging for Payment- Client side Validation Error Ends
		        		$("#expiredCCDummy").html('Enter a valid Expiration date');
			        	$("#errorMessage").hide();
			        	$("#expiredCCDummy").parent().parent().show();
			        	return false;
			        }
			        if (cvv !=undefined && cvv=='') {
			        	 DPXVI-6233 Coremetrics tagging for Payment- Client side Validation Error Starts
			        	if(coremetricsTagEnabled) {
							createCoremetricsElementTag('reEnteredCCVerificationNumber.invalid','Error_Checkout_Payment');
						}
			        	 DPXVI-6233 Coremetrics tagging for Payment- Client side Validation Error Ends
			        	$("#expiredCCDummy").html('Enter a valid CVV number');
			        	$("#errorMessage").hide();
			        	$("#expiredCCDummy").parent().parent().show();
			        	return false;
			        }
		        }*/
		        /*else{*/
		        	if (cardType != 'JCP') {
			        	if((enteredMonth!= undefined && enteredMonth =='month')|| (enteredYear!=undefined && enteredYear=='year')) {
			        		/* DPXVI-6233 Coremetrics tagging for Payment- Client side Validation Error Starts*/
			        		if(coremetricsTagEnabled) {
								createCoremetricsElementTag('reenteredExpDateYear.invalid','Error_Checkout_Payment');
							}
			        		/* DPXVI-6233 Coremetrics tagging for Payment- Client side Validation Error Ends*/
			        		$("#itrmCreditCardRegistered #errorMessage").find("ul li").html('Enter a valid Expiration date');
				        	return false;
				        }
		        	}
			        if (cvv !=undefined && cvv=='') {
			        	/* DPXVI-6233 Coremetrics tagging for Payment- Client side Validation Error Starts*/
			        	if(coremetricsTagEnabled) {
							createCoremetricsElementTag('reEnteredCCVerificationNumber.invalid','Error_Checkout_Payment');
						}
			        	/* DPXVI-6233 Coremetrics tagging for Payment- Client side Validation Error Ends*/
			        	$("#itrmCreditCardRegistered #errorMessage").find("ul li").html('Enter a valid CVV number');
			        	return false;
			        }
		       /* }*/

		/*}*/
	}
	
	return true;
}

function populateSavedCardContinueInformation(responseText, statusText, xhr, $form){

	//added to handle the session expirty redirection.
	if (responseText.redirectURL) {
		window.location.href = responseText.redirectURL + '?sessionExpired=true';
		return;
	}
	
	// Update the checkout progress bar
	var completedPanels = responseText.completedPanels;
	//updateProgressBar(responseText.currentPanel, completedPanels);
	
	/* DPXVI-10183 Session is not getting terminated after 5th unsuccessful attempt - Start*/
    if(typeof responseText.logoutOnAccountLocked != "undefined"){
    	var map = responseText.logoutOnAccountLocked;
    	var cookies = document.cookie.split(';');
    	for(var i=0; i<cookies.length; i++){
    		while (cookies[i].charAt(0) === ' ') {
    	            cookies[i] = cookies[i].substring(1);
    	    }
    		eraseCookie(cookies[i].split("=")[0]);
    	}
    	for (var key in map) {
    		if(key == 'accountLocked'){
    			window.location.href = '/dotcom/jsp/cart/viewShoppingBag.jsp?accountLocked=true';
    		} else if (key == 'guestAccountLocked'){
    			window.location.href = '/dotcom/jsp/cart/viewShoppingBag.jsp?guestAccountLocked=true';
    		}
    	}
    	return;
    }
    /* DPXVI-10183 Session is not getting terminated after 5th unsuccessful attempt - End*/
    var ccAuthorized = checkSavedCreditCardAuthorised(responseText.paymentErrors, false);
	
	if(typeof responseText.paymentErrors != "undefined") {
		if(ccAuthorized) {
			var errorHtml = '';
			var map = responseText.paymentErrors;
			var disableContinueButton = true;
			var errorCount = 0;
			var paymentRestrictedError = false;

	        for (var key in map) {
	        	/* DPXVI-5173 Coremetrics tagging for Payment Error Starts*/
	        	if(coremetricsTagEnabled) {
	        		if(key != "keyCardVerificationNumberInvalid") {
					createCoremetricsElementTag(key,'Error_Checkout_Payment');
	        		}
				}
	        	/* DPXVI-5173 Coremetrics tagging for Payment Error Ends*/
	        	if(key == "keyCCValidationFailed"){
	        		hasCCValidationError = true;
	        		disableContinueButton=false;
	        	}
	        	if(key == 'keyOCSAccountLock') {
	        		if (document.getElementById('checkoutlogoutForm')) {
						document.getElementById('checkoutlogoutForm').submit();
					}
	        	}
				if (key == "keyOCSCreditCardAuthFailed" || key== "keyOCSCallCreditCardAuthFailed") {
					disableContinueButton=false;
					if(coremetricsTagEnabled && enable2016S10CCDPXVI13877CoreMetCallCredit && key== "keyOCSCallCreditCardAuthFailed") {
						createCoremetricsElementTag('Checkout Call Credit Response','Call Credit Response');
						}
				}
				if (key == "keyCreditCardExpireDateMissing") {
					disableContinueButton=false;
				}
				
				errorHtml += map[key]

			}

	        /*if(editPaymentpanel){
	        	if($('#paymentInfoUpdateDiv').find("#isCreditCardExpired").val() == 'true') {
	        		$("#paymentInfoUpdateDiv").find('#expiredCardError').html("<ul>"+errorHtml+"</ul>");
			        $("#paymentInfoUpdateDiv").find('#savedCardAddressErrorContainerExpired').show();
	        	} else {
	        		$("#paymentInfoUpdateDiv").find('#savedCardAddressError').html("<ul>"+errorHtml+"</ul>");
			        $("#paymentInfoUpdateDiv").find('#savedCardAddressErrorContainer').show();
	        	}
	        	if($('#paymentInfoUpdateDiv').find("#hdnUpdateExpiredCreditCard").val() == 'true') {
		        	disableContinueButton=false;
		        }
			}else{*/
				/*if(isExpiredCard) {
					if($('#paymentInfoCreateDiv').find("#isCreditCardExpired").val() == 'true') {
		        		$('#expiredCCDummy').html("<ul>"+errorHtml+"</ul>");
				        $('#savedCardAddressErrorContainerExpired').show();
		        	} else {
		        		$('#expiredCCDummy').html("<ul>"+errorHtml+"</ul>");
				        $('#savedCardAddressErrorContainerExpired').show();
		        	}
					
			        if($('#paymentInfoCreateDiv').find("#hdnUpdateExpiredCreditCard").val() == 'true') {
			        	disableContinueButton=false;
			        }
				} 
				else {
					if($('#paymentInfoCreateDiv').find("#isCreditCardExpired").val() == 'true') {
		        		$("#paymentInfoCreateDiv").find('#expiredCardError').html("<ul>"+errorHtml+"</ul>");
				        $("#paymentInfoCreateDiv").find('#savedCardAddressErrorContainerExpired').show();
		        	} else {*/
	        			$("#itrmCreditCardRegistered #itrmCreditCardErrorContainer").find("ul li").html(errorHtml);
	        			$("#itrmCreditCardRegistered #itrmCreditCardErrorContainer").show();
/*		        		$("#paymentInfoCreateDiv").find('#savedCardAddressError').html("<ul>"+errorHtml+"</ul>");
				        $("#paymentInfoCreateDiv").find('#savedCardAddressErrorContainer').show();*/
		        	/*}*/
					
			       /* if($('#paymentInfoCreateDiv').find("#hdnUpdateExpiredCreditCard").val() == 'true') {
			        	disableContinueButton=false;
			        }
				}*/
			}
			/*if (disableContinueButton) {
				$("#paymentInfoUpdateDiv").find("#paymentSavedContiue").attr("disabled","disabled");
			}*/
		/*}*/


	} else if (responseText.isPromoFinacingOffersAvailable == 'true') {
		openModal("/dotcom/jsp/checkout/secure/checkoutsimplified/payment/promoFinancingOffersModal.jsp", null, null, "promoFinancingDeclineFormSubmit");		
	} else {
			// Continue to review panel
		if (responseText.reviewHtml != null) {
			$('#paymentPanel').hide();
			$('#orderReview').html(formatHTML(responseText.reviewHtml)).show();
			scrollToTop();
		}
		// Refresh the pricing summary panel
		if (responseText.pricingSummaryHtml != null) {
			$('#paymentPanel').hide();
			$('#checkoutPricingSummaryDiv .holder').html(formatHTML(responseText.pricingSummaryHtml));
			$('a.helpIcon').tooltip({
	    		overrideClass: "helpIcontip"
	    	});
		}
		if (responseText.checkoutStepsHtml != null) {
		    var checkoutStepsHtml = $('<textarea />').html(responseText.checkoutStepsHtml).text();
		    $('#checkoutProgressBarDiv').html(checkoutStepsHtml);
		}
		coreMetricsPart("Order Summary", "JCP|Checkout");
	}
	
}

function errorSavedCardContinueInformation(responseText, statusText, xhr, $form){
	hideSpinner();
	redirectOnError();
}

function checkSavedCreditCardAuthorised(paymenterrorsmap, infoMessage){
	var ccAuthorized = true;
	if(isCreditCardExpired) {
		

	} else {

		if(typeof paymenterrorsmap != "undefined")  {/*
			for (var key in paymenterrorsmap) {
				 DPXVI-5173 Coremetrics tagging for Payment Error Starts
				if(coremetricsTagEnabled) {
					createCoremetricsElementTag(key,'Error_Checkout_Payment');
				}
				 DPXVI-5173 Coremetrics tagging for Payment Error Ends
				if (key == "keyCCAuthorisationNeeded" || key == "keyCCValidationFailed"){
					ccNumTrimmed = getTrimmedCCNumber();
					if(infoMessage){
						if(editPaymentpanel) {
							$("#paymentInfoUpdateDiv").find('#checkCreditCardValidityDiv #errorMessage').hide();
							$("#paymentInfoUpdateDiv").find('#checkCreditCardValidityDiv #infoMessage div ul li').html(paymenterrorsmap[key]);
							$("#paymentInfoUpdateDiv").find('#checkCreditCardValidityDiv #infoMessage').show();
							$("#paymentInfoUpdateDiv").find('#checkCreditCardValidityDiv #reenteredCCNumberLablel span').html(ccNumTrimmed);
							$("#paymentInfoUpdateDiv").find('#checkCreditCardValidityDiv').show();

						}
						else {
							$("#paymentInfoCreateDiv").find('#checkCreditCardValidityDiv #errorMessage').hide();
							$("#paymentInfoCreateDiv").find('#checkCreditCardValidityDiv #infoMessage div ul li').html(paymenterrorsmap[key]);
							$("#paymentInfoCreateDiv").find('#checkCreditCardValidityDiv #infoMessage').show();
							$("#paymentInfoCreateDiv").find('#checkCreditCardValidityDiv #reenteredCCNumberLablel span').html(ccNumTrimmed);
							$("#paymentInfoCreateDiv").find('#checkCreditCardValidityDiv').show();
						}
					} else {
						$('input#reenteredCCNumber').addClass("error");
						if(editPaymentpanel) {
							$("#paymentInfoUpdateDiv").find('#checkCreditCardValidityDiv #errorMessage').hide();
							$("#paymentInfoUpdateDiv").find('#checkCreditCardValidityDiv #infoMessage').hide();
							$("#paymentInfoUpdateDiv").find('#checkCreditCardValidityDiv #errorMessage div span').html(paymenterrorsmap[key]);
							$("#paymentInfoUpdateDiv").find('#checkCreditCardValidityDiv #errorMessage').show();
							$("#paymentInfoUpdateDiv").find('#checkCreditCardValidityDiv #reenteredCCNumberLablel span').html(ccNumTrimmed);
							$("#paymentInfoUpdateDiv").find('#checkCreditCardValidityDiv').show();
						}
						else {
							$("#paymentInfoCreateDiv").find('#checkCreditCardValidityDiv #errorMessage').hide();
							$("#paymentInfoCreateDiv").find('#checkCreditCardValidityDiv #infoMessage').hide();
							$("#paymentInfoCreateDiv").find('#checkCreditCardValidityDiv #errorMessage div span').html(paymenterrorsmap[key]);
							$("#paymentInfoCreateDiv").find('#checkCreditCardValidityDiv #errorMessage').show();
							$("#paymentInfoCreateDiv").find('#checkCreditCardValidityDiv #reenteredCCNumberLablel span').html(ccNumTrimmed);
							$("#paymentInfoCreateDiv").find('#checkCreditCardValidityDiv').show();
						}
					}
					ccAuthorized = false;

				}else if (key == "accountLocked"){
					ccAuthorized = false;
					if (document.getElementById('checkoutlogoutForm')) {
						document.getElementById('checkoutlogoutForm').submit();
					}
				}
		    }
		*/}
	}
	return ccAuthorized;
}

function setCheckoutCheckbox(e) {
	var currCheckboxVal = $(e.target).is(':checked');
	
	$('li.card .card-payment-express input').attr('checked', false);
	$(e.target).attr('checked', currCheckboxVal);
	
	if(currCheckboxVal) {
		cmCreateConversionEventTag('Set default payment method', '2', 'Express Checkout');
	}
	$("#useCardForExpressCheckout").val(currCheckboxVal);
	$("#setCardForExpressCO").val(currCheckboxVal);
}

function getSMSCheckBoxValue() {
    if($("#text-alert").is(':checked')){
    	$('#savedCardPaymentContinueForm').find("#smsalertStatus").val('true');
    }else{
    	$('#savedCardPaymentContinueForm').find("#smsalertStatus").val('false');
    }
  }
