<%@ taglib uri="dsp" prefix="dsp" %>
<%@ taglib uri="c" prefix="c"  %>
<%@ taglib uri="dspel" prefix="dspel"  %>
<%@ taglib uri="fmt" prefix="fmt" %>

<dsp:page xml="true">
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/RQLQueryForEach"/>
	
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/com/jcpenney/profile/bean/UserSessionBean"/>	
	
	<dsp:importbean bean="/com/jcpenney/ocs/OCSApplicationConfiguration"/>
	<dsp:importbean bean="/com/jcpenney/core/ApplicationConfiguration" var="appConfig" />
	<dsp:importbean bean="/com/jcpenney/order/CheckoutApplicationConfiguration"/>
	<dsp:importbean bean="/com/jcpenney/profile/ProfileApplicationConfiguration"/>
	
	<dsp:getvalueof var="contextroot" vartype="java.lang.String" bean="/OriginatingRequest.contextPath"/>
	<dsp:getvalueof var="countryCodeUS" bean="ProfileApplicationConfiguration.countryCodeUS"/>
	<dsp:getvalueof var="countryCodeAP" bean="ProfileApplicationConfiguration.countryCodeAP"/>
    <dsp:getvalueof var="enableEmailValidationError" bean="ApplicationConfiguration.enable2016S8CCDPXVI7000EmailValidationError"/>
    <dsp:getvalueof var="isTransient" bean="Profile.transient"/>
    
    <fmt:setBundle basename="/com/jcpenney/dp/core/order/CheckoutResources" var="checkoutResources" scope="application"/>
    <fmt:setBundle basename="com/jcpenney/dp/core/profile/ProfileResources" var="profileBundle" scope="application"/>
	
	<dsp:getvalueof var="usBillingSpecific" vartype="java.lang.String" param="usBillingSpecific"/>
	<dsp:getvalueof var="militaryBillingSpecific" vartype="java.lang.String" param="militaryBillingSpecific"/>
	<dsp:getvalueof var="internationalBillingSpecific" vartype="java.lang.String" param="internationalBillingSpecific"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/BillingAddressFormHandler"/>
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
	<dsp:getvalueof var="cardSeq" vartype="java.lang.String" param="cardSeq"/>
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
	
	<dsp:getvalueof var="enableAddressTypeAhead" bean="ApplicationConfiguration.enableAddressTypeAhead"/>
	<dsp:getvalueof var="invokeAddressTypeAhead" value="" />
    
	<%-- Configurable messages for billing address page --%>    
	<dsp:include page="/jsp/global/configurableMessage.jsp">
		<dsp:param name="page" value="cam"/>
		<dsp:param name="keys" value="keyProfileZipCodeInvalid"/>
	</dsp:include>
	
	<%-- Changes added for international  - Start --%>
	<dsp:getvalueof var="countryNameId" bean="ShoppingCart.current.shipToCountry" />
	<%-- Changes added for international  - End --%>

	<c:choose>
		<c:when test="${usBillingSpecific == 'usBillingSpecific'}">
			<dsp:getvalueof var="fromPage" value="ccPaymentPage" />
		</c:when>
		<c:when test="${usBillingSpecific == 'usBillingSpecificForEdit'}">
			<dsp:getvalueof var="fromPage" value="ccPaymentPageForEdit" />
		</c:when>
		<c:otherwise>
		</c:otherwise>
	</c:choose>

	<dsp:droplet name="/atg/dynamo/droplet/ForEach">
		<dsp:param name="array" bean="ShoppingCart.current.shippingGroups" />
		<dsp:oparam name="output">
			<dsp:droplet name="Switch">
				<dsp:param name="value" param="element.shippingGroupClassType" />
				<dsp:oparam name="storeShippingGroup"></dsp:oparam>
				<dsp:oparam name="electronicShippingGroup"></dsp:oparam>
				<dsp:oparam name="hardgoodShippingGroup">
					<dsp:getvalueof var="giftRegistryAddressFlag" param="element.shippingAddress.registrantAddress" />
				</dsp:oparam>
				<dsp:oparam name="bopus"></dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
	
	<%-- Change billing address form --%>
	<div class="checkout update-billing billing-info">
		<h2 class="billingInfoHeader">Edit Billing Address</h2>
		<div class="gift_card_error_msgs mrgb10" id="billingAddRegErrorContainer" style="display:none">
			<dsp:droplet name="IsEmpty">
			<dsp:param bean="ProfileFormHandler.formExceptions" name="value"/>							  
			<dsp:oparam name="false">
			 <dsp:include page="/jsp/checkout/secure/checkoutsimplified/payment/billing/billingAddressEditRegisteredError.jsp"/>
			</dsp:oparam>
			</dsp:droplet>
		</div>
		
		<%-- Error messages for billing --%>
		<div class="bill_add_err_msgs dynamic_error_msgs hide" id="billingAddressErrorContainer">
			<span class="error-msg-header"><img alt="attention" src="/dotcom/images/error-fill-copy.png">Please correct the errors listed in red below:</span>
			<ul class="error-msgs"></ul>
		</div>

		<c:if test="${countryNameId != 'US'}">
			<div class="dynamic_error_msgs mrgb10 hide_display intrlPaymentErrorContainer" id="internationalPaymentErrorContainer">
				<div class="flt_lft mrgr15 exclamation_icn">
					<img alt="attention" src="<c:out value="${contextroot}"/>/images/spacer.gif" /><span>attention</span>
				</div>
				<div class="float_fix">
					<span class="disp_blk">Please correct the errors listed in red below:</span>
					<ul>
						<li class="hide_display"></li>
					</ul>
					<div id="internationalAddressError" class="paymentAddressError">
					</div>
				</div>
			</div>
		</c:if>
		

		<div id="coldBillingAddress" class="row hide_btm_space">
			<%-- Choose country dropdown --%>
			<span id="billingCountry${cardSeq}">
			<div id="shipCountry" class="row country_select">
				<label id="selectBillingCountryLabel" for="selectBillingCountry">country or APO/FPO/DPO</label>

				<%--Code Change to Display all International Countries Fix For defect 1841--%>
				<dspel:select bean="ProfileFormHandler.value.countryName" name="selectBillingCountry" onchange="onSelectboxChange(this);"
					id="selectCCCountry" iclass="flt_lft" onfocus="clearFieldStyle(this, 'selectBillingCountryLabel', 'flt_lft', '','false')">
					<dspel:option value="APO/FPO/DPO">APO/FPO/DPO</dspel:option>
					<dsp:droplet name="ForEach">
						<dsp:param name="array" bean="CheckoutApplicationConfiguration.countriesMap"/>
						<dsp:oparam name="output">
							<dsp:getvalueof var="countryCode" param="key" />
							<dsp:getvalueof var="countryName" param="element" />
							<dsp:droplet name="IsEmpty">
								<dspel:param bean="ProfileFormHandler.formExceptions" name="value" />
								<dsp:oparam name="true">
								<c:choose>
								<c:when test="${countryCode == countryNameId}">
									<dspel:option selected="true" value="${countryName}">${countryName}</dspel:option>
								</c:when>
								<c:otherwise>
									<dspel:option value="${countryName}">${countryName}</dspel:option>
								</c:otherwise>
							</c:choose>
								</dsp:oparam>
								<dsp:oparam name="false">
									<dspel:option value="${countryName}">${countryName}</dspel:option>
								</dsp:oparam>
							</dsp:droplet>
						</dsp:oparam>
					</dsp:droplet>
				</dspel:select>

			</div>
			</span>
			<%--First, Middle and Last Name--%>
			<div class="clear_floats"></div>
			<div class="row name_details">
				<div class="first_name">
					<div class="label-wrap">
						<label id="firstNameLabelbill" for="firstName">First Name</label>
						<span class="mandatory">*</span>
						<div id="firstNameBillingError"></div>
					</div>
						<dspel:input name="firstName" type="text" bean="ProfileFormHandler.value.firstName" value=""
							iclass="input_txt input_name" id="ccFirstName" maxlength="17" onfocus="clearFieldStyle(this, 'firstNameLabelbill', 'input_txt input_name', '','false')" />
				</div>
				<div class="last_name">
					<div class="label-wrap">
						<label id="lastNameLabelbill" for="lastName">Last Name</label>
						<span class="mandatory">*</span>
						<div id="lastNameBillingError"></div>
					</div>
					<dspel:input name="lastName" type="text" bean="ProfileFormHandler.value.lastName" value=""
						iclass="input_txt input_name" id="ccLastName" maxlength="24" onfocus="clearFieldStyle(this, 'lastNameLabelbill', 'input_txt input_name', '','false')" />
				</div>
			</div>
			<%--Company Name--%>
			<div id="companyBlock" class="row company_name optionalCol">
				<label for="companyName">Company (optional)</label>
				<dspel:input name="companyName" type="text" bean="ProfileFormHandler.value.companyName" maxlength="23" iclass="input_txt input_length_txt" id="ccCompanyName">
					<dspel:tagAttribute name="placeholder" value="optional" />
				</dspel:input>
			</div>
			
			<%--Specific to United States Country Location--%>
			<div id="<c:out value='${usBillingSpecific}'/>"  class="country-specific-fields">

				<%--United States Street Address--%>
				<div class="row bill_address">
					<div id="address1Blk" class="flt_lft bill_add1">
						<div class="label-wrap">
							<label id="streetAddress1Labelbill" for="streetAddress1">Street Address</label>
							<span class="mandatory">*</span>
							<div id="streetAddress1Error"></div>
						</div>
						<c:choose>
							<c:when test="${isStoreShippingSelectedFlag}">
								<input type="text" class="input_txt billingAddress1 input_address" id="ccStreetAddress1" maxlength="40" name="streetAddress1"
									autocomplete="off" autofocus="autofocus" onfocus="clearFieldStyle(this, 'streetAddress1Labelbill', 'input_txt input_address billingAddress1', '','false')" />
							</c:when>
							<c:otherwise>
								<c:if test="${enableAddressTypeAhead eq 'true' }" >
									<dsp:getvalueof var="invokeAddressTypeAhead" value="suggestAddress('#ccStreetAddress1', '#ccCity', '#ccState', '#ccZipCode');" />
								</c:if>
								<input type="text" class="input_txt billingAddress1 input_address" id="ccStreetAddress1" maxlength="40" name="streetAddress1"
									autocomplete="off" onkeypress="${invokeAddressTypeAhead}" onfocus="clearFieldStyle(this, 'streetAddress1Labelbill', 'input_txt input_address billingAddress1', '','false')" />
							</c:otherwise>
						</c:choose>
					</div>
					<div id="address2Blk" class="flt_lft bill_add2 optionalCol">
						<label for="streetAddress2">Apt, Suite, Floor (optional)</label> 
						<input type="text" placeholder="optional" class="input_txt billingAddress2" id="ccStreetAddress2"
							maxlength="40" name="streetAddress2" autocomplete="off"/>
					</div>
				</div>
				
				<%--United States City, State and Zip Code--%>
				<div class="row bill_location">
					<%--Auto Populate City/State from Zip Code (Payment) START--%>
					<div id="<c:out value='${fromPage}'/>_multiCityInfoMsg" class="multiCityInfoMsg bgcolor hide">
						<div class="flt_lft mrgr10 information_icn">
							<img alt="attention" src="/dotcom/images/spacer.gif">
						</div>
						<div class="float_fix">
							<p>Choose your city.</p>
						</div>
					</div>
					<div id="zipCityBillingError"></div>
					<div id="zipBlk" class="flt_lft bill_zip">
						<label id="zipCodeLabelbill" for="zipCode">Zip Code <span class="mandatory">*</span></label> 
						<input type="text" class="input_txt input_zip" id="ccZipCode" 
						name="zipCode" maxlength="5" autocomplete="off" onfocus="clearFieldStyle(this, 'zipCodeLabelbill', 'input_txt input_zip', '','false')"
							onblur="zipProc.validateZip(this.value,'<c:out value='${usBillingSpecific}'/> #cityBlk #ccCity',
								 '<c:out value='${usBillingSpecific}'/> #stateSelectBlk #ccState',
								 '<c:out value='${usBillingSpecific}'/> #zipBlk #ccZipCode','<c:out value='${fromPage}'/>')"; />
					</div>
					<div id="cityBlk" class="flt_lft bill_city">
						<label id="cityNameLabelbill" for="cityName">City <span class="mandatory">*</span></label> 
						<input type="text" class="input_txt billingAddresscity1 input_city" id="ccCity" name="cityName" maxlength="20" autocomplete="off" 
						onfocus="clearFieldStyle(this, 'cityNameLabelbill', 'input_txt billingAddresscity1 input_city', '','false')" />
					</div>
					<div id="stateSelectBlk" class="flt_lft bill_state">
						<label id="selectStateLabelbill" for="selectState">State <span class="mandatory">*</span></label>
						<dspel:select name="selectState" iclass="bill_state_select" id="ccState" bean="ProfileFormHandler.value.state"
							onfocus="clearFieldStyle(this, 'selectStateLabelbill', 'bill_state_select', '','false')" style=" width: 213px; height: 30px">
							<c:if test="${empty billingState}">
								<dsp:option value="">select a state</dsp:option>
							</c:if>
							<dsp:droplet name="ForEach">
								<dsp:param name="array" bean="CheckoutApplicationConfiguration.usStatesMap"/>
								<dsp:oparam name="output">
									<dsp:getvalueof var="stateAbbrev" param="key" />
									<dsp:getvalueof var="stateName" param="element" />
									<option value="<c:out value='${stateAbbrev}'/>">
										${stateName}
									</option>
								</dsp:oparam>
							</dsp:droplet>
						</dspel:select>
					</div>
					<div class="phone">
					<div class="label-wrap">
						<label id="phoneNumberLabelbill" class="flt_lft" for="phoneNumber">Phone <span>*</span></label>
						<span class="flt_rgt us_phone">Why do we need this?
							<!-- 8/27 LM added tabindex -->
							<a id="a1_up" href="#phoneTip" class="tooltipHelpIcon nomargintop contactinfo-tooltip" tabindex="-1">
							<img class="imgpos" alt="tooltip" src="/dotcom/images/tooltip_quesmark.png">
							<span class="contactinfo-tooltiptext" style="bottom: 34%;left: 12.3%" id="phoneTip">We'll use this phone to communicate important order information. Additionally we'll send text order status updates.</span>
							</a>
				   		</span>				
					</div>
			<input type="text" class="input_txt input_phone billingPhoneNumber" id="ccPhoneNumber" name="billingPhoneNumber" autocomplete="off" value="${phoneNumber}"
				maxlength="14" placeholder="(555) 555-5555" onfocus="clearFieldStyle(this, 'phoneNumberLabelbill', 'input_txt input_phone billingPhoneNumber', '','false')" />
			</div>
				</div>
				<%--Auto Populate City/State from Zip Code (Payment) START--%>
				<div id="<c:out value='${fromPage}'/>_autopopulateCitydiv" class="ol autoPopulateCity  mrgnLayoutForCity mrgnleftLayoutForCity">
					<div id="autopopulateCitydivInner" class="autoPopulateCityList"></div>
				</div>
				<div id="<c:out value='${fromPage}'/>_autopopulateStatediv" class="ol autoPopulateState mrgnLayoutForState mrgnleftLayoutForState">
					<div id="autopopulateStatedivInner" class="autoPopulateStateList"></div>
				</div>
				<%--Auto Populate City/State from Zip Code (Payment) END--%>
			</div>
			
			<div id="<c:out value='${militaryBillingSpecific}'/>" class="country-specific-fields">
				<div class="row bill_address">
					<div class="flt_lft bill_add1">
						<div class="label-wrap">
						<label id="militaryStreetAddress1Labelbill" for="militaryStreetAddress1">Street Address</label>
						<span class="mandatory">*</span>
						</div>
						<input type="text" class="input_txt mili_str1 billingAddress1"
							id="militaryStreetAddress1" maxlength="40" name="militaryStreetAddress1" 
							onfocus="clearFieldStyle(this, 'militaryStreetAddress1Labelbill', 'input_txt mili_str1 billingAddress1', '','false')" />
						 
						<div class="apo_info">
							<span><strong>Army/Navy/Marines: </strong>Enter Unit and Box Number</span>
							<span><strong>Ships: </strong>Enter Ship Name and Hull Number</span>
							<span><strong>Air Force: </strong>Enter PSC and Box Number</span>
						</div>
					</div>
					<div class="flt_lft bill_add2 optionalCol">
						<label for="militaryStreetAddress2">Street Address 2</label> 
						<input type="text" placeholder="optional" class="input_txt mili_str1 billingAddress2" 
						id="militaryStreetAddress2" maxlength="40" name="militaryStreetAddress2"/>
							
						<div class="apo_info">
							<span>Enter optional military command or organization name</span>
							<span>(e.g., USAG J or 2/566 Postal Co.)</span>
						</div>
					</div>
				</div>
				<div class="row bill_location">
					<div id="milZipCityBillingError" class="milZipCityBillingErrorModal"></div>
					<div class="flt_lft bill_zip">
						<label id="militaryZipCodeLabelbill" for="militaryZipCode">ZIP code</label>
						<input type="text" class="input_txt input_zip" id="militaryZipCode" maxlength="5" name="militaryZipCode"
							onfocus="clearFieldStyle(this, 'militaryZipCodeLabelbill', 'input_txt input_zip', '','false')"/>
						<div class="apo_info">must start with a 0, 3 or 9</div>
					</div>

					<div id="militaryBlk" class="flt_lft bill_city">
						<label id="militaryTypeLabelbill" for="militaryType">APO/FPO/DPO</label>
						<dspel:select iclass="bill_state_select mili_opt width_fix" id="militaryType" name="militaryType"
							bean="ProfileFormHandler.value.militaryAddressType" onfocus="clearFieldStyle(this, 'militaryTypeLabelbill', 'bill_state_select mili_opt width_fix', '','false')">
							<option value="">select</option>
			                <dsp:option value="APO">APO</dsp:option>
							<dsp:option value="FPO">FPO</dsp:option>
							<dsp:option value="DPO">DPO</dsp:option>
						</dspel:select>
					</div>
					<div class="flt_lft bill_state">
						<label id="militaryStateLabelbill" for="militaryState">state</label>
						<dspel:select id="militaryState" name="militaryState" iclass="bill_state_select mili_state width_fix"
							bean="ProfileFormHandler.value.state" onfocus="clearFieldStyle(this, 'militaryStateLabelbill', 'bill_state_select mili_state width_fix', '','false')">
							<dspel:option value="">select</dspel:option>
							<dspel:option value="AA">AA (Armed forces Americas)</dspel:option>
							<dspel:option value="AE">AE (Armed forces Europe)</dspel:option>
							<dspel:option value="AP">AP (Armed forces Pacific)</dspel:option>
						</dspel:select>
					</div>
					<div class="phone">
					<div class="label-wrap">
						<label id="phoneNumberLabelbill" class="flt_lft" for="phoneNumber">Phone <span>*</span></label>
						<span class="flt_rgt ap_pnone">Why do we need this?
						<!-- 8/27 LM added tabindex -->
							<a id="a1_up" href="javascript:void(0;)" class=" nomargintop contactinfo-tooltip" tabindex="-1">
							<img class="imgpos" alt="tooltip" src="/dotcom/images/tooltip_quesmark.png">
							<span class="contactinfo-tooltiptext" style="bottom: 31%;left: 12.3%" id="phoneTip">We'll use this phone to communicate important order information. Additionally we'll send text order status updates.</span>
							</a>
				   		</span>				
					</div>
					<input type="text" class="input_txt input_phone billingPhoneNumber" id="militaryPhoneNumber" name="militaryPhoneNumber" autocomplete="off" value="${phoneNumber}"
					maxlength="14" placeholder="(555) 555-5555" onfocus="clearFieldStyle(this, 'phoneNumberLabelbill', 'input_txt input_phone billingPhoneNumber', '','false')" />
				</div>
				</div>
				
			</div>

			<div id="<c:out value='${internationalBillingSpecific}'/>" class="country-specific-fields">
				<%--International Street Address--%>
				<div class="row intl_bill_address bill_address">
					<div class="flt_lft bill_add1">
						<div class="label-wrap">
							<label id="internationalStreetAddressLabelbill" for="internationalStreetAddress">Street Address</label>
							<span class="mandatory">*</span>
						</div>
						<input type="text" class="input_txt input_long_txt input-address intlBillingAddress1" id="internationalStreetAddress" maxlength="40" 
						name="internationalStreetAddress" autocomplete="off" onkeypress="suggestAddress('#internationalStreetAddress', '#internationalCity', '#internationalProvince', '#internationalPostalCode');" onfocus="clearFieldStyle(this, 'internationalStreetAddressLabelbill', 'input_txt input_long_txt intlBillingAddress1', '','false')" />
					</div>
					<%-- Fields added for international  - Start --%>
					<div class="flt_lft optionalCol" id="streetAddress2">
						<label for="internationalStreetAddress2">Street Address 2 (optional)</label>
						<input type="text" placeholder="optional" class="input_txt input_long_txt input-address intlBillingAddress2" 
						id="internationalStreetAddress2" maxlength="40" name="internationalStreetAddress2" autocomplete="off" />
					</div>
					<div class="flt_lft optionalCol" id="streetAddress3">
						<label for="internationalStreetAddress3">Street Address 3 (optional)</label>
						<input type="text" placeholder="optional" class="input_txt input_long_txt intlBillingAddress3" 
						id="internationalStreetAddress3" maxlength="40" name="internationalStreetAddress3" autocomplete="off" />
					</div>
					<%-- Fields added for international  - End --%>
				</div>
				<%--Australia City, Province & Postal Code--%>
				<div id="intZipCityBillingError"></div>
				<div class="row bill_location">
					<div class="flt_lft bill_city">
						<div class="label-wrap">
							<label id="internationalCityLabelbill" for="internationalCity">City</label>
							<span class="mandatory">*</span>
						</div>
						<input type="text" class="input_txt billingAddresscity1" id="internationalCity" maxlength="20" name="internationalCity"	
						autocomplete="off" onfocus="clearFieldStyle(this, 'internationalCityLabelbill', 'input_txt billingAddresscity1', '','false')" />
					</div>
					<div class="flt_lft bill_state can_province" id="ProvinceDivCanada" style="display: none">
						<label for="internationalProvince">Province/Territory/Region </label>
						<dspel:select iclass="input_txt billingAddressProvince1" id="canadaProvince" name="canadaProvince"
							bean="ProfileFormHandler.editValue.provinceRegionTerritory">
							<dsp:option value="">select a province</dsp:option>
							<dsp:droplet name="ForEach">
								<dsp:param name="array" bean="CheckoutApplicationConfiguration.canadaStatesMap"/>
								<dsp:oparam name="output">
									<dsp:getvalueof var="stateAbbrev" param="key" />
									<dsp:getvalueof var="stateName" param="element" />
									<option value="<c:out value='${stateAbbrev}'/>">
										${stateName}
									</option>
								</dsp:oparam>
							</dsp:droplet>
						</dspel:select>
					</div>
					<div class="flt_lft bill_state intl_province" id="ProvinceDiv">
						<label for="internationalProvince">Province/Territory/Region</label>
						<dspel:input type="text" iclass="input_txt billingAddressProvince1" id="internationalProvince" maxlength="10" name="internationalProvince" 
						bean="ProfileFormHandler.editValue.provinceRegionTerritory" onfocus="clearFieldStyle(this, 'internationalProvinceLabelbill', 'input_txt billingAddressProvince1', '','false')" />
					</div>
					<div class="flt_lft bill_zip">
						<label id="internationalPostalCodeLabelbill" for="internationalPostalCode">Postal Code</label>
						<input type="text" class="input_txt input_zip" id="internationalPostalCode" maxlength="8" 
						name="internationalPostalCode" autocomplete="off" onfocus="clearFieldStyle(this, 'internationalPostalCodeLabelbill', 'input_txt input_zip', '','false')" />
					</div>
				</div>
				<div class="row contact_detail optionalCol" id="phoneNumberSecondary">
					<div class="phone">
						<div class="label-wrap">
							<label id="phoneNumberLabelbill" class="flt_lft" for="phoneNumber">Phone <span>*</span></label>
							<span class="flt_rgt">Why do we need this?
							<!-- 8/27 LM added tabindex -->
								<a id="a1_up" href="#phoneTip" class="tooltipHelpIcon nomargintop contactinfo-tooltip" tabindex="-1">
								<img class="imgpos" alt="tooltip" src="/dotcom/images/tooltip_quesmark.png">
								<span class="contactinfo-tooltiptext" style="bottom: 34.5%;left: 11.4%" id="phoneTip">We'll use this phone to communicate important order information. Additionally we'll send text order status updates.</span>
								</a>
					   		</span>				
						</div>
						<input type="text" class="input_txt input_phone billingPhoneNumber" id="internationalPhoneNumber" name="internationalPhoneNumber" autocomplete="off" value="${phoneNumber}"
						maxlength="14" placeholder="(555) 555-5555" onfocus="clearFieldStyle(this, 'phoneNumberLabelbill', 'input_txt input_phone billingPhoneNumber', '','false')" />
					</div>
					<div class="phone">
						<label for="phoneNumber">Alt Phone Number</label>
						<input type="text" placeholder="optional" class="input_txt" id="billingPhoneNumberSecondary" name="billingPhoneNumberSecondary"
						autocomplete="off" maxlength="13" value="" />
					</div>
				</div>
			</div>
		</div>
		
		<div class="btn-row">
			<a herf="javascript: void(0);" class="cancel editCCCardCancel" onclick="showItrmContainer();">cancel</a>
			<input id="saveBillingAddress" name="saveBillingAddress" type="button" class="cr-card-save-btn" value="SAVE" onclick="validateBillingAddressForm('BillingAddressForm')"/>
				
		</div>
		
	</div>
		
	<dspel:input type="hidden" id="phoneNumberLength" name="phoneNumberLength" bean="OCSApplicationConfiguration.phoneNumberMaxLength" value=""/>
	<dspel:input type="hidden" id="internationalPhoneNumberLength" name="internationalPhoneNumberLength" bean="OCSApplicationConfiguration.internationalPhoneNumberMaxLength" />
	<dspel:input type="hidden" id="hdnCountry" name="hdnCountry" bean="ProfileFormHandler.value.countryName" />
	<dspel:input type="hidden" id="hdnPrimaryPhoneNumber" bean="ProfileFormHandler.value.phoneNumber" />
	<dspel:input type="hidden" id="hdnBillingStreetAddress1" name="hdnBillingStreetAddress1" bean="ProfileFormHandler.value.address1" />
	<dspel:input type="hidden" id="hdnBillingStreetAddress2" name="hdnBillingStreetAddress2" bean="ProfileFormHandler.value.address2" />
	<dspel:input type="hidden" id="hdnBillingCityName" name="hdnBillingCityName" bean="ProfileFormHandler.value.city" />
	<dspel:input type="hidden" id="hdnBillingZipCode" name="hdnBillingZipCode" bean="ProfileFormHandler.value.postalCode" />
	<%-- <dspel:input type="hidden" id="hdnBillingCanProvince" name="hdnBillingCanProvince" bean="ProfileFormHandler.editValue.provinceRegionTerritory" /> --%>
	<%-- Fields added for international  - Start --%>
	<dspel:input type="hidden" id="hdnBillingStreetAddress3" name="hdnBillingStreetAddress3" bean="ProfileFormHandler.value.address3" />
	<dspel:input type="hidden" id="hdnSecondaryPhoneNumber" name="hdnSecondaryPhoneNumber" bean="ProfileFormHandler.value.secondaryPhoneNumber" />
	<dspel:input type="hidden" id="addressRepositoryId" name="addressRepositoryId" bean="ProfileFormHandler.addressId" value=""/>
	<dsp:input type="hidden" value="editAddressNonAjax" bean="ProfileFormHandler.userAction"/>
	<input type="hidden" name="seqNum" value='<c:out value="${cardSeq}" />'/>
	<input type="hidden" name="addressRepositoryId" id="addressRepoIdhdn"/>
	<input type="hidden" name=redirectToCheckout id="redirectToCheckout" value='true'/>
	<input type="hidden" name=isBilling id="isBilling" value='true'/>
	<dsp:input type="hidden" value="" bean="ProfileFormHandler.editNewAddress"/>	
	<%-- Fields added for international  - End --%>
	
</dsp:page>