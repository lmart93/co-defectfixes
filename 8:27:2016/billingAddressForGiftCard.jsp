<%--
* -----------------------------------------------------------------------
* Copyright 2011, jcpenney Co., Inc.  All Rights Reserved.
*
* Reproduction or use of this file without explicit
* written consent is prohibited.
* Created by: cgeddam1
*
* Created on: June 296, 2016
* ----------------------------------------------------------------
--%>
<%-- ========================================= Description
/**
 * This Jsp is used to handle the create flow of the billing information section for the scenario GiftCard payment itself satisfies the order total.
 * under the following flows - Ship to home or ship to store & SDPU. 
 * 
 * Includes and Footer JSPFs.
 *
 * @author cgeddam1
 * @version 0.1 26-June-2016
 **/
--%>
<%@ taglib uri="dsp" prefix="dsp" %>
<%@ taglib uri="c" prefix="c"  %>
<%@ taglib uri="dspel" prefix="dspel"  %>
<%@ taglib uri="fmt" prefix="fmt" %>
<dsp:page xml="true">

	<dsp:importbean bean="/com/jcpenney/ocs/OCSApplicationConfiguration"/>
	<dsp:importbean bean="/com/jcpenney/order/CheckoutApplicationConfiguration"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CreateCreditCardFormHandler"/>
    
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	<dsp:importbean bean="/atg/dynamo/droplet/RQLQueryForEach"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/com/jcpenney/profile/ProfileApplicationConfiguration"/>
	<dsp:importbean bean="/com/jcpenney/core/applicationConfig/repository/ApplicationConfigurationRepository"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/BillingAddressFormHandler"/>
	
	<dsp:getvalueof var="formHandlerName" vartype="java.lang.String" param="handlerName"/>
	<dsp:getvalueof var="handlerNameOnly" vartype="java.lang.String" param="handlerNameOnly"/>
	<dsp:getvalueof var="shippingCheckBoxId" vartype="java.lang.String" param="shippingCheckBoxId"/>
	<dsp:getvalueof var="usBillingSpecific" vartype="java.lang.String" param="usBillingSpecific"/>
	<dsp:getvalueof var="militaryBillingSpecific" vartype="java.lang.String" param="militaryBillingSpecific"/>
	<dsp:getvalueof var="internationalBillingSpecific" vartype="java.lang.String" param="internationalBillingSpecific"/>
	<dsp:getvalueof var="countryCodeUS" bean="ProfileApplicationConfiguration.countryCodeUS"/>
	<dsp:getvalueof var="countryCodeAP" bean="ProfileApplicationConfiguration.countryCodeAP"/>
	<dsp:getvalueof var="contextroot" bean="/OriginatingRequest.contextPath" vartype="java.lang.String" />
	<dsp:getvalueof var="isTransient" bean="Profile.transient"/>
	<dsp:importbean bean="/com/jcpenney/dp/core/droplet/GetBillingAddressFromShippingGroup"/>	
	<dsp:importbean bean="/com/jcpenney/core/ApplicationConfiguration" var="appConfig" />
	<dsp:getvalueof var="enableEmailValidationError" bean="ApplicationConfiguration.enable2016S8CCDPXVI7000EmailValidationError"/>
	<dsp:getvalueof var="enableAddressTypeAhead" bean="ApplicationConfiguration.enableAddressTypeAhead"/>
	<dsp:getvalueof var="invokeAddressTypeAhead" value="" />
	
	<fmt:setBundle basename="/com/jcpenney/dp/core/order/CheckoutResources" var="checkoutResources" scope="application"/>
	<fmt:setBundle basename="com/jcpenney/dp/core/profile/ProfileResources" var="profileBundle" scope="application"/>
	<script type="text/javascript" src="/dotcom/js/checkoutsimplified/giftcard.js"></script>

	<%-- Configurable messages for billing address page --%>    
	<dsp:include page="/jsp/global/configurableMessage.jsp">
		<dsp:param name="page" value="cam"/>
		<dsp:param name="keys" value="keyProfileZipCodeInvalid"/>
	</dsp:include>
	
	<%-- Changes added for international  - Start --%>
	<dsp:getvalueof var="countryNameId" bean="ShoppingCart.current.shipToCountry" />
	<%-- Changes added for international  - End --%>

	<c:choose>
		<c:when test="${usBillingSpecific == 'usBillingSpecific'}">
			<dsp:getvalueof var="fromPage" value="ccPaymentPage" />
		</c:when>
		<c:when test="${usBillingSpecific == 'usBillingSpecificForEdit'}">
			<dsp:getvalueof var="fromPage" value="ccPaymentPageForEdit" />
		</c:when>
		<c:otherwise>
		</c:otherwise>
	</c:choose>
	<dsp:getvalueof var="billingEmail" bean="ShoppingCart.current.email" />
	<dsp:droplet name="/atg/dynamo/droplet/ForEach">
		<dsp:param name="array" bean="ShoppingCart.current.shippingGroups" />
		<dsp:oparam name="output">
			<dsp:droplet name="Switch">
				<dsp:param name="value" param="element.shippingGroupClassType" />
				<dsp:oparam name="storeShippingGroup"></dsp:oparam>
				<dsp:oparam name="electronicShippingGroup"></dsp:oparam>
				<dsp:oparam name="hardgoodShippingGroup">
					<dsp:getvalueof var="isHardgoodShippingGroup" value="true" />
					<dsp:getvalueof var="giftRegistryAddressFlag" param="element.shippingAddress.registrantAddress" />
				</dsp:oparam>
				<dsp:oparam name="bopus"></dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
	
	<c:choose>
		<c:when test="${isHardgoodShippingGroup eq true && giftRegistryAddressFlag ne true}">
			<%-- Billing summary populated from shipping address --%>
			<dsp:droplet name="GetBillingAddressFromShippingGroup">
				<dsp:param name="order" bean="ShoppingCart.current" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="firstName" param="firstName" />
					<dsp:getvalueof var="lastName" param="lastName" />
					<dsp:getvalueof var="companyName" param="companyName" />
					<dsp:getvalueof var="address1" param="address1" />
					<dsp:getvalueof var="address2" param="address2" />
					<dsp:getvalueof var="city" param="city" />
					<dsp:getvalueof var="state" param="state" />
					<dsp:getvalueof var="country" param="country" />
					<dsp:getvalueof var="zipCode" param="zipCode" />
					<dsp:getvalueof var="phoneNumber" param="phoneNumber" />					
				</dsp:oparam>
			</dsp:droplet>	
		</c:when>
		<c:otherwise>
			<%-- Billing summary populated from order.addressInfo --%>
			<dsp:droplet name="IsEmpty">
				<dsp:param bean="ShoppingCart.current.addressInfo"  name="value"/>
				<dsp:oparam name="false">
					<dsp:setvalue param="addressInfo" beanvalue="ShoppingCart.current.addressInfo"/>
					<dsp:getvalueof var="firstName" param="addressInfo.firstName" />
					<dsp:getvalueof var="lastName" param="addressInfo.lastName" />
					<dsp:getvalueof var="address1" param="addressInfo.addressOne" />
					<dsp:getvalueof var="address2" param="addressInfo.addressTwo" />
					<dsp:getvalueof var="city" param="addressInfo.city" />
					<dsp:getvalueof var="state" param="addressInfo.state" />
					<dsp:getvalueof var="country" param="addressInfo.country" />
					<dsp:getvalueof var="zipCode" param="addressInfo.postalCode" />
					<dsp:getvalueof var="phoneNumber" param="addressInfo.phoneNumber" />					
				</dsp:oparam>
			</dsp:droplet>		
		</c:otherwise>
	</c:choose>

	<%-- Contact information --%>
	<div class="contact-info">
		<h2 class="subhead">Order Contact Information</h2>
		
		<%-- Error messages for Order Contact Information --%>
        <div class="orderContact_err_msgs dynamic_error_msgs hide" id="billingAddressErrorContainer">
            <span class="error-msg-header"><img alt="attention" src="/dotcom/images/error-fill-copy.png">Please correct the errors listed in red below:</span>
            <ul class="error-msgs"></ul>
        </div>
		<c:if test="${isTransient}">
			<div class="email">
				<div class="label-wrap">
					<label class="flt_lft">Email <span>*</span></label>
					<span class="flt_rgt">Why do we need this?
					<!-- 8/27 LM added tabindex -->
						<a id="a1_up" href="#emailTip" class="tooltipHelpIcon nomargintop contactinfo-tooltip" tabindex="-1">
							<img class="imgpos" alt="tooltip" src="/dotcom/images/tooltip_quesmark.png">
							<span class="contactinfo-tooltiptext" id="emailTip">We'll use this email to send your order confirmation email.</span>
						</a>
		   			</span>
	   			</div>
				<input type="text" class="input_txt billingEmail input_email standardOptin ng-pristine ng-untouched ng-valid" placeholder="example@domain.com"
					   id="billingEmailId" name="emailId" autocomplete="off" onkeyup="checkCanadaEmail(this.value, antiSpamEmailAddressSuffixes);"
					   value="${billingEmail}" onfocus="clearFieldStyle(this, 'emailLabelbill', 'input_txt billingEmail input_email standardOptin', '','false')" />
				<div class="info">
					<dsp:include page="guestEmailOptin.jsp" />
				</div>
				<!-- <span ng-if="paymentEmailError" class="error ng-scope">This field is required</span> -->
			</div>
			<div class="clear"></div>
		</c:if>
		<div class="phone">
			<div class="label-wrap">
				<label id="phoneNumberLabelbill" class="flt_lft" for="phoneNumber">Phone <span>*</span></label>
				<span class="flt_rgt">Why do we need this?
					<!-- 8/27 LM added tabindex -->
					<a id="a1_up" href="#phoneTip" class="tooltipHelpIcon nomargintop contactinfo-tooltip" tabindex="-1">
					<img class="imgpos" alt="tooltip" src="/dotcom/images/tooltip_quesmark.png">
					<span class="contactinfo-tooltiptext" style="bottom: 36.5%;" id="phoneTip">We'll use this phone to communicate important order information. Additionally we'll send text order status updates.</span>
					</a>
		   		</span>
				
			</div>
			<input type="text" class="input_txt billingPhoneNumber input_phone"
				id="billingPhoneNumber" name="billingPhoneNumber" autocomplete="off" value="${phoneNumber}"
				maxlength="14" placeholder="(555) 555-5555"
				onfocus="clearFieldStyle(this, 'phoneNumberLabelbill', 'input_txt input_phone billingPhoneNumber', '','false')" />
		</div>
		<div class="sms-optin">
		   	<div class="text-alert">
		  		 <dsp:input type="checkbox" id="text-alert" name="text-alert" bean="BillingAddressFormHandler.enableTransactionalSms" onchange="smsOptIn();" onclick="cmCreateConversionEventTag('Mobile Opt in text', '1', 'Opt in');"/> 
			    <span class="cb"></span>
			    <span>Text me order status updates (optional)</span>
			</div>
			<div class="clear"></div>
			<!-- DPXVI-20388 - start -->
			<div id="smsPhone" class="sms-phone">
			<div class="clear"></div>
			    <div class="icon">
			        <img src="/dotcom/images/mobile.svg" alt="">
			    </div>
			    <div class="phone">
			         <label id="phoneNumberLabelSMS" for="phoneNumberSMS" >Mobile Phone<span>*</span>
			        </label>
			  		<input type="text" class="input_txt input_phone" autocomplete="off" id="textAlertNum" name="textAlertNum" maxlength="14" placeholder="(555) 555-5555" onfocus="clearFieldStyle(this, 'phoneNumberLabelSMS', 'input_txt input_phone textAlertNum', '','false')" value="" />
			    </div>
			    <div class="clear"></div>
			    <p><span id="orderTextMessage"><dsp:valueof valueishtml="true" bean="CheckoutApplicationConfiguration.orderStatusTextDefaultMessage"/> 
				    <span id="moreText" style="display: none;"><dsp:valueof valueishtml="true" bean="CheckoutApplicationConfiguration.orderStatusTextShowMoreMessage"/></span> 
				    <span><a id="seeLess" href="javascript:void(0);" onclick="smsTCs()">more details</a></span></span>
			    </p>
			</div>
		<!-- DPXVI-20388 - end -->
		</div>		
		<div class="clear"></div>
		<c:if test="${isHardgoodShippingGroup eq true && giftRegistryAddressFlag ne true}">
			<div class="billing-summary">
				<c:if test="${enableEmailValidationError eq true}">
					<h4 class="header">Billing Information</h4>
					<ul class="billing-lines">
						<li class="sbaline">
							<span id="sbafirsname">${firstName} </span><span id="sbalastname">${lastName}</span>
						</li>
						<li class="sbaline">
							<span id="sbastreetaddr1">${address1}</span>
						</li>
						<li class="sbaline">
							<span id="sbastreetaddr2">${address2}</span>
						</li>
						<li class="sbaline">
							<span id="sbacity">${city}, </span><span id="sbastate">${state} </span><span id="sbazip">${zipCode}</span>
						</li>
						<li class="sbaline">
							<span id="sbacountry">${country}</span>
						</li>
					</ul>
				</c:if>
			</div>
		</c:if>
		<%-- Change billing address form --%>
		<div class="checkout billing-info ${(isHardgoodShippingGroup && !giftRegistryAddressFlag) ? 'hide' : ''}">
			<h2 class="billingInfoHeader">Billing Information</h2>
			<p class="font14 mrgt5" id="guestDiv">Enter your full name and address exactly as they appear on your statement.</p>
			
			<%-- Error messages for billing --%>
			<div class="bill_add_err_msgs dynamic_error_msgs hide" id="billingAddressErrorContainer">
				<span class="error-msg-header"><img alt="attention" src="/dotcom/images/error-fill-copy.png">Please correct the errors listed in red below:</span>
				<ul class="error-msgs"></ul>
			</div>
	
			<c:if test="${countryNameId != 'US'}">
				<div class="dynamic_error_msgs mrgb10 hide_display intrlPaymentErrorContainer"
					id="internationalPaymentErrorContainer">
					<div class="flt_lft mrgr15 exclamation_icn">
						<img alt="attention" src="<c:out value="${contextroot}"/>/images/spacer.gif" /><span>attention</span>
					</div>
					<div class="float_fix">
						<span class="disp_blk">Please correct the errors listed in red below:</span>
						<ul>
							<li class="hide_display"></li>
						</ul>
						<div id="internationalAddressError" class="paymentAddressError">
						</div>
					</div>
				</div>
			</c:if>
			
			<div id="coldBillingAddress" class="row hide_btm_space">
				<%--Choose Country--%>
				<div id="shipCountry" class="row country_select">
					<label id="selectBillingCountryLabel" for="selectBillingCountry">country or APO/FPO/DPO</label>
					<%--Code Change to Display all International Countries Fix For defect 1841--%>
					<dspel:select bean="BillingAddressFormHandler.billingAddress.countryName" name="selectBillingCountry" onchange="onSelectboxChange();"
						id="selectBillingCountry" iclass="flt_lft" onfocus="clearFieldStyle(this, 'selectBillingCountryLabel', 'flt_lft', '','false')">
						<dspel:option value="AP">APO/FPO/DPO</dspel:option>
						<dsp:droplet name="/atg/dynamo/droplet/RQLQueryForEach">
							<dsp:param name="queryRQL" value="countryCode!=\"AP\" ORDER BY countryName" />
							<dsp:param name="repository" value="/com/jcpenney/core/applicationConfig/repository/ApplicationConfigurationRepository" />
							<dsp:param name="itemDescriptor" value="country" />
							<dsp:oparam name="output">
								<dsp:getvalueof var="keyValue" param="element.countryCode" />
								<dsp:droplet name="IsEmpty">
									<dspel:param bean="BillingAddressFormHandler.formExceptions" name="value" />
									<dsp:oparam name="true">
										<c:if test="${ keyValue == countryNameId }">
											<dspel:option selected="true" paramvalue="element.countryName">
												<dsp:valueof param="element.countryName" />
											</dspel:option>
										</c:if>
										<c:if test="${ keyValue != countryNameId }">
											<dspel:option paramvalue="element.countryName">
												<dsp:valueof param="element.countryName" />
											</dspel:option>
										</c:if>
									</dsp:oparam>
									<dsp:oparam name="false">
										<dspel:option paramvalue="element.countryName">
											<dsp:valueof param="element.countryName" />
										</dspel:option>
									</dsp:oparam>
								</dsp:droplet>
							</dsp:oparam>
						</dsp:droplet>
					</dspel:select>
	
				</div>
				<%--First, Middle and Last Name--%>
				<div class="clear_floats">
					
				</div>
				<div class="row name_details">
					<div class="first_name">
						<div class="label-wrap">
							<label id="firstNameLabelbill" for="firstName">First Name</label>
							<span class="mandatory">*</span>
							<div id="firstNameBillingError"></div>
						</div>
						<dspel:input name="firstName" type="text" bean="BillingAddressFormHandler.billingAddress.firstName"
							iclass="input_txt input_name" id="billingFirstName" maxlength="17" value="${firstName}"
							onfocus="clearFieldStyle(this, 'firstNameLabelbill', 'input_txt input_name', '','false')" />
					</div>
					<div class="last_name">
						<div class="label-wrap">
							<label id="lastNameLabelbill" for="lastName">Last Name</label>
							<span class="mandatory">*</span>
							<div id="lastNameBillingError"></div>
						</div>
						<dspel:input name="lastName" type="text" bean="BillingAddressFormHandler.billingAddress.lastName"
							iclass="input_txt input_name" id="billingLastName" maxlength="24" value="${lastName}"
							onfocus="clearFieldStyle(this, 'lastNameLabelbill', 'input_txt input_name', '','false')" />
					</div>
				</div>
				<%--Company Name--%>
				<div id="companyBlock" class="row company_name optionalCol">
					<label for="companyName">Company (optional)</label>
					<dspel:input name="companyName" type="text" bean="BillingAddressFormHandler.billingAddress.companyName"
						maxlength="23" value="${companyName}" iclass="input_txt input_length_txt" id="companyName">
						<dspel:tagAttribute name="placeholder" value="optional" />
					</dspel:input>
				</div>
				
				<%--Specific to United States Country Location--%>
				<div id="<c:out value='${usBillingSpecific}'/>" class="country-specific-fields" >
	
					<%--United States Street Address--%>
					<div class="row ship_address">
						<div id="address1Blk" class="flt_lft ship_add1">
							<div class="label-wrap">
								<label id="streetAddress1Labelbill" for="streetAddress1">Street Address</label>
								<span class="mandatory">*</span>
								<div id="streetAddress1Error"></div>
							</div>
							<c:choose>
								<c:when test="${isStoreShippingSelectedFlag}">
									<input type="text" class="input_txt input_address billingAddress1" id="billingStreetAddress1" maxlength="40" name="streetAddress1"
										autocomplete="off" autofocus="autofocus" onfocus="clearFieldStyle(this, 'streetAddress1Labelbill', 'input_txt input_address billingAddress1', '','false')" />
								</c:when>
								<c:otherwise>
									<c:if test="${enableAddressTypeAhead eq 'true' }" >
										<dsp:getvalueof var="invokeAddressTypeAhead" value="suggestAddress('#billingStreetAddress1', '#billingCity', '#billingState', '#billingZipCode');" />
									</c:if>
									<input type="text" class="input_txt input_address billingAddress1" id="billingStreetAddress1" maxlength="40" name="streetAddress1"
										autocomplete="off" value="${address1}" onkeypress="${invokeAddressTypeAhead}" onfocus="clearFieldStyle(this, 'streetAddress1Labelbill', 'input_txt input_address billingAddress1', '','false')" />
								</c:otherwise>
							</c:choose>
						</div>
						<div id="address2Blk" class="flt_lft ship_add2 optionalCol">
							<label for="streetAddress2">Apt, Suite, Floor (optional)</label> 
							<input type="text" placeholder="optional" class="input_txt billingAddress2" id="billingStreetAddress2"
								maxlength="40" name="streetAddress2" autocomplete="off" value="${address2}"/>
						</div>
					</div>
					<%--United States City, State and Zip Code--%>
					<div class="row ship_location">
						<%--Auto Populate City/State from Zip Code (Payment) START--%>
						<div id="<c:out value='${fromPage}'/>_autoPopulateError" class="autoPopulateError hide">
							<div class="flt_lft mrgr15 exclamation_icn">
								<img src="<c:out value="${contextroot}"/>/images/spacer.gif" alt="attention" /><span>attention</span>
							</div>
							<div class="float_fix">
								<span class="disp_blk">Please correct the errors listed in red below:</span>
								<ul>
									<li><div id="keyProfileZipCodeInvalid"></div></li>
								</ul>
							</div>
						</div>
						<div id="<c:out value='${fromPage}'/>_multiCityInfoMsg" class="multiCityInfoMsg bgcolor hide">
							<div class="flt_lft mrgr10 information_icn">
								<img alt="attention" src="/dotcom/images/spacer.gif">
							</div>
							<div class="float_fix">
								<p>Choose your city.</p>
							</div>
						</div>
						<div id="zipCityBillingError"></div>
						<div id="zipBlk" class="flt_lft ship_zip">
							<label id="zipCodeLabelbill" for="zipCode">Zip Code <span class="mandatory">*</span></label> 
							<input type="text" class="input_txt input_zip" id="billingZipCode"
								name="zipCode" maxlength="5" autocomplete="off" onfocus="clearFieldStyle(this, 'zipCodeLabelbill', 'input_txt input_zip', '','false')"
								onblur="zipProc.validateZip(this.value,'<c:out value='${usBillingSpecific}'/> #cityBlk #billingCity',
									 '<c:out value='${usBillingSpecific}'/> #stateSelectBlk #billingState',
									 '<c:out value='${usBillingSpecific}'/> #zipBlk #zipCode','<c:out value='${fromPage}'/>');" 
								value="${zipCode}"/>
							<%--Auto Populate City/State from Zip Code (Payment) END--%>
						</div>
						<div id="cityBlk" class="flt_lft ship_city">
							<label id="cityNameLabelbill" for="cityName">City <span class="mandatory">*</span></label> 
							<input type="text" class="input_txt input_city billingAddresscity1" id="billingCity" name="cityName" maxlength="20" autocomplete="off" value="${city}" 
								onfocus="clearFieldStyle(this, 'cityNameLabelbill', 'input_txt input_city billingAddresscity1', '','false')" />
						</div>
						<div id="stateSelectBlk" class="flt_lft ship_state">
							<label id="selectStateLabelbill" for="selectState">State <span class="mandatory">*</span></label>
							<dspel:select name="selectState" iclass="ship_state_select" id="billingState" bean="BillingAddressFormHandler.billingAddress.state"
								onfocus="clearFieldStyle(this, 'selectStateLabelbill', 'ship_state_select', '','false')">
								<c:if test="${empty state}">
									<dsp:option value="">select a state</dsp:option>
								</c:if>
								<dsp:droplet name="ForEach">
									<dsp:param name="array" bean="CheckoutApplicationConfiguration.usStatesMap"/>
									<dsp:param name="sortProperties" value="+_key"/>
									<dsp:oparam name="output">
										<dsp:getvalueof var="stateAbbrev" param="key" />
										<dsp:getvalueof var="stateName" param="element" />
										<option value="<c:out value='${stateAbbrev}'/>" ${state eq stateAbbrev ? 'selected' : ''}>
											${stateName}
										</option>
									</dsp:oparam>
								</dsp:droplet>
							</dspel:select>
						</div>
					</div>
					<%--Auto Populate City/State from Zip Code (Payment) START--%>
					<div id="<c:out value='${fromPage}'/>_autopopulateCitydiv" class="ol autoPopulateCity  mrgnLayoutForCity mrgnleftLayoutForCity">
						<div id="autopopulateCitydivInner" class="autoPopulateCityList"></div>
					</div>
					<div id="<c:out value='${fromPage}'/>_autopopulateStatediv" class="ol autoPopulateState mrgnLayoutForState mrgnleftLayoutForState">
						<div id="autopopulateStatedivInner" class="autoPopulateStateList"></div>
					</div>
					<%--Auto Populate City/State from Zip Code (Payment) END--%>
				</div>
				
				<div id="<c:out value='${militaryBillingSpecific}'/>" class="country-specific-fields">
					<div class="row ship_address">
						<div class="flt_lft ship_add1">
							<label id="militaryStreetAddress1Labelbill" for="militaryStreetAddress1">Street Address</label>
							<input type="text" class="input_txt mili_str1 billingAddress1"
								id="militaryStreetAddress1" maxlength="40" name="militaryStreetAddress1"
								onfocus="clearFieldStyle(this, 'militaryStreetAddress1Labelbill', 'input_txt mili_str1 billingAddress1', '','false')" />
							<div class="apo_info">
								<span><strong>Army/Navy/Marines: </strong>Enter Unit and Box Number</span>
								<span><strong>Ships: </strong>Enter Ship Name and Hull Number</span>
								<span><strong>Air Force: </strong>Enter PSC and Box Number</span>
							</div>
						</div>
						<div class="flt_lft ship_add2 optionalCol">
							<label for="militaryStreetAddress2">Street Address 2</label> 
							<input type="text" placeholder="optional" class="input_txt mili_str1 billingAddress2"
								id="militaryStreetAddress2" maxlength="40" name="militaryStreetAddress2" />
							<div class="apo_info">
								<span>Enter optional military command or organization name</span>
								<span>(e.g., USAG J or 2/566 Postal Co.)</span>
							</div>
						</div>
					</div>
					<div class="row ship_location">
						<div id="milZipCityBillingError" class="milZipCityBillingErrorModal"></div>
						<div class="flt_lft ship_zip">
							<label id="militaryZipCodeLabelbill" for="militaryZipCode">ZIP code</label>
							<input type="text" class="input_txt input_zip" id="militaryZipCode" maxlength="5" name="militaryZipCode"
								onfocus="clearFieldStyle(this, 'militaryZipCodeLabelbill', 'input_txt input_zip', '','false')" />
							<div class="apo_info">must start with a 0, 3 or 9</div>
						</div>
	
						<div id="militaryBlk" class="flt_lft ship_city">
							<label id="militaryTypeLabelbill" for="militaryType">APO/FPO/DPO</label>
							<dspel:select iclass="ship_state_select mili_opt" id="militaryType" name="militaryType"
								bean="BillingAddressFormHandler.billingAddress.militaryAddressType" onfocus="clearFieldStyle(this, 'militaryTypeLabelbill', 'ship_state_select mili_opt', '','false')">
								<option value="">select</option>
								<option value="APO">APO</option>
								<option value="FPO">FPO</option>
								<option value="DPO">DPO</option>
							</dspel:select>
						</div>
						<div class="flt_lft ship_state">
							<label id="militaryStateLabelbill" for="militaryState">state</label>
							<dspel:select id="militaryState" name="militaryState" iclass="ship_state_select mili_state"
								bean="BillingAddressFormHandler.billingAddress.state" onfocus="clearFieldStyle(this, 'militaryStateLabelbill', 'ship_state_select mili_state', '','false')">
								<option value="">select a state</option>
								<option value="AA">AA (Armed forces Americas)</option>
								<option value="AE">AE (Armed forces Europe)</option>
								<option value="AP">AP (Armed forces Pacific)</option>
							</dspel:select>
						</div>
	
					</div>
				</div>
	
				<div id="<c:out value='${internationalBillingSpecific}'/>" class="country-specific-fields">
					<%--International Street Address--%>
					<div class="row ship_address">
						<div class="flt_lft ship_add1">
							<label id="internationalStreetAddressLabelbill" for="internationalStreetAddress">street address 1</label>
							<input type="text" class="input_txt input_long_txt intlBillingAddress1" id="internationalStreetAddress" maxlength="40"
								name="internationalStreetAddress" autocomplete="off" onfocus="clearFieldStyle(this, 'internationalStreetAddressLabelbill', 'input_txt input_long_txt billingAddress1', '','false')" />
						</div>
						<%-- Fields added for international  - Start --%>
						<div class="flt_lft ship_add2 optionalCol">
							<label for="internationalStreetAddress2">street address 2</label>
							<input type="text" placeholder="optional" class="input_txt input_long_txt intlBillingAddress2"
								id="internationalStreetAddress2" maxlength="40" name="internationalStreetAddress2" autocomplete="off" />
						</div>
						<div class="clr_flt optionalLabel">
							<label><span>+</span>street address 2 (optional)</label>
						</div>
						<div class="flt_lft ship_add3 optionalCol" id="streetAddress3">
							<label for="internationalStreetAddress3">street address 3</label>
							<input type="text" placeholder="optional" class="input_txt input_long_txt intlBillingAddress3"
								id="internationalStreetAddress3" maxlength="40" name="internationalStreetAddress3" autocomplete="off" />
						</div>
						<div class="clr_flt optionalLabel">
							<label><span>+</span>street address 3 (optional)</label>
						</div>
						<%-- Fields added for international  - End --%>
					</div>
					<%--Australia City, Province & Postal Code--%>
					<div id="intZipCityBillingError"></div>
					<div class="row ship_location">
						<div class="flt_lft ship_city">
							<label id="internationalCityLabelbill" for="internationalCity">city</label>
							<input type="text" class="input_txt billingAddresscity1" id="internationalCity" maxlength="20" name="internationalCity"
								autocomplete="off" onfocus="clearFieldStyle(this, 'internationalCityLabelbill', 'input_txt billingAddresscity1', '','false')" />
						</div>
						<div class="flt_lft ship_state can_province" id="ProvinceDivCanada" style="display: none">
							<label for="internationalProvince">Province/Territory/Region </label>
							<dspel:select iclass="input_txt billingAddressProvince1" id="canadaProvince" name="canadaProvince"
								bean="BillingAddressFormHandler.billingAddress.provinceRegion">
								<dsp:option value="">select a province</dsp:option>
								<dsp:droplet name="ForEach">
									<dsp:param name="array" bean="CheckoutApplicationConfiguration.canadaStatesMap"/>
									<dsp:param name="sortProperties" value="+_key"/>
									<dsp:oparam name="output">
										<dsp:getvalueof var="stateAbbrev" param="key" />
										<dsp:getvalueof var="stateName" param="element" />
										<option value="<c:out value='${stateAbbrev}'/>" ${state eq stateAbbrev ? 'selected' : ''}>
											${stateName}
										</option>
									</dsp:oparam>
								</dsp:droplet>
							</dspel:select>
						</div>
						<div class="flt_lft ship_state intl_province" id="ProvinceDiv">
							<label for="internationalProvince">province/territory/region</label>
	
							<dspel:input type="text" iclass="input_txt billingAddressProvince1" id="internationalProvince" maxlength="10" name="internationalProvince"
								bean="BillingAddressFormHandler.billingAddress.provinceRegion" onfocus="clearFieldStyle(this, 'internationalProvinceLabelbill', 'input_txt billingAddressProvince1', '','false')" />
						</div>
						<div class="flt_lft ship_zip">
							<label id="internationalPostalCodeLabelbill" for="internationalPostalCode">postal code</label>
							<input type="text" class="input_txt input_zip" id="internationalPostalCode" maxlength="8"
								name="internationalPostalCode" autocomplete="off" onfocus="clearFieldStyle(this, 'internationalPostalCodeLabelbill', 'input_txt input_zip', '','false')" />
						</div>
					</div>
				</div>
			</div>
			<%-- Fields added for international  - Start --%>
			<div class="row contact_detail optionalCol" id="phoneNumberSecondary">
				<label for="phoneNumber">alt phone number</label> 
				<input type="text" placeholder="optional" class="input_txt" id="billingPhoneNumberSecondary" name="billingPhoneNumberSecondary"
					autocomplete="off" maxlength="13" />
			</div>
			<%-- Fields added for international  - End --%>
		
		</div>
		<c:if test="${isHardgoodShippingGroup eq true && giftRegistryAddressFlag ne true}">
				<%-- Enable edit billing address checkbox --%>
				<div class="edit-add-checkbox selected">
					<dsp:droplet name="/atg/dynamo/droplet/ForEach">
						<dsp:param name="array" bean="ShoppingCart.current.shippingGroups" />
						<dsp:oparam name="output">
							<dsp:droplet name="Switch">
								<dsp:param name="value" param="element.shippingGroupClassType" />
								<dsp:oparam name="hardgoodShippingGroup">
									<c:if test="${giftRegistryAddressFlag == false}">
										<dsp:input type="checkbox" iclass="input_check store_locator flt_lft" id="${shippingCheckBoxId}" bean="BillingAddressFormHandler.useShipAddress"
											checked="true" onclick="showBillingAddressForm(this);zipProc.clearAutoPopulateWidgets('${fromPage}');" />
										<span class="cb"></span>
									</c:if>
									<c:if test="${giftRegistryAddressFlag == true}">
										<input type="checkbox" disabled="disabled" class="flt_lft" id="shippingGRCreateId" name="shippingGRCreateId" />
										<span class="cb"></span>
									</c:if>
									<span class="shippingAddressCheckBox">Use my shipping address for billing</span>
								</dsp:oparam>
							</dsp:droplet>
						</dsp:oparam>
					</dsp:droplet>
				</div>
			</c:if>
		
	</div>
	<input id="paymentContiueGC" class="bp-red-btn continue" type="button" value="CONTINUE TO REVIEW" alt="continue" onclick="paymentSubmitClickForAddress(this.form.id);"/>
	
	<dsp:input id="addAddressFormSubmit2" type="hidden" bean="BillingAddressFormHandler.applyBillingAddressToOrder" value="" />
	<dspel:input type="hidden" id="phoneNumberLength" name="phoneNumberLength" bean="OCSApplicationConfiguration.phoneNumberMaxLength" />
	<dspel:input type="hidden" id="internationalPhoneNumberLength" name="internationalPhoneNumberLength" bean="OCSApplicationConfiguration.internationalPhoneNumberMaxLength" />
	<dspel:input type="hidden" id="hdnCountry" name="hdnCountry" bean="${formHandlerName}.billingAddress.countryName" />
	<dspel:input type="hidden" id="hdnPhoneNumber" name="hdnPhoneNumber" bean="${formHandlerName}.billingAddress.phoneNumber" />
	<dspel:input type="hidden" id="hdnBillingStreetAddress1" name="hdnBillingStreetAddress1" bean="${formHandlerName}.billingAddress.address1" />
	<dspel:input type="hidden" id="hdnBillingStreetAddress2" name="hdnBillingStreetAddress2" bean="${formHandlerName}.billingAddress.address2" />
	<dspel:input type="hidden" id="hdnBillingCityName" name="hdnBillingCityName" bean="${formHandlerName}.billingAddress.city" />
	<dspel:input type="hidden" id="hdnBillingZipCode" name="hdnBillingZipCode" bean="${formHandlerName}.billingAddress.postalCode" />
	<dspel:input type="hidden" id="hdnBillingCanProvince" name="hdnBillingCanProvince" bean="${formHandlerName}.billingAddress.provinceRegion" />
	<dspel:input type="hidden" id="hdnEmailAddress" name="hdnEmailAddress" bean="${handlerNameOnly}.emailAddress" />
	<dspel:input type="hidden" id="hdnTextAlertNum" name="hdnTextAlertNum" bean="${handlerNameOnly}.transSmsPhnNum" />	
	<%-- Fields added for international  - Start --%>
	<dspel:input type="hidden" id="hdnBillingStreetAddress3" name="hdnBillingStreetAddress3" bean="${formHandlerName}.billingAddress.address3" />
	<dspel:input type="hidden" id="hdnSecondaryPhoneNumber" name="hdnSecondaryPhoneNumber" bean="${formHandlerName}.billingAddress.secondaryPhoneNumber" />
	<%-- Fields added for international  - End --%>
	
</dsp:page>