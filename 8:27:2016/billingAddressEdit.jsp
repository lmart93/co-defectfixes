<%@ taglib uri="dsp" prefix="dsp" %>
<%@ taglib uri="c" prefix="c"  %>
<%@ taglib uri="dspel" prefix="dspel"  %>
<%@ taglib uri="fmt" prefix="fmt" %>

<dsp:page xml="true">
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/RQLQueryForEach"/>
	<dsp:importbean bean="/com/jcpenney/dp/core/droplet/GetBillingAddressFromShippingGroup"/>
	<dsp:importbean bean="/com/jcpenney/dp/core/droplet/GetBillingAddressFromPaymentGroup"/>
	
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/com/jcpenney/profile/bean/UserSessionBean"/>	
	
	<dsp:importbean bean="/com/jcpenney/ocs/OCSApplicationConfiguration"/>
	<dsp:importbean bean="/com/jcpenney/core/ApplicationConfiguration" var="appConfig" />
	<dsp:importbean bean="/com/jcpenney/order/CheckoutApplicationConfiguration"/>
	<dsp:importbean bean="/com/jcpenney/profile/ProfileApplicationConfiguration"/>
	
	<dsp:importbean bean="/atg/commerce/order/purchase/UpdateCreditCardFormHandler"/>
	
	<dsp:getvalueof var="contextroot" vartype="java.lang.String" bean="/OriginatingRequest.contextPath"/>
	<dsp:getvalueof var="countryCodeUS" bean="ProfileApplicationConfiguration.countryCodeUS"/>
	<dsp:getvalueof var="countryCodeAP" bean="ProfileApplicationConfiguration.countryCodeAP"/>
    <dsp:getvalueof var="enableEmailValidationError" bean="ApplicationConfiguration.enable2016S8CCDPXVI7000EmailValidationError"/>
    <dsp:getvalueof var="isTransient" bean="Profile.transient"/>
    <dsp:getvalueof var="isUseShippingAddress" bean="UserSessionBean.useShippingAddress"/>	
    <dsp:getvalueof var="enableAddressTypeAhead" bean="ApplicationConfiguration.enableAddressTypeAhead"/>
	<dsp:getvalueof var="invokeAddressTypeAhead" value="" />

    
    <fmt:setBundle basename="/com/jcpenney/dp/core/order/CheckoutResources" var="checkoutResources" scope="application"/>
    <fmt:setBundle basename="com/jcpenney/dp/core/profile/ProfileResources" var="profileBundle" scope="application"/>
	
	<dsp:getvalueof var="formHandlerName" vartype="java.lang.String" param="handlerName"/>
	<dsp:getvalueof var="handlerNameOnly" vartype="java.lang.String" param="handlerNameOnly"/>
	<dsp:getvalueof var="shippingCheckBoxId" vartype="java.lang.String" param="shippingCheckBoxId"/>
	<dsp:getvalueof var="saveCreditCardAddressToProfile" vartype="java.lang.String" param="saveCreditCardAddressToProfile"/>
	<dsp:getvalueof var="usBillingSpecific" vartype="java.lang.String" param="usBillingSpecific"/>
	<dsp:getvalueof var="militaryBillingSpecific" vartype="java.lang.String" param="militaryBillingSpecific"/>
	<dsp:getvalueof var="internationalBillingSpecific" vartype="java.lang.String" param="internationalBillingSpecific"/>
    
	<%-- Configurable messages for billing address page --%>    
	<dsp:include page="/jsp/global/configurableMessage.jsp">
		<dsp:param name="page" value="cam"/>
		<dsp:param name="keys" value="keyProfileZipCodeInvalid"/>
	</dsp:include>
	
	<%-- Changes added for international  - Start --%>
	<dsp:getvalueof var="countryNameId" bean="ShoppingCart.current.shipToCountry" />
	<%-- Changes added for international  - End --%>

	<c:choose>
		<c:when test="${usBillingSpecific == 'usBillingSpecific'}">
			<dsp:getvalueof var="fromPage" value="ccPaymentPage" />
		</c:when>
		<c:when test="${usBillingSpecific == 'usBillingSpecificForEdit'}">
			<dsp:getvalueof var="fromPage" value="ccPaymentPageForEdit" />
		</c:when>
		<c:otherwise>
		</c:otherwise>
	</c:choose>

	<dsp:droplet name="/atg/dynamo/droplet/ForEach">
		<dsp:param name="array" bean="ShoppingCart.current.shippingGroups" />
		<dsp:oparam name="output">
			<dsp:droplet name="Switch">
				<dsp:param name="value" param="element.shippingGroupClassType" />
				<dsp:oparam name="storeShippingGroup"></dsp:oparam>
				<dsp:oparam name="electronicShippingGroup"></dsp:oparam>
				<dsp:oparam name="hardgoodShippingGroup">
					<dsp:getvalueof var="giftRegistryAddressFlag" param="element.shippingAddress.registrantAddress" />
				</dsp:oparam>
				<dsp:oparam name="bopus"></dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
	
	<%-- Get address from shipping group to populate summary and billing address form --%>
	<c:if test="${!isUseShippingAddress}">
		<dsp:droplet name="GetBillingAddressFromShippingGroup">
			<dsp:param name="order" bean="ShoppingCart.current" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="firstName" param="firstName" />
				<dsp:getvalueof var="lastName" param="lastName" />
				<dsp:getvalueof var="companyName" param="companyName" />
				<dsp:getvalueof var="address1" param="address1" />
				<dsp:getvalueof var="address2" param="address2" />
				<dsp:getvalueof var="address3" param="address3" />
				<dsp:getvalueof var="city" param="city" />
				<dsp:getvalueof var="state" param="state" />
				<dsp:getvalueof var="country" param="country" />
				<dsp:getvalueof var="zipCode" param="zipCode" />
				<dsp:getvalueof var="phoneNumber" param="phoneNumber" />
			</dsp:oparam>
		</dsp:droplet>
	</c:if>
	
	<%-- Get address from payment group to populate the billing address form --%>
	<dsp:droplet name="GetBillingAddressFromPaymentGroup">
		<dsp:param name="order" bean="ShoppingCart.current" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="billingAddress" param="billingAddress" />
			<dsp:getvalueof var="isIntlAdd" param="isIntlAdd" />
			<dsp:getvalueof var="isAPOAdd" param="isAPOAdd" />
		</dsp:oparam>
	</dsp:droplet>
	
	<dsp:getvalueof var="transSmsPhnNum" bean="ShoppingCart.current.transSmsPhn" />

	<%-- Check if address selected is US address --%>
	<c:set var="isUSAdd" value="${!(isIntlAdd || isAPOAdd)}" />
	
	<%-- Get the billing address values --%>
	<c:set var="billingFirstName" value="${billingAddress.firstName}" />
	<c:set var="billingLastName" value="${billingAddress.lastName}" />
	<c:set var="billingAddress1" value="${billingAddress.address1}" />
	<c:set var="billingAddress2" value="${billingAddress.address2}" />
	<c:set var="billingAddress3" value="${billingAddress.address3}" />
	<c:set var="billingCity" value="${billingAddress.city}" />
	<c:set var="billingState" value="${billingAddress.state}" />
	<c:set var="billingCountry" value="${billingAddress.country}" />
	<c:set var="billingZipCode" value="${billingAddress.postalCode}" />
	<c:set var="billingPhoneNumber" value="${billingAddress.phoneNumber}" />
	<c:set var="billingAltPhoneNumber" value="${billingAddress.secondaryPhoneNumber}" />
	<c:set var="companyName" value="${billingAddress.companyName}" />
	<dsp:getvalueof var="billingEmail" bean="ShoppingCart.current.email" />
	
	<%-- Contact information --%>
	<div class="contact-info">
		<h2 class="subhead">Order Contact Information</h2>
		
		<%-- Error messages for Order Contact Information --%>
        <div class="orderContact_err_msgs dynamic_error_msgs hide" id="billingAddressErrorContainer">
            <span class="error-msg-header"><img alt="attention" src="/dotcom/images/error-fill-copy.png">Please correct the errors listed in red below:</span>
            <ul class="error-msgs"></ul>
        </div>
		<c:if test="${isTransient}">
			<div class="email">
				<div class="label-wrap">
					<label class="flt_lft">Email <span>*</span></label>
					<span class="flt_rgt">Why do we need this?
						<!-- 8/27 added tabindex -->
						<a id="a1_up" href="#emailTip" class="tooltipHelpIcon nomargintop contactinfo-tooltip" tabindex="-1">
						<img class="imgpos" alt="tooltip" src="/dotcom/images/tooltip_quesmark.png">
						<div class="contactinfo-tooltiptext" style="bottom:29.5%" id="emailTip">We'll use this email to send your order confirmation email.</div>
						</a>
		   			</span>
		   			
	   			</div>
				<input type="text" class="input_txt billingEmail input_email standardOptin ng-pristine ng-untouched ng-valid" value="${billingEmail}"
					id="billingEmailId" name="emailId" autocomplete="off" onkeyup="checkCanadaEmail(this.value, antiSpamEmailAddressSuffixes);" placeholder="example@domain.com"
					onfocus="clearFieldStyle(this, 'emailLabelbill', 'input_txt billingPhoneNumber standardOptin input_email', '','false')" />
				<div class="info">
					<dsp:include page="guestEmailOptin.jsp" />
				</div>
			</div>
			<div class="clear"></div>
		</c:if>
		<div class="phone">
			<div class="label-wrap">
				<label id="phoneNumberLabelbill" class="flt_lft" for="phoneNumber">Phone <span>*</span></label>
				<span class="flt_rgt">Why do we need this?
					<!-- 8/27 added tabindex -->
					<a id="a1_up" href="#phoneTip" class="tooltipHelpIcon nomargintop contactinfo-tooltip" tabindex="-1">
					<img class="imgpos" alt="tooltip" src="/dotcom/images/tooltip_quesmark.png">
					<span class="contactinfo-tooltiptext" style="bottom:25.5%" id="phoneTip">We'll use this phone to communicate important order information. Additionally we'll send text order status updates.</span>
					</a>
		   		</span>
				
			</div>
			<input type="text" class="input_txt input_phone billingPhoneNumber" id="billingPhoneNumber" name="billingPhoneNumber" autocomplete="off" value="${billingPhoneNumber}"
				maxlength="14" placeholder="(555) 555-5555" onfocus="clearFieldStyle(this, 'phoneNumberLabelbill', 'input_txt billingPhoneNumber input_phone', '','false')" />
		</div>
		<div class="sms-optin">
		   	<div class="text-alert">
		   	<c:choose>
			  <c:when test="${not empty transSmsPhnNum}">
			    <dsp:input type="checkbox" id="text-alert" bean="UpdateCreditCardFormHandler.enableTransactionalSms" onchange="smsOptIn();" onclick="cmCreateConversionEventTag('Mobile Opt in text', '1', 'Opt in');" checked="true" />
			  </c:when>
			  <c:otherwise>
			    <dsp:input type="checkbox" id="text-alert" bean="UpdateCreditCardFormHandler.enableTransactionalSms" onchange="smsOptIn();" onclick="cmCreateConversionEventTag('Mobile Opt in text', '1', 'Opt in');" />
			  </c:otherwise>
			</c:choose>
			    <span class="cb"></span>
			    <span>Text me order status updates (optional) </span>
			</div>
			<div class="clear"></div>
			<!-- DPXVI-20388 - start -->
			<div id="smsPhone" class="sms-phone">
			<div class="clear"></div>
			    <div class="icon">
			        <img src="/dotcom/images/mobile.svg" alt="">
			    </div>
			    <div class="phone">
			        <label id="phoneNumberLabelSMS" for="phoneNumberSMS" >Mobile Phone<span>*</span>
			        </label>
			        <input type="text" class="input_txt input_phone" autocomplete="off" id="textAlertNum" name="textAlertNum" maxlength="14" placeholder="(555) 555-5555" onfocus="clearFieldStyle(this, 'phoneNumberLabelSMS', 'input_txt input_phone textAlertNum', '','false')" value="${transSmsPhnNum}" />
			    </div>
			    <div class="clear"></div>
			    <p><span id="orderTextMessage"><dsp:valueof valueishtml="true" bean="CheckoutApplicationConfiguration.orderStatusTextDefaultMessage"/> 
				    <span id="moreText" style="display: none;"><dsp:valueof valueishtml="true" bean="CheckoutApplicationConfiguration.orderStatusTextShowMoreMessage"/></span> 
				    <span><a id="seeLess" href="javascript:void(0);" onclick="smsTCs()">more details</a></span></span>
			    </p>
			</div>
		<!-- DPXVI-20388 - end -->
		</div>		
	<div class="clear"></div>
	
	<%-- Billing summary populated from shipping address --%>
	<div class="billing-summary ${isUseShippingAddress ? '' : 'hide'}">
		<c:if test="${enableEmailValidationError eq true}">
			<h4 class="header">Billing Information</h4>
			<ul class="billing-lines">
				<c:choose>
					<c:when test="${isUseShippingAddress}">
						<li class="sbaline">
							<span id="sbafirstname">${billingFirstName} </span><span id="sbalastname">${billingLastName}</span>
						</li>
						<li class="sbaline">
							<span id="sbastreetaddr1">${billingAddress1}</span>
						</li>
						<li class="sbaline">
							<span id="sbastreetaddr2">${billingAddress2}</span>
						</li>
						<li class="sbaline">
							<span id="sbacity">${billingCity}</span><span id="sba-comma">, </span>
							<span id="sbastate">${billingState} </span><span id="sbazip">${billingZipCode}</span>
						</li>
						<li class="sbaline">
							<span id="sbacountry">${billingCountry}</span>
						</li>
					</c:when>
					<c:otherwise>
						<%-- If user has not selected use shipping address checkbox, below span are required to copy to form fields
						when user selects it in edit payment flow --%>
						<li class="sbaline"><span id="sbafirstname">${firstName} </span><span id="sbalastname">${lastName}</span></li>
						<li class="sbaline"><span id="sbastreetaddr1">${address1}</span></li>
						<li class="sbaline"><span id="sbastreetaddr2">${address2}</span></li>
						<li class="sbaline"><span id="sbacity">${city}</span><span id="sba-comma">, </span><span id="sbastate">${state} </span><span id="sbazip">${zipCode}</span></li>
						<li class="sbaline"><span id="sbacountry">${country}</span>
						</li>
					</c:otherwise>
				</c:choose>
				
			</ul>
		</c:if>
	</div>
	
	<%-- Change billing address form --%>
	<div class="checkout update-billing billing-info ${isUseShippingAddress ? 'hide' : ''}">
		<h4 class="billingInfoHeader">Billing Information</h4>
		<p class="font14 mrgt5" id="guestDiv">Enter your full name and address exactly as they appear on your statement.</p>
		
		<%-- Error messages for billing --%>
		<div class="bill_add_err_msgs dynamic_error_msgs hide" id="billingAddressErrorContainer">
			<span class="error-msg-header"><img alt="attention" src="/dotcom/images/error-fill-copy.png">Please correct the errors listed in red below:</span>
			<ul class="error-msgs"></ul>
		</div>

		<c:if test="${countryNameId != 'US'}">
			<div class="dynamic_error_msgs mrgb10 hide_display intrlPaymentErrorContainer" id="internationalPaymentErrorContainer">
				<div class="flt_lft mrgr15 exclamation_icn">
					<img alt="attention" src="<c:out value="${contextroot}"/>/images/spacer.gif" /><span>attention</span>
				</div>
				<div class="float_fix">
					<span class="disp_blk">Please correct the errors listed in red below:</span>
					<ul>
						<li class="hide_display"></li>
					</ul>
					<div id="internationalAddressError" class="paymentAddressError">
					</div>
				</div>
			</div>
		</c:if>
		
		<input name="<c:out value="${saveCreditCardAddressToProfile}"/>" id="<c:out value="${saveCreditCardAddressToProfile}"/>" type="hidden" value="true" />

		<div id="coldBillingAddress" class="row hide_btm_space">
			<%-- Choose country dropdown --%>
			<div id="shipCountry" class="row country_select">
				<label id="selectBillingCountryLabel" for="selectBillingCountry">country or APO/FPO/DPO</label>

				<%--Code Change to Display all International Countries Fix For defect 1841--%>
				<dspel:select bean="${formHandlerName}.billingAddress.countryName" name="selectBillingCountry" onchange="onSelectboxChange();"
					id="selectBillingCountry" iclass="flt_lft" onfocus="clearFieldStyle(this, 'selectBillingCountryLabel', 'flt_lft', '','false')">
					<dspel:option value="AP" ${isAPOAdd ? 'selected = "true"' : ''}>APO/FPO/DPO</dspel:option>
					<dsp:droplet name="ForEach">
						<dsp:param name="array" bean="CheckoutApplicationConfiguration.countriesMap"/>
						<dsp:oparam name="output">
							<dsp:getvalueof var="countryCode" param="key" />
							<dsp:getvalueof var="countryName" param="element" />
							<dsp:droplet name="IsEmpty">
								<dspel:param bean="${handlerNameOnly}.formExceptions" name="value" />
								<dsp:oparam name="true">
									<c:choose>
										<c:when test="${!isAPOAdd && countryCode == billingCountry}">
											<dspel:option selected="true" value="${countryName}">${countryName}</dspel:option>
										</c:when>
										<c:otherwise>
											<dspel:option value="${countryName}">${countryName}</dspel:option>
										</c:otherwise>
									</c:choose>
								</dsp:oparam>
								<dsp:oparam name="false">
									<dspel:option value="${countryName}">${countryName}</dspel:option>
								</dsp:oparam>
							</dsp:droplet>
						</dsp:oparam>
					</dsp:droplet>
				</dspel:select>

			</div>
			<%--First, Middle and Last Name--%>
			<div class="clear_floats"></div>
			<div class="row name_details">
				<div class="first_name">
					<div class="label-wrap">
						<label id="firstNameLabelbill" for="firstName">First Name</label>
						<span class="mandatory">*</span>
						<div id="firstNameBillingError"></div>
					</div>
					<dspel:input name="firstName" type="text" bean="${formHandlerName}.billingAddress.firstName" value="${billingFirstName}"
						iclass="input_txt input_name" id="billingFirstName" maxlength="17" onfocus="clearFieldStyle(this, 'firstNameLabelbill', 'input_txt input_name', '','false')" />
				</div>
				<div class="last_name">
					<div class="label-wrap">
						<label id="lastNameLabelbill" for="lastName">Last Name</label>
						<span class="mandatory">*</span>
						<div id="lastNameBillingError"></div>
					</div>
					<dspel:input name="lastName" type="text" bean="${formHandlerName}.billingAddress.lastName" value="${billingLastName}"
						iclass="input_txt input_name" id="billingLastName" maxlength="24" onfocus="clearFieldStyle(this, 'lastNameLabelbill', 'input_txt input_name', '','false')" />
				</div>
			</div>
			<%--Company Name--%>
			<div id="companyBlock" class="row company_name optionalCol">
				<label for="companyName">Company (optional)</label>
				<dspel:input name="companyName" type="text" bean="${formHandlerName}.billingAddress.companyName" maxlength="23" value="${companyName}" iclass="input_txt input_length_txt" id="companyName">
					<dspel:tagAttribute name="placeholder" value="optional" />
				</dspel:input>
			</div>
			
			<%--Specific to United States Country Location--%>
			<div id="<c:out value='${usBillingSpecific}'/>"  class="country-specific-fields">

				<%--United States Street Address--%>
				<div class="row bill_address">
					<div id="address1Blk" class="flt_lft bill_add1">
						<div class="label-wrap">
							<label id="streetAddress1Labelbill" for="streetAddress1">Street Address</label>
							<span class="mandatory">*</span>
							<div id="streetAddress1Error"></div>
						</div>
						<c:choose>
							<c:when test="${isStoreShippingSelectedFlag}">
								<input type="text" class="input_txt billingAddress1 input_address" id="billingStreetAddress1" maxlength="40" name="streetAddress1"
									autocomplete="off" autofocus="autofocus" onfocus="clearFieldStyle(this, 'streetAddress1Labelbill', 'input_txt input_address billingAddress1', '','false')" />
							</c:when>
							<c:otherwise>
								<c:if test="${enableAddressTypeAhead eq 'true' }" >
									<dsp:getvalueof var="invokeAddressTypeAhead" value="suggestAddress('#billingStreetAddress1', '#billingCity', '#billingState', '#billingZip');" />
								</c:if>
								<input type="text" class="input_txt billingAddress1 input_address" id="billingStreetAddress1" maxlength="40" name="streetAddress1" value="${isUSAdd ? billingAddress1 : ''}"
									autocomplete="off" onkeypress="${invokeAddressTypeAhead}" onfocus="clearFieldStyle(this, 'streetAddress1Labelbill', 'input_txt input_address billingAddress1', '','false')" />
							</c:otherwise>
						</c:choose>
					</div>
					<div id="address2Blk" class="flt_lft bill_add2 optionalCol">
						<label for="streetAddress2">Apt, Suite, Floor (optional)</label> 
						<input type="text" placeholder="optional" class="input_txt billingAddress2" id="billingStreetAddress2"
							maxlength="40" name="streetAddress2" autocomplete="off" value="${isUSAdd ? billingAddress2 : ''}"/>
					</div>
				</div>
				
				<%--United States City, State and Zip Code--%>
				<div class="row bill_location">
					<%--Auto Populate City/State from Zip Code (Payment) START--%>
					<div id="<c:out value='${fromPage}'/>_autoPopulateError" class="autoPopulateError hide">
						<div class="flt_lft mrgr15 exclamation_icn">
							<img src="<c:out value="${contextroot}"/>/images/spacer.gif" alt="attention" /><span>attention</span>
						</div>
						<div class="float_fix">
							<span class="disp_blk">Please correct the errors listed in red below:</span>
							<ul>
								<li><div id="keyProfileZipCodeInvalid"></div></li>
							</ul>
						</div>
					</div>
					<div id="<c:out value='${fromPage}'/>_multiCityInfoMsg" class="multiCityInfoMsg bgcolor hide">
						<div class="flt_lft mrgr10 information_icn">
							<img alt="attention" src="/dotcom/images/spacer.gif">
						</div>
						<div class="float_fix">
							<p>Choose your city.</p>
						</div>
					</div>
					<div id="zipCityBillingError"></div>
					<div id="zipBlk" class="flt_lft bill_zip">
						<label id="zipCodeLabelbill" for="zipCode">Zip Code <span class="mandatory">*</span></label>
						<input type="text" class="input_txt input_zip" id="billingZipCode" value="${isUSAdd ? billingZipCode : ''}"
							name="zipCode" maxlength="5" autocomplete="off" onfocus="clearFieldStyle(this, 'zipCodeLabelbill', 'input_txt input_zip', '','false')"
							onblur="zipProc.validateZip(this.value,'<c:out value='${usBillingSpecific}'/> #cityBlk #billingCity',
								 '<c:out value='${usBillingSpecific}'/> #stateSelectBlk #billingState',
								 '<c:out value='${usBillingSpecific}'/> #zipBlk #zipCode','<c:out value='${fromPage}'/>');" />
					</div>
					<div id="cityBlk" class="flt_lft bill_city">
						<label id="cityNameLabelbill" for="cityName">City <span class="mandatory">*</span></label> 
						<input type="text" class="input_txt billingAddresscity1 input_city" id="billingCity" name="cityName" maxlength="20" autocomplete="off" value="${isUSAdd ? billingCity : ''}" 
							onfocus="clearFieldStyle(this, 'cityNameLabelbill', 'input_txt billingAddresscity1 input_city', '','false')" />
					</div>
					<div id="stateSelectBlk" class="flt_lft bill_state">
						<label id="selectStateLabelbill" for="selectState">State <span class="mandatory">*</span></label> 
						<dspel:select name="selectState" iclass="bill_state_select" id="billingState" bean="${formHandlerName}.billingAddress.state"
							onfocus="clearFieldStyle(this, 'selectStateLabelbill', 'bill_state_select', '','false')">
							<c:if test="${empty billingState}">
								<dsp:option value="">select a state</dsp:option>
							</c:if>
							<dsp:droplet name="ForEach">
								<dsp:param name="array" bean="CheckoutApplicationConfiguration.usStatesMap"/>
								<dsp:oparam name="output">
									<dsp:getvalueof var="stateAbbrev" param="key" />
									<dsp:getvalueof var="stateName" param="element" />
									<option value="<c:out value='${stateAbbrev}'/>" ${billingState eq stateAbbrev ? 'selected' : ''}>
										${stateName}
									</option>
								</dsp:oparam>
							</dsp:droplet>
						</dspel:select>
					</div>
				</div>
				<%--Auto Populate City/State from Zip Code (Payment) START--%>
				<div id="<c:out value='${fromPage}'/>_autopopulateCitydiv" class="ol autoPopulateCity  mrgnLayoutForCity mrgnleftLayoutForCity">
					<div id="autopopulateCitydivInner" class="autoPopulateCityList"></div>
				</div>
				<div id="<c:out value='${fromPage}'/>_autopopulateStatediv" class="ol autoPopulateState mrgnLayoutForState mrgnleftLayoutForState">
					<div id="autopopulateStatedivInner" class="autoPopulateStateList"></div>
				</div>
				<%--Auto Populate City/State from Zip Code (Payment) END--%>
			</div>
			
			<div id="<c:out value='${militaryBillingSpecific}'/>" class="country-specific-fields">
				<div class="row bill_address">
					<div class="flt_lft bill_add1">
						<label id="militaryStreetAddress1Labelbill" for="militaryStreetAddress1">Street Address</label>
						<input type="text" class="input_txt mili_str1 billingAddress1"
							id="militaryStreetAddress1" maxlength="40" name="militaryStreetAddress1" value="${isAPOAdd ? billingAddress1 : ''}"
							onfocus="clearFieldStyle(this, 'militaryStreetAddress1Labelbill', 'input_txt mili_str1 billingAddress1', '','false')" />
						<div class="apo_info">
							<span><strong>Army/Navy/Marines: </strong>Enter Unit and Box Number</span>
							<span><strong>Ships: </strong>Enter Ship Name and Hull Number</span>
							<span><strong>Air Force: </strong>Enter PSC and Box Number</span>
						</div>
					</div>
					<div class="flt_lft bill_add2 optionalCol">
						<label for="militaryStreetAddress2">Street Address 2</label> 
						<input type="text" placeholder="optional" class="input_txt mili_str1 billingAddress2"
							id="militaryStreetAddress2" maxlength="40" name="militaryStreetAddress2" value="${isAPOAdd ? billingAddress2 : ''}" />
						<div class="apo_info">
							<span>Enter optional military command or organization name</span>
							<span>(e.g., USAG J or 2/566 Postal Co.)</span>
						</div>
					</div>
				</div>
				<div class="row bill_location">
					<div id="milZipCityBillingError" class="milZipCityBillingErrorModal"></div>
					<div class="flt_lft bill_zip">
						<label id="militaryZipCodeLabelbill" for="militaryZipCode">ZIP code</label>
						<input type="text" class="input_txt input_zip" id="militaryZipCode" maxlength="5" name="militaryZipCode"
							onfocus="clearFieldStyle(this, 'militaryZipCodeLabelbill', 'input_txt input_zip', '','false')" value="${isAPOAdd ? billingZipCode : ''}"/>
						<div class="apo_info">must start with a 0, 3 or 9</div>
					</div>

					<div id="militaryBlk" class="flt_lft bill_city">
						<label id="militaryTypeLabelbill" for="militaryType">APO/FPO/DPO</label>
						<dspel:select iclass="bill_state_select mili_opt" id="militaryType" name="militaryType"
							bean="${formHandlerName}.billingAddress.militaryAddressType" onfocus="clearFieldStyle(this, 'militaryTypeLabelbill', 'bill_state_select mili_opt', '','false')">
							<option value="">select</option>
							<option value="APO" ${billingCity eq 'APO' ? 'selected' : ''}>APO</option>
							<option value="FPO" ${billingCity eq 'FPO' ? 'selected' : ''}>FPO</option>
							<option value="DPO" ${billingCity eq 'DPO' ? 'selected' : ''}>DPO</option>
						</dspel:select>
					</div>
					<div class="flt_lft bill_state">
						<label id="militaryStateLabelbill" for="militaryState">state</label>
						<dspel:select id="militaryState" name="militaryState" iclass="bill_state_select mili_state"
							bean="${formHandlerName}.billingAddress.state" onfocus="clearFieldStyle(this, 'militaryStateLabelbill', 'bill_state_select mili_state', '','false')">
							<option value="">select a state</option>
							<option value="AA" ${billingState eq 'AA' ? 'selected' : ''}>AA (Armed forces Americas)</option>
							<option value="AE" ${billingState eq 'AE' ? 'selected' : ''}>AE (Armed forces Europe)</option>
							<option value="AP" ${billingState eq 'AP' ? 'selected' : ''}>AP (Armed forces Pacific)</option>
						</dspel:select>
					</div>
				</div>
			</div>

			<div id="<c:out value='${internationalBillingSpecific}'/>" class="country-specific-fields">
				<%--International Street Address--%>
				<div class="row intl_bill_address">
					<div class="flt_lft bill_add1">
						<div class="label-wrap">
							<label id="internationalStreetAddressLabelbill" for="internationalStreetAddress">Street Address</label>
							<span class="mandatory">*</span>
						</div>
						<input type="text" class="input_txt input_long_txt input-address intlBillingAddress1" id="internationalStreetAddress" maxlength="40" value="${isIntlAdd ? billingAddress1 : ''}"
							name="internationalStreetAddress" autocomplete="off" onfocus="clearFieldStyle(this, 'internationalStreetAddressLabelbill', 'input_txt input_long_txt intlBillingAddress1', '','false')" />
					</div>
					<%-- Fields added for international  - Start --%>
					<div class="flt_lft optionalCol" id="streetAddress2">
						<label for="internationalStreetAddress2">Street Address 2 (optional)</label>
						<input type="text" placeholder="optional" class="input_txt input_long_txt input-address intlBillingAddress2"
							value="${isIntlAdd ? billingAddress2 : ''}" id="internationalStreetAddress2" maxlength="40" name="internationalStreetAddress2" autocomplete="off" />
					</div>
					<div class="flt_lft optionalCol" id="streetAddress3">
						<label for="internationalStreetAddress3">Street Address 3 (optional)</label>
						<input type="text" placeholder="optional" class="input_txt input_long_txt intlBillingAddress3"
							value="${isIntlAdd ? billingAddress3 : ''}" id="internationalStreetAddress3" maxlength="40" name="internationalStreetAddress3" autocomplete="off" />
					</div>
					<%-- Fields added for international  - End --%>
				</div>
				<%--Australia City, Province & Postal Code--%>
				<div id="intZipCityBillingError"></div>
				<div class="row bill_location">
					<div class="flt_lft bill_city">
						<div class="label-wrap">
							<label id="internationalCityLabelbill" for="internationalCity">City</label>
							<span class="mandatory">*</span>
						</div>
						<input type="text" class="input_txt billingAddresscity1" id="internationalCity" maxlength="20" name="internationalCity"
							value="${isIntlAdd ? billingCity : ''}" autocomplete="off" onfocus="clearFieldStyle(this, 'internationalCityLabelbill', 'input_txt billingAddresscity1', '','false')" />
					</div>
					<div class="flt_lft bill_state can_province" id="ProvinceDivCanada" style="display: none">
						<label for="internationalProvince">Province/Territory/Region </label>
						<dspel:select iclass="input_txt billingAddressProvince1" id="canadaProvince" name="canadaProvince"
							bean="${formHandlerName}.billingAddress.provinceRegion">
							<dsp:option value="">select a province</dsp:option>
							<dsp:droplet name="ForEach">
								<dsp:param name="array" bean="CheckoutApplicationConfiguration.canadaStatesMap"/>
								<dsp:oparam name="output">
									<dsp:getvalueof var="stateAbbrev" param="key" />
									<dsp:getvalueof var="stateName" param="element" />
									<option value="<c:out value='${stateAbbrev}'/>" ${state eq stateAbbrev ? 'selected' : ''}>
										${stateName}
									</option>
								</dsp:oparam>
							</dsp:droplet>
						</dspel:select>
					</div>
					<div class="flt_lft bill_state intl_province" id="ProvinceDiv">
						<label for="internationalProvince">Province/Territory/Region</label>
						<dspel:input type="text" iclass="input_txt billingAddressProvince1" id="internationalProvince" maxlength="10" name="internationalProvince"
							value="${isIntlAdd ? billingState : ''}" bean="${formHandlerName}.billingAddress.provinceRegion" onfocus="clearFieldStyle(this, 'internationalProvinceLabelbill', 'input_txt billingAddressProvince1', '','false')" />
					</div>
					<div class="flt_lft bill_zip">
						<label id="internationalPostalCodeLabelbill" for="internationalPostalCode">Postal Code</label>
						<input type="text" class="input_txt input_zip" id="internationalPostalCode" maxlength="8"
							value="${isIntlAdd ? billingZipCode : ''}" name="internationalPostalCode" autocomplete="off" onfocus="clearFieldStyle(this, 'internationalPostalCodeLabelbill', 'input_txt input_zip', '','false')" />
					</div>
				</div>
			</div>
							
		</div>
			
		<%-- Fields added for international  - Start --%>
		<div class="row contact_detail optionalCol" id="phoneNumberSecondary">
			<label for="phoneNumber">Alt Phone Number</label>
			<input type="text" placeholder="optional" class="input_txt" id="billingPhoneNumberSecondary" name="billingPhoneNumberSecondary"
				autocomplete="off" maxlength="13" value="${billingAltPhoneNumber}" />
		</div>
		<%-- Fields added for international  - End --%>
	</div>
	
	<%-- Enable edit billing address checkbox --%>
	<div class="edit-add-checkbox ${isUseShippingAddress ? 'selected' : ''}">
		<dsp:droplet name="/atg/dynamo/droplet/ForEach">
			<dsp:param name="array" bean="ShoppingCart.current.shippingGroups" />
			<dsp:oparam name="output">
				<dsp:droplet name="Switch">
					<dsp:param name="value" param="element.shippingGroupClassType" />
					<dsp:oparam name="hardgoodShippingGroup">
						<c:if test="${giftRegistryAddressFlag == false}">
							<dsp:input type="checkbox" iclass="input_check store_locator flt_lft" id="${shippingCheckBoxId}" bean="UpdateCreditCardFormHandler.useShipAddress"
								checked="${isUseShippingAddress}" onclick="showBillingAddressForm(this);zipProc.clearAutoPopulateWidgets('${fromPage}');" />
							<span class="cb"></span>
						</c:if>
						<c:if test="${giftRegistryAddressFlag == true}">
							<input type="checkbox" disabled="disabled" class="flt_lft" id="shippingGRCreateId" name="shippingGRCreateId" />
							<span class="cb"></span>
						</c:if>
					</dsp:oparam>
					<dsp:oparam name="storeShippingGroup">
						<input type="checkbox" class="input_check store_locator flt_lft" id="<c:out value="${shippingCheckBoxId}"/>"
							name="<c:out value="${shippingCheckBoxId}"/>" onclick="showBillingAddressForm();zipProc.clearAutoPopulateWidgets('<c:out value='${fromPage}'/>');" DISABLED />
						<span class="cb"></span>
						<c:set var="isStoreShippingSelectedFlag" value="true"></c:set>
					</dsp:oparam>
					<dsp:oparam name="electronicShippingGroup">
						<input type="checkbox" class="input_check store_locator flt_lft" id="<c:out value="${shippingCheckBoxId}"/>"
							name="<c:out value="${shippingCheckBoxId}"/>" onclick="showBillingAddressForm();zipProc.clearAutoPopulateWidgets('<c:out value='${fromPage}'/>');" DISABLED />
						<span class="cb"></span>
					</dsp:oparam>
					<dsp:oparam name="bopusShippingGroup">
						<dsp:getvalueof var="sgArraySize" param="size" />
						<c:if test="${sgArraySize == 1}">
							<input type="checkbox" class="input_check store_locator flt_lft" id="<c:out value="${shippingCheckBoxId}"/>" name="<c:out value="${shippingCheckBoxId}"/>" DISABLED />
							<span class="cb"></span>
						</c:if>
					</dsp:oparam>
				</dsp:droplet>
			</dsp:oparam>
		</dsp:droplet>
		<span class="shippingAddressCheckBox">Use my shipping address for billing</span>
	</div>
	
	<dspel:input type="hidden" id="phoneNumberLength" name="phoneNumberLength" bean="OCSApplicationConfiguration.phoneNumberMaxLength" />
	<dspel:input type="hidden" id="internationalPhoneNumberLength" name="internationalPhoneNumberLength" bean="OCSApplicationConfiguration.internationalPhoneNumberMaxLength" />
	<dspel:input type="hidden" id="hdnCountry" name="hdnCountry" bean="${formHandlerName}.billingAddress.countryName" />
	<dspel:input type="hidden" id="hdnPhoneNumber" name="hdnPhoneNumber" bean="${formHandlerName}.billingAddress.phoneNumber" />
	<dspel:input type="hidden" id="hdnBillingStreetAddress1" name="hdnBillingStreetAddress1" bean="${formHandlerName}.billingAddress.address1" />
	<dspel:input type="hidden" id="hdnBillingStreetAddress2" name="hdnBillingStreetAddress2" bean="${formHandlerName}.billingAddress.address2" />
	<dspel:input type="hidden" id="hdnBillingCityName" name="hdnBillingCityName" bean="${formHandlerName}.billingAddress.city" />
	<dspel:input type="hidden" id="hdnBillingZipCode" name="hdnBillingZipCode" bean="${formHandlerName}.billingAddress.postalCode" />
	<dspel:input type="hidden" id="hdnBillingCanProvince" name="hdnBillingCanProvince" bean="${formHandlerName}.billingAddress.provinceRegion" />
	<dspel:input type="hidden" id="hdnEmailAddress" name="hdnEmailAddress" bean="${handlerNameOnly}.emailAddress" />
	<dspel:input type="hidden" id="hdnTextAlertNum" name="hdnTextAlertNum" bean="${handlerNameOnly}.transSmsPhnNum" />		
	<%-- Fields added for international  - Start --%>
	<dspel:input type="hidden" id="hdnBillingStreetAddress3" name="hdnBillingStreetAddress3" bean="${formHandlerName}.billingAddress.address3" />
	<dspel:input type="hidden" id="hdnSecondaryPhoneNumber" name="hdnSecondaryPhoneNumber" bean="${formHandlerName}.billingAddress.secondaryPhoneNumber" />
	<%-- Fields added for international  - End --%>
	
</dsp:page>