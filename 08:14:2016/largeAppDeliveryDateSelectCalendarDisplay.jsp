<%@ taglib prefix="c" uri="c"%><%@ taglib prefix="fmt" uri="fmt"%><%@ taglib
uri="dsp" prefix="dsp"%><%@ taglib uri="dspel" prefix="dspel"%>
<dsp:page><c:set var="isLargeAppItemInBag" value="false" scope="request" />
<dsp:importbean bean="/com/jcpenney/core/util/largeapp/LargeAppApplicationConfiguration"/><dsp:importbean bean="/com/jcpenney/catalog/droplet/PageCategoryByNameDroplet"/><dsp:importbean bean="/com/jcpenney/order/CheckoutApplicationConfiguration"/><dsp:droplet name="/com/jcpenney/core/order/droplet/LargeAppItemCheckDroplet"><dsp:param name="order" bean="ShoppingCart.current"/><dsp:oparam name="output"><dsp:droplet name="/atg/dynamo/droplet/Switch"><dsp:param name="value" param="largeAppliance"/><dsp:oparam name="true"><c:set var="isLargeAppItemInBag" value="true" scope="request" />
<dsp:droplet name='/com/jcpenney/core/integration/largeappliance/droplet/LargeAppDeliveryDateDroplet'><dsp:param name="order" bean="ShoppingCart.current" /><dsp:param name="zipCode" param="zipCode"/><dsp:oparam name="output"><dsp:getvalueof var="availableDates" param="deliveryDateArray"/><dsp:getvalueof var="minDate" param="deliveryDateMinDate"/><dsp:getvalueof var="maxDate" param="deliveryDateMaxDate"/><dsp:getvalueof var="isManualDate" param="isManualDeliveryDate"/></dsp:oparam></dsp:droplet></dsp:oparam></dsp:droplet></dsp:oparam></dsp:droplet><style type="text/css">
.ui-state-default, .ui-widget-content .ui-state-default {
border: none;!important;
background: none;!important;
color: #555;!important;
outline: 0;!important;
}
#datepicker_heading1,#datepicker_heading2 {
font: 14px Helvetica, Arial, Sans-serif;
color: #666;
margin:0;
}
.ui-widget-header {
background: none;!important;
border: none;!important;
}
.selectedDateStr {
font: bold 14px Helvetica,Arial,Sans-serif !important;
margin:0;
display: block;
}
.ui-datepicker .ui-datepicker-title {
text-align: left;!important;
/*margin-left: 0;!important;*/
}
</style>
<c:if test="${isLargeAppItemInBag == true}">
<dsp:droplet name="/atg/dynamo/droplet/ForEach"><dsp:param name="array" bean="ShoppingCart.current.shippingGroups" /><dsp:oparam name="output"><dsp:droplet name="Switch"><dsp:param name="value" param="element.shippingGroupClassType" /><dsp:oparam name="hardgoodShippingGroup"><span id="varselectedDeliveryDate" class="hide_display"><dsp:valueof param="element.deliveryDate"/></span>
<dsp:getvalueof var="selectedDelDate" param="element.deliveryDate"/></dsp:oparam></dsp:droplet></dsp:oparam></dsp:droplet><span class="delOptions">Delivery Options</span>
<p class="change_availability"><dsp:valueof bean="LargeAppApplicationConfiguration.changeDeliveryDateMessage"/></p>
<div id="datepicker_wrapper" class="padt15 mrgbtm10px">
<span class="hide_display" id="isManualDate">${isManualDate}</span>
<div id="dateReselect">
<c:if test="${isManualDate eq 'true' && (selectedDelDate == null || selectedDelDate == '')}">
<div class="flt_lft_largeApp_cal_info_container" id="manualReselectError">
<div class="hide_display" id="dateReselection">
<div class="flt_lft mrgr15">
<img title="attention" alt="attention" class="mrgltop10px"
src="<c:out value='${contextroot}'/>/images/dynamic_info_icn.gif" />
</div>
<div id="deliverydateInfo">
<fmt:message key="keySelectedDeliveryDateErrorServiceDown"
bundle="${checkoutResources}" >
</fmt:message>
</div>
</div>
<div id="manualDate">
<div class="flt_lft mrgr15">
<img title="attention" alt="attention" class="mrgltop10px"
src="<c:out value='${contextroot}'/>/images/info.svg" />
</div>
<div id="deliverydateInfo">
<fmt:message key="keySelectDeliveryDateFailError"
bundle="${checkoutResources}" />
</div>
</div>
</div>
</c:if>
<div class="flt_lft_largeApp_cal_info_container hide_display" id="reselectInfo">
<div class="flt_lft mrgr15">
<img title="attention" alt="attention" class="mrgltop10px"
src="<c:out value='${contextroot}'/>/images/dynamic_info_icn.gif" />
</div>
<div id="deliverydateInfoMsg">
<fmt:message key="keySelectedDeliveryDateError"
bundle="${checkoutResources}" />
</div>
</div>
</div>
<div class="truckable mrgt20" style="float: left;padding-top: 20px;width: 100%;">
<div id="truckableItemServerErrorContainer" class="dynamic_error_msgs mrgb10" style="display:none">
<div class="flt_lft mrgr15 exclamation_icn"><img title="attention" alt="attention" src="<c:out value='${contextroot}'/>/images/spacer.gif" /><span>attention</span>
</div>
<div class="float_fix mrgt5">
<ul id="truckableItemServerErrors">
</ul>
<ul id="selectedDateUnavailable"></ul>
</div>
</div>
</div>
<div id="delivery_blk">
<div id="datepicker" class="deliveryDatePicker"><p class="delivery_availability"><dsp:valueof bean="LargeAppApplicationConfiguration.availabilityWindowMessage"/></p></div>
<div class="date-col">
<div id="deliveryDateDiv" class="deliveryDateDiv">
<p id="datepicker_heading2">Select Delivery Date</p>
<p id="datepicker_heading1" class="hide_display">Selected delivery date: </p>
<div class="selectedDateCont">
<img src="<c:out value="${contextroot}"/>/images/calendar.svg">
<div class="desc">
<span class="selectedDateStr hide_display" id="selectedDate"></span>
<span id="lostDeliveryDate" class="hide_display"></span>
<div id="zipChange" class="zipChange">you may choose different available date</div>
</div>
</div>
<input type="hidden" id="selectedDeliveryDate1" name="selectedDeliveryDate1"/>
</div>
<c:choose>
<c:when test="${includeLargeAppItem == 'true'}">
<dsp:getvalueof var="pageCategoryName" bean="CheckoutApplicationConfiguration.largeAppTruckDeliveryInfoPageName" /></c:when>
<c:otherwise>
<dsp:getvalueof var="pageCategoryName" bean="CheckoutApplicationConfiguration.truckDeliveryInfoPageName" /></c:otherwise>
</c:choose>
<dsp:droplet name="PageCategoryByNameDroplet"><dspel:param name="name" value="${pageCategoryName}"/><dsp:oparam name="output"><dsp:getvalueof param="element.repositoryId" var="truckablemsgPGId"/></dsp:oparam></dsp:droplet><input type="hidden" id="pageId" value='<c:out value="${truckablemsgPGId}"/>'/>
<div id="shippingMethodDiv" class="shippingMethodDiv">
<p id="datepicker_heading2">Shipping Method:</p>
<div class="selectedDateCont">
<img id="deltruckImg" src="<c:out value="${contextroot}"/>/images/truck.svg">
<div class="desc">
<p class="selectedDateStr" id="shippingMethodContent" > Truck Delivery FREE</p>
<a href="#" title="See details" onclick="generateModalURL();">See details</a>
</div>
</div>
</div>
</div>
</div>
</div>
<dsp:getvalueof var="enableAltPhoneAndDeliveryInstruction" bean="/com/jcpenney/core/util/largeapp/LargeAppApplicationConfiguration.enableAltPhoneAndDeliveryInstruction" /><c:if test="${enableAltPhoneAndDeliveryInstruction}">
<dsp:droplet name="/atg/dynamo/droplet/ForEach"><dsp:param name="array" bean="ShoppingCart.current.shippingGroups" /><dsp:oparam name="output"><dsp:droplet name="Switch"><dsp:param name="value" param="element.shippingGroupClassType" /><dsp:oparam name="hardgoodShippingGroup"><div id="delIns">
<div class="delIns-head">
<span class="bold">Delivery Instructions (Optional)</span><span class="flt_rgt maxlength">max 120 characters, do not use "&" or "@" </span>
</div>
<c:set var="placeHolder" value="Ex: I live in a gated community and the key code is 5555; my subdivision is new and not on Google maps; knock on the back door; 3rd floor delivery." />
<dsp:getvalueof var="specialCharRegex" bean="/com/jcpenney/core/util/largeapp/LargeAppApplicationConfiguration.specialCharRegex" /><dspel:textarea autocomplete="off" iclass="deliveryins mrgtp10px" valueishtml="true" maxlength="120" id="largeApplianceDeliveryIns1" name="largeApplianceDeliveryIns1" value="${placeHolder}" onkeypress='if(event.keyCode == 13){event.preventDefault();} return ( this.value.length < 120);' onkeyup="preventSpecialChars('${specialCharRegex}');charCounter();"  onblur="preventSpecialChars('${specialCharRegex}');"
placeholder="${placeHolder}" bean="CreateHardgoodShippingGroupFormHandler.largeApplianceDeliveryIns" ><dsp:valueof param="element.deliveryInstruction"/></dspel:textarea>
<div class="flt_rgt" id="textarea_remain"></div>
</div>
</dsp:oparam></dsp:droplet></dsp:oparam></dsp:droplet></c:if>
<div class="delivery_info">
<div class="delivery_times">
<img id="delTimesImg" src="<c:out value="${contextroot}"/>/images/silver-clock.svg">
<div class="desc">
<span>Delivery times</span>
<span><dsp:valueof bean="LargeAppApplicationConfiguration.deliveryTimesMessage"/><span>
</div>
</div>
<div class="delivery_contact">
<img id="delContactImg" src="<c:out value="${contextroot}"/>/images/phone-grey.svg">
<div class="desc">
<span>We'll contact you</span>
<span><dsp:valueof bean="LargeAppApplicationConfiguration.deliveryAgentMessage"/></span>
</div>
</div>
</div>
<script type="text/javascript">
var availableDates ="<c:out value='${availableDates}'/>";
var avaialableDateArray = getAvailableDateValues(availableDates);
var dateArr = [], sortedDateArr = [], dateCount = avaialableDateArray.length;
for(var i = 0; i < dateCount; i++) {
dateArr[i] = (new Date(avaialableDateArray[i]));
}
sortedDateArr = dateArr.sort(function(a,b) {
return (a - b);
});
function available(date) {
return checkAvailable(date, avaialableDateArray, availableDates);
}
function checkSelDateAvail(selectedDt) {
if($.trim(selectedDt.length) > 0) {
var date = (new Date(selectedDt));
var dateStr = date.getFullYear() + "-" + ("0"+(date.getMonth() + 1 )).slice(-2) + "-" + ("0"+(date.getDate() + 1 )).slice(-2);
if(availableDates.indexOf(dateStr) < 0) {
if(document.getElementById('truckableItemServerErrorContainer') != undefined && document.getElementById('truckableItemServerErrorContainer') != null) {
document.getElementById('selectedDateUnavailable').innerHTML="The previously selected delivery date is no longer available. Please select a new date.";
}
$('#truckableItemServerErrorContainer').show();
}
}
}
$(function() {
$( "#datepicker" ).datepicker({
//numberOfMonths:[2,1],
//minDate: "<c:out value='${minDate}'/>",
minDate: addDays(sortedDateArr[0], 1),
maxDate: addDays(sortedDateArr[dateCount - 1], 1),
hideIfNoPrevNext: false,
prevText: "",
nextText: "",
dayNamesMin:[ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ],
beforeShowDay: available,
/*onChangeMonthYear: function(year, month, obj) {
if(month == lastAvlDtArrayMon) {
$("#delivery_blk .ui-datepicker-next").hide();
}
},*/
onSelect: function(date, obj, e) {
var date1 = $(this).datepicker('getDate');
handleDateSelection(date1, avaialableDateArray, availableDates);
}
});
var selectedDt = $("#varselectedDeliveryDate").text();
var arr = "";
var dt = "";
if(selectedDt != "" && selectedDt != undefined) {
arr = selectedDt.split("/");
dt = arr[2] + "-" + arr[0] + "-" + arr[1];
}
if($.trim(selectedDt.length) > 0) {
checkSelDateAvail(selectedDt);
}
});
$(document).ready(function() {
showDateUsingDatePicker(avaialableDateArray, availableDates);
if ($('#deliverydateInfo') && $('#deliverydateInfo').html() != '') {
//handleCoreMetricsLargeAppTag('Delivery Date Service Call Failed', 'Project Silver Choose Delivery Date');
}
if ($('#streetAddress1') && $('#streetAddress1').val() != '' && ($('#varselectedDeliveryDate').html() == null || $('#varselectedDeliveryDate').html() == '')) {
//handleCoreMetricsLargeAppTag('Date Reselect Required', 'Project Silver Choose Delivery Date');
}
});
</script>
</c:if>
</dsp:page>
