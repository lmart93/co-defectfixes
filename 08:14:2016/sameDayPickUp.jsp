<%--
* -----------------------------------------------------------------------
* Copyright 2011, jcpenney Co., Inc.  All Rights Reserved.
*
* Reproduction or use of this file without explicit
* written consent is prohibited.
* Created by: cgeddam1
*
* Created on: June 296, 2016
* ----------------------------------------------------------------
--%>
<%-- ========================================= Description
/**
* This page is used to display
*  1.This Jsp contains all the read only pages
* Includes Header and Footer JSPFs.
*
* @author cgeddam1
* @version 0.1 26-June-2016
**/
--%>
<%@ taglib uri="dsp" prefix="dsp" %><%@ taglib uri="dspel" prefix="dspel" %><%@ taglib uri="c" prefix="c"  %><%@ taglib uri="fmt" prefix="fmt" %><%@ page pageEncoding="UTF-8" %><dsp:page xml="true"><dsp:importbean bean="/atg/commerce/ShoppingCart" scope="session"/><dsp:importbean bean="/com/jcpenney/storelocator/droplet/StoreLookupDroplet" /><dsp:importbean bean="/com/jcpenney/catalog/custom/ProductPresentationItemLookupDroplet" /><dsp:importbean bean="/com/jcpenney/core/order/droplet/DisplayShippingPanelDroplet"/><dsp:importbean bean="/com/jcpenney/core/order/droplet/CommerceItemImageLookupDroplet" /><dsp:importbean bean="/atg/dynamo/droplet/Switch"/><dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/><dsp:importbean bean="/atg/dynamo/droplet/ForEach"/><dsp:importbean bean="/com/jcpenney/core/ApplicationConfiguration"/><dsp:importbean bean="/com/jcpenney/core/order/formhandler/UpdateBopusShippingGroupFormHandler"/><dsp:getvalueof var="contextroot" vartype="java.lang.String" bean="/OriginatingRequest.contextPath"/><dsp:getvalueof var="containsOnlyBopusItems" param="containsOnlyBopusItems"/><script type="text/javascript" src="<c:out value="${contextroot}"/>/js/checkoutsimplified/commonValidations.js?v=<dsp:valueof bean="ApplicationConfiguration.releaseVersion"/>"></script>
<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/bopuscheckoutsimplified.js?v=<dsp:valueof bean="ApplicationConfiguration.releaseVersion"/>"></script>
<script type="text/javascript" src="<c:out value="${contextroot}"/>/js/checkoutsimplified/formValidator.js?v=<dsp:valueof bean="ApplicationConfiguration.releaseVersion"/>"></script>
<dsp:getvalueof var="panel" param="panel" /><div class="step sameday ng-scope ng-enter">
<div class="header ng-scope">
  <h1>
  <img src="<c:out value='${contextroot}'/>/images/same-day.svg" alt="SAME DAY PICKUP" />
    SAME DAY PICKUP
  </h1>
</div>
<form id="pickupDetailsForm" class="jcp-form ng-pristine ng-valid ng-scope">
<dsp:droplet name="/atg/dynamo/droplet/ForEach"><dsp:param name="array" bean="ShoppingCart.current.shippingGroups" /><dsp:oparam name="output"><dsp:droplet name="Switch"><dsp:param name="value" param="element.shippingGroupClassType"/><dsp:oparam name="bopusShippingGroup"><dsp:getvalueof var="bopusShippingGroup" param="element" scope="request"/><input type="hidden" id="isPrimaryPickup" value="${bopusShippingGroup.primaryPickup}" />
</dsp:oparam></dsp:droplet></dsp:oparam></dsp:droplet><dspel:setvalue param="bopuShippingGroup" value="${bopusShippingGroup}"/><div class="location">
<div class="icon">
<img src="<c:out value='${contextroot}'/>/images/location.svg" alt="">
</div>
<div class="store">
<dsp:droplet name="StoreLookupDroplet"><dsp:param name="storeId" param="bopuShippingGroup.storeId" /><dsp:oparam name="storeDetails"><div class="snippet">
<h5><dsp:valueof param="storeName" /></h5>
<p><dsp:valueof param="street" /></p>
<p><dsp:valueof param="city" />,<dsp:valueof param="state" />&nbsp;<dsp:valueof param="zipCode" /></p>
<p><dsp:valueof param="storePhone" /></p>
</div>
<div class="snippet hours">
<dsp:droplet name="IsEmpty"><dsp:param name="value" param="overrideText" /><dsp:oparam name="true"><dsp:droplet name="ForEach"><dsp:param name="array" param="storeTimingMap" /><dsp:oparam name="output"><p><dsp:valueof param="key" valueishtml="true" />:&nbsp;<dsp:valueof param="element" valueishtml="true" /></p>
</dsp:oparam></dsp:droplet></dsp:oparam><dsp:oparam name="false"><dsp:valueof param="overrideText" valueishtml="true" /></dsp:oparam></dsp:droplet></div>
</dsp:oparam></dsp:droplet><div class="clear"></div>
</div>
</div>
<div class="pickup-select">
<h2>Who will pickup these items?</h2>
<input type="radio" name="bopisPerson" id="primarypickup" checked="${bopusShippingGroup.primaryPickup}" onclick="changesecondaypickuprevamped();triggerCoremetricsPageViewTag('Same-Day Pickup', 'JCP|Shipping', 'I will pick up');" value="true">
<span></span>
<label for="pickupperson" class="iwill">I will</label>
<input type="radio" name="bopisPerson" id="secondarypickup" onclick="changeprimarypickuprevamped();clearSomeoneElseTextFieldsRevamped();triggerCoremetricsPageViewTag('Same-Day Pickup', 'JCP|Shipping', 'Someone else will');">
<span></span>
<label for="pickupperson">Someone else</label>
</div>
<div class="form pickupinfo" id="someoneelse">
<h4>Enter their contact information</h4>
<label id="pickupfirstNameLabel" for="pickupfirstName">First Name<span>*</span></label>
<input type="text" autocomplete="off" id="pickupfirstName" name="pickupfirstName" maxlength="17" onfocus="clearFieldStyle(this, 'pickupfirstNameLabel', 'input_txt', '','false')">
<div id="pickupfirstNameError"></div>
<label id="pickuplastNameLabel" for="pickuplastName">Last Name<span>*</span></label>
<input type="text" autocomplete="off"  id="pickuplastName" name="pickuplastName" maxlength="17" onfocus="clearFieldStyle(this, 'pickuplastName', 'input_txt', '','false')">
<div id="pickuplastNameError"></div>
<label id="pickupemailLabel" for="pickupemail">Email<span>*</span></label>
<span class="flt_rgt font10px">Why do we need this?
<a id="a1_up" href="#emailTip" class="tooltipHelpIcon nomargintop"><img class="imgpos" alt="tooltip" src="/dotcom/images/tooltip_quesmark.png"></a>
</span>
<div class="hide_display" id="emailTip">We'll send an email when the order is ready to pickup.</div>
<input type="text" autocomplete="off" placeholder="example@domain.com" id="pickupemail" name="pickupemail"  maxlength="50" onfocus="clearFieldStyle(this, 'pickupemail', 'input_txt', '','false')">
<div id="pickupemailError"></div>
<label id="pickupphoneLabel" for="pickupphone">Phone Number<span>*</span></label>
<span class="flt_rgt font10px">What do we use this for?
<a id="a1_up" href="#phoneTip" placeholder="(555) 555-5555" class="tooltipHelpIcon nomargintop"><img class="imgpos" alt="tooltip" src="/dotcom/images/tooltip_quesmark.png"></a>
</span>
<div class="hide_display" id="phoneTip">We'll contact this phone number when the order is ready to pickup.</div>
<input type="text" type="input_text input_phone" autocomplete="off" id="pickupphone"  name="pickupphone" maxlength="14" placeholder="(555) 555-5555" onfocus="clearFieldStyle(this, 'pickupemail', 'input_txt', '','false')">
<div id="pickupphoneError"></div>
</div>
</form>
<dsp:droplet name="/com/jcpenney/core/order/droplet/CommerceItemsByShippingGroupDroplet"><dsp:param name="order" bean="ShoppingCart.current"/><dsp:oparam name="output"><dsp:getvalueof var="shippingItemMap" param="shippingItemMap"/></dsp:oparam></dsp:droplet><c:forEach items="${shippingItemMap}" var="entry">
<c:if test="${(entry.key eq 'bopusItems')&& (not empty entry.value )}">
<c:set var="bopusCommItems" value="${entry.value}"/>
<dspel:setvalue param="bopusCommItems" value='${bopusCommItems}'/></c:if>
<c:if test="${(entry.key eq 'shipToStoreItems') && (not empty entry.value )}">
<c:set var="shipToStoreCommItems" value="${entry.value}"/>
</c:if>
<c:if test="${(entry.key eq 'shipToHomeItems') && (not empty entry.value )}">
<c:set var="shipToHomeCommItems" value="${entry.value}"/>
<dspel:setvalue param="shipToHomeCommItems" value='${shipToHomeCommItems}'/></c:if>
<c:if test="${(entry.key eq 'shipToItems') && (not empty entry.value )}">
<c:set var="shipToCommItems" value="${entry.value}"/>
<dspel:setvalue param="shipToCommItems" value='${shipToCommItems}'/></c:if>
</c:forEach>
<div class="pickup-items">
<dsp:droplet name="/atg/dynamo/droplet/ForEach"><dsp:param name="array" param="bopusCommItems"/><dsp:setvalue param="commerceItem" paramvalue="element"/><dsp:oparam name="outputStart"><div class="pickup-items-header">
<h3><img src="<c:out value='${contextroot}'/>/images/same-day.svg" alt="same day pickup items" />
Same Day Pickup Items</h3>
</div>
<div class="pickup-items-body">
<ul>
</dsp:oparam><dsp:oparam name="output"><li>
<dsp:droplet name="CommerceItemImageLookupDroplet"><dsp:param name="comItem" param="commerceItem"/><dsp:param name="pageSource" value="shoppingBag"/><dsp:oparam name="output"><dsp:getvalueof var="imageUrl" param="image" /><dsp:getvalueof var="defaultImageAvailableVar" param="defaultImageFlag"/><c:choose>
<c:when test="${(defaultImageAvailableVar != 'true')}">
<img id="imageId" src="<c:out value='${imageUrl}'/>" width="50px" height="50px" alt="title of product" />
</c:when>
<c:otherwise>
<div class="bp-default-image"></div>
</c:otherwise>
</c:choose>
</dsp:oparam></dsp:droplet><div class="pickup-items-body-meta">
<h2>
<dsp:getvalueof var="lotType" param="commerceItem.auxiliaryData.productRef.lotFeed.lotType" /><dsp:getvalueof var="itemType" param="commerceItem.itemType" /><dsp:getvalueof var="syndicateItem" param="commerceItem.itemSource"/><c:if test="${itemType != 'componentCommerceItem'}">
<c:choose>
<c:when test="${lotType != null &&  (lotType == 'Service' || lotType == 'Recycle')}">
<dsp:valueof param="commerceItem.auxiliaryData.productRef.displayName" valueishtml="true"/></c:when>
<c:otherwise>
<dsp:droplet name="IsEmpty"><dsp:param name="value" param="commerceItem.productPresentationId"/><dsp:oparam name="false"><dsp:droplet name='ProductPresentationItemLookupDroplet'><dsp:param name="id" param="commerceItem.productPresentationId"/><dsp:setvalue param="productPresentation" paramvalue="element" /><dsp:oparam name="output"><c:choose>
<c:when test="${syndicateItem ne null && syndicateItem ne ''}">
<dsp:valueof param="commerceItem.auxiliaryData.productRef.displayName" valueishtml="true"/></c:when>
<c:otherwise>
<dsp:droplet name='IsEmpty'><dsp:param name="value" param="element"/><dsp:oparam name="false"><dsp:valueof param="productPresentation.displayName" valueishtml="true"/></dsp:oparam><dsp:oparam name="true"><dsp:valueof param="commerceItem.auxiliaryData.productRef.displayName" valueishtml="true"/></dsp:oparam></dsp:droplet></c:otherwise>
</c:choose>
</dsp:oparam></dsp:droplet></dsp:oparam><dsp:oparam name="true"><c:choose>
<c:when test="${syndicateItem ne null && syndicateItem ne ''}">
<dsp:valueof param="commerceItem.auxiliaryData.productRef.displayName" valueishtml="true"/></c:when>
<c:otherwise>
<dsp:valueof param="commerceItem.auxiliaryData.productRef.displayName" valueishtml="true"/></c:otherwise>
</c:choose>
</dsp:oparam></dsp:droplet></c:otherwise>
</c:choose>
</c:if>
</h2>
</div>
</li>
</dsp:oparam><dsp:oparam name="outputEnd"></ul>
</div>
</dsp:oparam></dsp:droplet></div>
<div class="step-footer ng-scope">
<dsp:getvalueof var="action" param="action" /><h4>
<img src="<c:out value='${contextroot}'/>/images/secure-lock.svg" alt="">
SECURE CHECKOUT
</h4>
<c:choose>
<c:when test="${action eq 'edit'}">
<button class="primary ng-binding small" type="submit" onclick="validateandsubmitBopusValues();">CONTINUE</button>
</c:when>
<c:when test="${panel eq 'both'}">
<button class="primary ng-binding small" type="submit" onclick="validateandsubmitBopusValues();">CONTINUE TO SHIPPING</button>
</c:when>
<c:otherwise>
<button class="primary ng-binding small" type="submit" onclick="validateandsubmitBopusValues();">CONTINUE TO PAYMENT</button>
</c:otherwise>
</c:choose>
<dsp:form id="updateBopusform" method="post" iclass="jcp_form hide_display" action="/dotcom/jsp/checkout/secure/checkoutsimplified/checkout.jsp"><%--  QBNo:964 -start --%>
<input type="hidden" id="hdnShippingMethodName" class="hdnShippingMethodName" name="hdnShippingMethodName" />
<dsp:input type="hidden" id="hdnPrimaryPickup"  name="hdnPrimaryPickup" bean="UpdateBopusShippingGroupFormHandler.bopusShippingGroup.primaryPickup"/><dsp:input type="hidden" id="hdnPickupFirstName" name="hdnPickupFirstName" bean="UpdateBopusShippingGroupFormHandler.bopusShippingGroup.firstName"/><dsp:input type="hidden" id="hdnPickuplastName" name="hdnPickuplastName" bean="UpdateBopusShippingGroupFormHandler.bopusShippingGroup.lastName"/><dsp:input type="hidden" id="hdnPickupPhoneNumber"  name="hdnPickupPhoneNumber" bean="UpdateBopusShippingGroupFormHandler.bopusShippingGroup.contactPhoneNumber"/><dsp:input type="hidden" id="hdnPickupEmailId"  name="hdnPickupEmailId" bean="UpdateBopusShippingGroupFormHandler.bopusShippingGroup.emailId"/><dsp:input type="hidden" id="hdnPickupAlertPhoneNumber"  name="hdnPickupAlertPhoneNumber" bean="UpdateBopusShippingGroupFormHandler.bopusShippingGroup.alertPhoneNumber"/><dsp:input type="hidden" id="hdnPickupTermsAndConditons"  name="hdnUpdateTermsAndConditons" bean="UpdateBopusShippingGroupFormHandler.bopusShippingGroup.termsAndConditions"/><dsp:input type="hidden" value="" bean="UpdateBopusShippingGroupFormHandler.updateBopusShippingGroup" priority="-20"/><dsp:input type="hidden" id="hdnSmsAlertPhoneNumber"  name="hdnSmsAlertPhoneNumber" bean="UpdateBopusShippingGroupFormHandler.transSmsPhnNum"/><dsp:input type="hidden" id="hdnSmsAlertCheckBox"  name="hdnSmsAlertCheckBox" bean="UpdateBopusShippingGroupFormHandler.enabletransactionalsms"/></dsp:form><div class="clear"></div>
</div>
</div>
</dsp:page>
