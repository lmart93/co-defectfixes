<%@ taglib uri="dsp" prefix="dsp" %><%@ taglib uri="c" prefix="c"  %><%@ taglib uri="dspel" prefix="dspel"  %><%@ taglib uri="fmt" prefix="fmt" %><dsp:importbean bean="/atg/commerce/ShoppingCart" scope="session" /><dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/><dsp:importbean bean="atg/dynamo/droplet/ForEach"/><dsp:importbean bean="/com/jcpenney/core/order/droplet/PricingDisplayDroplet"/><dsp:importbean bean="/com/jcpenney/core/order/droplet/RestrictedCouponMessageDroplet"/><dsp:importbean bean="/atg/dynamo/droplet/Switch"/><dsp:importbean bean="/com/jcpenney/core/order/droplet/TruckableItemDroplet"/><dsp:importbean bean="/com/jcpenney/core/order/droplet/ItemSavingDroplet"/><dsp:importbean bean="/atg/userprofiling/Profile" /><dsp:importbean bean="/com/jcpenney/core/order/droplet/RewardsTotalDroplet"/><dsp:importbean bean="/com/jcpenney/core/util/JSPInclude" /><dsp:importbean bean="/com/jcpenney/ocs/OCSApplicationConfiguration"/><dsp:importbean bean="/com/jcpenney/core/ApplicationConfiguration" /><dsp:importbean bean="/com/jcpenney/order/CheckoutApplicationConfiguration"/><dsp:importbean bean="/com/jcpenney/core/order/droplet/DisplaySameDayShippingChargesDroplet"/><dsp:page xml="true"><fmt:setBundle basename="/com/jcpenney/dp/core/order/CheckoutResources" var="checkoutResources" scope="application"/>
<dsp:getvalueof var="domesticCountry" bean="OCSApplicationConfiguration.domesticCountry" /><dsp:getvalueof var="channel" bean="ApplicationConfiguration.channel" /><dsp:getvalueof var="contextroot" vartype="java.lang.String" bean="/OriginatingRequest.contextPath"/><dsp:getvalueof var="dotcomContextPath" bean="JSPInclude.dotcomContextPath" /><dsp:getvalueof bean="JSPInclude.contextPath" var="contextPath"/><dsp:getvalueof var="priceDisplayjspName" bean="JSPInclude.internationalPricingDisplay"/><dsp:getvalueof var="showFindStoreURL" bean="JSPInclude.retailMode" /><dsp:getvalueof var="serverName" vartype="java.lang.String" bean="/OriginatingRequest.serverName"/><dsp:getvalueof var="insecurePort" bean="/com/jcpenney/core/pipeline/ProtocolChangeServlet.InsecurePort"/><dspel:getvalueof var="listReward" param="listReward"/><dsp:getvalueof var="currencyCode" param="order.shipToCurrency" /><dsp:getvalueof var="priceAccurate" param="order.priceAccurate"/><dsp:getvalueof var="fromConfirmationPage" param="fromConfirmationPage"/><dsp:droplet name="ForEach"><dsp:param name="array" param="order.commerceItems"/><dsp:oparam name="output"><dsp:getvalueof var="lotType" param="element.auxiliaryData.productRef.lotFeed.lotType" /><c:if test="${lotType != null && lotType == 'largeAppliance'}">
<c:set var="orderHasLargeAppliance" value="true" scope="request"/>
</c:if>
<dsp:droplet name="TruckableItemDroplet"><dsp:param name="commerceItemId" param="element.id"/><dsp:param name="order" param="order"/><dsp:oparam name="output"><dsp:getvalueof var="isExtendedChargesApply" param="isExtendedChargeApplicable"/><c:if test="${isExtendedChargesApply != null}">
<c:set var="extendedShippingFlag" value="true" scope="page"/>
</c:if>
<dsp:getvalueof var="isTruckable" param="truckable"/><c:if test="${isTruckable != null}">
<c:set var="truckableFlag" value="true" scope="page"/>
</c:if>
</dsp:oparam></dsp:droplet></dsp:oparam></dsp:droplet><c:if test="${orderHasLargeAppliance == null || orderHasLargeAppliance == '' }">
<c:set var="orderHasLargeAppliance" value="false" scope="request"/>
</c:if>
<c:choose>
<c:when test="${showFindStoreURL eq true}">
<dsp:setvalue bean="Profile.shipToCountry" value="US"/><dsp:getvalueof bean="Profile.shipToCountry" var="shipToCountry"/></c:when>
<c:otherwise>
<dsp:getvalueof param="order.shipToCountry" var="shipToCountry"/></c:otherwise>
</c:choose>
<dsp:droplet name="ForEach"><dsp:param name="array"  param="order.coupons"/><dsp:setvalue param="Coupon" paramvalue="element"/><dsp:oparam name="output"><dsp:getvalueof var="size" param="size"/><dsp:getvalueof var="isCouponApplied" param="Coupon.couponApplied"/><dsp:getvalueof var="couponErrorCode" param="Coupon.errorCode"/><c:if test="${isCouponApplied eq 'FALSE' and (couponErrorCode eq 'keyOCSCouponInvalid' or couponErrorCode eq 'keyOCSCouponMobileBarcodeInvalid')}">
<img alt="attention" src="<c:out value="${contextroot}"/>/images/info_icn.gif" class="flt_lft mrgt5 mrgr10" />
<dsp:droplet name="RestrictedCouponMessageDroplet"><dsp:param name="coupon" param="element"/><dsp:oparam name="true">The code <dsp:valueof param="element.couponPromoCode"></dsp:valueof> was not applied. Please refer to the coupon details and enter a qualifying method of payment.
</dsp:oparam></dsp:droplet></p>
</c:if>
</dsp:oparam></dsp:droplet><div id="pricing-summary" class="pricing-summary flt_lft">
<h3>PRICING SUMMARY</h3>
<dsp:droplet name="PricingDisplayDroplet"><dsp:param name="order" param="order"/><dsp:oparam name="result"><dsp:getvalueof var="orderSubTotal" param="rawSubTotal"/><dsp:getvalueof var="couponType" param="element.couponType"/><dsp:getvalueof var="parantheticalSurcharge" param="ParentheticalCharge"/><dsp:getvalueof var="bopusParentheticalCharge" param="bopusParentheticalCharge"/><dsp:getvalueof var="bopusMinusSurcharge" param="bopusMinusSurcharge" vartype="java.lang.Double"/><dsp:getvalueof var="shipParentheticalCharge" param="shipParentheticalCharge"/><dsp:getvalueof var="shippingHandlingFeeMinusSurcharge" param="shippingHandlingFeeMinusSurcharge" vartype="java.lang.Double"/><dsp:getvalueof var="shipToHomeMinusSurcharge" param="shipToHomeMinusSurcharge" vartype="java.lang.Double"/><dsp:getvalueof var="hasItemCoupon"  param="hasItemCoupon"/><dsp:getvalueof var="hasShippingCoupon"  param="hasShippingCoupon"/><dsp:getvalueof var="shippingCouponCode"  param="shippingCouponCode"/><dsp:getvalueof var="itemCouponCode"  param="itemCouponCode"/><dsp:getvalueof var="shippingCouponSavings"  param="shippingCouponSavings" vartype="java.lang.Double"/><dsp:getvalueof var="itemCouponSavings"  param="itemCouponSavings" vartype="java.lang.Double"/><dsp:getvalueof var="PreferredRewardDiscount" param="PreferredReward"/><dsp:getvalueof var="total1" param="order.priceInfo.total"/><dsp:getvalueof var="subtotal1" param="order.priceInfo.rawSubtotal"/><dsp:getvalueof var="giftWrap" vartype="java.lang.Double" param="giftWrapTotal"/><dsp:getvalueof var="associateDiscount" param="order.priceInfo.associateDiscount"/><dsp:getvalueof var="programType" param="order.commerceItems[0].auxiliaryData.productRef.lotFeed.programType"/><dspel:getvalueof var="orderSavingsVar" param="orderSavings"/><div class="flt_wdt">
<span class="merch-text flt_lft maxwdt170">merchandise subtotal</span>
<dsp:droplet name="Switch"><dsp:param name="value" bean="ApplicationConfiguration.enableDP2016S2SPROJSP276ShoppingBagRevisedMdseFlag"/><dsp:oparam name="true"><div class="merch-tooltip flt_lft">
<img alt="tooltip" src="<c:out value="${contextroot}"/>/images/tooltip.svg">
<span class="merch-tooltiptext"><dsp:valueof bean="CheckoutApplicationConfiguration.largeAppToolTipMerchSubTotal" /></span>
</div>
</dsp:oparam><dsp:oparam name="false"><c:if test="${orderHasLargeAppliance != null && orderHasLargeAppliance == 'true' }">
<div class="merch-tooltip flt_lft">
<img src="ques.png">
<span class="merch-tooltiptext"><dsp:valueof bean="CheckoutApplicationConfiguration.largeAppToolTip" /></span>
</div>
</c:if>
</dsp:oparam></dsp:droplet><span class="merch_amt flt_lft">
<dsp:getvalueof var="targetRawSubtotal" param="order.priceInfo.targetCurrencyRawSubTotal" /><c:choose>
<c:when test="${targetRawSubtotal eq 0.0}">
<c:choose>
<c:when test="${shipToCountry ne 'US'}">
<dsp:valueof param="order.shipToCurrency" />&nbsp;<dsp:valueof param="merchandiseSubTotal" number="0.00" />
</c:when>
<c:otherwise>
<dspel:include page="${priceDisplayjspName}" otherContext="${contextPath}" ><dsp:param name="moneyAsDouble" param="merchandiseSubTotal"/><dsp:param name="nonBrowsePage" value="true" /></dspel:include></c:otherwise>
</c:choose>
</c:when>
<c:otherwise>
<c:out value="${currencyCode}"/>&nbsp;<dsp:valueof param="order.priceInfo.targetCurrencyRawSubTotal"  number="0.00" />
</c:otherwise>
</c:choose>
</span>
</div>
<c:if test="${hasItemCoupon == true || orderSavingsVar > 0 || associateDiscount > 0 || hasRewardCertificates eq true  || PreferredRewardDiscount > 0}">
<dsp:droplet name="IsEmpty"><dsp:param name="value" param="order.coupons"/><dsp:oparam name="false"><c:if test="${hasItemCoupon == true}">
<c:if test="${couponType ne 'CouponSHFree'}">
<div class="row padTopBtm couponTotal">
<span class="flt_lft">
<fmt:message key="keyCouponCode" bundle="${checkoutResources}"/>
</span>
<span class="flt_rgt">
<c:if test="${priceAccurate}">
-<dsp:valueof param="itemCouponSavings" number="0.00" converter="currency"/>
</c:if>
<c:if test="${!priceAccurate}">
TBD
</c:if>
</span>
</div>
</c:if>
</c:if>
</dsp:oparam></dsp:droplet><dsp:droplet name="IsEmpty"><dsp:param name="value" bean="ShoppingCart.current.rewardCertificates"/><dsp:oparam name="true"><dspel:getvalueof var="hasRewardCertificates" value="false" /></dsp:oparam><dsp:oparam name="false"><dspel:getvalueof var="hasRewardCertificates" value="true" /></dsp:oparam></dsp:droplet><dsp:droplet name="Switch"><dsp:param name="value" bean="ApplicationConfiguration.enableCouponRewardRedemptionInCheckout"/><dsp:oparam name="true"><c:if test="${hasRewardCertificates eq true}">
<div class="flt_wdt row rewardsSB">
<dsp:include page="/jsp/cart/revamp/global/displayRewardCode.jsp"></dsp:include></div>
</c:if>
</dsp:oparam><dsp:oparam name="false"><div id="rewardsSB" class="rewardsSB">
<dsp:include page="/jsp/cart/revamp/global/displayRewardCode.jsp" /><div class="row addEditCodeLink">
<dsp:getvalueof param="fromPOSF" var="fromPOSF"></dsp:getvalueof>
<a href="<dspel:valueof value='${contextPath}' />/jsp/cart/viewShoppingBag.jsp">add or edit coupons and rewards</a>
<c:if test="${channel eq 'POSF' or channel eq 'posf'}">
<a id="anReCetposfTool" href="#certificatePOSFCodeTip" class="helpIcon"><img src="<c:out value="${contextroot}"/>/images/tooltip.svg" alt="?"/></a>
<div id="certificatePOSFCodeTip" class="hidden">Certificates will display on the Shopping Bag page for customers with linked Rewards accounts.</div>
</c:if>
</div>
</div>
</dsp:oparam></dsp:droplet><c:if test="${associateDiscount > 0}">
<p>
<span class="flt_lft">
<fmt:message key="VIEW_SHOPPING_BAG.ASSOCIATE_DISCOUNTS" bundle="${checkoutResources}"/>
</span>
<span class="flt_rgt">-
<dsp:valueof converter="currency" bean="ShoppingCart.current.priceInfo.associateDiscount"/></span>
</p>
</c:if>
<c:if test="${PreferredRewardDiscount > 0}">
<c:if test="${priceAccurate}">
<div class="clear preferred-rewards-details-container">
<span class="flt_lft">
<fmt:message key="VIEW_SHOPPING_BAG.PREFERRED_REWARDS" bundle="${checkoutResources}"/>
</span>
<span class="flt_rgt">-
<dspel:valueof converter="currency" value="${PreferredRewardDiscount}"/></span>
</div>
</c:if>
<c:if test="${!priceAccurate}">
<div class="clear preferred-rewards-details-container">
<span class="flt_lft">
<fmt:message key="VIEW_SHOPPING_BAG.PREFERRED_REWARDS" bundle="${checkoutResources}"/>
</span>
<span class="flt_rgt">
TBD
</span>
</div>
</c:if>
</c:if>
<div class="clear"></div>
<div class="total-after-discounts-container">
<div class="flt_lft">total after discounts</div>
<div class="flt_rgt"><dspel:valueof converter="currency" value="${orderSubTotal}"/></div>
</div>
<div class="total-after-discount-message"></div>
</c:if>
<div class="flt_wdt">
<div class="ship_detail flt_wdt">
<dsp:droplet name="DisplaySameDayShippingChargesDroplet"><dsp:param name="order" param="order"/><dsp:oparam name="output"><dspel:getvalueof var="displayBopusShipping" param="displayBopusShipping"/><dspel:getvalueof  var="bopusShipping" param="bopusShipping" /></dsp:oparam></dsp:droplet><c:if test="${displayBopusShipping eq 'false' }">
<dsp:droplet name="/atg/dynamo/droplet/ForEach"><dsp:param name="array" param="order.shippingGroups" /><dsp:oparam name="output"><dsp:droplet name="Switch"><dsp:param name="value" param="element.shippingGroupClassType"/><dsp:oparam name="storeShippingGroup"><div class="ship_store ship_store_lft_margin rewardsShipToStore">
<span class="flt_lft maxwdt170">shipping &amp; handling
<a href="#estimatedShippingTip" class="helpIcon"><img src="<c:out value="${contextroot}"/>/images/tooltip.svg" alt="?"/></a>
</span>
<div class="hide" id="estimatedShippingTip"><fmt:message key="shippingAndHandlingToolTipMessage" bundle="${checkoutResources}"/></div>
<div id="storePrice">
<span class="flt_rgt">
<dsp:getvalueof var="shipToStore" vartype="java.lang.Double" param="shippingHandlingWillCall"/><dsp:getvalueof var="shippingWillCallMinusSurcharge" vartype="java.lang.Double" param="shippingWillCallMinusSurcharge"/><c:choose>
<c:when test="${!priceAccurate}">
TBD
</c:when>
<c:otherwise>
<c:choose>
<c:when test="${shipToStore > 0}">
<c:choose>
<c:when test="${(not empty parantheticalSurcharge)}">
<c:choose>
<c:when test="${(not empty shippingWillCallMinusSurcharge) and shippingWillCallMinusSurcharge > 0}">
<dspel:valueof converter="currency" value="${shippingWillCallMinusSurcharge}"/></c:when>
<c:otherwise>
FREE
</c:otherwise>
</c:choose>
</c:when>
<c:otherwise>
<dsp:valueof converter="currency" param="shippingHandlingWillCall"/></c:otherwise>
</c:choose>
</c:when>
<c:otherwise>
<c:choose>
<c:when test="${truckableFlag == 'true'}">
shipping included
</c:when>
<c:otherwise>
FREE
</c:otherwise>
</c:choose>
</c:otherwise>
</c:choose>
</c:otherwise>
</c:choose>
</span>
</div>
</div>
</dsp:oparam><dsp:oparam name="hardgoodShippingGroup"><dsp:getvalueof var="shippingAddress1" vartype="java.lang.String" param="element.shippingAddress.address1"/><dsp:getvalueof var="countryCode" vartype="java.lang.String" param="element.shippingAddress.country"/><div class="flt_wdt ship_home">
<c:choose>
<c:when test="${!empty shippingAddress1}">
<c:choose>
<c:when test="${shipToCountry ne domesticCountry }">
<span class="flt_lft maxwdt170">
shipping &amp; handling
</span>
<div class="estimatedShiptohomeTip flt_lft">
<img alt="tooltip" src="<c:out value="${contextroot}"/>/images/tooltip.svg">
<span class="estimatedShiptohomeTip-tooltiptext"><fmt:message key="shippingAndHandlingToolTipMessage" bundle="${checkoutResources}"/></span>
</div>
<span class="flt_rgt">
<dsp:getvalueof  var="targetShippingPrice" param="order.priceInfo.targetCurrencyShippingPrice" /><c:choose>
<c:when test="${targetShippingPrice eq 0.0}">
<dsp:valueof param="order.shipToCurrency" />&nbsp;0.00
</c:when>
<c:otherwise>
<c:out value="${currencyCode}"/>&nbsp;<dsp:valueof  param="order.priceInfo.targetCurrencyShippingPrice" number="0.00" />
</c:otherwise>
</c:choose>
</span>
</c:when>
<c:otherwise>
<span class="flt_lft maxwdt170">
shipping &amp; handling
</span>
<div class="estimatedShiptohomeTip flt_lft">
<img alt="tooltip" src="<c:out value="${contextroot}"/>/images/tooltip.svg">
<span class="estimatedShiptohomeTip-tooltiptext"><fmt:message key="shippingAndHandlingToolTipMessage" bundle="${checkoutResources}"/></span>
</div>
<span class="flt_rgt">
<dsp:getvalueof var="shipToHome" vartype="java.lang.Double" param="element.priceInfo.amount"/><c:choose>
<c:when test="${!priceAccurate}">
TBD
</c:when>
<c:otherwise>
<c:choose>
<c:when test="${shipToHome > 0}">
<c:choose>
<c:when test="${(not empty parantheticalSurcharge)}">
<c:choose>
<c:when test="${(not empty shippingHandlingFeeMinusSurcharge) and shippingHandlingFeeMinusSurcharge > 0}">
<dspel:valueof converter="currency" value="${shippingHandlingFeeMinusSurcharge}"/></c:when>
<c:otherwise>
FREE
</c:otherwise>
</c:choose>
</c:when>
<c:otherwise>
<dsp:valueof converter="currency" param="element.priceInfo.amount"/></c:otherwise>
</c:choose>
</c:when>
<c:otherwise>
<c:choose>
<c:when test="${truckableFlag == 'true'}">
shipping included
</c:when>
<c:otherwise>
FREE
</c:otherwise>
</c:choose>
</c:otherwise>
</c:choose>
</c:otherwise>
</c:choose>
</span>
<dsp:droplet name="IsEmpty"><dsp:param name="value"  param="order.coupons"/><dsp:oparam name="false"><c:if test="${hasShippingCoupon == true and priceAccurate}">
<span class="shipping-coupon-savings">
<strong><dspel:valueof converter="currency" value="${shippingCouponCode}" /></strong>
</span>
</c:if>
</dsp:oparam></dsp:droplet></c:otherwise>
</c:choose>
</c:when>
<c:otherwise>
<dsp:droplet name="IsEmpty"><dsp:param name="value" param="order.estimatedShipToHome"/><dsp:oparam name="false"><dsp:getvalueof var="shipToHome" vartype="java.lang.Double" param="order.estimatedShipToHome"/><c:if test="${programType != '41' and extendedShippingFlag != 'true'}">
<c:if test="${shipToCountry eq domesticCountry}">
<span class="flt_lft maxwdt170">
shipping &amp; handling
</span>
<div class="estimatedShiptohomeTip flt_lft">
<img alt="tooltip" src="<c:out value="${contextroot}"/>/images/tooltip.svg">
<span class="estimatedShiptohomeTip-tooltiptext"><fmt:message key="shippingAndHandlingToolTipMessage" bundle="${checkoutResources}"/></span>
</div>
<span class="flt_rgt">
<c:choose>
<c:when test="${!priceAccurate}">
TBD
</c:when>
<c:otherwise>
<c:choose>
<c:when test="${shipToHome > 0}">
<c:choose>
<c:when test="${not empty shipParentheticalCharge}">
<c:choose>
<c:when test ="${not empty shipToHomeMinusSurcharge and shipToHomeMinusSurcharge > 0}">
<dspel:valueof converter="currency" value="${shipToHomeMinusSurcharge}" /></c:when>
<c:otherwise>
FREE
</c:otherwise>
</c:choose>
</c:when>
<c:otherwise>
<dsp:valueof converter="currency" param="order.estimatedShipToHome" /></c:otherwise>
</c:choose>
</c:when>
<c:otherwise>
<c:choose>
<c:when test="${truckableFlag == 'true'}">
shipping included
</c:when>
<c:otherwise>
FREE
</c:otherwise>
</c:choose>
</c:otherwise>
</c:choose>
</c:otherwise>
</c:choose>
</span>
<dsp:droplet name="IsEmpty"><dsp:param name="value" param="order.coupons"/><dsp:oparam name="false"><c:if test="${hasShippingCoupon == true and priceAccurate}">
<span class="shipping-coupon-savings">
<strong><dspel:valueof converter="currency" value="${shippingCouponCode}" /></strong>
</span>
</c:if>
</dsp:oparam></dsp:droplet></c:if>
</c:if>
</dsp:oparam></dsp:droplet><c:if test="${shipToCountry ne domesticCountry}">
<span class="flt_lft maxwdt170">
shipping &amp; handling
</span>
<div class="estimatedShiptohomeTip flt_lft">
<img alt="tooltip" src="<c:out value="${contextroot}"/>/images/tooltip.svg">
<span class="estimatedShiptohomeTip-tooltiptext"><fmt:message key="shippingAndHandlingToolTipMessage" bundle="${checkoutResources}"/></span>
</div>
<span class="flt_rgt">
<dsp:valueof param="order.shipToCurrency" />&nbsp;<dsp:valueof  param="order.priceInfo.targetCurrencyShippingPrice" number="0.00" />
</span>
</c:if>
</c:otherwise>
</c:choose>
</div>
</dsp:oparam></dsp:droplet></dsp:oparam></dsp:droplet></c:if>
<c:if test="${displayBopusShipping ne 'false' }">
<div class="flt_wdt ship_home"">
<span class="flt_lft maxwdt170">shipping &amp; handling
<a href="#estimatedShippingTip" class="helpIcon"><img src="<c:out value="${contextroot}"/>/images/tooltip.svg" alt="?"/></a>
</span>
<div class="hidden" id="estimatedShippingTip"><fmt:message key="shippingAndHandlingToolTipMessage" bundle="${checkoutResources}"/></div>
<div id="BOPUSPrice">
<span class="flt_rgt">
<c:choose>
<c:when test="${!priceAccurate}">
TBD
</c:when>
<c:otherwise>
<c:choose>
<c:when test="${bopusShipping > 0}">
<c:choose>
<c:when test="${not empty bopusParentheticalCharge}">
<c:choose>
<c:when test="${not empty bopusMinusSurcharge and bopusMinusSurcharge > 0}">
<dspel:valueof converter="currency" value="${bopusMinusSurcharge}" /></c:when>
<c:otherwise>
FREE
</c:otherwise>
</c:choose>
</c:when>
<c:otherwise>
<dsp:valueof converter="currency" value="${bopusShipping}"/></c:otherwise>
</c:choose>
</c:when>
<c:otherwise>
FREE
</c:otherwise>
</c:choose>
</c:otherwise>
</c:choose>
</span>
</div>
</div>
</c:if>
</div>
<c:if test="${not empty parantheticalSurcharge}">
<div class="row ship_store marginAllign bag_surcharge_padding">
<span class="flt_lft">oversized item surcharge</span>
<%-- <div class="hidden" id="surchargeForOversizedItemsTip">
<fmt:message key="surchargeForOversizedItemsToolTipMessage" bundle="${checkoutResources}" />
</div>
<a id="a1_up" href="#surchargeForOversizedItemsTip" class="helpIcon">
<img src="<c:out value="${contextroot}"/>/images/ques_icn.png" alt="?" />
</a> --%>
<span class="flt_rgt">
<c:choose>
<c:when test="${shipToCountry != null && shipToCountry !='US'}">
<dsp:valueof bean="Profile.shipToCurrencyCode" />&nbsp;<span data-anid="productSurchargePrice" style="display:none"><dspel:valueof value="${parantheticalSurcharge}" number="0.00" /></span>
</c:when>
<c:otherwise>
<dspel:valueof value="${parantheticalSurcharge}" number="0.00" converter="currency"  /><span data-anid="productSurchargePrice" style="display:none"><dspel:valueof value="${parantheticalSurcharge}"/></span>
</c:otherwise>
</c:choose>
</span>
</div>
</c:if>
<c:if test="${giftWrap > 0}">
<div class="row ship_home">
<span class="flt_lft">gift wrap:</span>
<span class="flt_rgt">
<dsp:valueof param="giftWrapTotal" number="0.00" converter="currency"/></span>
</div>
</c:if>
<dsp:droplet name="IsEmpty"><dsp:param name="value" param="order.priceInfo.tax"/><dsp:oparam name="false"><dsp:getvalueof var="orderTax" vartype="java.lang.Double" param="order.priceInfo.tax"/></dsp:oparam></dsp:droplet><c:if test="${orderTax > 0}">
<div class="row flt_wdt taxcont">
<span class="flt_lft">
<span  class="flt_lft">
<c:choose>
<c:when test="${shipToCountry ne  domesticCountry}">
duty and VAT
</c:when>
<c:otherwise>
tax
</c:otherwise>
</c:choose>
</span>
<a href="#taxTip" class="helpIcon flt_lft"><img src="<c:out value="${contextroot}"/>/images/tooltip.svg" alt="?"/></a>
</span>
<c:choose>
<c:when test="${shipToCountry ne  domesticCountry}">
<div class="hidden" id="taxTip">Your total tax is based on the destination of your order.  jcpenney is required by law to collect all applicable taxes, whether the sale was made in on jcp.com, in-store or by phone.</div>
</c:when>
<c:otherwise>
<div class="hidden" id="taxTip">Your total tax is based on the destination of your order.  Because jcpenney does business in all 50 states, we are required by law to collect all state and local sales taxes, whether the sale was made in on jcp.com, in-store or by phone.</div>
</c:otherwise>
</c:choose>
<span class="flt_rgt">
<c:choose>
<c:when test="${shipToCountry ne  domesticCountry}">
<dsp:getvalueof var="targetCurrenctyTaxPrice" param="order.priceInfo.targetCurrencyTaxDutyPrice"  /><c:if test="${targetCurrenctyTaxPrice eq '0.0'}">
<dsp:valueof param="order.shipToCurrency" />&nbsp;0.00
</c:if>
<c:if test="${targetCurrenctyTaxPrice ne '0.0'}">
<dsp:valueof param="order.shipToCurrency" />&nbsp;<dsp:valueof  param="order.priceInfo.targetCurrencyTaxDutyPrice" number="0.00" />
</c:if>
</c:when>
<c:otherwise>
<dsp:valueof converter="currency" param="order.priceInfo.tax" /></c:otherwise>
</c:choose>
</span>
</div>
</c:if>
<c:if test="${orderTax == '0.0'}">
<div class="row flt_wdt taxcont">
<span class="flt_lft">
<span  class="flt_lft">
<c:choose>
<c:when test="${shipToCountry ne  domesticCountry}">
duty and VAT
</c:when>
<c:otherwise>
tax
</c:otherwise>
</c:choose>
</span>
<a href="#taxTip" class="helpIcon flt_lft"><img src="<c:out value="${contextroot}"/>/images/tooltip.svg" alt="?"/></a>
</span>
<c:choose>
<c:when test="${shipToCountry ne  domesticCountry}">
<div class="hide_display" id="taxTip">Your total tax is based on the destination of your order.  jcpenney is required by law to collect all applicable taxes, whether the sale was made in on jcp.com, in-store or by phone.</div>
</c:when>
<c:otherwise>
<div class="hide_display" id="taxTip">Your total tax is based on the destination of your order.  Because jcpenney does business in all 50 states, we are required by law to collect all state and local sales taxes, whether the sale was made in on jcp.com, in-store or by phone.</div>
</c:otherwise>
</c:choose>
<span class="flt_rgt">
<c:choose>
<c:when test="${shipToCountry ne  domesticCountry}">
<dsp:getvalueof var="targetCurrenctyTaxPrice" param="order.priceInfo.targetCurrencyTaxDutyPrice" /><c:if test="${targetCurrenctyTaxPrice eq '0.0'}">
<dsp:valueof param="order.shipToCurrency" />&nbsp;0.00
</c:if>
<c:if test="${targetCurrenctyTaxPrice ne '0.0'}">
<dsp:valueof param="order.shipToCurrency" />&nbsp;<dsp:valueof  param="order.priceInfo.targetCurrencyTaxDutyPrice" number="0.00" />
</c:if>
</c:when>
<c:otherwise>
<dsp:valueof converter="currency" param="order.priceInfo.tax" /></c:otherwise>
</c:choose>
</span>
</div>
</c:if>
<c:choose>
<c:when test="${fromConfirmationPage ==  true}">
<%@ include file="/jsp/checkout/secure/roundup/roundUpConfirmation.jspf"%></c:when>
<c:otherwise>
<%@ include file="/jsp/checkout/secure/roundup/roundUpCheckoutLineItem.jspf"%></c:otherwise>
</c:choose>
<div class="row order_total"><span class="flt_lft">ORDER TOTAL </span>
<span class="flt_rgt">
<span data-anid="checkOut_cartTot" style="display:none"><dsp:valueof param="orderTotal"></dsp:valueof></span>
<c:if test="${!priceAccurate}">
TBD
</c:if>
<c:if test="${priceAccurate}">
<dsp:getvalueof var="targetTotalPrice" param="order.priceInfo.targetCurrencyTotalPrice" /><c:choose>
<c:when test="${targetTotalPrice eq 0.0}">
<c:choose>
<c:when test="${shipToCountry eq domesticCountry}">
<dsp:getvalueof var="total1" param="orderTotal"/><dspel:include page="${priceDisplayjspName}" otherContext="${contextPath}" ><dspel:param name="moneyAsDouble" value="${total1}" /><dsp:param name="nonBrowsePage" value="true" /></dspel:include></c:when>
<c:otherwise>
<dsp:valueof param="order.shipToCurrency" />&nbsp;<dsp:valueof param="internationalOrderSubTotal" number="0.00" />
</c:otherwise>
</c:choose>
</c:when>
<c:otherwise>
<dsp:valueof param="order.shipToCurrency" />&nbsp;<dsp:valueof  param="order.priceInfo.targetCurrencyTotalPrice" number="0.00" />
</c:otherwise>
</c:choose>
</c:if>
</span>
</div>
<c:choose>
<c:when test="${shipToCountry eq domesticCountry}">
<div class="totalOrderDiscountsSummary">
</c:when>
<c:otherwise>
<div class="totalIntlOrderDiscountsSummary">
</c:otherwise>
</c:choose>
<dsp:getvalueof var="totalOrderDiscount" param="totalOrderDiscount" /><dsp:include page="/jsp/global/totalDiscountsSummary.jsp"><dspel:param name="displayTotalOrderDisount" value="true" /><dspel:param name="totalOrderDiscount" param="totalOrderDiscount" /><dspel:param name="fromSimplifiedCheckout" value="true" /><dsp:param name="fromConfirmationPage" param="fromConfirmationPage" /></dsp:include></div>
</dsp:oparam></dsp:droplet></div>
</dsp:page>
