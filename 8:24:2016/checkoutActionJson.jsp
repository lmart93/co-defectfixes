<%@ taglib uri="dsp" prefix="dsp"%>
<%@ taglib prefix="dspel" uri="dspel" %> 
<%@ taglib uri="json" prefix="json" %>
<%@ taglib uri="c" prefix="c"  %>
<%@ page contentType="text/json;charset=UTF-8" %>
<dsp:page xml="true">  
	<dsp:importbean bean="/com/jcpenney/core/order/formhandler/UpdateBopusShippingGroupFormHandler"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CreateHardgoodShippingGroupFormHandler"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/PaymentGroupFormHandler"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CommitOrderFormHandler"/>
	<dsp:importbean bean="/com/jcpenney/dp/core/droplet/CheckoutPageRendererDroplet"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/com/jcpenney/profile/bean/UserSessionBean"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	
	<dsp:getvalueof param="formHandler" var="formHandler" />
	
	<json:object>
	<dsp:droplet name="CheckoutPageRendererDroplet">
		<dsp:param name="order" bean="ShoppingCart.current"/>
		<dsp:param name="nextAction" param="nextAction"/>
		<dsp:param name="currentPanel" param="currentPanel"/>
		<dsp:param name="pageName" value="checkoutActionJSON" />
		<dsp:oparam name="output">	
			<dsp:getvalueof var="currentPanel" param="panel"/>
			<dsp:getvalueof var="pageListToRender" param="pageRenderInfoBean.pageListToBeRender" />
		</dsp:oparam>
	</dsp:droplet>
	<json:property name="currentPanel" value="${currentPanel}"/>
	<c:forEach var="pageToRender" items="${pageListToRender}">
		<json:property name="${pageToRender.key}">
			<dspel:include page="${pageToRender.value}" />		
		</json:property>
	</c:forEach>
	
	<dsp:getvalueof var="completedPanels"  bean="UserSessionBean.completedPanels"/>
	<json:array name="completedPanels" var="completedPanel" items="${completedPanels}">
		 <json:object>
		    <json:property name="name" value="${completedPanel}"/>
		  </json:object>
	</json:array>
	<dsp:droplet name="Switch">
	<dsp:param bean="UpdateBopusShippingGroupFormHandler.formError" name="value"/>
	<dsp:oparam name="false">
	<json:property name="IsErrorExist">
		No
	</json:property>
	</dsp:oparam>
	<dsp:oparam name="true">
	<json:property name="IsErrorExist">
		Yes
	</json:property>
	<json:object name="errorArray">
		<json:array name="addShippingErrors">	
			<dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
				<dsp:param bean="UpdateBopusShippingGroupFormHandler.formExceptions" name="exceptions"/>
				<dsp:oparam name="output">
					<json:object>
						<json:property name="propertyName">
							<dsp:valueof param="propertyName"/>
						</json:property>
						<json:property name="errorMessage">
							<dsp:valueof param="message"/>
						</json:property>						
					</json:object>
				</dsp:oparam>
			</dsp:droplet>
		</json:array>
	</json:object>
	</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="Switch">
	<dsp:param bean="CreateHardgoodShippingGroupFormHandler.formError" name="value"/>
		<dsp:oparam name="true">			
			 <json:property name="IsErrorExist" value="Yes"/>
			 	<json:object name="errorArray">
				<json:array name="addShippingErrors">	
					<dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
						<dsp:param bean="CreateHardgoodShippingGroupFormHandler.formExceptions" name="exceptions"/>
						<dsp:oparam name="output">
							<json:object>
								<json:property name="propertyName">
									<dsp:valueof param="propertyName"/>
								</json:property>
								<json:property name="errorMessage">
									<dsp:valueof param="message"/>
								</json:property>						
							</json:object>
						</dsp:oparam>
					</dsp:droplet>
				</json:array>
			</json:object>			
		</dsp:oparam>
		<dsp:oparam name="false">
		<json:property name="IsErrorExist" value="No"/>				
     </dsp:oparam>
	</dsp:droplet>
	<c:if test="${formHandler == 'PaymentGroupFormHandler'}">
		<json:object name="errorArray">
			<json:array name="paymentErrors">	
				<dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
					<dsp:param bean="PaymentGroupFormHandler.formExceptions" name="exceptions"/>
					<dsp:oparam name="output">
						<json:object>
							<json:property name="propertyName">
								<dsp:valueof param="propertyName"/>
							</json:property>
							<json:property name="errorMessage">
								<dsp:valueof param="message"/>
							</json:property>						
						</json:object>
					</dsp:oparam>
				</dsp:droplet>
			</json:array>
		</json:object>
	</c:if>
	<dsp:droplet name="Switch">
		<dsp:param bean="CommitOrderFormHandler.formError" name="value"/>
		<dsp:oparam name="false">
			<json:property name="IsErrorExist">
				No
			</json:property>
		</dsp:oparam>
		<dsp:oparam name="true">
			<json:property name="IsErrorExist">
				Yes
			</json:property>
			<json:array name="paymentErrors">	
				<dsp:droplet name="/atg/dynamo/droplet/ForEach">
					<dsp:param bean="CommitOrderFormHandler.formExceptions" name="array"/>
					<dsp:oparam name="output">
						<json:object>
							<dsp:getvalueof var="key" param="element.key" />
							<json:property name="${key}">
								<dsp:valueof param="element.message"/>
							</json:property>
						</json:object>
					</dsp:oparam>
				</dsp:droplet>
			</json:array>
		</dsp:oparam>
	</dsp:droplet>
	</json:object>
</dsp:page>